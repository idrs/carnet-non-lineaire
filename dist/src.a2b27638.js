// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"node_modules/mithril/render/vnode.js":[function(require,module,exports) {
"use strict"

function Vnode(tag, key, attrs, children, text, dom) {
	return {tag: tag, key: key, attrs: attrs, children: children, text: text, dom: dom, domSize: undefined, state: undefined, events: undefined, instance: undefined}
}
Vnode.normalize = function(node) {
	if (Array.isArray(node)) return Vnode("[", undefined, undefined, Vnode.normalizeChildren(node), undefined, undefined)
	if (node == null || typeof node === "boolean") return null
	if (typeof node === "object") return node
	return Vnode("#", undefined, undefined, String(node), undefined, undefined)
}
Vnode.normalizeChildren = function(input) {
	var children = []
	if (input.length) {
		var isKeyed = input[0] != null && input[0].key != null
		// Note: this is a *very* perf-sensitive check.
		// Fun fact: merging the loop like this is somehow faster than splitting
		// it, noticeably so.
		for (var i = 1; i < input.length; i++) {
			if ((input[i] != null && input[i].key != null) !== isKeyed) {
				throw new TypeError("Vnodes must either always have keys or never have keys!")
			}
		}
		for (var i = 0; i < input.length; i++) {
			children[i] = Vnode.normalize(input[i])
		}
	}
	return children
}

module.exports = Vnode

},{}],"node_modules/mithril/render/hyperscriptVnode.js":[function(require,module,exports) {
"use strict"

var Vnode = require("../render/vnode")

// Call via `hyperscriptVnode.apply(startOffset, arguments)`
//
// The reason I do it this way, forwarding the arguments and passing the start
// offset in `this`, is so I don't have to create a temporary array in a
// performance-critical path.
//
// In native ES6, I'd instead add a final `...args` parameter to the
// `hyperscript` and `fragment` factories and define this as
// `hyperscriptVnode(...args)`, since modern engines do optimize that away. But
// ES5 (what Mithril requires thanks to IE support) doesn't give me that luxury,
// and engines aren't nearly intelligent enough to do either of these:
//
// 1. Elide the allocation for `[].slice.call(arguments, 1)` when it's passed to
//    another function only to be indexed.
// 2. Elide an `arguments` allocation when it's passed to any function other
//    than `Function.prototype.apply` or `Reflect.apply`.
//
// In ES6, it'd probably look closer to this (I'd need to profile it, though):
// module.exports = function(attrs, ...children) {
//     if (attrs == null || typeof attrs === "object" && attrs.tag == null && !Array.isArray(attrs)) {
//         if (children.length === 1 && Array.isArray(children[0])) children = children[0]
//     } else {
//         children = children.length === 0 && Array.isArray(attrs) ? attrs : [attrs, ...children]
//         attrs = undefined
//     }
//
//     if (attrs == null) attrs = {}
//     return Vnode("", attrs.key, attrs, children)
// }
module.exports = function() {
	var attrs = arguments[this], start = this + 1, children

	if (attrs == null) {
		attrs = {}
	} else if (typeof attrs !== "object" || attrs.tag != null || Array.isArray(attrs)) {
		attrs = {}
		start = this
	}

	if (arguments.length === start + 1) {
		children = arguments[start]
		if (!Array.isArray(children)) children = [children]
	} else {
		children = []
		while (start < arguments.length) children.push(arguments[start++])
	}

	return Vnode("", attrs.key, attrs, children)
}

},{"../render/vnode":"node_modules/mithril/render/vnode.js"}],"node_modules/mithril/render/hyperscript.js":[function(require,module,exports) {
"use strict"

var Vnode = require("../render/vnode")
var hyperscriptVnode = require("./hyperscriptVnode")

var selectorParser = /(?:(^|#|\.)([^#\.\[\]]+))|(\[(.+?)(?:\s*=\s*("|'|)((?:\\["'\]]|.)*?)\5)?\])/g
var selectorCache = {}
var hasOwn = {}.hasOwnProperty

function isEmpty(object) {
	for (var key in object) if (hasOwn.call(object, key)) return false
	return true
}

function compileSelector(selector) {
	var match, tag = "div", classes = [], attrs = {}
	while (match = selectorParser.exec(selector)) {
		var type = match[1], value = match[2]
		if (type === "" && value !== "") tag = value
		else if (type === "#") attrs.id = value
		else if (type === ".") classes.push(value)
		else if (match[3][0] === "[") {
			var attrValue = match[6]
			if (attrValue) attrValue = attrValue.replace(/\\(["'])/g, "$1").replace(/\\\\/g, "\\")
			if (match[4] === "class") classes.push(attrValue)
			else attrs[match[4]] = attrValue === "" ? attrValue : attrValue || true
		}
	}
	if (classes.length > 0) attrs.className = classes.join(" ")
	return selectorCache[selector] = {tag: tag, attrs: attrs}
}

function execSelector(state, vnode) {
	var attrs = vnode.attrs
	var children = Vnode.normalizeChildren(vnode.children)
	var hasClass = hasOwn.call(attrs, "class")
	var className = hasClass ? attrs.class : attrs.className

	vnode.tag = state.tag
	vnode.attrs = null
	vnode.children = undefined

	if (!isEmpty(state.attrs) && !isEmpty(attrs)) {
		var newAttrs = {}

		for (var key in attrs) {
			if (hasOwn.call(attrs, key)) newAttrs[key] = attrs[key]
		}

		attrs = newAttrs
	}

	for (var key in state.attrs) {
		if (hasOwn.call(state.attrs, key) && key !== "className" && !hasOwn.call(attrs, key)){
			attrs[key] = state.attrs[key]
		}
	}
	if (className != null || state.attrs.className != null) attrs.className =
		className != null
			? state.attrs.className != null
				? String(state.attrs.className) + " " + String(className)
				: className
			: state.attrs.className != null
				? state.attrs.className
				: null

	if (hasClass) attrs.class = null

	for (var key in attrs) {
		if (hasOwn.call(attrs, key) && key !== "key") {
			vnode.attrs = attrs
			break
		}
	}

	if (Array.isArray(children) && children.length === 1 && children[0] != null && children[0].tag === "#") {
		vnode.text = children[0].children
	} else {
		vnode.children = children
	}

	return vnode
}

function hyperscript(selector) {
	if (selector == null || typeof selector !== "string" && typeof selector !== "function" && typeof selector.view !== "function") {
		throw Error("The selector must be either a string or a component.");
	}

	var vnode = hyperscriptVnode.apply(1, arguments)

	if (typeof selector === "string") {
		vnode.children = Vnode.normalizeChildren(vnode.children)
		if (selector !== "[") return execSelector(selectorCache[selector] || compileSelector(selector), vnode)
	}

	vnode.tag = selector
	return vnode
}

module.exports = hyperscript

},{"../render/vnode":"node_modules/mithril/render/vnode.js","./hyperscriptVnode":"node_modules/mithril/render/hyperscriptVnode.js"}],"node_modules/mithril/render/trust.js":[function(require,module,exports) {
"use strict"

var Vnode = require("../render/vnode")

module.exports = function(html) {
	if (html == null) html = ""
	return Vnode("<", undefined, undefined, html, undefined, undefined)
}

},{"../render/vnode":"node_modules/mithril/render/vnode.js"}],"node_modules/mithril/render/fragment.js":[function(require,module,exports) {
"use strict"

var Vnode = require("../render/vnode")
var hyperscriptVnode = require("./hyperscriptVnode")

module.exports = function() {
	var vnode = hyperscriptVnode.apply(0, arguments)

	vnode.tag = "["
	vnode.children = Vnode.normalizeChildren(vnode.children)
	return vnode
}

},{"../render/vnode":"node_modules/mithril/render/vnode.js","./hyperscriptVnode":"node_modules/mithril/render/hyperscriptVnode.js"}],"node_modules/mithril/hyperscript.js":[function(require,module,exports) {
"use strict"

var hyperscript = require("./render/hyperscript")

hyperscript.trust = require("./render/trust")
hyperscript.fragment = require("./render/fragment")

module.exports = hyperscript

},{"./render/hyperscript":"node_modules/mithril/render/hyperscript.js","./render/trust":"node_modules/mithril/render/trust.js","./render/fragment":"node_modules/mithril/render/fragment.js"}],"node_modules/mithril/promise/polyfill.js":[function(require,module,exports) {
"use strict"
/** @constructor */
var PromisePolyfill = function(executor) {
	if (!(this instanceof PromisePolyfill)) throw new Error("Promise must be called with `new`")
	if (typeof executor !== "function") throw new TypeError("executor must be a function")

	var self = this, resolvers = [], rejectors = [], resolveCurrent = handler(resolvers, true), rejectCurrent = handler(rejectors, false)
	var instance = self._instance = {resolvers: resolvers, rejectors: rejectors}
	var callAsync = typeof setImmediate === "function" ? setImmediate : setTimeout
	function handler(list, shouldAbsorb) {
		return function execute(value) {
			var then
			try {
				if (shouldAbsorb && value != null && (typeof value === "object" || typeof value === "function") && typeof (then = value.then) === "function") {
					if (value === self) throw new TypeError("Promise can't be resolved w/ itself")
					executeOnce(then.bind(value))
				}
				else {
					callAsync(function() {
						if (!shouldAbsorb && list.length === 0) console.error("Possible unhandled promise rejection:", value)
						for (var i = 0; i < list.length; i++) list[i](value)
						resolvers.length = 0, rejectors.length = 0
						instance.state = shouldAbsorb
						instance.retry = function() {execute(value)}
					})
				}
			}
			catch (e) {
				rejectCurrent(e)
			}
		}
	}
	function executeOnce(then) {
		var runs = 0
		function run(fn) {
			return function(value) {
				if (runs++ > 0) return
				fn(value)
			}
		}
		var onerror = run(rejectCurrent)
		try {then(run(resolveCurrent), onerror)} catch (e) {onerror(e)}
	}

	executeOnce(executor)
}
PromisePolyfill.prototype.then = function(onFulfilled, onRejection) {
	var self = this, instance = self._instance
	function handle(callback, list, next, state) {
		list.push(function(value) {
			if (typeof callback !== "function") next(value)
			else try {resolveNext(callback(value))} catch (e) {if (rejectNext) rejectNext(e)}
		})
		if (typeof instance.retry === "function" && state === instance.state) instance.retry()
	}
	var resolveNext, rejectNext
	var promise = new PromisePolyfill(function(resolve, reject) {resolveNext = resolve, rejectNext = reject})
	handle(onFulfilled, instance.resolvers, resolveNext, true), handle(onRejection, instance.rejectors, rejectNext, false)
	return promise
}
PromisePolyfill.prototype.catch = function(onRejection) {
	return this.then(null, onRejection)
}
PromisePolyfill.prototype.finally = function(callback) {
	return this.then(
		function(value) {
			return PromisePolyfill.resolve(callback()).then(function() {
				return value
			})
		},
		function(reason) {
			return PromisePolyfill.resolve(callback()).then(function() {
				return PromisePolyfill.reject(reason);
			})
		}
	)
}
PromisePolyfill.resolve = function(value) {
	if (value instanceof PromisePolyfill) return value
	return new PromisePolyfill(function(resolve) {resolve(value)})
}
PromisePolyfill.reject = function(value) {
	return new PromisePolyfill(function(resolve, reject) {reject(value)})
}
PromisePolyfill.all = function(list) {
	return new PromisePolyfill(function(resolve, reject) {
		var total = list.length, count = 0, values = []
		if (list.length === 0) resolve([])
		else for (var i = 0; i < list.length; i++) {
			(function(i) {
				function consume(value) {
					count++
					values[i] = value
					if (count === total) resolve(values)
				}
				if (list[i] != null && (typeof list[i] === "object" || typeof list[i] === "function") && typeof list[i].then === "function") {
					list[i].then(consume, reject)
				}
				else consume(list[i])
			})(i)
		}
	})
}
PromisePolyfill.race = function(list) {
	return new PromisePolyfill(function(resolve, reject) {
		for (var i = 0; i < list.length; i++) {
			list[i].then(resolve, reject)
		}
	})
}

module.exports = PromisePolyfill

},{}],"node_modules/mithril/promise/promise.js":[function(require,module,exports) {
var global = arguments[3];
"use strict"

var PromisePolyfill = require("./polyfill")

if (typeof window !== "undefined") {
	if (typeof window.Promise === "undefined") {
		window.Promise = PromisePolyfill
	} else if (!window.Promise.prototype.finally) {
		window.Promise.prototype.finally = PromisePolyfill.prototype.finally
	}
	module.exports = window.Promise
} else if (typeof global !== "undefined") {
	if (typeof global.Promise === "undefined") {
		global.Promise = PromisePolyfill
	} else if (!global.Promise.prototype.finally) {
		global.Promise.prototype.finally = PromisePolyfill.prototype.finally
	}
	module.exports = global.Promise
} else {
	module.exports = PromisePolyfill
}

},{"./polyfill":"node_modules/mithril/promise/polyfill.js"}],"node_modules/mithril/render/render.js":[function(require,module,exports) {
"use strict"

var Vnode = require("../render/vnode")

module.exports = function($window) {
	var $doc = $window && $window.document
	var currentRedraw

	var nameSpace = {
		svg: "http://www.w3.org/2000/svg",
		math: "http://www.w3.org/1998/Math/MathML"
	}

	function getNameSpace(vnode) {
		return vnode.attrs && vnode.attrs.xmlns || nameSpace[vnode.tag]
	}

	//sanity check to discourage people from doing `vnode.state = ...`
	function checkState(vnode, original) {
		if (vnode.state !== original) throw new Error("`vnode.state` must not be modified")
	}

	//Note: the hook is passed as the `this` argument to allow proxying the
	//arguments without requiring a full array allocation to do so. It also
	//takes advantage of the fact the current `vnode` is the first argument in
	//all lifecycle methods.
	function callHook(vnode) {
		var original = vnode.state
		try {
			return this.apply(original, arguments)
		} finally {
			checkState(vnode, original)
		}
	}

	// IE11 (at least) throws an UnspecifiedError when accessing document.activeElement when
	// inside an iframe. Catch and swallow this error, and heavy-handidly return null.
	function activeElement() {
		try {
			return $doc.activeElement
		} catch (e) {
			return null
		}
	}
	//create
	function createNodes(parent, vnodes, start, end, hooks, nextSibling, ns) {
		for (var i = start; i < end; i++) {
			var vnode = vnodes[i]
			if (vnode != null) {
				createNode(parent, vnode, hooks, ns, nextSibling)
			}
		}
	}
	function createNode(parent, vnode, hooks, ns, nextSibling) {
		var tag = vnode.tag
		if (typeof tag === "string") {
			vnode.state = {}
			if (vnode.attrs != null) initLifecycle(vnode.attrs, vnode, hooks)
			switch (tag) {
				case "#": createText(parent, vnode, nextSibling); break
				case "<": createHTML(parent, vnode, ns, nextSibling); break
				case "[": createFragment(parent, vnode, hooks, ns, nextSibling); break
				default: createElement(parent, vnode, hooks, ns, nextSibling)
			}
		}
		else createComponent(parent, vnode, hooks, ns, nextSibling)
	}
	function createText(parent, vnode, nextSibling) {
		vnode.dom = $doc.createTextNode(vnode.children)
		insertNode(parent, vnode.dom, nextSibling)
	}
	var possibleParents = {caption: "table", thead: "table", tbody: "table", tfoot: "table", tr: "tbody", th: "tr", td: "tr", colgroup: "table", col: "colgroup"}
	function createHTML(parent, vnode, ns, nextSibling) {
		var match = vnode.children.match(/^\s*?<(\w+)/im) || []
		// not using the proper parent makes the child element(s) vanish.
		//     var div = document.createElement("div")
		//     div.innerHTML = "<td>i</td><td>j</td>"
		//     console.log(div.innerHTML)
		// --> "ij", no <td> in sight.
		var temp = $doc.createElement(possibleParents[match[1]] || "div")
		if (ns === "http://www.w3.org/2000/svg") {
			temp.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\">" + vnode.children + "</svg>"
			temp = temp.firstChild
		} else {
			temp.innerHTML = vnode.children
		}
		vnode.dom = temp.firstChild
		vnode.domSize = temp.childNodes.length
		// Capture nodes to remove, so we don't confuse them.
		vnode.instance = []
		var fragment = $doc.createDocumentFragment()
		var child
		while (child = temp.firstChild) {
			vnode.instance.push(child)
			fragment.appendChild(child)
		}
		insertNode(parent, fragment, nextSibling)
	}
	function createFragment(parent, vnode, hooks, ns, nextSibling) {
		var fragment = $doc.createDocumentFragment()
		if (vnode.children != null) {
			var children = vnode.children
			createNodes(fragment, children, 0, children.length, hooks, null, ns)
		}
		vnode.dom = fragment.firstChild
		vnode.domSize = fragment.childNodes.length
		insertNode(parent, fragment, nextSibling)
	}
	function createElement(parent, vnode, hooks, ns, nextSibling) {
		var tag = vnode.tag
		var attrs = vnode.attrs
		var is = attrs && attrs.is

		ns = getNameSpace(vnode) || ns

		var element = ns ?
			is ? $doc.createElementNS(ns, tag, {is: is}) : $doc.createElementNS(ns, tag) :
			is ? $doc.createElement(tag, {is: is}) : $doc.createElement(tag)
		vnode.dom = element

		if (attrs != null) {
			setAttrs(vnode, attrs, ns)
		}

		insertNode(parent, element, nextSibling)

		if (!maybeSetContentEditable(vnode)) {
			if (vnode.text != null) {
				if (vnode.text !== "") element.textContent = vnode.text
				else vnode.children = [Vnode("#", undefined, undefined, vnode.text, undefined, undefined)]
			}
			if (vnode.children != null) {
				var children = vnode.children
				createNodes(element, children, 0, children.length, hooks, null, ns)
				if (vnode.tag === "select" && attrs != null) setLateSelectAttrs(vnode, attrs)
			}
		}
	}
	function initComponent(vnode, hooks) {
		var sentinel
		if (typeof vnode.tag.view === "function") {
			vnode.state = Object.create(vnode.tag)
			sentinel = vnode.state.view
			if (sentinel.$$reentrantLock$$ != null) return
			sentinel.$$reentrantLock$$ = true
		} else {
			vnode.state = void 0
			sentinel = vnode.tag
			if (sentinel.$$reentrantLock$$ != null) return
			sentinel.$$reentrantLock$$ = true
			vnode.state = (vnode.tag.prototype != null && typeof vnode.tag.prototype.view === "function") ? new vnode.tag(vnode) : vnode.tag(vnode)
		}
		initLifecycle(vnode.state, vnode, hooks)
		if (vnode.attrs != null) initLifecycle(vnode.attrs, vnode, hooks)
		vnode.instance = Vnode.normalize(callHook.call(vnode.state.view, vnode))
		if (vnode.instance === vnode) throw Error("A view cannot return the vnode it received as argument")
		sentinel.$$reentrantLock$$ = null
	}
	function createComponent(parent, vnode, hooks, ns, nextSibling) {
		initComponent(vnode, hooks)
		if (vnode.instance != null) {
			createNode(parent, vnode.instance, hooks, ns, nextSibling)
			vnode.dom = vnode.instance.dom
			vnode.domSize = vnode.dom != null ? vnode.instance.domSize : 0
		}
		else {
			vnode.domSize = 0
		}
	}

	//update
	/**
	 * @param {Element|Fragment} parent - the parent element
	 * @param {Vnode[] | null} old - the list of vnodes of the last `render()` call for
	 *                               this part of the tree
	 * @param {Vnode[] | null} vnodes - as above, but for the current `render()` call.
	 * @param {Function[]} hooks - an accumulator of post-render hooks (oncreate/onupdate)
	 * @param {Element | null} nextSibling - the next DOM node if we're dealing with a
	 *                                       fragment that is not the last item in its
	 *                                       parent
	 * @param {'svg' | 'math' | String | null} ns) - the current XML namespace, if any
	 * @returns void
	 */
	// This function diffs and patches lists of vnodes, both keyed and unkeyed.
	//
	// We will:
	//
	// 1. describe its general structure
	// 2. focus on the diff algorithm optimizations
	// 3. discuss DOM node operations.

	// ## Overview:
	//
	// The updateNodes() function:
	// - deals with trivial cases
	// - determines whether the lists are keyed or unkeyed based on the first non-null node
	//   of each list.
	// - diffs them and patches the DOM if needed (that's the brunt of the code)
	// - manages the leftovers: after diffing, are there:
	//   - old nodes left to remove?
	// 	 - new nodes to insert?
	// 	 deal with them!
	//
	// The lists are only iterated over once, with an exception for the nodes in `old` that
	// are visited in the fourth part of the diff and in the `removeNodes` loop.

	// ## Diffing
	//
	// Reading https://github.com/localvoid/ivi/blob/ddc09d06abaef45248e6133f7040d00d3c6be853/packages/ivi/src/vdom/implementation.ts#L617-L837
	// may be good for context on longest increasing subsequence-based logic for moving nodes.
	//
	// In order to diff keyed lists, one has to
	//
	// 1) match nodes in both lists, per key, and update them accordingly
	// 2) create the nodes present in the new list, but absent in the old one
	// 3) remove the nodes present in the old list, but absent in the new one
	// 4) figure out what nodes in 1) to move in order to minimize the DOM operations.
	//
	// To achieve 1) one can create a dictionary of keys => index (for the old list), then iterate
	// over the new list and for each new vnode, find the corresponding vnode in the old list using
	// the map.
	// 2) is achieved in the same step: if a new node has no corresponding entry in the map, it is new
	// and must be created.
	// For the removals, we actually remove the nodes that have been updated from the old list.
	// The nodes that remain in that list after 1) and 2) have been performed can be safely removed.
	// The fourth step is a bit more complex and relies on the longest increasing subsequence (LIS)
	// algorithm.
	//
	// the longest increasing subsequence is the list of nodes that can remain in place. Imagine going
	// from `1,2,3,4,5` to `4,5,1,2,3` where the numbers are not necessarily the keys, but the indices
	// corresponding to the keyed nodes in the old list (keyed nodes `e,d,c,b,a` => `b,a,e,d,c` would
	//  match the above lists, for example).
	//
	// In there are two increasing subsequences: `4,5` and `1,2,3`, the latter being the longest. We
	// can update those nodes without moving them, and only call `insertNode` on `4` and `5`.
	//
	// @localvoid adapted the algo to also support node deletions and insertions (the `lis` is actually
	// the longest increasing subsequence *of old nodes still present in the new list*).
	//
	// It is a general algorithm that is fireproof in all circumstances, but it requires the allocation
	// and the construction of a `key => oldIndex` map, and three arrays (one with `newIndex => oldIndex`,
	// the `LIS` and a temporary one to create the LIS).
	//
	// So we cheat where we can: if the tails of the lists are identical, they are guaranteed to be part of
	// the LIS and can be updated without moving them.
	//
	// If two nodes are swapped, they are guaranteed not to be part of the LIS, and must be moved (with
	// the exception of the last node if the list is fully reversed).
	//
	// ## Finding the next sibling.
	//
	// `updateNode()` and `createNode()` expect a nextSibling parameter to perform DOM operations.
	// When the list is being traversed top-down, at any index, the DOM nodes up to the previous
	// vnode reflect the content of the new list, whereas the rest of the DOM nodes reflect the old
	// list. The next sibling must be looked for in the old list using `getNextSibling(... oldStart + 1 ...)`.
	//
	// In the other scenarios (swaps, upwards traversal, map-based diff),
	// the new vnodes list is traversed upwards. The DOM nodes at the bottom of the list reflect the
	// bottom part of the new vnodes list, and we can use the `v.dom`  value of the previous node
	// as the next sibling (cached in the `nextSibling` variable).


	// ## DOM node moves
	//
	// In most scenarios `updateNode()` and `createNode()` perform the DOM operations. However,
	// this is not the case if the node moved (second and fourth part of the diff algo). We move
	// the old DOM nodes before updateNode runs because it enables us to use the cached `nextSibling`
	// variable rather than fetching it using `getNextSibling()`.
	//
	// The fourth part of the diff currently inserts nodes unconditionally, leading to issues
	// like #1791 and #1999. We need to be smarter about those situations where adjascent old
	// nodes remain together in the new list in a way that isn't covered by parts one and
	// three of the diff algo.

	function updateNodes(parent, old, vnodes, hooks, nextSibling, ns) {
		if (old === vnodes || old == null && vnodes == null) return
		else if (old == null || old.length === 0) createNodes(parent, vnodes, 0, vnodes.length, hooks, nextSibling, ns)
		else if (vnodes == null || vnodes.length === 0) removeNodes(parent, old, 0, old.length)
		else {
			var isOldKeyed = old[0] != null && old[0].key != null
			var isKeyed = vnodes[0] != null && vnodes[0].key != null
			var start = 0, oldStart = 0
			if (!isOldKeyed) while (oldStart < old.length && old[oldStart] == null) oldStart++
			if (!isKeyed) while (start < vnodes.length && vnodes[start] == null) start++
			if (isKeyed === null && isOldKeyed == null) return // both lists are full of nulls
			if (isOldKeyed !== isKeyed) {
				removeNodes(parent, old, oldStart, old.length)
				createNodes(parent, vnodes, start, vnodes.length, hooks, nextSibling, ns)
			} else if (!isKeyed) {
				// Don't index past the end of either list (causes deopts).
				var commonLength = old.length < vnodes.length ? old.length : vnodes.length
				// Rewind if necessary to the first non-null index on either side.
				// We could alternatively either explicitly create or remove nodes when `start !== oldStart`
				// but that would be optimizing for sparse lists which are more rare than dense ones.
				start = start < oldStart ? start : oldStart
				for (; start < commonLength; start++) {
					o = old[start]
					v = vnodes[start]
					if (o === v || o == null && v == null) continue
					else if (o == null) createNode(parent, v, hooks, ns, getNextSibling(old, start + 1, nextSibling))
					else if (v == null) removeNode(parent, o)
					else updateNode(parent, o, v, hooks, getNextSibling(old, start + 1, nextSibling), ns)
				}
				if (old.length > commonLength) removeNodes(parent, old, start, old.length)
				if (vnodes.length > commonLength) createNodes(parent, vnodes, start, vnodes.length, hooks, nextSibling, ns)
			} else {
				// keyed diff
				var oldEnd = old.length - 1, end = vnodes.length - 1, map, o, v, oe, ve, topSibling

				// bottom-up
				while (oldEnd >= oldStart && end >= start) {
					oe = old[oldEnd]
					ve = vnodes[end]
					if (oe.key !== ve.key) break
					if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns)
					if (ve.dom != null) nextSibling = ve.dom
					oldEnd--, end--
				}
				// top-down
				while (oldEnd >= oldStart && end >= start) {
					o = old[oldStart]
					v = vnodes[start]
					if (o.key !== v.key) break
					oldStart++, start++
					if (o !== v) updateNode(parent, o, v, hooks, getNextSibling(old, oldStart, nextSibling), ns)
				}
				// swaps and list reversals
				while (oldEnd >= oldStart && end >= start) {
					if (start === end) break
					if (o.key !== ve.key || oe.key !== v.key) break
					topSibling = getNextSibling(old, oldStart, nextSibling)
					moveNodes(parent, oe, topSibling)
					if (oe !== v) updateNode(parent, oe, v, hooks, topSibling, ns)
					if (++start <= --end) moveNodes(parent, o, nextSibling)
					if (o !== ve) updateNode(parent, o, ve, hooks, nextSibling, ns)
					if (ve.dom != null) nextSibling = ve.dom
					oldStart++; oldEnd--
					oe = old[oldEnd]
					ve = vnodes[end]
					o = old[oldStart]
					v = vnodes[start]
				}
				// bottom up once again
				while (oldEnd >= oldStart && end >= start) {
					if (oe.key !== ve.key) break
					if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns)
					if (ve.dom != null) nextSibling = ve.dom
					oldEnd--, end--
					oe = old[oldEnd]
					ve = vnodes[end]
				}
				if (start > end) removeNodes(parent, old, oldStart, oldEnd + 1)
				else if (oldStart > oldEnd) createNodes(parent, vnodes, start, end + 1, hooks, nextSibling, ns)
				else {
					// inspired by ivi https://github.com/ivijs/ivi/ by Boris Kaul
					var originalNextSibling = nextSibling, vnodesLength = end - start + 1, oldIndices = new Array(vnodesLength), li=0, i=0, pos = 2147483647, matched = 0, map, lisIndices
					for (i = 0; i < vnodesLength; i++) oldIndices[i] = -1
					for (i = end; i >= start; i--) {
						if (map == null) map = getKeyMap(old, oldStart, oldEnd + 1)
						ve = vnodes[i]
						var oldIndex = map[ve.key]
						if (oldIndex != null) {
							pos = (oldIndex < pos) ? oldIndex : -1 // becomes -1 if nodes were re-ordered
							oldIndices[i-start] = oldIndex
							oe = old[oldIndex]
							old[oldIndex] = null
							if (oe !== ve) updateNode(parent, oe, ve, hooks, nextSibling, ns)
							if (ve.dom != null) nextSibling = ve.dom
							matched++
						}
					}
					nextSibling = originalNextSibling
					if (matched !== oldEnd - oldStart + 1) removeNodes(parent, old, oldStart, oldEnd + 1)
					if (matched === 0) createNodes(parent, vnodes, start, end + 1, hooks, nextSibling, ns)
					else {
						if (pos === -1) {
							// the indices of the indices of the items that are part of the
							// longest increasing subsequence in the oldIndices list
							lisIndices = makeLisIndices(oldIndices)
							li = lisIndices.length - 1
							for (i = end; i >= start; i--) {
								v = vnodes[i]
								if (oldIndices[i-start] === -1) createNode(parent, v, hooks, ns, nextSibling)
								else {
									if (lisIndices[li] === i - start) li--
									else moveNodes(parent, v, nextSibling)
								}
								if (v.dom != null) nextSibling = vnodes[i].dom
							}
						} else {
							for (i = end; i >= start; i--) {
								v = vnodes[i]
								if (oldIndices[i-start] === -1) createNode(parent, v, hooks, ns, nextSibling)
								if (v.dom != null) nextSibling = vnodes[i].dom
							}
						}
					}
				}
			}
		}
	}
	function updateNode(parent, old, vnode, hooks, nextSibling, ns) {
		var oldTag = old.tag, tag = vnode.tag
		if (oldTag === tag) {
			vnode.state = old.state
			vnode.events = old.events
			if (shouldNotUpdate(vnode, old)) return
			if (typeof oldTag === "string") {
				if (vnode.attrs != null) {
					updateLifecycle(vnode.attrs, vnode, hooks)
				}
				switch (oldTag) {
					case "#": updateText(old, vnode); break
					case "<": updateHTML(parent, old, vnode, ns, nextSibling); break
					case "[": updateFragment(parent, old, vnode, hooks, nextSibling, ns); break
					default: updateElement(old, vnode, hooks, ns)
				}
			}
			else updateComponent(parent, old, vnode, hooks, nextSibling, ns)
		}
		else {
			removeNode(parent, old)
			createNode(parent, vnode, hooks, ns, nextSibling)
		}
	}
	function updateText(old, vnode) {
		if (old.children.toString() !== vnode.children.toString()) {
			old.dom.nodeValue = vnode.children
		}
		vnode.dom = old.dom
	}
	function updateHTML(parent, old, vnode, ns, nextSibling) {
		if (old.children !== vnode.children) {
			removeHTML(parent, old)
			createHTML(parent, vnode, ns, nextSibling)
		}
		else {
			vnode.dom = old.dom
			vnode.domSize = old.domSize
			vnode.instance = old.instance
		}
	}
	function updateFragment(parent, old, vnode, hooks, nextSibling, ns) {
		updateNodes(parent, old.children, vnode.children, hooks, nextSibling, ns)
		var domSize = 0, children = vnode.children
		vnode.dom = null
		if (children != null) {
			for (var i = 0; i < children.length; i++) {
				var child = children[i]
				if (child != null && child.dom != null) {
					if (vnode.dom == null) vnode.dom = child.dom
					domSize += child.domSize || 1
				}
			}
			if (domSize !== 1) vnode.domSize = domSize
		}
	}
	function updateElement(old, vnode, hooks, ns) {
		var element = vnode.dom = old.dom
		ns = getNameSpace(vnode) || ns

		if (vnode.tag === "textarea") {
			if (vnode.attrs == null) vnode.attrs = {}
			if (vnode.text != null) {
				vnode.attrs.value = vnode.text //FIXME handle multiple children
				vnode.text = undefined
			}
		}
		updateAttrs(vnode, old.attrs, vnode.attrs, ns)
		if (!maybeSetContentEditable(vnode)) {
			if (old.text != null && vnode.text != null && vnode.text !== "") {
				if (old.text.toString() !== vnode.text.toString()) old.dom.firstChild.nodeValue = vnode.text
			}
			else {
				if (old.text != null) old.children = [Vnode("#", undefined, undefined, old.text, undefined, old.dom.firstChild)]
				if (vnode.text != null) vnode.children = [Vnode("#", undefined, undefined, vnode.text, undefined, undefined)]
				updateNodes(element, old.children, vnode.children, hooks, null, ns)
			}
		}
	}
	function updateComponent(parent, old, vnode, hooks, nextSibling, ns) {
		vnode.instance = Vnode.normalize(callHook.call(vnode.state.view, vnode))
		if (vnode.instance === vnode) throw Error("A view cannot return the vnode it received as argument")
		updateLifecycle(vnode.state, vnode, hooks)
		if (vnode.attrs != null) updateLifecycle(vnode.attrs, vnode, hooks)
		if (vnode.instance != null) {
			if (old.instance == null) createNode(parent, vnode.instance, hooks, ns, nextSibling)
			else updateNode(parent, old.instance, vnode.instance, hooks, nextSibling, ns)
			vnode.dom = vnode.instance.dom
			vnode.domSize = vnode.instance.domSize
		}
		else if (old.instance != null) {
			removeNode(parent, old.instance)
			vnode.dom = undefined
			vnode.domSize = 0
		}
		else {
			vnode.dom = old.dom
			vnode.domSize = old.domSize
		}
	}
	function getKeyMap(vnodes, start, end) {
		var map = Object.create(null)
		for (; start < end; start++) {
			var vnode = vnodes[start]
			if (vnode != null) {
				var key = vnode.key
				if (key != null) map[key] = start
			}
		}
		return map
	}
	// Lifted from ivi https://github.com/ivijs/ivi/
	// takes a list of unique numbers (-1 is special and can
	// occur multiple times) and returns an array with the indices
	// of the items that are part of the longest increasing
	// subsequece
	var lisTemp = []
	function makeLisIndices(a) {
		var result = [0]
		var u = 0, v = 0, i = 0
		var il = lisTemp.length = a.length
		for (var i = 0; i < il; i++) lisTemp[i] = a[i]
		for (var i = 0; i < il; ++i) {
			if (a[i] === -1) continue
			var j = result[result.length - 1]
			if (a[j] < a[i]) {
				lisTemp[i] = j
				result.push(i)
				continue
			}
			u = 0
			v = result.length - 1
			while (u < v) {
				// Fast integer average without overflow.
				// eslint-disable-next-line no-bitwise
				var c = (u >>> 1) + (v >>> 1) + (u & v & 1)
				if (a[result[c]] < a[i]) {
					u = c + 1
				}
				else {
					v = c
				}
			}
			if (a[i] < a[result[u]]) {
				if (u > 0) lisTemp[i] = result[u - 1]
				result[u] = i
			}
		}
		u = result.length
		v = result[u - 1]
		while (u-- > 0) {
			result[u] = v
			v = lisTemp[v]
		}
		lisTemp.length = 0
		return result
	}

	function getNextSibling(vnodes, i, nextSibling) {
		for (; i < vnodes.length; i++) {
			if (vnodes[i] != null && vnodes[i].dom != null) return vnodes[i].dom
		}
		return nextSibling
	}

	// This covers a really specific edge case:
	// - Parent node is keyed and contains child
	// - Child is removed, returns unresolved promise in `onbeforeremove`
	// - Parent node is moved in keyed diff
	// - Remaining children still need moved appropriately
	//
	// Ideally, I'd track removed nodes as well, but that introduces a lot more
	// complexity and I'm not exactly interested in doing that.
	function moveNodes(parent, vnode, nextSibling) {
		var frag = $doc.createDocumentFragment()
		moveChildToFrag(parent, frag, vnode)
		insertNode(parent, frag, nextSibling)
	}
	function moveChildToFrag(parent, frag, vnode) {
		// Dodge the recursion overhead in a few of the most common cases.
		while (vnode.dom != null && vnode.dom.parentNode === parent) {
			if (typeof vnode.tag !== "string") {
				vnode = vnode.instance
				if (vnode != null) continue
			} else if (vnode.tag === "<") {
				for (var i = 0; i < vnode.instance.length; i++) {
					frag.appendChild(vnode.instance[i])
				}
			} else if (vnode.tag !== "[") {
				// Don't recurse for text nodes *or* elements, just fragments
				frag.appendChild(vnode.dom)
			} else if (vnode.children.length === 1) {
				vnode = vnode.children[0]
				if (vnode != null) continue
			} else {
				for (var i = 0; i < vnode.children.length; i++) {
					var child = vnode.children[i]
					if (child != null) moveChildToFrag(parent, frag, child)
				}
			}
			break
		}
	}

	function insertNode(parent, dom, nextSibling) {
		if (nextSibling != null) parent.insertBefore(dom, nextSibling)
		else parent.appendChild(dom)
	}

	function maybeSetContentEditable(vnode) {
		if (vnode.attrs == null || (
			vnode.attrs.contenteditable == null && // attribute
			vnode.attrs.contentEditable == null // property
		)) return false
		var children = vnode.children
		if (children != null && children.length === 1 && children[0].tag === "<") {
			var content = children[0].children
			if (vnode.dom.innerHTML !== content) vnode.dom.innerHTML = content
		}
		else if (vnode.text != null || children != null && children.length !== 0) throw new Error("Child node of a contenteditable must be trusted")
		return true
	}

	//remove
	function removeNodes(parent, vnodes, start, end) {
		for (var i = start; i < end; i++) {
			var vnode = vnodes[i]
			if (vnode != null) removeNode(parent, vnode)
		}
	}
	function removeNode(parent, vnode) {
		var mask = 0
		var original = vnode.state
		var stateResult, attrsResult
		if (typeof vnode.tag !== "string" && typeof vnode.state.onbeforeremove === "function") {
			var result = callHook.call(vnode.state.onbeforeremove, vnode)
			if (result != null && typeof result.then === "function") {
				mask = 1
				stateResult = result
			}
		}
		if (vnode.attrs && typeof vnode.attrs.onbeforeremove === "function") {
			var result = callHook.call(vnode.attrs.onbeforeremove, vnode)
			if (result != null && typeof result.then === "function") {
				// eslint-disable-next-line no-bitwise
				mask |= 2
				attrsResult = result
			}
		}
		checkState(vnode, original)

		// If we can, try to fast-path it and avoid all the overhead of awaiting
		if (!mask) {
			onremove(vnode)
			removeChild(parent, vnode)
		} else {
			if (stateResult != null) {
				var next = function () {
					// eslint-disable-next-line no-bitwise
					if (mask & 1) { mask &= 2; if (!mask) reallyRemove() }
				}
				stateResult.then(next, next)
			}
			if (attrsResult != null) {
				var next = function () {
					// eslint-disable-next-line no-bitwise
					if (mask & 2) { mask &= 1; if (!mask) reallyRemove() }
				}
				attrsResult.then(next, next)
			}
		}

		function reallyRemove() {
			checkState(vnode, original)
			onremove(vnode)
			removeChild(parent, vnode)
		}
	}
	function removeHTML(parent, vnode) {
		for (var i = 0; i < vnode.instance.length; i++) {
			parent.removeChild(vnode.instance[i])
		}
	}
	function removeChild(parent, vnode) {
		// Dodge the recursion overhead in a few of the most common cases.
		while (vnode.dom != null && vnode.dom.parentNode === parent) {
			if (typeof vnode.tag !== "string") {
				vnode = vnode.instance
				if (vnode != null) continue
			} else if (vnode.tag === "<") {
				removeHTML(parent, vnode)
			} else {
				if (vnode.tag !== "[") {
					parent.removeChild(vnode.dom)
					if (!Array.isArray(vnode.children)) break
				}
				if (vnode.children.length === 1) {
					vnode = vnode.children[0]
					if (vnode != null) continue
				} else {
					for (var i = 0; i < vnode.children.length; i++) {
						var child = vnode.children[i]
						if (child != null) removeChild(parent, child)
					}
				}
			}
			break
		}
	}
	function onremove(vnode) {
		if (typeof vnode.tag !== "string" && typeof vnode.state.onremove === "function") callHook.call(vnode.state.onremove, vnode)
		if (vnode.attrs && typeof vnode.attrs.onremove === "function") callHook.call(vnode.attrs.onremove, vnode)
		if (typeof vnode.tag !== "string") {
			if (vnode.instance != null) onremove(vnode.instance)
		} else {
			var children = vnode.children
			if (Array.isArray(children)) {
				for (var i = 0; i < children.length; i++) {
					var child = children[i]
					if (child != null) onremove(child)
				}
			}
		}
	}

	//attrs
	function setAttrs(vnode, attrs, ns) {
		for (var key in attrs) {
			setAttr(vnode, key, null, attrs[key], ns)
		}
	}
	function setAttr(vnode, key, old, value, ns) {
		if (key === "key" || key === "is" || value == null || isLifecycleMethod(key) || (old === value && !isFormAttribute(vnode, key)) && typeof value !== "object") return
		if (key[0] === "o" && key[1] === "n") return updateEvent(vnode, key, value)
		if (key.slice(0, 6) === "xlink:") vnode.dom.setAttributeNS("http://www.w3.org/1999/xlink", key.slice(6), value)
		else if (key === "style") updateStyle(vnode.dom, old, value)
		else if (hasPropertyKey(vnode, key, ns)) {
			if (key === "value") {
				// Only do the coercion if we're actually going to check the value.
				/* eslint-disable no-implicit-coercion */
				//setting input[value] to same value by typing on focused element moves cursor to end in Chrome
				if ((vnode.tag === "input" || vnode.tag === "textarea") && vnode.dom.value === "" + value && vnode.dom === activeElement()) return
				//setting select[value] to same value while having select open blinks select dropdown in Chrome
				if (vnode.tag === "select" && old !== null && vnode.dom.value === "" + value) return
				//setting option[value] to same value while having select open blinks select dropdown in Chrome
				if (vnode.tag === "option" && old !== null && vnode.dom.value === "" + value) return
				/* eslint-enable no-implicit-coercion */
			}
			// If you assign an input type that is not supported by IE 11 with an assignment expression, an error will occur.
			if (vnode.tag === "input" && key === "type") vnode.dom.setAttribute(key, value)
			else vnode.dom[key] = value
		} else {
			if (typeof value === "boolean") {
				if (value) vnode.dom.setAttribute(key, "")
				else vnode.dom.removeAttribute(key)
			}
			else vnode.dom.setAttribute(key === "className" ? "class" : key, value)
		}
	}
	function removeAttr(vnode, key, old, ns) {
		if (key === "key" || key === "is" || old == null || isLifecycleMethod(key)) return
		if (key[0] === "o" && key[1] === "n" && !isLifecycleMethod(key)) updateEvent(vnode, key, undefined)
		else if (key === "style") updateStyle(vnode.dom, old, null)
		else if (
			hasPropertyKey(vnode, key, ns)
			&& key !== "className"
			&& !(key === "value" && (
				vnode.tag === "option"
				|| vnode.tag === "select" && vnode.dom.selectedIndex === -1 && vnode.dom === activeElement()
			))
			&& !(vnode.tag === "input" && key === "type")
		) {
			vnode.dom[key] = null
		} else {
			var nsLastIndex = key.indexOf(":")
			if (nsLastIndex !== -1) key = key.slice(nsLastIndex + 1)
			if (old !== false) vnode.dom.removeAttribute(key === "className" ? "class" : key)
		}
	}
	function setLateSelectAttrs(vnode, attrs) {
		if ("value" in attrs) {
			if(attrs.value === null) {
				if (vnode.dom.selectedIndex !== -1) vnode.dom.value = null
			} else {
				var normalized = "" + attrs.value // eslint-disable-line no-implicit-coercion
				if (vnode.dom.value !== normalized || vnode.dom.selectedIndex === -1) {
					vnode.dom.value = normalized
				}
			}
		}
		if ("selectedIndex" in attrs) setAttr(vnode, "selectedIndex", null, attrs.selectedIndex, undefined)
	}
	function updateAttrs(vnode, old, attrs, ns) {
		if (attrs != null) {
			for (var key in attrs) {
				setAttr(vnode, key, old && old[key], attrs[key], ns)
			}
		}
		var val
		if (old != null) {
			for (var key in old) {
				if (((val = old[key]) != null) && (attrs == null || attrs[key] == null)) {
					removeAttr(vnode, key, val, ns)
				}
			}
		}
	}
	function isFormAttribute(vnode, attr) {
		return attr === "value" || attr === "checked" || attr === "selectedIndex" || attr === "selected" && vnode.dom === activeElement() || vnode.tag === "option" && vnode.dom.parentNode === $doc.activeElement
	}
	function isLifecycleMethod(attr) {
		return attr === "oninit" || attr === "oncreate" || attr === "onupdate" || attr === "onremove" || attr === "onbeforeremove" || attr === "onbeforeupdate"
	}
	function hasPropertyKey(vnode, key, ns) {
		// Filter out namespaced keys
		return ns === undefined && (
			// If it's a custom element, just keep it.
			vnode.tag.indexOf("-") > -1 || vnode.attrs != null && vnode.attrs.is ||
			// If it's a normal element, let's try to avoid a few browser bugs.
			key !== "href" && key !== "list" && key !== "form" && key !== "width" && key !== "height"// && key !== "type"
			// Defer the property check until *after* we check everything.
		) && key in vnode.dom
	}

	//style
	var uppercaseRegex = /[A-Z]/g
	function toLowerCase(capital) { return "-" + capital.toLowerCase() }
	function normalizeKey(key) {
		return key[0] === "-" && key[1] === "-" ? key :
			key === "cssFloat" ? "float" :
				key.replace(uppercaseRegex, toLowerCase)
	}
	function updateStyle(element, old, style) {
		if (old === style) {
			// Styles are equivalent, do nothing.
		} else if (style == null) {
			// New style is missing, just clear it.
			element.style.cssText = ""
		} else if (typeof style !== "object") {
			// New style is a string, let engine deal with patching.
			element.style.cssText = style
		} else if (old == null || typeof old !== "object") {
			// `old` is missing or a string, `style` is an object.
			element.style.cssText = ""
			// Add new style properties
			for (var key in style) {
				var value = style[key]
				if (value != null) element.style.setProperty(normalizeKey(key), String(value))
			}
		} else {
			// Both old & new are (different) objects.
			// Update style properties that have changed
			for (var key in style) {
				var value = style[key]
				if (value != null && (value = String(value)) !== String(old[key])) {
					element.style.setProperty(normalizeKey(key), value)
				}
			}
			// Remove style properties that no longer exist
			for (var key in old) {
				if (old[key] != null && style[key] == null) {
					element.style.removeProperty(normalizeKey(key))
				}
			}
		}
	}

	// Here's an explanation of how this works:
	// 1. The event names are always (by design) prefixed by `on`.
	// 2. The EventListener interface accepts either a function or an object
	//    with a `handleEvent` method.
	// 3. The object does not inherit from `Object.prototype`, to avoid
	//    any potential interference with that (e.g. setters).
	// 4. The event name is remapped to the handler before calling it.
	// 5. In function-based event handlers, `ev.target === this`. We replicate
	//    that below.
	// 6. In function-based event handlers, `return false` prevents the default
	//    action and stops event propagation. We replicate that below.
	function EventDict() {
		// Save this, so the current redraw is correctly tracked.
		this._ = currentRedraw
	}
	EventDict.prototype = Object.create(null)
	EventDict.prototype.handleEvent = function (ev) {
		var handler = this["on" + ev.type]
		var result
		if (typeof handler === "function") result = handler.call(ev.currentTarget, ev)
		else if (typeof handler.handleEvent === "function") handler.handleEvent(ev)
		if (this._ && ev.redraw !== false) (0, this._)()
		if (result === false) {
			ev.preventDefault()
			ev.stopPropagation()
		}
	}

	//event
	function updateEvent(vnode, key, value) {
		if (vnode.events != null) {
			if (vnode.events[key] === value) return
			if (value != null && (typeof value === "function" || typeof value === "object")) {
				if (vnode.events[key] == null) vnode.dom.addEventListener(key.slice(2), vnode.events, false)
				vnode.events[key] = value
			} else {
				if (vnode.events[key] != null) vnode.dom.removeEventListener(key.slice(2), vnode.events, false)
				vnode.events[key] = undefined
			}
		} else if (value != null && (typeof value === "function" || typeof value === "object")) {
			vnode.events = new EventDict()
			vnode.dom.addEventListener(key.slice(2), vnode.events, false)
			vnode.events[key] = value
		}
	}

	//lifecycle
	function initLifecycle(source, vnode, hooks) {
		if (typeof source.oninit === "function") callHook.call(source.oninit, vnode)
		if (typeof source.oncreate === "function") hooks.push(callHook.bind(source.oncreate, vnode))
	}
	function updateLifecycle(source, vnode, hooks) {
		if (typeof source.onupdate === "function") hooks.push(callHook.bind(source.onupdate, vnode))
	}
	function shouldNotUpdate(vnode, old) {
		do {
			if (vnode.attrs != null && typeof vnode.attrs.onbeforeupdate === "function") {
				var force = callHook.call(vnode.attrs.onbeforeupdate, vnode, old)
				if (force !== undefined && !force) break
			}
			if (typeof vnode.tag !== "string" && typeof vnode.state.onbeforeupdate === "function") {
				var force = callHook.call(vnode.state.onbeforeupdate, vnode, old)
				if (force !== undefined && !force) break
			}
			return false
		} while (false); // eslint-disable-line no-constant-condition
		vnode.dom = old.dom
		vnode.domSize = old.domSize
		vnode.instance = old.instance
		// One would think having the actual latest attributes would be ideal,
		// but it doesn't let us properly diff based on our current internal
		// representation. We have to save not only the old DOM info, but also
		// the attributes used to create it, as we diff *that*, not against the
		// DOM directly (with a few exceptions in `setAttr`). And, of course, we
		// need to save the children and text as they are conceptually not
		// unlike special "attributes" internally.
		vnode.attrs = old.attrs
		vnode.children = old.children
		vnode.text = old.text
		return true
	}

	return function(dom, vnodes, redraw) {
		if (!dom) throw new TypeError("Ensure the DOM element being passed to m.route/m.mount/m.render is not undefined.")
		var hooks = []
		var active = activeElement()
		var namespace = dom.namespaceURI

		// First time rendering into a node clears it out
		if (dom.vnodes == null) dom.textContent = ""

		vnodes = Vnode.normalizeChildren(Array.isArray(vnodes) ? vnodes : [vnodes])
		var prevRedraw = currentRedraw
		try {
			currentRedraw = typeof redraw === "function" ? redraw : undefined
			updateNodes(dom, dom.vnodes, vnodes, hooks, null, namespace === "http://www.w3.org/1999/xhtml" ? undefined : namespace)
		} finally {
			currentRedraw = prevRedraw
		}
		dom.vnodes = vnodes
		// `document.activeElement` can return null: https://html.spec.whatwg.org/multipage/interaction.html#dom-document-activeelement
		if (active != null && activeElement() !== active && typeof active.focus === "function") active.focus()
		for (var i = 0; i < hooks.length; i++) hooks[i]()
	}
}

},{"../render/vnode":"node_modules/mithril/render/vnode.js"}],"node_modules/mithril/render.js":[function(require,module,exports) {
"use strict"

module.exports = require("./render/render")(window)

},{"./render/render":"node_modules/mithril/render/render.js"}],"node_modules/mithril/api/mount-redraw.js":[function(require,module,exports) {
"use strict"

var Vnode = require("../render/vnode")

module.exports = function(render, schedule, console) {
	var subscriptions = []
	var rendering = false
	var pending = false

	function sync() {
		if (rendering) throw new Error("Nested m.redraw.sync() call")
		rendering = true
		for (var i = 0; i < subscriptions.length; i += 2) {
			try { render(subscriptions[i], Vnode(subscriptions[i + 1]), redraw) }
			catch (e) { console.error(e) }
		}
		rendering = false
	}

	function redraw() {
		if (!pending) {
			pending = true
			schedule(function() {
				pending = false
				sync()
			})
		}
	}

	redraw.sync = sync

	function mount(root, component) {
		if (component != null && component.view == null && typeof component !== "function") {
			throw new TypeError("m.mount(element, component) expects a component, not a vnode")
		}

		var index = subscriptions.indexOf(root)
		if (index >= 0) {
			subscriptions.splice(index, 2)
			render(root, [], redraw)
		}

		if (component != null) {
			subscriptions.push(root, component)
			render(root, Vnode(component), redraw)
		}
	}

	return {mount: mount, redraw: redraw}
}

},{"../render/vnode":"node_modules/mithril/render/vnode.js"}],"node_modules/mithril/mount-redraw.js":[function(require,module,exports) {
"use strict"

var render = require("./render")

module.exports = require("./api/mount-redraw")(render, requestAnimationFrame, console)

},{"./render":"node_modules/mithril/render.js","./api/mount-redraw":"node_modules/mithril/api/mount-redraw.js"}],"node_modules/mithril/querystring/build.js":[function(require,module,exports) {
"use strict"

module.exports = function(object) {
	if (Object.prototype.toString.call(object) !== "[object Object]") return ""

	var args = []
	for (var key in object) {
		destructure(key, object[key])
	}

	return args.join("&")

	function destructure(key, value) {
		if (Array.isArray(value)) {
			for (var i = 0; i < value.length; i++) {
				destructure(key + "[" + i + "]", value[i])
			}
		}
		else if (Object.prototype.toString.call(value) === "[object Object]") {
			for (var i in value) {
				destructure(key + "[" + i + "]", value[i])
			}
		}
		else args.push(encodeURIComponent(key) + (value != null && value !== "" ? "=" + encodeURIComponent(value) : ""))
	}
}

},{}],"node_modules/mithril/pathname/assign.js":[function(require,module,exports) {
"use strict"

module.exports = Object.assign || function(target, source) {
	if(source) Object.keys(source).forEach(function(key) { target[key] = source[key] })
}

},{}],"node_modules/mithril/pathname/build.js":[function(require,module,exports) {
"use strict"

var buildQueryString = require("../querystring/build")
var assign = require("./assign")

// Returns `path` from `template` + `params`
module.exports = function(template, params) {
	if ((/:([^\/\.-]+)(\.{3})?:/).test(template)) {
		throw new SyntaxError("Template parameter names *must* be separated")
	}
	if (params == null) return template
	var queryIndex = template.indexOf("?")
	var hashIndex = template.indexOf("#")
	var queryEnd = hashIndex < 0 ? template.length : hashIndex
	var pathEnd = queryIndex < 0 ? queryEnd : queryIndex
	var path = template.slice(0, pathEnd)
	var query = {}

	assign(query, params)

	var resolved = path.replace(/:([^\/\.-]+)(\.{3})?/g, function(m, key, variadic) {
		delete query[key]
		// If no such parameter exists, don't interpolate it.
		if (params[key] == null) return m
		// Escape normal parameters, but not variadic ones.
		return variadic ? params[key] : encodeURIComponent(String(params[key]))
	})

	// In case the template substitution adds new query/hash parameters.
	var newQueryIndex = resolved.indexOf("?")
	var newHashIndex = resolved.indexOf("#")
	var newQueryEnd = newHashIndex < 0 ? resolved.length : newHashIndex
	var newPathEnd = newQueryIndex < 0 ? newQueryEnd : newQueryIndex
	var result = resolved.slice(0, newPathEnd)

	if (queryIndex >= 0) result += template.slice(queryIndex, queryEnd)
	if (newQueryIndex >= 0) result += (queryIndex < 0 ? "?" : "&") + resolved.slice(newQueryIndex, newQueryEnd)
	var querystring = buildQueryString(query)
	if (querystring) result += (queryIndex < 0 && newQueryIndex < 0 ? "?" : "&") + querystring
	if (hashIndex >= 0) result += template.slice(hashIndex)
	if (newHashIndex >= 0) result += (hashIndex < 0 ? "" : "&") + resolved.slice(newHashIndex)
	return result
}

},{"../querystring/build":"node_modules/mithril/querystring/build.js","./assign":"node_modules/mithril/pathname/assign.js"}],"node_modules/mithril/request/request.js":[function(require,module,exports) {
"use strict"

var buildPathname = require("../pathname/build")

module.exports = function($window, Promise, oncompletion) {
	var callbackCount = 0

	function PromiseProxy(executor) {
		return new Promise(executor)
	}

	// In case the global Promise is some userland library's where they rely on
	// `foo instanceof this.constructor`, `this.constructor.resolve(value)`, or
	// similar. Let's *not* break them.
	PromiseProxy.prototype = Promise.prototype
	PromiseProxy.__proto__ = Promise // eslint-disable-line no-proto

	function makeRequest(factory) {
		return function(url, args) {
			if (typeof url !== "string") { args = url; url = url.url }
			else if (args == null) args = {}
			var promise = new Promise(function(resolve, reject) {
				factory(buildPathname(url, args.params), args, function (data) {
					if (typeof args.type === "function") {
						if (Array.isArray(data)) {
							for (var i = 0; i < data.length; i++) {
								data[i] = new args.type(data[i])
							}
						}
						else data = new args.type(data)
					}
					resolve(data)
				}, reject)
			})
			if (args.background === true) return promise
			var count = 0
			function complete() {
				if (--count === 0 && typeof oncompletion === "function") oncompletion()
			}

			return wrap(promise)

			function wrap(promise) {
				var then = promise.then
				// Set the constructor, so engines know to not await or resolve
				// this as a native promise. At the time of writing, this is
				// only necessary for V8, but their behavior is the correct
				// behavior per spec. See this spec issue for more details:
				// https://github.com/tc39/ecma262/issues/1577. Also, see the
				// corresponding comment in `request/tests/test-request.js` for
				// a bit more background on the issue at hand.
				promise.constructor = PromiseProxy
				promise.then = function() {
					count++
					var next = then.apply(promise, arguments)
					next.then(complete, function(e) {
						complete()
						if (count === 0) throw e
					})
					return wrap(next)
				}
				return promise
			}
		}
	}

	function hasHeader(args, name) {
		for (var key in args.headers) {
			if ({}.hasOwnProperty.call(args.headers, key) && name.test(key)) return true
		}
		return false
	}

	return {
		request: makeRequest(function(url, args, resolve, reject) {
			var method = args.method != null ? args.method.toUpperCase() : "GET"
			var body = args.body
			var assumeJSON = (args.serialize == null || args.serialize === JSON.serialize) && !(body instanceof $window.FormData)
			var responseType = args.responseType || (typeof args.extract === "function" ? "" : "json")

			var xhr = new $window.XMLHttpRequest(), aborted = false
			var original = xhr, replacedAbort
			var abort = xhr.abort

			xhr.abort = function() {
				aborted = true
				abort.call(this)
			}

			xhr.open(method, url, args.async !== false, typeof args.user === "string" ? args.user : undefined, typeof args.password === "string" ? args.password : undefined)

			if (assumeJSON && body != null && !hasHeader(args, /^content-type$/i)) {
				xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8")
			}
			if (typeof args.deserialize !== "function" && !hasHeader(args, /^accept$/i)) {
				xhr.setRequestHeader("Accept", "application/json, text/*")
			}
			if (args.withCredentials) xhr.withCredentials = args.withCredentials
			if (args.timeout) xhr.timeout = args.timeout
			xhr.responseType = responseType

			for (var key in args.headers) {
				if ({}.hasOwnProperty.call(args.headers, key)) {
					xhr.setRequestHeader(key, args.headers[key])
				}
			}

			xhr.onreadystatechange = function(ev) {
				// Don't throw errors on xhr.abort().
				if (aborted) return

				if (ev.target.readyState === 4) {
					try {
						var success = (ev.target.status >= 200 && ev.target.status < 300) || ev.target.status === 304 || (/^file:\/\//i).test(url)
						// When the response type isn't "" or "text",
						// `xhr.responseText` is the wrong thing to use.
						// Browsers do the right thing and throw here, and we
						// should honor that and do the right thing by
						// preferring `xhr.response` where possible/practical.
						var response = ev.target.response, message

						if (responseType === "json") {
							// For IE and Edge, which don't implement
							// `responseType: "json"`.
							if (!ev.target.responseType && typeof args.extract !== "function") response = JSON.parse(ev.target.responseText)
						} else if (!responseType || responseType === "text") {
							// Only use this default if it's text. If a parsed
							// document is needed on old IE and friends (all
							// unsupported), the user should use a custom
							// `config` instead. They're already using this at
							// their own risk.
							if (response == null) response = ev.target.responseText
						}

						if (typeof args.extract === "function") {
							response = args.extract(ev.target, args)
							success = true
						} else if (typeof args.deserialize === "function") {
							response = args.deserialize(response)
						}
						if (success) resolve(response)
						else {
							try { message = ev.target.responseText }
							catch (e) { message = response }
							var error = new Error(message)
							error.code = ev.target.status
							error.response = response
							reject(error)
						}
					}
					catch (e) {
						reject(e)
					}
				}
			}

			if (typeof args.config === "function") {
				xhr = args.config(xhr, args, url) || xhr

				// Propagate the `abort` to any replacement XHR as well.
				if (xhr !== original) {
					replacedAbort = xhr.abort
					xhr.abort = function() {
						aborted = true
						replacedAbort.call(this)
					}
				}
			}

			if (body == null) xhr.send()
			else if (typeof args.serialize === "function") xhr.send(args.serialize(body))
			else if (body instanceof $window.FormData) xhr.send(body)
			else xhr.send(JSON.stringify(body))
		}),
		jsonp: makeRequest(function(url, args, resolve, reject) {
			var callbackName = args.callbackName || "_mithril_" + Math.round(Math.random() * 1e16) + "_" + callbackCount++
			var script = $window.document.createElement("script")
			$window[callbackName] = function(data) {
				delete $window[callbackName]
				script.parentNode.removeChild(script)
				resolve(data)
			}
			script.onerror = function() {
				delete $window[callbackName]
				script.parentNode.removeChild(script)
				reject(new Error("JSONP request failed"))
			}
			script.src = url + (url.indexOf("?") < 0 ? "?" : "&") +
				encodeURIComponent(args.callbackKey || "callback") + "=" +
				encodeURIComponent(callbackName)
			$window.document.documentElement.appendChild(script)
		}),
	}
}

},{"../pathname/build":"node_modules/mithril/pathname/build.js"}],"node_modules/mithril/request.js":[function(require,module,exports) {
"use strict"

var PromisePolyfill = require("./promise/promise")
var mountRedraw = require("./mount-redraw")

module.exports = require("./request/request")(window, PromisePolyfill, mountRedraw.redraw)

},{"./promise/promise":"node_modules/mithril/promise/promise.js","./mount-redraw":"node_modules/mithril/mount-redraw.js","./request/request":"node_modules/mithril/request/request.js"}],"node_modules/mithril/querystring/parse.js":[function(require,module,exports) {
"use strict"

module.exports = function(string) {
	if (string === "" || string == null) return {}
	if (string.charAt(0) === "?") string = string.slice(1)

	var entries = string.split("&"), counters = {}, data = {}
	for (var i = 0; i < entries.length; i++) {
		var entry = entries[i].split("=")
		var key = decodeURIComponent(entry[0])
		var value = entry.length === 2 ? decodeURIComponent(entry[1]) : ""

		if (value === "true") value = true
		else if (value === "false") value = false

		var levels = key.split(/\]\[?|\[/)
		var cursor = data
		if (key.indexOf("[") > -1) levels.pop()
		for (var j = 0; j < levels.length; j++) {
			var level = levels[j], nextLevel = levels[j + 1]
			var isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10))
			if (level === "") {
				var key = levels.slice(0, j).join()
				if (counters[key] == null) {
					counters[key] = Array.isArray(cursor) ? cursor.length : 0
				}
				level = counters[key]++
			}
			// Disallow direct prototype pollution
			else if (level === "__proto__") break
			if (j === levels.length - 1) cursor[level] = value
			else {
				// Read own properties exclusively to disallow indirect
				// prototype pollution
				var desc = Object.getOwnPropertyDescriptor(cursor, level)
				if (desc != null) desc = desc.value
				if (desc == null) cursor[level] = desc = isNumber ? [] : {}
				cursor = desc
			}
		}
	}
	return data
}

},{}],"node_modules/mithril/pathname/parse.js":[function(require,module,exports) {
"use strict"

var parseQueryString = require("../querystring/parse")

// Returns `{path, params}` from `url`
module.exports = function(url) {
	var queryIndex = url.indexOf("?")
	var hashIndex = url.indexOf("#")
	var queryEnd = hashIndex < 0 ? url.length : hashIndex
	var pathEnd = queryIndex < 0 ? queryEnd : queryIndex
	var path = url.slice(0, pathEnd).replace(/\/{2,}/g, "/")

	if (!path) path = "/"
	else {
		if (path[0] !== "/") path = "/" + path
		if (path.length > 1 && path[path.length - 1] === "/") path = path.slice(0, -1)
	}
	return {
		path: path,
		params: queryIndex < 0
			? {}
			: parseQueryString(url.slice(queryIndex + 1, queryEnd)),
	}
}

},{"../querystring/parse":"node_modules/mithril/querystring/parse.js"}],"node_modules/mithril/pathname/compileTemplate.js":[function(require,module,exports) {
"use strict"

var parsePathname = require("./parse")

// Compiles a template into a function that takes a resolved path (without query
// strings) and returns an object containing the template parameters with their
// parsed values. This expects the input of the compiled template to be the
// output of `parsePathname`. Note that it does *not* remove query parameters
// specified in the template.
module.exports = function(template) {
	var templateData = parsePathname(template)
	var templateKeys = Object.keys(templateData.params)
	var keys = []
	var regexp = new RegExp("^" + templateData.path.replace(
		// I escape literal text so people can use things like `:file.:ext` or
		// `:lang-:locale` in routes. This is all merged into one pass so I
		// don't also accidentally escape `-` and make it harder to detect it to
		// ban it from template parameters.
		/:([^\/.-]+)(\.{3}|\.(?!\.)|-)?|[\\^$*+.()|\[\]{}]/g,
		function(m, key, extra) {
			if (key == null) return "\\" + m
			keys.push({k: key, r: extra === "..."})
			if (extra === "...") return "(.*)"
			if (extra === ".") return "([^/]+)\\."
			return "([^/]+)" + (extra || "")
		}
	) + "$")
	return function(data) {
		// First, check the params. Usually, there isn't any, and it's just
		// checking a static set.
		for (var i = 0; i < templateKeys.length; i++) {
			if (templateData.params[templateKeys[i]] !== data.params[templateKeys[i]]) return false
		}
		// If no interpolations exist, let's skip all the ceremony
		if (!keys.length) return regexp.test(data.path)
		var values = regexp.exec(data.path)
		if (values == null) return false
		for (var i = 0; i < keys.length; i++) {
			data.params[keys[i].k] = keys[i].r ? values[i + 1] : decodeURIComponent(values[i + 1])
		}
		return true
	}
}

},{"./parse":"node_modules/mithril/pathname/parse.js"}],"node_modules/mithril/api/router.js":[function(require,module,exports) {
"use strict"

var Vnode = require("../render/vnode")
var m = require("../render/hyperscript")
var Promise = require("../promise/promise")

var buildPathname = require("../pathname/build")
var parsePathname = require("../pathname/parse")
var compileTemplate = require("../pathname/compileTemplate")
var assign = require("../pathname/assign")

var sentinel = {}

module.exports = function($window, mountRedraw) {
	var fireAsync

	function setPath(path, data, options) {
		path = buildPathname(path, data)
		if (fireAsync != null) {
			fireAsync()
			var state = options ? options.state : null
			var title = options ? options.title : null
			if (options && options.replace) $window.history.replaceState(state, title, route.prefix + path)
			else $window.history.pushState(state, title, route.prefix + path)
		}
		else {
			$window.location.href = route.prefix + path
		}
	}

	var currentResolver = sentinel, component, attrs, currentPath, lastUpdate

	var SKIP = route.SKIP = {}

	function route(root, defaultRoute, routes) {
		if (root == null) throw new Error("Ensure the DOM element that was passed to `m.route` is not undefined")
		// 0 = start
		// 1 = init
		// 2 = ready
		var state = 0

		var compiled = Object.keys(routes).map(function(route) {
			if (route[0] !== "/") throw new SyntaxError("Routes must start with a `/`")
			if ((/:([^\/\.-]+)(\.{3})?:/).test(route)) {
				throw new SyntaxError("Route parameter names must be separated with either `/`, `.`, or `-`")
			}
			return {
				route: route,
				component: routes[route],
				check: compileTemplate(route),
			}
		})
		var callAsync = typeof setImmediate === "function" ? setImmediate : setTimeout
		var p = Promise.resolve()
		var scheduled = false
		var onremove

		fireAsync = null

		if (defaultRoute != null) {
			var defaultData = parsePathname(defaultRoute)

			if (!compiled.some(function (i) { return i.check(defaultData) })) {
				throw new ReferenceError("Default route doesn't match any known routes")
			}
		}

		function resolveRoute() {
			scheduled = false
			// Consider the pathname holistically. The prefix might even be invalid,
			// but that's not our problem.
			var prefix = $window.location.hash
			if (route.prefix[0] !== "#") {
				prefix = $window.location.search + prefix
				if (route.prefix[0] !== "?") {
					prefix = $window.location.pathname + prefix
					if (prefix[0] !== "/") prefix = "/" + prefix
				}
			}
			// This seemingly useless `.concat()` speeds up the tests quite a bit,
			// since the representation is consistently a relatively poorly
			// optimized cons string.
			var path = prefix.concat()
				.replace(/(?:%[a-f89][a-f0-9])+/gim, decodeURIComponent)
				.slice(route.prefix.length)
			var data = parsePathname(path)

			assign(data.params, $window.history.state)

			function fail() {
				if (path === defaultRoute) throw new Error("Could not resolve default route " + defaultRoute)
				setPath(defaultRoute, null, {replace: true})
			}

			loop(0)
			function loop(i) {
				// 0 = init
				// 1 = scheduled
				// 2 = done
				for (; i < compiled.length; i++) {
					if (compiled[i].check(data)) {
						var payload = compiled[i].component
						var matchedRoute = compiled[i].route
						var localComp = payload
						var update = lastUpdate = function(comp) {
							if (update !== lastUpdate) return
							if (comp === SKIP) return loop(i + 1)
							component = comp != null && (typeof comp.view === "function" || typeof comp === "function")? comp : "div"
							attrs = data.params, currentPath = path, lastUpdate = null
							currentResolver = payload.render ? payload : null
							if (state === 2) mountRedraw.redraw()
							else {
								state = 2
								mountRedraw.redraw.sync()
							}
						}
						// There's no understating how much I *wish* I could
						// use `async`/`await` here...
						if (payload.view || typeof payload === "function") {
							payload = {}
							update(localComp)
						}
						else if (payload.onmatch) {
							p.then(function () {
								return payload.onmatch(data.params, path, matchedRoute)
							}).then(update, fail)
						}
						else update("div")
						return
					}
				}
				fail()
			}
		}

		// Set it unconditionally so `m.route.set` and `m.route.Link` both work,
		// even if neither `pushState` nor `hashchange` are supported. It's
		// cleared if `hashchange` is used, since that makes it automatically
		// async.
		fireAsync = function() {
			if (!scheduled) {
				scheduled = true
				callAsync(resolveRoute)
			}
		}

		if (typeof $window.history.pushState === "function") {
			onremove = function() {
				$window.removeEventListener("popstate", fireAsync, false)
			}
			$window.addEventListener("popstate", fireAsync, false)
		} else if (route.prefix[0] === "#") {
			fireAsync = null
			onremove = function() {
				$window.removeEventListener("hashchange", resolveRoute, false)
			}
			$window.addEventListener("hashchange", resolveRoute, false)
		}

		return mountRedraw.mount(root, {
			onbeforeupdate: function() {
				state = state ? 2 : 1
				return !(!state || sentinel === currentResolver)
			},
			oncreate: resolveRoute,
			onremove: onremove,
			view: function() {
				if (!state || sentinel === currentResolver) return
				// Wrap in a fragment to preserve existing key semantics
				var vnode = [Vnode(component, attrs.key, attrs)]
				if (currentResolver) vnode = currentResolver.render(vnode[0])
				return vnode
			},
		})
	}
	route.set = function(path, data, options) {
		if (lastUpdate != null) {
			options = options || {}
			options.replace = true
		}
		lastUpdate = null
		setPath(path, data, options)
	}
	route.get = function() {return currentPath}
	route.prefix = "#!"
	route.Link = {
		view: function(vnode) {
			var options = vnode.attrs.options
			// Remove these so they don't get overwritten
			var attrs = {}, onclick, href
			assign(attrs, vnode.attrs)
			// The first two are internal, but the rest are magic attributes
			// that need censored to not screw up rendering.
			attrs.selector = attrs.options = attrs.key = attrs.oninit =
			attrs.oncreate = attrs.onbeforeupdate = attrs.onupdate =
			attrs.onbeforeremove = attrs.onremove = null

			// Do this now so we can get the most current `href` and `disabled`.
			// Those attributes may also be specified in the selector, and we
			// should honor that.
			var child = m(vnode.attrs.selector || "a", attrs, vnode.children)

			// Let's provide a *right* way to disable a route link, rather than
			// letting people screw up accessibility on accident.
			//
			// The attribute is coerced so users don't get surprised over
			// `disabled: 0` resulting in a button that's somehow routable
			// despite being visibly disabled.
			if (child.attrs.disabled = Boolean(child.attrs.disabled)) {
				child.attrs.href = null
				child.attrs["aria-disabled"] = "true"
				// If you *really* do want to do this on a disabled link, use
				// an `oncreate` hook to add it.
				child.attrs.onclick = null
			} else {
				onclick = child.attrs.onclick
				href = child.attrs.href
				child.attrs.href = route.prefix + href
				child.attrs.onclick = function(e) {
					var result
					if (typeof onclick === "function") {
						result = onclick.call(e.currentTarget, e)
					} else if (onclick == null || typeof onclick !== "object") {
						// do nothing
					} else if (typeof onclick.handleEvent === "function") {
						onclick.handleEvent(e)
					}

					// Adapted from React Router's implementation:
					// https://github.com/ReactTraining/react-router/blob/520a0acd48ae1b066eb0b07d6d4d1790a1d02482/packages/react-router-dom/modules/Link.js
					//
					// Try to be flexible and intuitive in how we handle links.
					// Fun fact: links aren't as obvious to get right as you
					// would expect. There's a lot more valid ways to click a
					// link than this, and one might want to not simply click a
					// link, but right click or command-click it to copy the
					// link target, etc. Nope, this isn't just for blind people.
					if (
						// Skip if `onclick` prevented default
						result !== false && !e.defaultPrevented &&
						// Ignore everything but left clicks
						(e.button === 0 || e.which === 0 || e.which === 1) &&
						// Let the browser handle `target=_blank`, etc.
						(!e.currentTarget.target || e.currentTarget.target === "_self") &&
						// No modifier keys
						!e.ctrlKey && !e.metaKey && !e.shiftKey && !e.altKey
					) {
						e.preventDefault()
						e.redraw = false
						route.set(href, null, options)
					}
				}
			}
			return child
		},
	}
	route.param = function(key) {
		return attrs && key != null ? attrs[key] : attrs
	}

	return route
}

},{"../render/vnode":"node_modules/mithril/render/vnode.js","../render/hyperscript":"node_modules/mithril/render/hyperscript.js","../promise/promise":"node_modules/mithril/promise/promise.js","../pathname/build":"node_modules/mithril/pathname/build.js","../pathname/parse":"node_modules/mithril/pathname/parse.js","../pathname/compileTemplate":"node_modules/mithril/pathname/compileTemplate.js","../pathname/assign":"node_modules/mithril/pathname/assign.js"}],"node_modules/mithril/route.js":[function(require,module,exports) {
"use strict"

var mountRedraw = require("./mount-redraw")

module.exports = require("./api/router")(window, mountRedraw)

},{"./mount-redraw":"node_modules/mithril/mount-redraw.js","./api/router":"node_modules/mithril/api/router.js"}],"node_modules/mithril/index.js":[function(require,module,exports) {
"use strict"

var hyperscript = require("./hyperscript")
var request = require("./request")
var mountRedraw = require("./mount-redraw")

var m = function m() { return hyperscript.apply(this, arguments) }
m.m = hyperscript
m.trust = hyperscript.trust
m.fragment = hyperscript.fragment
m.mount = mountRedraw.mount
m.route = require("./route")
m.render = require("./render")
m.redraw = mountRedraw.redraw
m.request = request.request
m.jsonp = request.jsonp
m.parseQueryString = require("./querystring/parse")
m.buildQueryString = require("./querystring/build")
m.parsePathname = require("./pathname/parse")
m.buildPathname = require("./pathname/build")
m.vnode = require("./render/vnode")
m.PromisePolyfill = require("./promise/polyfill")

module.exports = m

},{"./hyperscript":"node_modules/mithril/hyperscript.js","./request":"node_modules/mithril/request.js","./mount-redraw":"node_modules/mithril/mount-redraw.js","./route":"node_modules/mithril/route.js","./render":"node_modules/mithril/render.js","./querystring/parse":"node_modules/mithril/querystring/parse.js","./querystring/build":"node_modules/mithril/querystring/build.js","./pathname/parse":"node_modules/mithril/pathname/parse.js","./pathname/build":"node_modules/mithril/pathname/build.js","./render/vnode":"node_modules/mithril/render/vnode.js","./promise/polyfill":"node_modules/mithril/promise/polyfill.js"}],"node_modules/lodash.debounce/index.js":[function(require,module,exports) {
var global = arguments[3];
/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as the `TypeError` message for "Functions" methods. */
var FUNC_ERROR_TEXT = 'Expected a function';

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        result = wait - timeSinceLastCall;

    return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && objectToString.call(value) == symbolTag);
}

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = debounce;

},{}],"node_modules/file-saver/dist/FileSaver.min.js":[function(require,module,exports) {
var define;
var global = arguments[3];
(function(a,b){if("function"==typeof define&&define.amd)define([],b);else if("undefined"!=typeof exports)b();else{b(),a.FileSaver={exports:{}}.exports}})(this,function(){"use strict";function b(a,b){return"undefined"==typeof b?b={autoBom:!1}:"object"!=typeof b&&(console.warn("Deprecated: Expected third argument to be a object"),b={autoBom:!b}),b.autoBom&&/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(a.type)?new Blob(["\uFEFF",a],{type:a.type}):a}function c(a,b,c){var d=new XMLHttpRequest;d.open("GET",a),d.responseType="blob",d.onload=function(){g(d.response,b,c)},d.onerror=function(){console.error("could not download file")},d.send()}function d(a){var b=new XMLHttpRequest;b.open("HEAD",a,!1);try{b.send()}catch(a){}return 200<=b.status&&299>=b.status}function e(a){try{a.dispatchEvent(new MouseEvent("click"))}catch(c){var b=document.createEvent("MouseEvents");b.initMouseEvent("click",!0,!0,window,0,0,0,80,20,!1,!1,!1,!1,0,null),a.dispatchEvent(b)}}var f="object"==typeof window&&window.window===window?window:"object"==typeof self&&self.self===self?self:"object"==typeof global&&global.global===global?global:void 0,a=f.navigator&&/Macintosh/.test(navigator.userAgent)&&/AppleWebKit/.test(navigator.userAgent)&&!/Safari/.test(navigator.userAgent),g=f.saveAs||("object"!=typeof window||window!==f?function(){}:"download"in HTMLAnchorElement.prototype&&!a?function(b,g,h){var i=f.URL||f.webkitURL,j=document.createElement("a");g=g||b.name||"download",j.download=g,j.rel="noopener","string"==typeof b?(j.href=b,j.origin===location.origin?e(j):d(j.href)?c(b,g,h):e(j,j.target="_blank")):(j.href=i.createObjectURL(b),setTimeout(function(){i.revokeObjectURL(j.href)},4E4),setTimeout(function(){e(j)},0))}:"msSaveOrOpenBlob"in navigator?function(f,g,h){if(g=g||f.name||"download","string"!=typeof f)navigator.msSaveOrOpenBlob(b(f,h),g);else if(d(f))c(f,g,h);else{var i=document.createElement("a");i.href=f,i.target="_blank",setTimeout(function(){e(i)})}}:function(b,d,e,g){if(g=g||open("","_blank"),g&&(g.document.title=g.document.body.innerText="downloading..."),"string"==typeof b)return c(b,d,e);var h="application/octet-stream"===b.type,i=/constructor/i.test(f.HTMLElement)||f.safari,j=/CriOS\/[\d]+/.test(navigator.userAgent);if((j||h&&i||a)&&"undefined"!=typeof FileReader){var k=new FileReader;k.onloadend=function(){var a=k.result;a=j?a:a.replace(/^data:[^;]*;/,"data:attachment/file;"),g?g.location.href=a:location=a,g=null},k.readAsDataURL(b)}else{var l=f.URL||f.webkitURL,m=l.createObjectURL(b);g?g.location=m:location.href=m,g=null,setTimeout(function(){l.revokeObjectURL(m)},4E4)}});f.saveAs=g.saveAs=g,"undefined"!=typeof module&&(module.exports=g)});


},{}],"node_modules/remotestoragejs/release/remotestorage.js":[function(require,module,exports) {
var define;
/*! remotestorage.js 1.2.2, https://remotestorage.io, MIT licensed */
!function (e, t) {
  "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define("RemoteStorage", [], t) : "object" == typeof exports ? exports.RemoteStorage = t() : e.RemoteStorage = t();
}(this, function () {
  return function (e) {
    var t = {};

    function r(n) {
      if (t[n]) return t[n].exports;
      var o = t[n] = {
        i: n,
        l: !1,
        exports: {}
      };
      return e[n].call(o.exports, o, o.exports, r), o.l = !0, o.exports;
    }

    return r.m = e, r.c = t, r.d = function (e, t, n) {
      r.o(e, t) || Object.defineProperty(e, t, {
        enumerable: !0,
        get: n
      });
    }, r.r = function (e) {
      "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
        value: "Module"
      }), Object.defineProperty(e, "__esModule", {
        value: !0
      });
    }, r.t = function (e, t) {
      if (1 & t && (e = r(e)), 8 & t) return e;
      if (4 & t && "object" == typeof e && e && e.__esModule) return e;
      var n = Object.create(null);
      if (r.r(n), Object.defineProperty(n, "default", {
        enumerable: !0,
        value: e
      }), 2 & t && "string" != typeof e) for (var o in e) r.d(n, o, function (t) {
        return e[t];
      }.bind(null, o));
      return n;
    }, r.n = function (e) {
      var t = e && e.__esModule ? function () {
        return e.default;
      } : function () {
        return e;
      };
      return r.d(t, "a", t), t;
    }, r.o = function (e, t) {
      return Object.prototype.hasOwnProperty.call(e, t);
    }, r.p = "", r(r.s = 17);
  }([function (e, t, r) {
    (function (t, r) {
      function n(e) {
        return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
          return typeof e;
        } : function (e) {
          return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
        })(e);
      }

      var o = {
        logError: function (e) {
          "string" == typeof e ? console.error(e) : console.error(e.message, e.stack);
        },
        globalContext: "undefined" != typeof window ? window : "object" === ("undefined" == typeof self ? "undefined" : n(self)) ? self : t,
        getGlobalContext: function () {
          return "undefined" != typeof window ? window : "object" === ("undefined" == typeof self ? "undefined" : n(self)) ? self : t;
        },
        extend: function (e) {
          return Array.prototype.slice.call(arguments, 1).forEach(function (t) {
            for (var r in t) e[r] = t[r];
          }), e;
        },
        containingFolder: function (e) {
          if ("" === e) return "/";
          if (!e) throw "Path not given!";
          return e.replace(/\/+/g, "/").replace(/[^\/]+\/?$/, "");
        },
        isFolder: function (e) {
          return "/" === e.substr(-1);
        },
        isDocument: function (e) {
          return !o.isFolder(e);
        },
        baseName: function (e) {
          var t = e.split("/");
          return o.isFolder(e) ? t[t.length - 2] + "/" : t[t.length - 1];
        },
        cleanPath: function (e) {
          return e.replace(/\/+/g, "/").split("/").map(encodeURIComponent).join("/").replace(/'/g, "%27");
        },
        bindAll: function (e) {
          for (var t in this) "function" == typeof e[t] && (e[t] = e[t].bind(e));
        },
        equal: function (e, t, r) {
          var i;
          if (r = r || [], n(e) !== n(t)) return !1;
          if ("number" == typeof e || "boolean" == typeof e || "string" == typeof e) return e === t;
          if ("function" == typeof e) return e.toString() === t.toString();

          if (e instanceof ArrayBuffer && t instanceof ArrayBuffer && (e = new Uint8Array(e), t = new Uint8Array(t)), e instanceof Array) {
            if (e.length !== t.length) return !1;

            for (var s = 0, a = e.length; s < a; s++) if (!o.equal(e[s], t[s], r)) return !1;
          } else {
            for (i in e) if (e.hasOwnProperty(i) && !(i in t)) return !1;

            for (i in t) if (t.hasOwnProperty(i)) {
              if (!(i in e)) return !1;
              var u;

              if ("object" === n(t[i])) {
                if (r.indexOf(t[i]) >= 0) continue;
                (u = r.slice()).push(t[i]);
              }

              if (!o.equal(e[i], t[i], u)) return !1;
            }
          }

          return !0;
        },
        deepClone: function (e) {
          var t;
          return void 0 === e ? void 0 : (function e(t, r) {
            var o, i;
            if ("object" === n(t) && !Array.isArray(t) && null !== t) for (o in t) "object" === n(t[o]) && null !== t[o] && ("[object ArrayBuffer]" === t[o].toString() ? (r[o] = new ArrayBuffer(t[o].byteLength), i = new Int8Array(t[o]), new Int8Array(r[o]).set(i)) : e(t[o], r[o]));
          }(e, t = JSON.parse(JSON.stringify(e))), t);
        },
        pathsFromRoot: function (e) {
          for (var t = [e], r = e.replace(/\/$/, "").split("/"); r.length > 1;) r.pop(), t.push(r.join("/") + "/");

          return t;
        },
        localStorageAvailable: function () {
          var e = o.getGlobalContext();
          if (!("localStorage" in e)) return !1;

          try {
            return e.localStorage.setItem("rs-check", 1), e.localStorage.removeItem("rs-check"), !0;
          } catch (e) {
            return !1;
          }
        },
        getJSONFromLocalStorage: function (e) {
          var t = o.getGlobalContext();

          try {
            return JSON.parse(t.localStorage.getItem(e));
          } catch (e) {}
        },
        shouldBeTreatedAsBinary: function (e, t) {
          return t && t.match(/charset=binary/) || /[\x00-\x08\x0E-\x1F\uFFFD]/.test(e);
        },
        getTextFromArrayBuffer: function (e, n) {
          return new Promise(function (i) {
            if ("undefined" == typeof Blob) {
              var s = new r(new Uint8Array(e));
              i(s.toString(n));
            } else {
              var a;

              if (o.globalContext.BlobBuilder = o.globalContext.BlobBuilder || o.globalContext.WebKitBlobBuilder, void 0 !== o.globalContext.BlobBuilder) {
                var u = new t.BlobBuilder();
                u.append(e), a = u.getBlob();
              } else a = new Blob([e]);

              var c = new FileReader();
              "function" == typeof c.addEventListener ? c.addEventListener("loadend", function (e) {
                i(e.target.result);
              }) : c.onloadend = function (e) {
                i(e.target.result);
              }, c.readAsText(a, n);
            }
          });
        }
      };
      e.exports = o;
    }).call(this, r(10), r(18).Buffer);
  }, function (e, t, r) {
    var n = r(3);

    e.exports = function () {
      n.logging && console.log.apply(console, arguments);
    };
  }, function (e, t, r) {
    var n = r(1),
        o = {
      addEventListener: function (e, t) {
        if ("string" != typeof e) throw new Error("Argument eventName should be a string");
        if ("function" != typeof t) throw new Error("Argument handler should be a function");
        n("[Eventhandling] Adding event listener", e), this._validateEvent(e), this._handlers[e].push(t);
      },
      removeEventListener: function (e, t) {
        this._validateEvent(e);

        for (var r = this._handlers[e].length, n = 0; n < r; n++) if (this._handlers[e][n] === t) return void this._handlers[e].splice(n, 1);
      },
      _emit: function (e) {
        this._validateEvent(e);

        var t = Array.prototype.slice.call(arguments, 1);

        this._handlers[e].slice().forEach(function (e) {
          e.apply(this, t);
        });
      },
      _validateEvent: function (e) {
        if (!(e in this._handlers)) throw new Error("Unknown event: " + e);
      },
      _delegateEvent: function (e, t) {
        t.on(e, function (t) {
          this._emit(e, t);
        }.bind(this));
      },
      _addEvent: function (e) {
        this._handlers[e] = [];
      }
    };
    o.on = o.addEventListener, o.off = o.removeEventListener, e.exports = function (e) {
      var t = Array.prototype.slice.call(arguments, 1);

      for (var r in o) e[r] = o[r];

      e._handlers = {}, t.forEach(function (t) {
        e._addEvent(t);
      });
    };
  }, function (e, t) {
    var r = {
      cache: !0,
      changeEvents: {
        local: !0,
        window: !1,
        remote: !0,
        conflict: !0
      },
      cordovaRedirectUri: void 0,
      logging: !1,
      modules: [],
      backgroundSyncInterval: 6e4,
      disableFeatures: [],
      discoveryTimeout: 1e4,
      isBackground: !1,
      requestTimeout: 3e4,
      syncInterval: 1e4
    };
    e.exports = r;
  }, function (e, t, r) {
    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o = r(1),
        i = r(0);

    function s(e) {
      var t,
          r = e || u.getLocation().href,
          n = r.indexOf("#");
      if (-1 !== n && -1 !== (t = r.substring(n + 1)).indexOf("=")) return t.split("&").reduce(function (e, t) {
        var r = t.split("=");

        if ("state" === r[0] && r[1].match(/rsDiscovery/)) {
          var n = decodeURIComponent(r[1]),
              o = n.substr(n.indexOf("rsDiscovery=")).split("&")[0].split("=")[1];
          e.rsDiscovery = JSON.parse(atob(o)), (n = n.replace(new RegExp("&?rsDiscovery=" + o), "")).length > 0 && (e.state = n);
        } else e[decodeURIComponent(r[0])] = decodeURIComponent(r[1]);

        return e;
      }, {});
    }

    var a,
        u = function e(t, r) {
      var n = r.authURL,
          s = r.scope,
          a = r.redirectUri,
          u = r.clientId;

      if (o("[Authorize] authURL = ", n, "scope = ", s, "redirectUri = ", a, "clientId = ", u), !i.localStorageAvailable() && "remotestorage" === t.backend) {
        a += a.indexOf("#") > 0 ? "&" : "#";
        var c = {
          userAddress: t.remote.userAddress,
          href: t.remote.href,
          storageApi: t.remote.storageApi,
          properties: t.remote.properties
        };
        a += "rsDiscovery=" + btoa(JSON.stringify(c));
      }

      var l = function (e, t, r, n) {
        var o = t.indexOf("#"),
            i = e;
        return i += e.indexOf("?") > 0 ? "&" : "?", i += "redirect_uri=" + encodeURIComponent(t.replace(/#.*$/, "")), i += "&scope=" + encodeURIComponent(r), i += "&client_id=" + encodeURIComponent(n), -1 !== o && o + 1 !== t.length && (i += "&state=" + encodeURIComponent(t.substring(o + 1))), i += "&response_type=token";
      }(n, a, s, u);

      if (i.globalContext.cordova) return e.openWindow(l, a, "location=yes,clearsessioncache=yes,clearcache=yes").then(function (e) {
        t.remote.configure({
          token: e.access_token
        });
      });
      e.setLocation(l);
    };

    u.IMPLIED_FAKE_TOKEN = !1, u.Unauthorized = function (e) {
      var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
      this.name = "Unauthorized", this.message = void 0 === e ? "App authorization expired or revoked." : e, void 0 !== t.code && (this.code = t.code), this.stack = new Error().stack;
    }, u.Unauthorized.prototype = Object.create(Error.prototype), u.Unauthorized.prototype.constructor = u.Unauthorized, u.getLocation = function () {
      return document.location;
    }, u.setLocation = function (e) {
      if ("string" == typeof e) document.location.href = e;else {
        if ("object" !== n(e)) throw "Invalid location " + e;
        document.location = e;
      }
    }, u.openWindow = function (e, t, r) {
      return new Promise(function (n, o) {
        var i = open(e, "_blank", r);
        if (!i || i.closed) return o("Authorization popup was blocked");

        var a = function () {
          return o("Authorization was canceled");
        };

        i.addEventListener("loadstart", function (e) {
          if (0 === e.url.indexOf(t)) {
            i.removeEventListener("exit", a), i.close();
            var r = s(e.url);
            return r ? n(r) : o("Authorization error");
          }
        }), i.addEventListener("exit", a);
      });
    }, u._rs_supported = function () {
      return "undefined" != typeof document;
    }, u._rs_init = function (e) {
      a = function () {
        var n = !1;

        if (r) {
          if (r.error) throw "access_denied" === r.error ? new u.Unauthorized("Authorization failed: access denied", {
            code: "access_denied"
          }) : new u.Unauthorized("Authorization failed: ".concat(r.error));
          r.rsDiscovery && e.remote.configure(r.rsDiscovery), r.access_token && (e.remote.configure({
            token: r.access_token
          }), n = !0), r.remotestorage && (e.connect(r.remotestorage), n = !0), r.state && (t = u.getLocation(), u.setLocation(t.href.split("#")[0] + "#" + r.state));
        }

        n || e.remote.stopWaitingForToken();
      };

      var t,
          r = s();
      r && ((t = u.getLocation()).hash = ""), e.on("features-loaded", a);
    }, u._rs_cleanup = function (e) {
      e.removeEventListener("features-loaded", a);
    }, e.exports = u;
  }, function (e, t, r) {
    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o = r(2),
        i = r(0),
        s = r(3),
        a = r(22),
        u = r(23),
        c = u.SchemaNotFound,
        l = function (e, t) {
      if ("/" !== t[t.length - 1]) throw "Not a folder: " + t;
      "/" === t && (this.makePath = function (e) {
        return ("/" === e[0] ? "" : "/") + e;
      }), this.storage = e, this.base = t;
      var r = this.base.split("/");
      r.length > 2 ? this.moduleName = r[1] : this.moduleName = "root", o(this, "change"), this.on = this.on.bind(this), e.onChange(this.base, this._fireChange.bind(this));
    };

    l.Types = u, l.prototype = {
      scope: function (e) {
        return new l(this.storage, this.makePath(e));
      },
      getListing: function (e, t) {
        if ("string" != typeof e) e = "";else if (e.length > 0 && "/" !== e[e.length - 1]) return Promise.reject("Not a folder: " + e);
        return this.storage.get(this.makePath(e), t).then(function (e) {
          return 404 === e.statusCode ? {} : e.body;
        });
      },
      getAll: function (e, t) {
        if ("string" != typeof e) e = "";else if (e.length > 0 && "/" !== e[e.length - 1]) return Promise.reject("Not a folder: " + e);
        return this.storage.get(this.makePath(e), t).then(function (r) {
          if (404 === r.statusCode) return {};

          if ("object" === n(r.body)) {
            var o = Object.keys(r.body);
            if (0 === o.length) return {};
            var i = o.map(function (o) {
              return this.storage.get(this.makePath(e + o), t).then(function (e) {
                if ("string" == typeof e.body) try {
                  e.body = JSON.parse(e.body);
                } catch (e) {}
                "object" === n(e.body) && (r.body[o] = e.body);
              });
            }.bind(this));
            return Promise.all(i).then(function () {
              return r.body;
            });
          }
        }.bind(this));
      },
      getFile: function (e, t) {
        return "string" != typeof e ? Promise.reject("Argument 'path' of baseClient.getFile must be a string") : this.storage.get(this.makePath(e), t).then(function (e) {
          return {
            data: e.body,
            contentType: e.contentType,
            revision: e.revision
          };
        });
      },
      storeFile: function (e, t, r) {
        return "string" != typeof e ? Promise.reject("Argument 'mimeType' of baseClient.storeFile must be a string") : "string" != typeof t ? Promise.reject("Argument 'path' of baseClient.storeFile must be a string") : "string" != typeof r && "object" !== n(r) ? Promise.reject("Argument 'body' of baseClient.storeFile must be a string, ArrayBuffer, or ArrayBufferView") : (this.storage.access.checkPathPermission(this.makePath(t), "rw") || console.warn("WARNING: Editing a document to which only read access ('r') was claimed"), this.storage.put(this.makePath(t), r, e).then(function (e) {
          return 200 === e.statusCode || 201 === e.statusCode ? e.revision : Promise.reject("Request (PUT " + this.makePath(t) + ") failed with status: " + e.statusCode);
        }.bind(this)));
      },
      getObject: function (e, t) {
        return "string" != typeof e ? Promise.reject("Argument 'path' of baseClient.getObject must be a string") : this.storage.get(this.makePath(e), t).then(function (t) {
          if ("object" === n(t.body)) return t.body;
          if ("string" == typeof t.body) try {
            return JSON.parse(t.body);
          } catch (t) {
            throw "Not valid JSON: " + this.makePath(e);
          } else if (void 0 !== t.body && 200 === t.statusCode) return Promise.reject("Not an object: " + this.makePath(e));
        }.bind(this));
      },
      storeObject: function (e, t, r) {
        if ("string" != typeof e) return Promise.reject("Argument 'typeAlias' of baseClient.storeObject must be a string");
        if ("string" != typeof t) return Promise.reject("Argument 'path' of baseClient.storeObject must be a string");
        if ("object" !== n(r)) return Promise.reject("Argument 'object' of baseClient.storeObject must be an object");

        this._attachType(r, e);

        try {
          var o = this.validate(r);
          if (!o.valid) return Promise.reject(o);
        } catch (e) {
          return Promise.reject(e);
        }

        return this.storage.put(this.makePath(t), JSON.stringify(r), "application/json; charset=UTF-8").then(function (e) {
          return 200 === e.statusCode || 201 === e.statusCode ? e.revision : Promise.reject("Request (PUT " + this.makePath(t) + ") failed with status: " + e.statusCode);
        }.bind(this));
      },
      remove: function (e) {
        return "string" != typeof e ? Promise.reject("Argument 'path' of baseClient.remove must be a string") : (this.storage.access.checkPathPermission(this.makePath(e), "rw") || console.warn("WARNING: Removing a document to which only read access ('r') was claimed"), this.storage.delete(this.makePath(e)));
      },
      getItemURL: function (e) {
        if ("string" != typeof e) throw "Argument 'path' of baseClient.getItemURL must be a string";
        return this.storage.connected ? (e = this._cleanPath(this.makePath(e)), this.storage.remote.href + e) : void 0;
      },
      cache: function (e, t) {
        if ("string" != typeof e) throw "Argument 'path' of baseClient.cache must be a string";
        if (void 0 === t) t = "ALL";else if ("string" != typeof t) throw "Argument 'strategy' of baseClient.cache must be a string or undefined";
        if ("FLUSH" !== t && "SEEN" !== t && "ALL" !== t) throw 'Argument \'strategy\' of baseclient.cache must be one of ["FLUSH", "SEEN", "ALL"]';
        return this.storage.caching.set(this.makePath(e), t), this;
      },
      flush: function (e) {
        return this.storage.local.flush(e);
      },
      declareType: function (e, t, r) {
        r || (r = t, t = this._defaultTypeURI(e)), l.Types.declare(this.moduleName, e, t, r);
      },
      validate: function (e) {
        var t = l.Types.getSchema(e["@context"]);
        if (t) return a.validateResult(e, t);
        throw new c(e["@context"]);
      },
      schemas: {
        configurable: !0,
        get: function () {
          return l.Types.inScope(this.moduleName);
        }
      },
      _defaultTypeURI: function (e) {
        return "http://remotestorage.io/spec/modules/" + encodeURIComponent(this.moduleName) + "/" + encodeURIComponent(e);
      },
      _attachType: function (e, t) {
        e["@context"] = l.Types.resolveAlias(this.moduleName + "/" + t) || this._defaultTypeURI(t);
      },
      makePath: function (e) {
        return this.base + (e || "");
      },
      _fireChange: function (e) {
        s.changeEvents[e.origin] && (["new", "old", "lastCommon"].forEach(function (t) {
          if ((!e[t + "ContentType"] || /^application\/(.*)json(.*)/.exec(e[t + "ContentType"])) && "string" == typeof e[t + "Value"]) try {
            e[t + "Value"] = JSON.parse(e[t + "Value"]);
          } catch (e) {}
        }), this._emit("change", e));
      },
      _cleanPath: i.cleanPath
    }, l._rs_init = function () {}, e.exports = l;
  }, function (e, t, r) {
    "use strict";

    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o,
        i,
        s = r(1),
        a = r(0),
        u = r(2),
        c = r(4),
        l = r(3),
        h = "remotestorage:wireclient",
        f = {
      "draft-dejong-remotestorage-00": 2,
      "draft-dejong-remotestorage-01": 3,
      "draft-dejong-remotestorage-02": 4,
      "https://www.w3.org/community/rww/wiki/read-write-web-00#simple": 1
    };
    if ("function" == typeof ArrayBufferView) i = function (e) {
      return e && e instanceof ArrayBufferView;
    };else {
      var d = [Int8Array, Uint8Array, Int16Array, Uint16Array, Int32Array, Uint32Array, Float32Array, Float64Array];

      i = function (e) {
        for (var t = 0; t < 8; t++) if (e instanceof d[t]) return !0;

        return !1;
      };
    }
    var p = a.isFolder,
        m = a.cleanPath,
        y = a.shouldBeTreatedAsBinary,
        g = a.getJSONFromLocalStorage,
        v = a.getTextFromArrayBuffer;

    function b(e) {
      return "string" != typeof e ? e : "*" === e ? "*" : '"' + e + '"';
    }

    function _(e) {
      return "string" != typeof e ? e : e.replace(/^["']|["']$/g, "");
    }

    var w = function (e) {
      if (this.rs = e, this.connected = !1, u(this, "connected", "not-connected"), o) {
        var t = g(h);
        t && setTimeout(function () {
          this.configure(t);
        }.bind(this), 0);
      }

      this._revisionCache = {}, this.connected && setTimeout(this._emit.bind(this), 0, "connected");
    };

    w.prototype = {
      _request: function (e, t, r, n, o, i, a) {
        if (("PUT" === e || "DELETE" === e) && "/" === t[t.length - 1]) return Promise.reject("Don't " + e + " on directories!");
        var u,
            l = this;
        return r !== c.IMPLIED_FAKE_TOKEN && (n.Authorization = "Bearer " + r), this.rs._emit("wire-busy", {
          method: e,
          isFolder: p(t)
        }), w.request(e, t, {
          body: o,
          headers: n,
          responseType: "arraybuffer"
        }).then(function (r) {
          if (l.online || (l.online = !0, l.rs._emit("network-online")), l.rs._emit("wire-done", {
            method: e,
            isFolder: p(t),
            success: !0
          }), o = r.status, [401, 403, 404, 412].indexOf(o) >= 0) return s("[WireClient] Error response status", r.status), u = i ? _(r.getResponseHeader("ETag")) : void 0, 401 === r.status && l.rs._emit("error", new c.Unauthorized()), Promise.resolve({
            statusCode: r.status,
            revision: u
          });
          if (function (e) {
            return [201, 204, 304].indexOf(e) >= 0;
          }(r.status) || 200 === r.status && "GET" !== e) return u = _(r.getResponseHeader("ETag")), s("[WireClient] Successful request", u), Promise.resolve({
            statusCode: r.status,
            revision: u
          });
          var n = r.getResponseHeader("Content-Type");
          u = i ? _(r.getResponseHeader("ETag")) : 200 === r.status ? a : void 0;

          var o,
              h = function (e) {
            var t,
                r = "UTF-8";
            return e && (t = e.match(/charset=(.+)$/)) && (r = t[1]), r;
          }(n);

          return y(r.response, n) ? (s("[WireClient] Successful request with unknown or binary mime-type", u), Promise.resolve({
            statusCode: r.status,
            body: r.response,
            contentType: n,
            revision: u
          })) : v(r.response, h).then(function (e) {
            return s("[WireClient] Successful request", u), Promise.resolve({
              statusCode: r.status,
              body: e,
              contentType: n,
              revision: u
            });
          });
        }, function (r) {
          return l.online && (l.online = !1, l.rs._emit("network-offline")), l.rs._emit("wire-done", {
            method: e,
            isFolder: p(t),
            success: !1
          }), Promise.reject(r);
        });
      },
      configure: function (e) {
        if ("object" !== n(e)) throw new Error("WireClient configure settings parameter should be an object");
        void 0 !== e.userAddress && (this.userAddress = e.userAddress), void 0 !== e.href && (this.href = e.href), void 0 !== e.storageApi && (this.storageApi = e.storageApi), void 0 !== e.token && (this.token = e.token), void 0 !== e.properties && (this.properties = e.properties), void 0 !== this.storageApi && (this._storageApi = f[this.storageApi] || 5, this.supportsRevs = this._storageApi >= 2), this.href && this.token ? (this.connected = !0, this.online = !0, this._emit("connected")) : this.connected = !1, o && (localStorage[h] = JSON.stringify({
          userAddress: this.userAddress,
          href: this.href,
          storageApi: this.storageApi,
          token: this.token,
          properties: this.properties
        }));
      },
      stopWaitingForToken: function () {
        this.connected || this._emit("not-connected");
      },
      get: function (e, t) {
        var r = this;
        if (!this.connected) return Promise.reject("not connected (path: " + e + ")");
        t || (t = {});
        var o = {};
        return this.supportsRevs && t.ifNoneMatch && (o["If-None-Match"] = b(t.ifNoneMatch)), this._request("GET", this.href + m(e), this.token, o, void 0, this.supportsRevs, this._revisionCache[e]).then(function (t) {
          if (!p(e)) return Promise.resolve(t);
          var o,
              i = {};
          if (void 0 !== t.body) try {
            t.body = JSON.parse(t.body);
          } catch (t) {
            return Promise.reject("Folder description at " + r.href + m(e) + " is not JSON");
          }

          if (200 === t.statusCode && "object" === n(t.body)) {
            if (0 === Object.keys(t.body).length) t.statusCode = 404;else if ("http://remotestorage.io/spec/folder-description" === (o = t.body)["@context"] && "object" === n(o.items)) {
              for (var s in t.body.items) r._revisionCache[e + s] = t.body.items[s].ETag;

              i = t.body.items;
            } else Object.keys(t.body).forEach(function (n) {
              r._revisionCache[e + n] = t.body[n], i[n] = {
                ETag: t.body[n]
              };
            });
            return t.body = i, Promise.resolve(t);
          }

          return Promise.resolve(t);
        });
      },
      put: function (e, t, r, n) {
        if (!this.connected) return Promise.reject("not connected (path: " + e + ")");
        n || (n = {}), !r.match(/charset=/) && (t instanceof ArrayBuffer || i(t)) && (r += "; charset=binary");
        var o = {
          "Content-Type": r
        };
        return this.supportsRevs && (n.ifMatch && (o["If-Match"] = b(n.ifMatch)), n.ifNoneMatch && (o["If-None-Match"] = b(n.ifNoneMatch))), this._request("PUT", this.href + m(e), this.token, o, t, this.supportsRevs);
      },
      delete: function (e, t) {
        if (!this.connected) throw new Error("not connected (path: " + e + ")");
        t || (t = {});
        var r = {};
        return this.supportsRevs && t.ifMatch && (r["If-Match"] = b(t.ifMatch)), this._request("DELETE", this.href + m(e), this.token, r, void 0, this.supportsRevs);
      }
    }, w.isArrayBufferView = i, w.request = function (e, t, r) {
      return "function" == typeof fetch ? w._fetchRequest(e, t, r) : "function" == typeof XMLHttpRequest ? w._xhrRequest(e, t, r) : (s("[WireClient] add a polyfill for fetch or XMLHttpRequest"), Promise.reject("[WireClient] add a polyfill for fetch or XMLHttpRequest"));
    }, w._fetchRequest = function (e, t, r) {
      var n,
          o,
          i = {};
      "function" == typeof AbortController && (o = new AbortController());
      var a = fetch(t, {
        method: e,
        headers: r.headers,
        body: r.body,
        signal: o ? o.signal : void 0
      }).then(function (e) {
        switch (s("[WireClient fetch]", e), e.headers.forEach(function (e, t) {
          i[t.toUpperCase()] = e;
        }), n = {
          readyState: 4,
          status: e.status,
          statusText: e.statusText,
          response: void 0,
          getResponseHeader: function (e) {
            return i[e.toUpperCase()] || null;
          },
          responseType: r.responseType,
          responseURL: t
        }, r.responseType) {
          case "arraybuffer":
            return e.arrayBuffer();

          case "blob":
            return e.blob();

          case "json":
            return e.json();

          case void 0:
          case "":
          case "text":
            return e.text();

          default:
            throw new Error("responseType 'document' is not currently supported using fetch");
        }
      }).then(function (e) {
        return n.response = e, r.responseType && "text" !== r.responseType || (n.responseText = e), n;
      }),
          u = new Promise(function (e, t) {
        setTimeout(function () {
          t("timeout"), o && o.abort();
        }, l.requestTimeout);
      });
      return Promise.race([a, u]);
    }, w._xhrRequest = function (e, t, r) {
      return new Promise(function (o, a) {
        s("[WireClient]", e, t);
        var u = !1,
            c = setTimeout(function () {
          u = !0, a("timeout");
        }, l.requestTimeout),
            h = new XMLHttpRequest();
        if (h.open(e, t, !0), r.responseType && (h.responseType = r.responseType), r.headers) for (var f in r.headers) h.setRequestHeader(f, r.headers[f]);
        h.onload = function () {
          u || (clearTimeout(c), o(h));
        }, h.onerror = function (e) {
          u || (clearTimeout(c), a(e));
        };
        var d = r.body;
        "object" === n(d) && !i(d) && d instanceof ArrayBuffer && (d = new Uint8Array(d)), h.send(d);
      });
    }, Object.defineProperty(w.prototype, "storageType", {
      get: function () {
        if (this.storageApi) {
          var e = this.storageApi.match(/draft-dejong-(remotestorage-\d\d)/);
          return e ? e[1] : "2012.04";
        }
      }
    }), w._rs_init = function (e) {
      o = a.localStorageAvailable(), e.remote = new w(e), this.online = !0;
    }, w._rs_supported = function () {
      return "function" == typeof fetch || "function" == typeof XMLHttpRequest;
    }, w._rs_cleanup = function () {
      o && delete localStorage[h];
    }, e.exports = w;
  }, function (e, t, r) {
    function n(e, t) {
      return !t || "object" !== u(t) && "function" != typeof t ? function (e) {
        if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return e;
      }(e) : t;
    }

    function o(e) {
      var t = "function" == typeof Map ? new Map() : void 0;
      return (o = function (e) {
        if (null === e || (r = e, -1 === Function.toString.call(r).indexOf("[native code]"))) return e;
        var r;
        if ("function" != typeof e) throw new TypeError("Super expression must either be null or a function");

        if (void 0 !== t) {
          if (t.has(e)) return t.get(e);
          t.set(e, n);
        }

        function n() {
          return i(e, arguments, a(this).constructor);
        }

        return n.prototype = Object.create(e.prototype, {
          constructor: {
            value: n,
            enumerable: !1,
            writable: !0,
            configurable: !0
          }
        }), s(n, e);
      })(e);
    }

    function i(e, t, r) {
      return (i = function () {
        if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
        if (Reflect.construct.sham) return !1;
        if ("function" == typeof Proxy) return !0;

        try {
          return Date.prototype.toString.call(Reflect.construct(Date, [], function () {})), !0;
        } catch (e) {
          return !1;
        }
      }() ? Reflect.construct : function (e, t, r) {
        var n = [null];
        n.push.apply(n, t);
        var o = new (Function.bind.apply(e, n))();
        return r && s(o, r.prototype), o;
      }).apply(null, arguments);
    }

    function s(e, t) {
      return (s = Object.setPrototypeOf || function (e, t) {
        return e.__proto__ = t, e;
      })(e, t);
    }

    function a(e) {
      return (a = Object.setPrototypeOf ? Object.getPrototypeOf : function (e) {
        return e.__proto__ || Object.getPrototypeOf(e);
      })(e);
    }

    function u(e) {
      return (u = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    function c(e, t) {
      if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
    }

    function l(e, t) {
      for (var r = 0; r < t.length; r++) {
        var n = t[r];
        n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(e, n.key, n);
      }
    }

    var h,
        f,
        d = r(0),
        p = d.isFolder,
        m = d.isDocument,
        y = d.equal,
        g = d.deepClone,
        v = d.pathsFromRoot,
        b = r(12),
        _ = r(2),
        w = r(1),
        P = r(4),
        E = r(3);

    function S(e, t, r) {
      return {
        action: e,
        path: t,
        promise: r
      };
    }

    function T(e, t) {
      return e.common.revision !== t && (!e.remote || e.remote.revision !== t);
    }

    function A(e) {
      return e.common && e.common.revision;
    }

    var R = function () {
      function e(t) {
        var r = this;
        c(this, e), this.rs = t, this._tasks = {}, this._running = {}, this._timeStarted = {}, this.numThreads = 10, this.rs.local.onDiff(function (e) {
          r.addTask(e), r.doTasks();
        }), this.rs.caching.onActivate(function (e) {
          r.addTask(e), r.doTasks();
        }), _(this, "done", "req-done");
      }

      var t, r, n;
      return t = e, n = [{
        key: "_rs_init",
        value: function (t) {
          h = function () {
            w("[Sync] syncCycleCb calling syncCycle"), b.isBrowser() && function (e) {
              function t(t) {
                var r, n;
                r = e.getCurrentSyncInterval(), E.isBackground = !t, n = e.getCurrentSyncInterval(), e._emit("sync-interval-change", {
                  oldValue: r,
                  newValue: n
                });
              }

              b.on("background", function () {
                return t(!1);
              }), b.on("foreground", function () {
                return t(!0);
              });
            }(t), t.sync || (t.sync = new e(t), t.syncStopped && (w("[Sync] Instantiating sync stopped"), t.sync.stopped = !0, delete t.syncStopped)), w("[Sync] syncCycleCb calling syncCycle"), t.syncCycle();
          }, f = function () {
            t.removeEventListener("connected", f), t.startSync();
          }, t.on("ready", h), t.on("connected", f);
        }
      }, {
        key: "_rs_cleanup",
        value: function (e) {
          e.stopSync(), e.removeEventListener("ready", h), e.removeEventListener("connected", f), e.sync = void 0, delete e.sync;
        }
      }], (r = [{
        key: "now",
        value: function () {
          return new Date().getTime();
        }
      }, {
        key: "queueGetRequest",
        value: function (e) {
          var t = this;
          return new Promise(function (r, n) {
            t.rs.remote.connected ? t.rs.remote.online ? (t.addTask(e, function () {
              this.rs.local.get(e).then(function (e) {
                return r(e);
              });
            }.bind(t)), t.doTasks()) : n("cannot fulfill maxAge requirement - remote is not online") : n("cannot fulfill maxAge requirement - remote is not connected");
          });
        }
      }, {
        key: "corruptServerItemsMap",
        value: function (e, t) {
          if ("object" !== u(e) || Array.isArray(e)) return !0;

          for (var r in e) {
            var n = e[r];
            if ("object" !== u(n)) return !0;
            if ("string" != typeof n.ETag) return !0;

            if (p(r)) {
              if (-1 !== r.substring(0, r.length - 1).indexOf("/")) return !0;
            } else {
              if (-1 !== r.indexOf("/")) return !0;

              if (t) {
                if ("string" != typeof n["Content-Type"]) return !0;
                if ("number" != typeof n["Content-Length"]) return !0;
              }
            }
          }

          return !1;
        }
      }, {
        key: "corruptItemsMap",
        value: function (e) {
          if ("object" !== u(e) || Array.isArray(e)) return !0;

          for (var t in e) if ("boolean" != typeof e[t]) return !0;

          return !1;
        }
      }, {
        key: "corruptRevision",
        value: function (e) {
          return "object" !== u(e) || Array.isArray(e) || e.revision && "string" != typeof e.revision || e.body && "string" != typeof e.body && "object" !== u(e.body) || e.contentType && "string" != typeof e.contentType || e.contentLength && "number" != typeof e.contentLength || e.timestamp && "number" != typeof e.timestamp || e.itemsMap && this.corruptItemsMap(e.itemsMap);
        }
      }, {
        key: "isCorrupt",
        value: function (e) {
          return "object" !== u(e) || Array.isArray(e) || "string" != typeof e.path || this.corruptRevision(e.common) || e.local && this.corruptRevision(e.local) || e.remote && this.corruptRevision(e.remote) || e.push && this.corruptRevision(e.push);
        }
      }, {
        key: "hasTasks",
        value: function () {
          return Object.getOwnPropertyNames(this._tasks).length > 0;
        }
      }, {
        key: "collectDiffTasks",
        value: function () {
          var e = this,
              t = 0;
          return this.rs.local.forAllNodes(function (r) {
            t > 100 || (e.isCorrupt(r) ? (w("[Sync] WARNING: corrupt node in local cache", r), "object" === u(r) && r.path && (e.addTask(r.path), t++)) : e.needsFetch(r) && e.rs.access.checkPathPermission(r.path, "r") ? (e.addTask(r.path), t++) : m(r.path) && e.needsPush(r) && e.rs.access.checkPathPermission(r.path, "rw") && (e.addTask(r.path), t++));
          }).then(function () {
            return t;
          }, function (e) {
            throw e;
          });
        }
      }, {
        key: "inConflict",
        value: function (e) {
          return e.local && e.remote && (void 0 !== e.remote.body || e.remote.itemsMap);
        }
      }, {
        key: "needsRefresh",
        value: function (e) {
          return !!e.common && (!e.common.timestamp || this.now() - e.common.timestamp > E.syncInterval);
        }
      }, {
        key: "needsFetch",
        value: function (e) {
          return !!this.inConflict(e) || !(!e.common || void 0 !== e.common.itemsMap || void 0 !== e.common.body) || !(!e.remote || void 0 !== e.remote.itemsMap || void 0 !== e.remote.body);
        }
      }, {
        key: "needsPush",
        value: function (e) {
          return !this.inConflict(e) && (!(!e.local || e.push) || void 0);
        }
      }, {
        key: "needsRemotePut",
        value: function (e) {
          return e.local && e.local.body;
        }
      }, {
        key: "needsRemoteDelete",
        value: function (e) {
          return e.local && !1 === e.local.body;
        }
      }, {
        key: "getParentPath",
        value: function (e) {
          var t = e.match(/^(.*\/)([^\/]+\/?)$/);
          if (t) return t[1];
          throw new Error('Not a valid path: "' + e + '"');
        }
      }, {
        key: "deleteChildPathsFromTasks",
        value: function () {
          for (var e in this._tasks) for (var t = v(e), r = 1; r < t.length; r++) this._tasks[t[r]] && (Array.isArray(this._tasks[e]) && this._tasks[e].length && Array.prototype.push.apply(this._tasks[t[r]], this._tasks[e]), delete this._tasks[e]);
        }
      }, {
        key: "collectRefreshTasks",
        value: function () {
          var e = this;
          return this.rs.local.forAllNodes(function (t) {
            var r;

            if (e.needsRefresh(t)) {
              try {
                r = e.getParentPath(t.path);
              } catch (e) {}

              r && e.rs.access.checkPathPermission(r, "r") ? e.addTask(r) : e.rs.access.checkPathPermission(t.path, "r") && e.addTask(t.path);
            }
          }).then(function () {
            e.deleteChildPathsFromTasks();
          }, function (e) {
            throw e;
          });
        }
      }, {
        key: "flush",
        value: function (e) {
          for (var t in e) "FLUSH" === this.rs.caching.checkPath(t) && e[t] && !e[t].local && (w("[Sync] Flushing", t), e[t] = void 0);

          return e;
        }
      }, {
        key: "doTask",
        value: function (e) {
          var t = this;
          return this.rs.local.getNodes([e]).then(function (r) {
            var n = r[e];
            return void 0 === n ? S("get", e, t.rs.remote.get(e)) : function (e) {
              return e.remote && e.remote.revision && !e.remote.itemsMap && !e.remote.body;
            }(n) ? S("get", e, t.rs.remote.get(e)) : t.needsRemotePut(n) ? (n.push = g(n.local), n.push.timestamp = t.now(), t.rs.local.setNodes(t.flush(r)).then(function () {
              var r;
              return r = A(n) ? {
                ifMatch: n.common.revision
              } : {
                ifNoneMatch: "*"
              }, S("put", e, t.rs.remote.put(e, n.push.body, n.push.contentType, r));
            })) : t.needsRemoteDelete(n) ? (n.push = {
              body: !1,
              timestamp: t.now()
            }, t.rs.local.setNodes(t.flush(r)).then(function () {
              return A(n) ? S("delete", e, t.rs.remote.delete(e, {
                ifMatch: n.common.revision
              })) : S("get", e, t.rs.remote.get(e));
            })) : A(n) ? S("get", e, t.rs.remote.get(e, {
              ifNoneMatch: n.common.revision
            })) : S("get", e, t.rs.remote.get(e));
          });
        }
      }, {
        key: "autoMergeFolder",
        value: function (e) {
          if (e.remote.itemsMap && (e.common = e.remote, delete e.remote, e.common.itemsMap)) {
            for (var t in e.common.itemsMap) e.local.itemsMap[t] || (e.local.itemsMap[t] = !1);

            y(e.local.itemsMap, e.common.itemsMap) && delete e.local;
          }

          return e;
        }
      }, {
        key: "autoMergeDocument",
        value: function (e) {
          return !function (e) {
            return (!e.remote || !e.remote.revision || e.remote.revision === e.common.revision) && (void 0 === e.common.body && !1 === e.remote.body || e.remote.body === e.common.body && e.remote.contentType === e.common.contentType);
          }(e) ? void 0 !== e.remote.body && (w("[Sync] Emitting keep/revert"), this.rs.local._emitChange({
            origin: "conflict",
            path: e.path,
            oldValue: e.local.body,
            newValue: e.remote.body,
            lastCommonValue: e.common.body,
            oldContentType: e.local.contentType,
            newContentType: e.remote.contentType,
            lastCommonContentType: e.common.contentType
          }), e.remote.body ? e.common = e.remote : e.common = {}, delete e.remote, delete e.local) : delete (e = function (e) {
            return e.remote && !1 === e.remote.body && e.local && !1 === e.local.body && delete e.local, e;
          }(e)).remote, e;
        }
      }, {
        key: "autoMerge",
        value: function (e) {
          if (e.remote) {
            if (e.local) return p(e.path) ? this.autoMergeFolder(e) : this.autoMergeDocument(e);
            if (p(e.path)) void 0 !== e.remote.itemsMap && (e.common = e.remote, delete e.remote);else if (void 0 !== e.remote.body) {
              var t = {
                origin: "remote",
                path: e.path,
                oldValue: !1 === e.common.body ? void 0 : e.common.body,
                newValue: !1 === e.remote.body ? void 0 : e.remote.body,
                oldContentType: e.common.contentType,
                newContentType: e.remote.contentType
              };
              if ((t.oldValue || t.newValue) && this.rs.local._emitChange(t), !e.remote.body) return;
              e.common = e.remote, delete e.remote;
            }
            return e;
          }

          e.common.body && this.rs.local._emitChange({
            origin: "remote",
            path: e.path,
            oldValue: e.common.body,
            newValue: void 0,
            oldContentType: e.common.contentType,
            newContentType: void 0
          });
        }
      }, {
        key: "updateCommonTimestamp",
        value: function (e, t) {
          var r = this;
          return this.rs.local.getNodes([e]).then(function (n) {
            return n[e] && n[e].common && n[e].common.revision === t && (n[e].common.timestamp = r.now()), r.rs.local.setNodes(r.flush(n));
          });
        }
      }, {
        key: "markChildren",
        value: function (e, t, r, n) {
          var o = this,
              i = [],
              s = {},
              a = {};

          for (var u in t) i.push(e + u), s[e + u] = t[u];

          for (var c in n) i.push(e + c);

          return this.rs.local.getNodes(i).then(function (t) {
            var i;

            for (var u in t) if (i = t[u], s[u]) i && i.common ? T(i, s[u].ETag) && (r[u] = g(i), r[u].remote = {
              revision: s[u].ETag,
              timestamp: o.now()
            }, r[u] = o.autoMerge(r[u])) : "ALL" === o.rs.caching.checkPath(u) && (r[u] = {
              path: u,
              common: {
                timestamp: o.now()
              },
              remote: {
                revision: s[u].ETag,
                timestamp: o.now()
              }
            }), r[u] && s[u]["Content-Type"] && (r[u].remote.contentType = s[u]["Content-Type"]), r[u] && s[u]["Content-Length"] && (r[u].remote.contentLength = s[u]["Content-Length"]);else if (n[u.substring(e.length)] && i && i.common) {
              if (i.common.itemsMap) for (var c in i.common.itemsMap) a[u + c] = !0;
              if (i.local && i.local.itemsMap) for (var l in i.local.itemsMap) a[u + l] = !0;
              if (i.remote || p(u)) r[u] = void 0;else if (r[u] = o.autoMerge(i), void 0 === r[u]) {
                var h = o.getParentPath(u),
                    f = r[h],
                    d = u.substring(e.length);
                f && f.local && (delete f.local.itemsMap[d], y(f.local.itemsMap, f.common.itemsMap) && delete f.local);
              }
            }

            return o.deleteRemoteTrees(Object.keys(a), r).then(function (e) {
              return o.rs.local.setNodes(o.flush(e));
            });
          });
        }
      }, {
        key: "deleteRemoteTrees",
        value: function (e, t) {
          var r = this;
          return 0 === e.length ? Promise.resolve(t) : this.rs.local.getNodes(e).then(function (e) {
            var n = {},
                o = function (e, t) {
              if (e && e.itemsMap) for (var r in e.itemsMap) n[t + r] = !0;
            };

            for (var i in e) {
              var s = e[i];
              s && (p(i) ? (o(s.common, i), o(s.local, i)) : s.common && void 0 !== u(s.common.body) && (t[i] = g(s), t[i].remote = {
                body: !1,
                timestamp: r.now()
              }, t[i] = r.autoMerge(t[i])));
            }

            return r.deleteRemoteTrees(Object.keys(n), t).then(function (e) {
              return r.rs.local.setNodes(r.flush(e));
            });
          });
        }
      }, {
        key: "completeFetch",
        value: function (e, t, r, n) {
          var o,
              i,
              s = this,
              a = v(e);
          return p(e) ? o = [e] : (i = a[1], o = [e, i]), this.rs.local.getNodes(o).then(function (o) {
            var a,
                c,
                l = {},
                h = o[e],
                f = function (e) {
              if (e && e.itemsMap) for (a in e.itemsMap) t[a] || (l[a] = !0);
            };

            if ("object" === u(h) && h.path === e && "object" === u(h.common) || (h = {
              path: e,
              common: {}
            }, o[e] = h), h.remote = {
              revision: n,
              timestamp: s.now()
            }, p(e)) for (a in f(h.common), f(h.remote), h.remote.itemsMap = {}, t) h.remote.itemsMap[a] = !0;else h.remote.body = t, h.remote.contentType = r, (c = o[i]) && c.local && c.local.itemsMap && (a = e.substring(i.length), c.local.itemsMap[a] = !0, y(c.local.itemsMap, c.common.itemsMap) && delete c.local);
            return o[e] = s.autoMerge(h), {
              toBeSaved: o,
              missingChildren: l
            };
          });
        }
      }, {
        key: "completePush",
        value: function (e, t, r, n) {
          var o = this;
          return this.rs.local.getNodes([e]).then(function (i) {
            var s = i[e];
            if (!s.push) throw o.stopped = !0, new Error("completePush called but no push version!");
            return r ? (w("[Sync] We have a conflict"), s.remote && s.remote.revision === n || (s.remote = {
              revision: n || "conflict",
              timestamp: o.now()
            }, delete s.push), i[e] = o.autoMerge(s)) : (s.common = {
              revision: n,
              timestamp: o.now()
            }, "put" === t ? (s.common.body = s.push.body, s.common.contentType = s.push.contentType, y(s.local.body, s.push.body) && s.local.contentType === s.push.contentType && delete s.local, delete s.push) : "delete" === t && (!1 === s.local.body ? i[e] = void 0 : delete s.push)), o.rs.local.setNodes(o.flush(i));
          });
        }
      }, {
        key: "dealWithFailure",
        value: function (e) {
          var t = this;
          return this.rs.local.getNodes([e]).then(function (r) {
            if (r[e]) return delete r[e].push, t.rs.local.setNodes(t.flush(r));
          });
        }
      }, {
        key: "interpretStatus",
        value: function (e) {
          var t = {
            statusCode: e,
            successful: void 0,
            conflict: void 0,
            unAuth: void 0,
            notFound: void 0,
            changed: void 0,
            networkProblems: void 0
          };
          if ("offline" === e || "timeout" === e) return t.successful = !1, t.networkProblems = !0, t;
          var r = Math.floor(e / 100);
          return t.successful = 2 === r || 304 === e || 412 === e || 404 === e, t.conflict = 412 === e, t.unAuth = 401 === e && this.rs.remote.token !== P.IMPLIED_FAKE_TOKEN || 402 === e || 403 === e, t.notFound = 404 === e, t.changed = 304 !== e, t;
        }
      }, {
        key: "handleGetResponse",
        value: function (e, t, r, n, o) {
          var i = this;
          return t.notFound && (r = !!p(e) && {}), t.changed ? this.completeFetch(e, r, n, o).then(function (t) {
            return p(e) ? i.corruptServerItemsMap(r) ? (w("[Sync] WARNING: Discarding corrupt folder description from server for " + e), !1) : i.markChildren(e, r, t.toBeSaved, t.missingChildren).then(function () {
              return !0;
            }) : i.rs.local.setNodes(i.flush(t.toBeSaved)).then(function () {
              return !0;
            });
          }) : this.updateCommonTimestamp(e, o).then(function () {
            return !0;
          });
        }
      }, {
        key: "handleResponse",
        value: function (t, r, n) {
          var o,
              i = this,
              s = this.interpretStatus(n.statusCode);

          if (s.successful) {
            if ("get" === r) return this.handleGetResponse(t, s, n.body, n.contentType, n.revision);
            if ("put" === r || "delete" === r) return this.completePush(t, r, s.conflict, n.revision).then(function () {
              return !0;
            });
            throw new Error("cannot handle response for unknown action ".concat(r));
          }

          return o = s.unAuth ? new P.Unauthorized() : s.networkProblems ? new e.SyncError("Network request failed.") : new Error("HTTP response code " + s.statusCode + " received."), this.dealWithFailure(t).then(function () {
            throw i.rs._emit("error", o), o;
          });
        }
      }, {
        key: "finishTask",
        value: function (e) {
          var t = this;
          if (void 0 !== e.action) return e.promise.then(function (r) {
            return t.handleResponse(e.path, e.action, r);
          }, function (r) {
            return w("[Sync] wireclient rejects its promise!", e.path, e.action, r), t.handleResponse(e.path, e.action, {
              statusCode: "offline"
            });
          }).then(function (r) {
            if (delete t._timeStarted[e.path], delete t._running[e.path], r && t._tasks[e.path]) {
              for (var n = 0; n < t._tasks[e.path].length; n++) t._tasks[e.path][n]();

              delete t._tasks[e.path];
            }

            t.rs._emit("sync-req-done"), t.collectTasks(!1).then(function () {
              !t.hasTasks() || t.stopped ? (w("[Sync] Sync is done! Reschedule?", Object.getOwnPropertyNames(t._tasks).length, t.stopped), t.done || (t.done = !0, t.rs._emit("sync-done"))) : setTimeout(function () {
                t.doTasks();
              }, 10);
            });
          }, function (r) {
            w("[Sync] Error", r), delete t._timeStarted[e.path], delete t._running[e.path], t.rs._emit("sync-req-done"), t.done || (t.done = !0, t.rs._emit("sync-done"));
          });
          delete this._running[e.path];
        }
      }, {
        key: "doTasks",
        value: function () {
          var e,
              t,
              r = 0;
          if ((e = (this.rs.remote.connected ? this.rs.remote.online ? this.numThreads : 1 : 0) - Object.getOwnPropertyNames(this._running).length) <= 0) return !0;

          for (t in this._tasks) if (!this._running[t] && (this._timeStarted[t] = this.now(), this._running[t] = this.doTask(t), this._running[t].then(this.finishTask.bind(this)), ++r >= e)) return !0;

          return r >= e;
        }
      }, {
        key: "collectTasks",
        value: function (e) {
          var t = this;
          return this.hasTasks() || this.stopped ? Promise.resolve() : this.collectDiffTasks().then(function (r) {
            return r || !1 === e ? Promise.resolve() : t.collectRefreshTasks();
          }, function (e) {
            throw e;
          });
        }
      }, {
        key: "addTask",
        value: function (e, t) {
          this._tasks[e] || (this._tasks[e] = []), "function" == typeof t && this._tasks[e].push(t);
        }
      }, {
        key: "sync",
        value: function () {
          var e = this;
          return this.done = !1, this.doTasks() ? Promise.resolve() : this.collectTasks().then(function () {
            try {
              e.doTasks();
            } catch (e) {
              w("[Sync] doTasks error", e);
            }
          }, function (e) {
            throw w("[Sync] Sync error", e), new Error("Local cache unavailable");
          });
        }
      }]) && l(t.prototype, r), n && l(t, n), e;
    }();

    R.SyncError = function (e) {
      function t(e) {
        var r;
        c(this, t), (r = n(this, a(t).call(this))).name = "SyncError";
        var o = "Sync failed: ";
        return "object" === u(e) && "message" in e ? (o += e.message, r.stack = e.stack, r.originalError = e) : o += e, r.message = o, r;
      }

      return function (e, t) {
        if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function");
        e.prototype = Object.create(t && t.prototype, {
          constructor: {
            value: e,
            writable: !0,
            configurable: !0
          }
        }), t && s(e, t);
      }(t, o(Error)), t;
    }(), e.exports = R;
  }, function (e, t, r) {
    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o = r(0),
        i = r(3),
        s = r(1),
        a = o.isFolder,
        u = o.isDocument,
        c = o.deepClone;

    function l(e) {
      if ("object" === n(e) && "string" == typeof e.path) if (a(e.path)) {
        if (e.local && e.local.itemsMap) return e.local;
        if (e.common && e.common.itemsMap) return e.common;
      } else {
        if (e.local) {
          if (e.local.body && e.local.contentType) return e.local;
          if (!1 === e.local.body) return;
        }

        if (e.common && e.common.body && e.common.contentType) return e.common;
        if (e.body && e.contentType) return {
          body: e.body,
          contentType: e.contentType
        };
      }
    }

    function h(e, t) {
      var r;

      for (r in e) {
        if (e[r] && e[r].remote) return !0;
        var n = l(e[r]);
        if (n && n.timestamp && new Date().getTime() - n.timestamp <= t) return !1;
        if (!n) return !0;
      }

      return !0;
    }

    var f = o.pathsFromRoot;

    function d(e) {
      var t = {
        path: e,
        common: {}
      };
      return a(e) && (t.common.itemsMap = {}), t;
    }

    function p(e, t) {
      return e.common || (e.common = {
        itemsMap: {}
      }), e.common.itemsMap || (e.common.itemsMap = {}), e.local || (e.local = c(e.common)), e.local.itemsMap || (e.local.itemsMap = e.common.itemsMap), e.local.itemsMap[t] = !0, e;
    }

    var m = {
      get: function (e, t, r) {
        return "number" == typeof t ? this.getNodes(f(e)).then(function (n) {
          var o = l(n[e]);
          return h(n, t) ? r(e) : o ? {
            statusCode: 200,
            body: o.body || o.itemsMap,
            contentType: o.contentType
          } : {
            statusCode: 404
          };
        }) : this.getNodes([e]).then(function (t) {
          var r = l(t[e]);

          if (r) {
            if (a(e)) for (var n in r.itemsMap) r.itemsMap.hasOwnProperty(n) && !1 === r.itemsMap[n] && delete r.itemsMap[n];
            return {
              statusCode: 200,
              body: r.body || r.itemsMap,
              contentType: r.contentType
            };
          }

          return {
            statusCode: 404
          };
        });
      },
      put: function (e, t, r) {
        var n = f(e);
        return this._updateNodes(n, function (e, n) {
          try {
            for (var o = 0, i = e.length; o < i; o++) {
              var a = e[o],
                  u = n[a],
                  c = void 0;
              u || (n[a] = u = d(a)), 0 === o ? (c = l(u), u.local = {
                body: t,
                contentType: r,
                previousBody: c ? c.body : void 0,
                previousContentType: c ? c.contentType : void 0
              }) : u = p(u, e[o - 1].substring(a.length));
            }

            return n;
          } catch (e) {
            throw s("[Cachinglayer] Error during PUT", n, e), e;
          }
        });
      },
      delete: function (e) {
        var t = f(e);
        return this._updateNodes(t, function (e, t) {
          for (var r = 0, n = e.length; r < n; r++) {
            var o = e[r],
                i = t[o],
                s = void 0;
            if (i) {
              if (0 === r) s = l(i), i.local = {
                body: !1,
                previousBody: s ? s.body : void 0,
                previousContentType: s ? s.contentType : void 0
              };else {
                i.local || (i.local = c(i.common));
                var a = e[r - 1].substring(o.length);
                if (delete i.local.itemsMap[a], Object.getOwnPropertyNames(i.local.itemsMap).length > 0) break;
              }
            } else console.error("Cannot delete non-existing node " + o);
          }

          return t;
        });
      },
      flush: function (e) {
        var t = this;
        return t._getAllDescendentPaths(e).then(function (e) {
          return t.getNodes(e);
        }).then(function (e) {
          for (var r in e) {
            var n = e[r];
            n && n.common && n.local && t._emitChange({
              path: n.path,
              origin: "local",
              oldValue: !1 === n.local.body ? void 0 : n.local.body,
              newValue: !1 === n.common.body ? void 0 : n.common.body
            }), e[r] = void 0;
          }

          return t.setNodes(e);
        });
      },
      _emitChange: function (e) {
        i.changeEvents[e.origin] && this._emit("change", e);
      },
      fireInitial: function () {
        if (i.changeEvents.local) {
          var e = this;
          e.forAllNodes(function (t) {
            var r;
            u(t.path) && (r = l(t)) && e._emitChange({
              path: t.path,
              origin: "local",
              oldValue: void 0,
              oldContentType: void 0,
              newValue: r.body,
              newContentType: r.contentType
            });
          }).then(function () {
            e._emit("local-events-done");
          });
        }
      },
      onDiff: function (e) {
        this.diffHandler = e;
      },
      migrate: function (e) {
        return "object" !== n(e) || e.common || (e.common = {}, "string" == typeof e.path ? "/" === e.path.substr(-1) && "object" === n(e.body) && (e.common.itemsMap = e.body) : (e.local || (e.local = {}), e.local.body = e.body, e.local.contentType = e.contentType)), e;
      },
      _updateNodesRunning: !1,
      _updateNodesQueued: [],
      _updateNodes: function (e, t) {
        return new Promise(function (r, n) {
          this._doUpdateNodes(e, t, {
            resolve: r,
            reject: n
          });
        }.bind(this));
      },
      _doUpdateNodes: function (e, t, r) {
        var n = this;
        n._updateNodesRunning ? n._updateNodesQueued.push({
          paths: e,
          cb: t,
          promise: r
        }) : (n._updateNodesRunning = !0, n.getNodes(e).then(function (i) {
          var s,
              a = c(i),
              l = [],
              h = o.equal;

          for (var f in i = t(e, i)) h(s = i[f], a[f]) ? delete i[f] : u(f) && (h(s.local.body, s.local.previousBody) && s.local.contentType === s.local.previousContentType || l.push({
            path: f,
            origin: "window",
            oldValue: s.local.previousBody,
            newValue: !1 === s.local.body ? void 0 : s.local.body,
            oldContentType: s.local.previousContentType,
            newContentType: s.local.contentType
          }), delete s.local.previousBody, delete s.local.previousContentType);

          n.setNodes(i).then(function () {
            n._emitChangeEvents(l), r.resolve({
              statusCode: 200
            });
          });
        }).then(function () {
          return Promise.resolve();
        }, function (e) {
          r.reject(e);
        }).then(function () {
          n._updateNodesRunning = !1;

          var e = n._updateNodesQueued.shift();

          e && n._doUpdateNodes(e.paths, e.cb, e.promise);
        }));
      },
      _emitChangeEvents: function (e) {
        for (var t = 0, r = e.length; t < r; t++) this._emitChange(e[t]), this.diffHandler && this.diffHandler(e[t].path);
      },
      _getAllDescendentPaths: function (e) {
        var t = this;
        return a(e) ? t.getNodes([e]).then(function (r) {
          var n = [e],
              o = l(r[e]),
              i = Object.keys(o.itemsMap).map(function (r) {
            return t._getAllDescendentPaths(e + r).then(function (e) {
              for (var t = 0, r = e.length; t < r; t++) n.push(e[t]);
            });
          });
          return Promise.all(i).then(function () {
            return n;
          });
        }) : Promise.resolve([e]);
      },
      _getInternals: function () {
        return {
          getLatest: l,
          makeNode: d,
          isOutdated: h
        };
      }
    };

    e.exports = function (e) {
      for (var t in m) e[t] = m[t];
    };
  }, function (e, t, r) {
    "use strict";

    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o,
        i = r(0),
        s = r(11),
        a = r(13),
        u = r(14),
        c = r(5),
        l = r(3),
        h = r(4),
        f = r(7),
        d = r(1),
        p = r(27),
        m = i.getGlobalContext(),
        y = r(2),
        g = i.getJSONFromLocalStorage;

    var v = function (e) {
      "object" === n(e) && i.extend(l, e), y(this, "ready", "authing", "connecting", "connected", "disconnected", "not-connected", "conflict", "error", "features-loaded", "sync-interval-change", "sync-req-done", "sync-done", "wire-busy", "wire-done", "network-offline", "network-online"), this._pending = [], this._setGPD({
        get: this._pendingGPD("get"),
        put: this._pendingGPD("put"),
        delete: this._pendingGPD("delete")
      }), this._cleanups = [], this._pathHandlers = {
        change: {}
      }, this.apiKeys = {}, (o = i.localStorageAvailable()) && (this.apiKeys = g("remotestorage:api-keys") || {}, this.setBackend(localStorage.getItem("remotestorage:backend") || "remotestorage"));
      var t = this.on;
      this.on = function (e, r) {
        if (this._allLoaded) switch (e) {
          case "features-loaded":
            setTimeout(r, 0);
            break;

          case "ready":
            this.remote && setTimeout(r, 0);
            break;

          case "connected":
            this.remote && this.remote.connected && setTimeout(r, 0);
            break;

          case "not-connected":
            this.remote && !this.remote.connected && setTimeout(r, 0);
        }
        return t.call(this, e, r);
      }, this._init(), this.fireInitial = function () {
        this.local && setTimeout(this.local.fireInitial.bind(this.local), 0);
      }.bind(this), this.on("ready", this.fireInitial.bind(this)), this.loadModules();
    };

    function b(e) {
      return "number" == typeof e && e > 1e3 && e < 36e5;
    }

    v.Authorize = h, v.SyncError = f.SyncError, v.Unauthorized = h.Unauthorized, v.DiscoveryError = u.DiscoveryError, v.prototype = {
      loadModules: function () {
        l.modules.forEach(this.addModule.bind(this));
      },
      authorize: function (e) {
        this.access.setStorageType(this.remote.storageApi), void 0 === e.scope && (e.scope = this.access.scopeParameter), e.redirectUri = m.cordova ? l.cordovaRedirectUri : String(h.getLocation()), void 0 === e.clientId && (e.clientId = e.redirectUri.match(/^(https?:\/\/[^\/]+)/)[0]), h(this, e);
      },
      impliedauth: function (e, t) {
        e = this.remote.storageApi, t = String(document.location), d("ImpliedAuth proceeding due to absent authURL; storageApi = " + e + " redirectUri = " + t), this.remote.configure({
          token: h.IMPLIED_FAKE_TOKEN
        }), document.location = t;
      },
      connect: function (e, t) {
        var r = this;
        if (this.setBackend("remotestorage"), e.indexOf("@") < 0) this._emit("error", new v.DiscoveryError("User address doesn't contain an @."));else {
          if (m.cordova) {
            if ("string" != typeof l.cordovaRedirectUri) return void this._emit("error", new v.DiscoveryError("Please supply a custom HTTPS redirect URI for your Cordova app"));
            if (!m.cordova.InAppBrowser) return void this._emit("error", new v.DiscoveryError("Please include the InAppBrowser Cordova plugin to enable OAuth"));
          }

          this.remote.configure({
            userAddress: e
          }), this._emit("connecting");
          var n = setTimeout(function () {
            this._emit("error", new v.DiscoveryError("No storage information found for this user address."));
          }.bind(this), l.discoveryTimeout);
          u(e).then(function (o) {
            if (clearTimeout(n), r._emit("authing"), o.userAddress = e, r.remote.configure(o), !r.remote.connected) if (o.authURL) {
              if (void 0 === t) r.authorize({
                authURL: o.authURL
              });else {
                if ("string" != typeof t) throw new Error("Supplied bearer token must be a string");
                d("Skipping authorization sequence and connecting with known token"), r.remote.configure({
                  token: t
                });
              }
            } else r.impliedauth();
          }, function () {
            clearTimeout(n), r._emit("error", new v.DiscoveryError("No storage information found for this user address."));
          });
        }
      },
      reconnect: function () {
        this.remote.configure({
          token: null
        }), "remotestorage" === this.backend ? this.connect(this.remote.userAddress) : this.remote.connect();
      },
      disconnect: function () {
        this.remote && this.remote.configure({
          userAddress: null,
          href: null,
          storageApi: null,
          token: null,
          properties: null
        }), this._setGPD({
          get: this._pendingGPD("get"),
          put: this._pendingGPD("put"),
          delete: this._pendingGPD("delete")
        });

        var e = this._cleanups.length,
            t = 0,
            r = function () {
          ++t >= e && (this._init(), d("Done cleaning up, emitting disconnected and disconnect events"), this._emit("disconnected"));
        }.bind(this);

        e > 0 ? this._cleanups.forEach(function (e) {
          var t = e(this);
          "object" === n(t) && "function" == typeof t.then ? t.then(r) : r();
        }.bind(this)) : r();
      },
      setBackend: function (e) {
        this.backend = e, o && (e ? localStorage.setItem("remotestorage:backend", e) : localStorage.removeItem("remotestorage:backend"));
      },
      onChange: function (e, t) {
        this._pathHandlers.change[e] || (this._pathHandlers.change[e] = []), this._pathHandlers.change[e].push(t);
      },
      enableLog: function () {
        l.logging = !0;
      },
      disableLog: function () {
        l.logging = !1;
      },
      log: function () {
        d.apply(v, arguments);
      },
      setApiKeys: function (e) {
        var t = this,
            r = ["googledrive", "dropbox"];
        if ("object" !== n(e) || !Object.keys(e).every(function (e) {
          return r.includes(e);
        })) return console.error("setApiKeys() was called with invalid arguments"), !1;
        Object.keys(e).forEach(function (r) {
          var n = e[r];

          if (n) {
            switch (r) {
              case "dropbox":
                t.apiKeys.dropbox = {
                  appKey: n
                }, void 0 !== t.dropbox && t.dropbox.clientId === n || s._rs_init(t);
                break;

              case "googledrive":
                t.apiKeys.googledrive = {
                  clientId: n
                }, void 0 !== t.googledrive && t.googledrive.clientId === n || a._rs_init(t);
            }

            return !0;
          }

          delete t.apiKeys[r];
        }), o && localStorage.setItem("remotestorage:api-keys", JSON.stringify(this.apiKeys));
      },
      setCordovaRedirectUri: function (e) {
        if ("string" != typeof e || !e.match(/http(s)?:\/\//)) throw new Error("Cordova redirect URI must be a URI string");
        l.cordovaRedirectUri = e;
      },
      _init: p.loadFeatures,
      features: p.features,
      loadFeature: p.loadFeature,
      featureSupported: p.featureSupported,
      featureDone: p.featureDone,
      featuresDone: p.featuresDone,
      featuresLoaded: p.featuresLoaded,
      featureInitialized: p.featureInitialized,
      featureFailed: p.featureFailed,
      hasFeature: p.hasFeature,
      _setCachingModule: p._setCachingModule,
      _collectCleanupFunctions: p._collectCleanupFunctions,
      _fireReady: p._fireReady,
      initFeature: p.initFeature,
      _setGPD: function (e, t) {
        function r(e) {
          return function () {
            return e.apply(t, arguments).then(function (e) {
              return 403 !== e.statusCode && 401 !== e.statusCode || this._emit("error", new h.Unauthorized()), Promise.resolve(e);
            }.bind(this));
          };
        }

        this.get = r(e.get), this.put = r(e.put), this.delete = r(e.delete);
      },
      _pendingGPD: function (e) {
        return function () {
          var t = Array.prototype.slice.call(arguments);
          return new Promise(function (r, n) {
            this._pending.push({
              method: e,
              args: t,
              promise: {
                resolve: r,
                reject: n
              }
            });
          }.bind(this));
        }.bind(this);
      },
      _processPending: function () {
        this._pending.forEach(function (e) {
          try {
            this[e.method].apply(this, e.args).then(e.promise.resolve, e.promise.reject);
          } catch (t) {
            e.promise.reject(t);
          }
        }.bind(this)), this._pending = [];
      },
      _bindChange: function (e) {
        e.on("change", this._dispatchEvent.bind(this, "change"));
      },
      _dispatchEvent: function (e, t) {
        var r = this;
        Object.keys(this._pathHandlers[e]).forEach(function (n) {
          var o = n.length;
          t.path.substr(0, o) === n && r._pathHandlers[e][n].forEach(function (e) {
            var o = {};

            for (var i in t) o[i] = t[i];

            o.relativePath = t.path.replace(new RegExp("^" + n), "");

            try {
              e(o);
            } catch (e) {
              console.error("'change' handler failed: ", e, e.stack), r._emit("error", e);
            }
          });
        });
      },
      scope: function (e) {
        if ("string" != typeof e) throw "Argument 'path' of baseClient.scope must be a string";

        if (!this.access.checkPathPermission(e, "r")) {
          var t = e.replace(/(['\\])/g, "\\$1");
          console.warn("WARNING: please call remoteStorage.access.claim('" + t + "', 'r') (read only) or remoteStorage.access.claim('" + t + "', 'rw') (read/write) first");
        }

        return new c(this, e);
      },
      getSyncInterval: function () {
        return l.syncInterval;
      },
      setSyncInterval: function (e) {
        if (!b(e)) throw e + " is not a valid sync interval";
        var t = l.syncInterval;
        l.syncInterval = parseInt(e, 10), this._emit("sync-interval-change", {
          oldValue: t,
          newValue: e
        });
      },
      getBackgroundSyncInterval: function () {
        return l.backgroundSyncInterval;
      },
      setBackgroundSyncInterval: function (e) {
        if (!b(e)) throw e + " is not a valid sync interval";
        var t = l.backgroundSyncInterval;
        l.backgroundSyncInterval = parseInt(e, 10), this._emit("sync-interval-change", {
          oldValue: t,
          newValue: e
        });
      },
      getCurrentSyncInterval: function () {
        return l.isBackground ? l.backgroundSyncInterval : l.syncInterval;
      },
      getRequestTimeout: function () {
        return l.requestTimeout;
      },
      setRequestTimeout: function (e) {
        l.requestTimeout = parseInt(e, 10);
      },
      syncCycle: function () {
        this.sync && !this.sync.stopped && (this.on("sync-done", function () {
          d("[Sync] Sync done. Setting timer to", this.getCurrentSyncInterval()), this.sync && !this.sync.stopped && (this._syncTimer && (clearTimeout(this._syncTimer), this._syncTimer = void 0), this._syncTimer = setTimeout(this.sync.sync.bind(this.sync), this.getCurrentSyncInterval()));
        }.bind(this)), this.sync.sync());
      },
      startSync: function () {
        return l.cache ? (this.sync.stopped = !1, this.syncStopped = !1, this.sync.sync()) : (console.warn("Nothing to sync, because caching is disabled."), Promise.resolve());
      },
      stopSync: function () {
        clearTimeout(this._syncTimer), this._syncTimer = void 0, this.sync ? (d("[Sync] Stopping sync"), this.sync.stopped = !0) : (d("[Sync] Will instantiate sync stopped"), this.syncStopped = !0);
      }
    }, v.util = i, Object.defineProperty(v.prototype, "connected", {
      get: function () {
        return this.remote.connected;
      }
    });

    var _ = r(15);

    Object.defineProperty(v.prototype, "access", {
      get: function () {
        var e = new _();
        return Object.defineProperty(this, "access", {
          value: e
        }), e;
      },
      configurable: !0
    });
    var w = r(16);
    Object.defineProperty(v.prototype, "caching", {
      configurable: !0,
      get: function () {
        var e = new w();
        return Object.defineProperty(this, "caching", {
          value: e
        }), e;
      }
    }), e.exports = v, r(32);
  }, function (e, t) {
    var r;

    r = function () {
      return this;
    }();

    try {
      r = r || new Function("return this")();
    } catch (e) {
      "object" == typeof window && (r = window);
    }

    e.exports = r;
  }, function (e, t, r) {
    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o,
        i = r(4),
        s = r(5),
        a = r(6),
        u = r(0),
        c = r(2),
        l = r(24),
        h = r(7),
        f = "remotestorage:dropbox",
        d = u.isFolder,
        p = u.cleanPath,
        m = u.shouldBeTreatedAsBinary,
        y = u.getJSONFromLocalStorage,
        g = u.getTextFromArrayBuffer,
        v = function (e) {
      return p("/remotestorage/" + e).replace(/\/$/, "");
    },
        b = function (e, t) {
      return new RegExp("^" + t.join("\\/") + "(\\/|$)").test(e.error_summary);
    },
        _ = function (e) {
      return e instanceof ArrayBuffer || a.isArrayBufferView(e);
    },
        w = function (e) {
      if (this.rs = e, this.connected = !1, this.rs = e, this._initialFetchDone = !1, c(this, "connected", "not-connected"), this.clientId = e.apiKeys.dropbox.appKey, this._revCache = new l("rev"), this._fetchDeltaCursor = null, this._fetchDeltaPromise = null, this._itemRefs = {}, o = u.localStorageAvailable()) {
        var t = y(f);
        t && this.configure(t), this._itemRefs = y("".concat(f, ":shares")) || {};
      }

      this.connected && setTimeout(this._emit.bind(this), 0, "connected");
    };

    function P(e) {
      e._dropboxOrigSync || (e._dropboxOrigSync = e.sync.sync.bind(e.sync), e.sync.sync = function () {
        return this.dropbox.fetchDelta.apply(this.dropbox, arguments).then(e._dropboxOrigSync, function (t) {
          e._emit("error", new h.SyncError(t)), e._emit("sync-done");
        });
      }.bind(e));
    }

    function E(e) {
      e._dropboxOrigSyncCycle && (e.syncCycle = e._dropboxOrigSyncCycle, delete e._dropboxOrigSyncCycle);
    }

    function S(e) {
      !function (e) {
        e._origRemote || (e._origRemote = e.remote, e.remote = e.dropbox);
      }(e), e.sync ? P(e) : function (e) {
        var t = arguments;
        e._dropboxOrigSyncCycle || (e._dropboxOrigSyncCycle = e.syncCycle, e.syncCycle = function () {
          if (!e.sync) throw new Error("expected sync to be initialized by now");
          P(e), e._dropboxOrigSyncCycle(t), E(e);
        });
      }(e), function (e) {
        e._origBaseClientGetItemURL || (e._origBaseClientGetItemURL = s.prototype.getItemURL, s.prototype.getItemURL = function () {
          throw new Error("getItemURL is not implemented for Dropbox yet");
        });
      }(e);
    }

    function T(e) {
      !function (e) {
        e._origRemote && (e.remote = e._origRemote, delete e._origRemote);
      }(e), function (e) {
        e._dropboxOrigSync && (e.sync.sync = e._dropboxOrigSync, delete e._dropboxOrigSync);
      }(e), function (e) {
        e._origBaseClientGetItemURL && (s.prototype.getItemURL = e._origBaseClientGetItemURL, delete e._origBaseClientGetItemURL);
      }(e), E(e);
    }

    w.prototype = {
      online: !0,
      connect: function () {
        this.rs.setBackend("dropbox"), this.token ? S(this.rs) : this.rs.authorize({
          authURL: "https://www.dropbox.com/oauth2/authorize",
          scope: "",
          clientId: this.clientId
        });
      },
      configure: function (e) {
        void 0 !== e.userAddress && (this.userAddress = e.userAddress), void 0 !== e.token && (this.token = e.token);

        var t = function () {
          o && localStorage.setItem(f, JSON.stringify({
            userAddress: this.userAddress,
            token: this.token
          }));
        },
            r = function () {
          this.connected = !1, o && localStorage.removeItem(f);
        };

        this.token ? (this.connected = !0, this.userAddress ? (this._emit("connected"), t.apply(this)) : this.info().then(function (e) {
          this.userAddress = e.email, this._emit("connected"), t.apply(this);
        }.bind(this)).catch(function () {
          r.apply(this), this.rs._emit("error", new Error("Could not fetch user info."));
        }.bind(this))) : r.apply(this);
      },
      stopWaitingForToken: function () {
        this.connected || this._emit("not-connected");
      },
      _getFolder: function (e) {
        var t = this._revCache,
            r = this,
            n = function (n) {
          var i, s;
          if (200 !== n.status && 409 !== n.status) return Promise.reject("Unexpected response status: " + n.status);

          try {
            i = JSON.parse(n.responseText);
          } catch (e) {
            return Promise.reject(e);
          }

          return 409 === n.status ? b(i, ["path", "not_found"]) ? Promise.resolve({}) : Promise.reject(new Error("API returned an error: " + i.error_summary)) : (s = i.entries.reduce(function (n, o) {
            var i = "folder" === o[".tag"],
                s = o.path_lower.split("/").slice(-1)[0] + (i ? "/" : "");
            return i ? n[s] = {
              ETag: t.get(e + s)
            } : (n[s] = {
              ETag: o.rev
            }, r._revCache.set(e + s, o.rev)), n;
          }, {}), i.has_more ? o(i.cursor).then(function (e) {
            return Object.assign(s, e);
          }) : Promise.resolve(s));
        },
            o = function (e) {
          var t = {
            body: {
              cursor: e
            }
          };
          return r._request("POST", "https://api.dropboxapi.com/2/files/list_folder/continue", t).then(n);
        };

        return this._request("POST", "https://api.dropboxapi.com/2/files/list_folder", {
          body: {
            path: v(e)
          }
        }).then(n).then(function (r) {
          return Promise.resolve({
            statusCode: 200,
            body: r,
            contentType: "application/json; charset=UTF-8",
            revision: t.get(e)
          });
        });
      },
      get: function (e, t) {
        var r = this;
        if (!this.connected) return Promise.reject("not connected (path: " + e + ")");

        var n = this,
            o = this._revCache.get(e);

        if (null === o) return Promise.resolve({
          statusCode: 404
        });

        if (t && t.ifNoneMatch) {
          if (!this._initialFetchDone) return this.fetchDelta().then(function () {
            return r.get(e, t);
          });
          if (o && o === t.ifNoneMatch) return Promise.resolve({
            statusCode: 304
          });
        }

        if ("/" === e.substr(-1)) return this._getFolder(e, t);
        var i = {
          headers: {
            "Dropbox-API-Arg": JSON.stringify({
              path: v(e)
            })
          },
          responseType: "arraybuffer"
        };
        return t && t.ifNoneMatch && (i.headers["If-None-Match"] = t.ifNoneMatch), this._request("GET", "https://content.dropboxapi.com/2/files/download", i).then(function (t) {
          var r,
              o,
              i,
              s,
              a = t.status;
          return 200 !== a && 409 !== a ? Promise.resolve({
            statusCode: a
          }) : (r = t.getResponseHeader("Dropbox-API-Result"), g(t.response, "UTF-8").then(function (u) {
            o = u, 409 === a && (r = o);

            try {
              r = JSON.parse(r);
            } catch (e) {
              return Promise.reject(e);
            }

            if (409 === a) return b(r, ["path", "not_found"]) ? {
              statusCode: 404
            } : Promise.reject(new Error('API error while downloading file ("' + e + '"): ' + r.error_summary));
            if (i = t.getResponseHeader("Content-Type"), s = r.rev, n._revCache.set(e, s), n._shareIfNeeded(e), m(u, i)) o = t.response;else try {
              o = JSON.parse(o), i = "application/json; charset=UTF-8";
            } catch (e) {}
            return {
              statusCode: a,
              body: o,
              contentType: i,
              revision: s
            };
          }));
        });
      },
      put: function (e, t, r, n) {
        var o = this;
        if (!this.connected) throw new Error("not connected (path: " + e + ")");

        var i = this._revCache.get(e);

        if (n && n.ifMatch && i && i !== n.ifMatch) return Promise.resolve({
          statusCode: 412,
          revision: i
        });
        if (n && "*" === n.ifNoneMatch && i && "rev" !== i) return Promise.resolve({
          statusCode: 412,
          revision: i
        });
        if (!r.match(/charset=/) && _(t) && (r += "; charset=binary"), t.length > 157286400) return Promise.reject(new Error("Cannot upload file larger than 150MB"));
        var s = n && (n.ifMatch || "*" === n.ifNoneMatch),
            a = {
          body: t,
          contentType: r,
          path: e
        };
        return (s ? this._getMetadata(e).then(function (e) {
          return n && "*" === n.ifNoneMatch && e ? Promise.resolve({
            statusCode: 412,
            revision: e.rev
          }) : n && n.ifMatch && e && e.rev !== n.ifMatch ? Promise.resolve({
            statusCode: 412,
            revision: e.rev
          }) : o._uploadSimple(a);
        }) : o._uploadSimple(a)).then(function (t) {
          return o._shareIfNeeded(e), t;
        });
      },
      delete: function (e, t) {
        var r = this;
        if (!this.connected) throw new Error("not connected (path: " + e + ")");

        var n = this._revCache.get(e);

        return t && t.ifMatch && n && t.ifMatch !== n ? Promise.resolve({
          statusCode: 412,
          revision: n
        }) : t && t.ifMatch ? this._getMetadata(e).then(function (n) {
          return t && t.ifMatch && n && n.rev !== t.ifMatch ? Promise.resolve({
            statusCode: 412,
            revision: n.rev
          }) : r._deleteSimple(e);
        }) : this._deleteSimple(e);
      },
      _shareIfNeeded: function (e) {
        e.match(/^\/public\/.*[^\/]$/) && void 0 === this._itemRefs[e] && this.share(e);
      },
      share: function (e) {
        var t = this,
            r = {
          body: {
            path: v(e)
          }
        };
        return this._request("POST", "https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings", r).then(function (r) {
          if (200 !== r.status && 409 !== r.status) return Promise.reject(new Error("Invalid response status:" + r.status));
          var n;

          try {
            n = JSON.parse(r.responseText);
          } catch (e) {
            return Promise.reject(new Error("Invalid response body: " + r.responseText));
          }

          return 409 === r.status ? b(n, ["shared_link_already_exists"]) ? t._getSharedLink(e) : Promise.reject(new Error("API error: " + n.error_summary)) : Promise.resolve(n.url);
        }).then(function (r) {
          return t._itemRefs[e] = r, o && localStorage.setItem(f + ":shares", JSON.stringify(t._itemRefs)), Promise.resolve(r);
        }, function (t) {
          return t.message = 'Sharing Dropbox file or folder ("' + e + '") failed: ' + t.message, Promise.reject(t);
        });
      },
      info: function () {
        return this._request("POST", "https://api.dropboxapi.com/2/users/get_current_account", {}).then(function (e) {
          var t = e.responseText;

          try {
            t = JSON.parse(t);
          } catch (e) {
            return Promise.reject(new Error("Could not query current account info: Invalid API response: " + t));
          }

          return Promise.resolve({
            email: t.email
          });
        });
      },
      _request: function (e, t, r) {
        var o = this;
        return r.headers || (r.headers = {}), r.headers.Authorization = "Bearer " + this.token, "object" !== n(r.body) || _(r.body) || (r.body = JSON.stringify(r.body), r.headers["Content-Type"] = "application/json; charset=UTF-8"), this.rs._emit("wire-busy", {
          method: e,
          isFolder: d(t)
        }), a.request.call(this, e, t, r).then(function (n) {
          return n && 503 === n.status ? (o.online && (o.online = !1, o.rs._emit("network-offline")), setTimeout(o._request(e, t, r), 3210)) : (o.online || (o.online = !0, o.rs._emit("network-online")), o.rs._emit("wire-done", {
            method: e,
            isFolder: d(t),
            success: !0
          }), Promise.resolve(n));
        }, function (r) {
          return o.online && (o.online = !1, o.rs._emit("network-offline")), o.rs._emit("wire-done", {
            method: e,
            isFolder: d(t),
            success: !1
          }), Promise.reject(r);
        });
      },
      fetchDelta: function () {
        var e = this;
        if (this._fetchDeltaPromise) return this._fetchDeltaPromise;
        var t = Array.prototype.slice.call(arguments),
            r = this;
        return this._fetchDeltaPromise = function e(n) {
          var o,
              s = "https://api.dropboxapi.com/2/files/list_folder";
          return "string" == typeof n ? (s += "/continue", o = {
            cursor: n
          }) : o = {
            path: "/remotestorage",
            recursive: !0,
            include_deleted: !0
          }, r._request("POST", s, {
            body: o
          }).then(function (o) {
            if (401 === o.status) return r.rs._emit("error", new i.Unauthorized()), Promise.resolve(t);
            if (200 !== o.status && 409 !== o.status) return Promise.reject(new Error("Invalid response status: " + o.status));
            var s;

            try {
              s = JSON.parse(o.responseText);
            } catch (e) {
              return Promise.reject(new Error("Invalid response body: " + o.responseText));
            }

            if (409 === o.status) {
              if (!b(s, ["path", "not_found"])) return Promise.reject(new Error("API returned an error: " + s.error_summary));
              s = {
                cursor: null,
                entries: [],
                has_more: !1
              };
            }

            if (n || r._revCache.deactivatePropagation(), s.entries.forEach(function (e) {
              var t = e.path_lower.substr("/remotestorage".length);
              "deleted" === e[".tag"] ? (r._revCache.delete(t), r._revCache.delete(t + "/")) : "file" === e[".tag"] && r._revCache.set(t, e.rev);
            }), r._fetchDeltaCursor = s.cursor, s.has_more) return e(s.cursor);
            r._revCache.activatePropagation(), r._initialFetchDone = !0;
          }).catch(function (e) {
            return "timeout" === e || e instanceof ProgressEvent ? Promise.resolve() : Promise.reject(e);
          });
        }(r._fetchDeltaCursor).catch(function (t) {
          return "object" === n(t) && "message" in t ? t.message = "Dropbox: fetchDelta: " + t.message : t = "Dropbox: fetchDelta: ".concat(t), e._fetchDeltaPromise = null, Promise.reject(t);
        }).then(function () {
          return e._fetchDeltaPromise = null, Promise.resolve(t);
        }), this._fetchDeltaPromise;
      },
      _getMetadata: function (e) {
        var t = {
          path: v(e)
        };
        return this._request("POST", "https://api.dropboxapi.com/2/files/get_metadata", {
          body: t
        }).then(function (e) {
          if (200 !== e.status && 409 !== e.status) return Promise.reject(new Error("Invalid response status:" + e.status));
          var t;

          try {
            t = JSON.parse(e.responseText);
          } catch (t) {
            return Promise.reject(new Error("Invalid response body: " + e.responseText));
          }

          return 409 === e.status ? b(t, ["path", "not_found"]) ? Promise.resolve() : Promise.reject(new Error("API error: " + t.error_summary)) : Promise.resolve(t);
        }).then(void 0, function (t) {
          return t.message = 'Could not load metadata for file or folder ("' + e + '"): ' + t.message, Promise.reject(t);
        });
      },
      _uploadSimple: function (e) {
        var t = this,
            r = {
          path: v(e.path),
          mode: {
            ".tag": "overwrite"
          },
          mute: !0
        };
        return e.ifMatch && (r.mode = {
          ".tag": "update",
          update: e.ifMatch
        }), this._request("POST", "https://content.dropboxapi.com/2/files/upload", {
          body: e.body,
          headers: {
            "Content-Type": "application/octet-stream",
            "Dropbox-API-Arg": JSON.stringify(r)
          }
        }).then(function (r) {
          if (200 !== r.status && 409 !== r.status) return Promise.resolve({
            statusCode: r.status
          });
          var n = r.responseText;

          try {
            n = JSON.parse(n);
          } catch (e) {
            return Promise.reject(new Error("Invalid API result: " + n));
          }

          return 409 === r.status ? b(n, ["path", "conflict"]) ? t._getMetadata(e.path).then(function (e) {
            return Promise.resolve({
              statusCode: 412,
              revision: e.rev
            });
          }) : Promise.reject(new Error("API error: " + n.error_summary)) : (t._revCache.set(e.path, n.rev), Promise.resolve({
            statusCode: r.status,
            revision: n.rev
          }));
        });
      },
      _deleteSimple: function (e) {
        var t = this,
            r = {
          path: v(e)
        };
        return this._request("POST", "https://api.dropboxapi.com/2/files/delete", {
          body: r
        }).then(function (e) {
          if (200 !== e.status && 409 !== e.status) return Promise.resolve({
            statusCode: e.status
          });
          var t = e.responseText;

          try {
            t = JSON.parse(t);
          } catch (e) {
            return Promise.reject(new Error("Invalid response body: " + t));
          }

          return 409 === e.status ? b(t, ["path_lookup", "not_found"]) ? Promise.resolve({
            statusCode: 404
          }) : Promise.reject(new Error("API error: " + t.error_summary)) : Promise.resolve({
            statusCode: 200
          });
        }).then(function (r) {
          return 200 !== r.statusCode && 404 !== r.statusCode || (t._revCache.delete(e), delete t._itemRefs[e]), Promise.resolve(r);
        }, function (t) {
          return t.message = 'Could not delete Dropbox file or folder ("' + e + '"): ' + t.message, Promise.reject(t);
        });
      },
      _getSharedLink: function (e) {
        var t = {
          body: {
            path: v(e),
            direct_only: !0
          }
        };
        return this._request("POST", "https://api.dropbox.com/2/sharing/list_shared_links", t).then(function (e) {
          if (200 !== e.status && 409 !== e.status) return Promise.reject(new Error("Invalid response status: " + e.status));
          var t;

          try {
            t = JSON.parse(e.responseText);
          } catch (t) {
            return Promise.reject(new Error("Invalid response body: " + e.responseText));
          }

          return 409 === e.status ? Promise.reject(new Error("API error: " + e.error_summary)) : t.links.length ? Promise.resolve(t.links[0].url) : Promise.reject(new Error("No links returned"));
        }, function (t) {
          return t.message = 'Could not get link to a shared file or folder ("' + e + '"): ' + t.message, Promise.reject(t);
        });
      }
    }, w._rs_init = function (e) {
      o = u.localStorageAvailable(), e.apiKeys.dropbox && (e.dropbox = new w(e)), "dropbox" === e.backend && S(e);
    }, w._rs_supported = function () {
      return !0;
    }, w._rs_cleanup = function (e) {
      T(e), o && localStorage.removeItem(f), e.setBackend(void 0);
    }, e.exports = w;
  }, function (e, t, r) {
    var n = r(2),
        o = "undefined" != typeof window ? "browser" : "node",
        i = {},
        s = function () {
      return i;
    };

    s.isBrowser = function () {
      return "browser" === o;
    }, s.isNode = function () {
      return "node" === o;
    }, s.goBackground = function () {
      s._emit("background");
    }, s.goForeground = function () {
      s._emit("foreground");
    }, s._rs_init = function () {
      function e() {
        document[i.hiddenProperty] ? s.goBackground() : s.goForeground();
      }

      n(s, "background", "foreground"), "browser" === o && (void 0 !== document.hidden ? (i.hiddenProperty = "hidden", i.visibilityChangeEvent = "visibilitychange") : void 0 !== document.mozHidden ? (i.hiddenProperty = "mozHidden", i.visibilityChangeEvent = "mozvisibilitychange") : void 0 !== document.msHidden ? (i.hiddenProperty = "msHidden", i.visibilityChangeEvent = "msvisibilitychange") : void 0 !== document.webkitHidden && (i.hiddenProperty = "webkitHidden", i.visibilityChangeEvent = "webkitvisibilitychange"), document.addEventListener(i.visibilityChangeEvent, e, !1), e());
    }, s._rs_cleanup = function () {}, e.exports = s;
  }, function (e, t, r) {
    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o,
        i = r(5),
        s = r(6),
        a = r(2),
        u = r(0),
        c = "https://www.googleapis.com",
        l = "remotestorage:googledrive",
        h = "/remotestorage",
        f = u.isFolder,
        d = u.cleanPath,
        p = u.shouldBeTreatedAsBinary,
        m = u.getJSONFromLocalStorage,
        y = u.getTextFromArrayBuffer;

    function g(e) {
      return "/" === e.substr(-1) && (e = e.substr(0, e.length - 1)), decodeURIComponent(e);
    }

    function v(e) {
      return e.replace(/[^\/]+\/?$/, "");
    }

    function b(e) {
      var t = e.split("/");
      return "/" === e.substr(-1) ? t[t.length - 2] + "/" : t[t.length - 1];
    }

    function _(e) {
      return d("".concat(h, "/").concat(e));
    }

    function w(e) {
      return e.replace(/^["'](.*)["']$/, "$1");
    }

    var P = function (e) {
      this.maxAge = e, this._items = {};
    };

    P.prototype = {
      get: function (e) {
        var t = this._items[e],
            r = new Date().getTime();
        return t && t.t >= r - this.maxAge ? t.v : void 0;
      },
      set: function (e, t) {
        this._items[e] = {
          v: t,
          t: new Date().getTime()
        };
      }
    };

    var E = function (e, t) {
      if (a(this, "connected", "not-connected"), this.rs = e, this.clientId = t, this._fileIdCache = new P(300), o = u.localStorageAvailable()) {
        var r = m(l);
        r && this.configure(r);
      }
    };

    E.prototype = {
      connected: !1,
      online: !0,
      configure: function (e) {
        var t = this;
        void 0 !== e.userAddress && (this.userAddress = e.userAddress), void 0 !== e.token && (this.token = e.token);

        var r = function () {
          o && localStorage.setItem(l, JSON.stringify({
            userAddress: this.userAddress,
            token: this.token
          }));
        },
            n = function () {
          this.connected = !1, delete this.token, o && localStorage.removeItem(l);
        };

        this.token ? (this.connected = !0, this.userAddress ? (this._emit("connected"), r.apply(this)) : this.info().then(function (e) {
          t.userAddress = e.user.emailAddress, t._emit("connected"), r.apply(t);
        }).catch(function () {
          n.apply(t), t.rs._emit("error", new Error("Could not fetch user info."));
        })) : n.apply(this);
      },
      connect: function () {
        this.rs.setBackend("googledrive"), this.rs.authorize({
          authURL: "https://accounts.google.com/o/oauth2/auth",
          scope: "https://www.googleapis.com/auth/drive",
          clientId: this.clientId
        });
      },
      stopWaitingForToken: function () {
        this.connected || this._emit("not-connected");
      },
      get: function (e, t) {
        return "/" === e.substr(-1) ? this._getFolder(_(e), t) : this._getFile(_(e), t);
      },
      put: function (e, t, r, n) {
        var o = this,
            i = _(e);

        function s(e) {
          if (e.status >= 200 && e.status < 300) {
            var t = JSON.parse(e.responseText),
                r = w(t.etag);
            return Promise.resolve({
              statusCode: 200,
              contentType: t.mimeType,
              revision: r
            });
          }

          return 412 === e.status ? Promise.resolve({
            statusCode: 412,
            revision: "conflict"
          }) : Promise.reject("PUT failed with status " + e.status + " (" + e.responseText + ")");
        }

        return this._getFileId(i).then(function (e) {
          return e ? n && "*" === n.ifNoneMatch ? s({
            status: 412
          }) : o._updateFile(e, i, t, r, n).then(s) : o._createFile(i, t, r, n).then(s);
        });
      },
      delete: function (e, t) {
        var r = this,
            o = _(e);

        return this._getFileId(o).then(function (e) {
          return e ? r._getMeta(e).then(function (o) {
            var i;
            return "object" === n(o) && "string" == typeof o.etag && (i = w(o.etag)), t && t.ifMatch && t.ifMatch !== i ? {
              statusCode: 412,
              revision: i
            } : r._request("DELETE", c + "/drive/v2/files/" + e, {}).then(function (e) {
              return 200 === e.status || 204 === e.status ? {
                statusCode: 200
              } : Promise.reject("Delete failed: " + e.status + " (" + e.responseText + ")");
            });
          }) : Promise.resolve({
            statusCode: 200
          });
        });
      },
      info: function () {
        return this._request("GET", "https://www.googleapis.com/drive/v2/about?fields=user", {}).then(function (e) {
          try {
            var t = JSON.parse(e.responseText);
            return Promise.resolve(t);
          } catch (e) {
            return Promise.reject(e);
          }
        });
      },
      _updateFile: function (e, t, r, n, o) {
        var i = this,
            s = {
          mimeType: n
        },
            a = {
          "Content-Type": "application/json; charset=UTF-8"
        };
        return o && o.ifMatch && (a["If-Match"] = '"' + o.ifMatch + '"'), this._request("PUT", c + "/upload/drive/v2/files/" + e + "?uploadType=resumable", {
          body: JSON.stringify(s),
          headers: a
        }).then(function (e) {
          return 412 === e.status ? e : i._request("PUT", e.getResponseHeader("Location"), {
            body: n.match(/^application\/json/) ? JSON.stringify(r) : r
          });
        });
      },
      _createFile: function (e, t, r) {
        var n = this;
        return this._getParentId(e).then(function (o) {
          var i = {
            title: g(b(e)),
            mimeType: r,
            parents: [{
              kind: "drive#fileLink",
              id: o
            }]
          };
          return n._request("POST", c + "/upload/drive/v2/files?uploadType=resumable", {
            body: JSON.stringify(i),
            headers: {
              "Content-Type": "application/json; charset=UTF-8"
            }
          }).then(function (e) {
            return n._request("POST", e.getResponseHeader("Location"), {
              body: r.match(/^application\/json/) ? JSON.stringify(t) : t
            });
          });
        });
      },
      _getFile: function (e, t) {
        var r = this;
        return this._getFileId(e).then(function (e) {
          return r._getMeta(e).then(function (e) {
            var o;
            if ("object" === n(e) && "string" == typeof e.etag && (o = w(e.etag)), t && t.ifNoneMatch && o === t.ifNoneMatch) return Promise.resolve({
              statusCode: 304
            });

            if (!e.downloadUrl) {
              if (!e.exportLinks || !e.exportLinks["text/html"]) return Promise.resolve({
                statusCode: 200,
                body: "",
                contentType: e.mimeType,
                revision: o
              });
              e.mimeType += ";export=text/html", e.downloadUrl = e.exportLinks["text/html"];
            }

            return r._request("GET", e.downloadUrl, {
              responseType: "arraybuffer"
            }).then(function (t) {
              return y(t.response, "UTF-8").then(function (r) {
                var n = r;
                if (e.mimeType.match(/^application\/json/)) try {
                  n = JSON.parse(n);
                } catch (e) {} else p(r, e.mimeType) && (n = t.response);
                return {
                  statusCode: 200,
                  body: n,
                  contentType: e.mimeType,
                  revision: o
                };
              });
            });
          });
        });
      },
      _getFolder: function (e) {
        var t = this;
        return this._getFileId(e).then(function (r) {
          var n, o, i, s;
          return r ? (n = "'" + r + "' in parents", "items(downloadUrl,etag,fileSize,id,mimeType,title)", t._request("GET", c + "/drive/v2/files?q=" + encodeURIComponent(n) + "&fields=" + encodeURIComponent("items(downloadUrl,etag,fileSize,id,mimeType,title)") + "&maxResults=1000", {}).then(function (r) {
            if (200 !== r.status) return Promise.reject("request failed or something: " + r.status);

            try {
              o = JSON.parse(r.responseText);
            } catch (e) {
              return Promise.reject("non-JSON response from GoogleDrive");
            }

            s = {};
            var n = !0,
                a = !1,
                u = void 0;

            try {
              for (var c, l = o.items[Symbol.iterator](); !(n = (c = l.next()).done); n = !0) {
                var h = c.value;
                i = w(h.etag), "application/vnd.google-apps.folder" === h.mimeType ? (t._fileIdCache.set(e + h.title + "/", h.id), s[h.title + "/"] = {
                  ETag: i
                }) : (t._fileIdCache.set(e + h.title, h.id), s[h.title] = {
                  ETag: i,
                  "Content-Type": h.mimeType,
                  "Content-Length": h.fileSize
                });
              }
            } catch (e) {
              a = !0, u = e;
            } finally {
              try {
                n || null == l.return || l.return();
              } finally {
                if (a) throw u;
              }
            }

            return Promise.resolve({
              statusCode: 200,
              body: s,
              contentType: "application/json; charset=UTF-8",
              revision: void 0
            });
          })) : Promise.resolve({
            statusCode: 404
          });
        });
      },
      _getParentId: function (e) {
        var t = this,
            r = v(e);
        return this._getFileId(r).then(function (e) {
          return e ? Promise.resolve(e) : t._createFolder(r);
        });
      },
      _createFolder: function (e) {
        var t = this;
        return this._getParentId(e).then(function (r) {
          return t._request("POST", c + "/drive/v2/files", {
            body: JSON.stringify({
              title: g(b(e)),
              mimeType: "application/vnd.google-apps.folder",
              parents: [{
                id: r
              }]
            }),
            headers: {
              "Content-Type": "application/json; charset=UTF-8"
            }
          }).then(function (e) {
            var t = JSON.parse(e.responseText);
            return Promise.resolve(t.id);
          });
        });
      },
      _getFileId: function (e) {
        var t,
            r = this;
        return "/" === e ? Promise.resolve("root") : (t = this._fileIdCache.get(e)) ? Promise.resolve(t) : this._getFolder(v(e)).then(function () {
          return (t = r._fileIdCache.get(e)) ? Promise.resolve(t) : "/" === e.substr(-1) ? r._createFolder(e).then(function () {
            return r._getFileId(e);
          }) : Promise.resolve();
        });
      },
      _getMeta: function (e) {
        return this._request("GET", c + "/drive/v2/files/" + e, {}).then(function (t) {
          return 200 === t.status ? Promise.resolve(JSON.parse(t.responseText)) : Promise.reject("request (getting metadata for " + e + ") failed with status: " + t.status);
        });
      },
      _request: function (e, t, r) {
        var n = this;
        return r.headers || (r.headers = {}), r.headers.Authorization = "Bearer " + this.token, this.rs._emit("wire-busy", {
          method: e,
          isFolder: f(t)
        }), s.request.call(this, e, t, r).then(function (r) {
          return r && 401 === r.status ? void n.connect() : (n.online || (n.online = !0, n.rs._emit("network-online")), n.rs._emit("wire-done", {
            method: e,
            isFolder: f(t),
            success: !0
          }), Promise.resolve(r));
        }, function (r) {
          return n.online && (n.online = !1, n.rs._emit("network-offline")), n.rs._emit("wire-done", {
            method: e,
            isFolder: f(t),
            success: !1
          }), Promise.reject(r);
        });
      }
    }, E._rs_init = function (e) {
      var t,
          r = e.apiKeys.googledrive;
      r && (e.googledrive = new E(e, r.clientId), "googledrive" === e.backend && (e._origRemote = e.remote, e.remote = e.googledrive, (t = e)._origBaseClientGetItemURL || (t._origBaseClientGetItemURL = i.prototype.getItemURL, i.prototype.getItemURL = function () {
        throw new Error("getItemURL is not implemented for Google Drive yet");
      })));
    }, E._rs_supported = function () {
      return !0;
    }, E._rs_cleanup = function (e) {
      var t;
      e.setBackend(void 0), e._origRemote && (e.remote = e._origRemote, delete e._origRemote), (t = e)._origBaseClientGetItemURL && (i.prototype.getItemURL = t._origBaseClientGetItemURL, delete t._origBaseClientGetItemURL);
    }, e.exports = E;
  }, function (e, t, r) {
    "use strict";

    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o,
        i = r(1),
        s = r(0),
        a = r(25),
        u = {},
        c = function (e) {
      return new Promise(function (t, r) {
        return e in u ? t(u[e]) : new a({
          tls_only: !1,
          uri_fallback: !0,
          request_timeout: 5e3
        }).lookup(e, function (s, a) {
          if (s) return r(s);
          if ("object" !== n(a.idx.links.remotestorage) || "number" != typeof a.idx.links.remotestorage.length || a.idx.links.remotestorage.length <= 0) return i("[Discover] WebFinger record for " + e + " does not have remotestorage defined in the links section ", JSON.stringify(a.json)), r("WebFinger record for " + e + " does not have remotestorage defined in the links section.");
          var c = a.idx.links.remotestorage[0],
              l = c.properties["http://tools.ietf.org/html/rfc6749#section-4.2"] || c.properties["auth-endpoint"],
              h = c.properties["http://remotestorage.io/spec/version"] || c.type;
          return u[e] = {
            href: c.href,
            storageApi: h,
            authURL: l,
            properties: c.properties
          }, o && (localStorage["remotestorage:discover"] = JSON.stringify({
            cache: u
          })), t(u[e]);
        });
      });
    };

    (c.DiscoveryError = function (e) {
      this.name = "DiscoveryError", this.message = e, this.stack = new Error().stack;
    }).prototype = Object.create(Error.prototype), c.DiscoveryError.prototype.constructor = c.DiscoveryError, c._rs_init = function () {
      if (o = s.localStorageAvailable()) {
        var e;

        try {
          e = JSON.parse(localStorage["remotestorage:discover"]);
        } catch (e) {}

        e && (u = e.cache);
      }
    }, c._rs_supported = function () {
      return !!s.globalContext.XMLHttpRequest;
    }, c._rs_cleanup = function () {
      o && delete localStorage["remotestorage:discover"];
    }, e.exports = c;
  }, function (e, t) {
    var r = function () {
      this.reset();
    };

    r.prototype = {
      claim: function (e, t) {
        if ("string" != typeof e || -1 !== e.indexOf("/") || 0 === e.length) throw new Error("Scope should be a non-empty string without forward slashes");
        if (!t.match(/^rw?$/)) throw new Error("Mode should be either 'r' or 'rw'");
        this._adjustRootPaths(e), this.scopeModeMap[e] = t;
      },
      get: function (e) {
        return this.scopeModeMap[e];
      },
      remove: function (e) {
        var t,
            r = {};

        for (t in this.scopeModeMap) r[t] = this.scopeModeMap[t];

        for (t in this.reset(), delete r[e], r) this.set(t, r[t]);
      },
      checkPermission: function (e, t) {
        var r = this.get(e);
        return r && ("r" === t || "rw" === r);
      },
      checkPathPermission: function (e, t) {
        return !!this.checkPermission("*", t) || !!this.checkPermission(this._getModuleName(e), t);
      },
      reset: function () {
        this.rootPaths = [], this.scopeModeMap = {};
      },
      _getModuleName: function (e) {
        if ("/" !== e[0]) throw new Error("Path should start with a slash");
        var t = e.replace(/^\/public/, "").match(/^\/([^\/]*)\//);
        return t ? t[1] : "*";
      },
      _adjustRootPaths: function (e) {
        "*" in this.scopeModeMap || "*" === e ? this.rootPaths = ["/"] : e in this.scopeModeMap || (this.rootPaths.push("/" + e + "/"), this.rootPaths.push("/public/" + e + "/"));
      },
      _scopeNameForParameter: function (e) {
        if ("*" === e.name && this.storageType) {
          if ("2012.04" === this.storageType) return "";
          if (this.storageType.match(/remotestorage-0[01]/)) return "root";
        }

        return e.name;
      },
      setStorageType: function (e) {
        this.storageType = e;
      }
    }, Object.defineProperty(r.prototype, "scopes", {
      get: function () {
        return Object.keys(this.scopeModeMap).map(function (e) {
          return {
            name: e,
            mode: this.scopeModeMap[e]
          };
        }.bind(this));
      }
    }), Object.defineProperty(r.prototype, "scopeParameter", {
      get: function () {
        return this.scopes.map(function (e) {
          return this._scopeNameForParameter(e) + ":" + e.mode;
        }.bind(this)).join(" ");
      }
    }), r._rs_init = function () {}, e.exports = r;
  }, function (e, t, r) {
    var n = r(0),
        o = r(1),
        i = n.containingFolder,
        s = function () {
      this.reset();
    };

    s.prototype = {
      pendingActivations: [],
      set: function (e, t) {
        if ("string" != typeof e) throw new Error("path should be a string");
        if (!n.isFolder(e)) throw new Error("path should be a folder");
        if (this._remoteStorage && this._remoteStorage.access && !this._remoteStorage.access.checkPathPermission(e, "r")) throw new Error('No access to path "' + e + '". You have to claim access to it first.');
        if (!t.match(/^(FLUSH|SEEN|ALL)$/)) throw new Error("strategy should be 'FLUSH', 'SEEN', or 'ALL'");
        this._rootPaths[e] = t, "ALL" === t && (this.activateHandler ? this.activateHandler(e) : this.pendingActivations.push(e));
      },
      enable: function (e) {
        this.set(e, "ALL");
      },
      disable: function (e) {
        this.set(e, "FLUSH");
      },
      onActivate: function (e) {
        var t;

        for (o("[Caching] Setting activate handler", e, this.pendingActivations), this.activateHandler = e, t = 0; t < this.pendingActivations.length; t++) e(this.pendingActivations[t]);

        delete this.pendingActivations;
      },
      checkPath: function (e) {
        return void 0 !== this._rootPaths[e] ? this._rootPaths[e] : "/" === e ? "SEEN" : this.checkPath(i(e));
      },
      reset: function () {
        this._rootPaths = {}, this._remoteStorage = null;
      }
    }, s._rs_init = function (e) {
      this._remoteStorage = e;
    }, e.exports = s;
  }, function (e, t, r) {
    e.exports = r(9);
  }, function (e, t, r) {
    "use strict";

    (function (e) {
      /*!
       * The buffer module from node.js, for the browser.
       *
       * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
       * @license  MIT
       */
      var n = r(19),
          o = r(20),
          i = r(21);

      function s() {
        return u.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823;
      }

      function a(e, t) {
        if (s() < t) throw new RangeError("Invalid typed array length");
        return u.TYPED_ARRAY_SUPPORT ? (e = new Uint8Array(t)).__proto__ = u.prototype : (null === e && (e = new u(t)), e.length = t), e;
      }

      function u(e, t, r) {
        if (!(u.TYPED_ARRAY_SUPPORT || this instanceof u)) return new u(e, t, r);

        if ("number" == typeof e) {
          if ("string" == typeof t) throw new Error("If encoding is specified then the first argument must be a string");
          return h(this, e);
        }

        return c(this, e, t, r);
      }

      function c(e, t, r, n) {
        if ("number" == typeof t) throw new TypeError('"value" argument must not be a number');
        return "undefined" != typeof ArrayBuffer && t instanceof ArrayBuffer ? function (e, t, r, n) {
          if (t.byteLength, r < 0 || t.byteLength < r) throw new RangeError("'offset' is out of bounds");
          if (t.byteLength < r + (n || 0)) throw new RangeError("'length' is out of bounds");
          t = void 0 === r && void 0 === n ? new Uint8Array(t) : void 0 === n ? new Uint8Array(t, r) : new Uint8Array(t, r, n);
          u.TYPED_ARRAY_SUPPORT ? (e = t).__proto__ = u.prototype : e = f(e, t);
          return e;
        }(e, t, r, n) : "string" == typeof t ? function (e, t, r) {
          "string" == typeof r && "" !== r || (r = "utf8");
          if (!u.isEncoding(r)) throw new TypeError('"encoding" must be a valid string encoding');
          var n = 0 | p(t, r),
              o = (e = a(e, n)).write(t, r);
          o !== n && (e = e.slice(0, o));
          return e;
        }(e, t, r) : function (e, t) {
          if (u.isBuffer(t)) {
            var r = 0 | d(t.length);
            return 0 === (e = a(e, r)).length ? e : (t.copy(e, 0, 0, r), e);
          }

          if (t) {
            if ("undefined" != typeof ArrayBuffer && t.buffer instanceof ArrayBuffer || "length" in t) return "number" != typeof t.length || (n = t.length) != n ? a(e, 0) : f(e, t);
            if ("Buffer" === t.type && i(t.data)) return f(e, t.data);
          }

          var n;
          throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.");
        }(e, t);
      }

      function l(e) {
        if ("number" != typeof e) throw new TypeError('"size" argument must be a number');
        if (e < 0) throw new RangeError('"size" argument must not be negative');
      }

      function h(e, t) {
        if (l(t), e = a(e, t < 0 ? 0 : 0 | d(t)), !u.TYPED_ARRAY_SUPPORT) for (var r = 0; r < t; ++r) e[r] = 0;
        return e;
      }

      function f(e, t) {
        var r = t.length < 0 ? 0 : 0 | d(t.length);
        e = a(e, r);

        for (var n = 0; n < r; n += 1) e[n] = 255 & t[n];

        return e;
      }

      function d(e) {
        if (e >= s()) throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + s().toString(16) + " bytes");
        return 0 | e;
      }

      function p(e, t) {
        if (u.isBuffer(e)) return e.length;
        if ("undefined" != typeof ArrayBuffer && "function" == typeof ArrayBuffer.isView && (ArrayBuffer.isView(e) || e instanceof ArrayBuffer)) return e.byteLength;
        "string" != typeof e && (e = "" + e);
        var r = e.length;
        if (0 === r) return 0;

        for (var n = !1;;) switch (t) {
          case "ascii":
          case "latin1":
          case "binary":
            return r;

          case "utf8":
          case "utf-8":
          case void 0:
            return B(e).length;

          case "ucs2":
          case "ucs-2":
          case "utf16le":
          case "utf-16le":
            return 2 * r;

          case "hex":
            return r >>> 1;

          case "base64":
            return q(e).length;

          default:
            if (n) return B(e).length;
            t = ("" + t).toLowerCase(), n = !0;
        }
      }

      function m(e, t, r) {
        var n = e[t];
        e[t] = e[r], e[r] = n;
      }

      function y(e, t, r, n, o) {
        if (0 === e.length) return -1;

        if ("string" == typeof r ? (n = r, r = 0) : r > 2147483647 ? r = 2147483647 : r < -2147483648 && (r = -2147483648), r = +r, isNaN(r) && (r = o ? 0 : e.length - 1), r < 0 && (r = e.length + r), r >= e.length) {
          if (o) return -1;
          r = e.length - 1;
        } else if (r < 0) {
          if (!o) return -1;
          r = 0;
        }

        if ("string" == typeof t && (t = u.from(t, n)), u.isBuffer(t)) return 0 === t.length ? -1 : g(e, t, r, n, o);
        if ("number" == typeof t) return t &= 255, u.TYPED_ARRAY_SUPPORT && "function" == typeof Uint8Array.prototype.indexOf ? o ? Uint8Array.prototype.indexOf.call(e, t, r) : Uint8Array.prototype.lastIndexOf.call(e, t, r) : g(e, [t], r, n, o);
        throw new TypeError("val must be string, number or Buffer");
      }

      function g(e, t, r, n, o) {
        var i,
            s = 1,
            a = e.length,
            u = t.length;

        if (void 0 !== n && ("ucs2" === (n = String(n).toLowerCase()) || "ucs-2" === n || "utf16le" === n || "utf-16le" === n)) {
          if (e.length < 2 || t.length < 2) return -1;
          s = 2, a /= 2, u /= 2, r /= 2;
        }

        function c(e, t) {
          return 1 === s ? e[t] : e.readUInt16BE(t * s);
        }

        if (o) {
          var l = -1;

          for (i = r; i < a; i++) if (c(e, i) === c(t, -1 === l ? 0 : i - l)) {
            if (-1 === l && (l = i), i - l + 1 === u) return l * s;
          } else -1 !== l && (i -= i - l), l = -1;
        } else for (r + u > a && (r = a - u), i = r; i >= 0; i--) {
          for (var h = !0, f = 0; f < u; f++) if (c(e, i + f) !== c(t, f)) {
            h = !1;
            break;
          }

          if (h) return i;
        }

        return -1;
      }

      function v(e, t, r, n) {
        r = Number(r) || 0;
        var o = e.length - r;
        n ? (n = Number(n)) > o && (n = o) : n = o;
        var i = t.length;
        if (i % 2 != 0) throw new TypeError("Invalid hex string");
        n > i / 2 && (n = i / 2);

        for (var s = 0; s < n; ++s) {
          var a = parseInt(t.substr(2 * s, 2), 16);
          if (isNaN(a)) return s;
          e[r + s] = a;
        }

        return s;
      }

      function b(e, t, r, n) {
        return J(B(t, e.length - r), e, r, n);
      }

      function _(e, t, r, n) {
        return J(function (e) {
          for (var t = [], r = 0; r < e.length; ++r) t.push(255 & e.charCodeAt(r));

          return t;
        }(t), e, r, n);
      }

      function w(e, t, r, n) {
        return _(e, t, r, n);
      }

      function P(e, t, r, n) {
        return J(q(t), e, r, n);
      }

      function E(e, t, r, n) {
        return J(function (e, t) {
          for (var r, n, o, i = [], s = 0; s < e.length && !((t -= 2) < 0); ++s) r = e.charCodeAt(s), n = r >> 8, o = r % 256, i.push(o), i.push(n);

          return i;
        }(t, e.length - r), e, r, n);
      }

      function S(e, t, r) {
        return 0 === t && r === e.length ? n.fromByteArray(e) : n.fromByteArray(e.slice(t, r));
      }

      function T(e, t, r) {
        r = Math.min(e.length, r);

        for (var n = [], o = t; o < r;) {
          var i,
              s,
              a,
              u,
              c = e[o],
              l = null,
              h = c > 239 ? 4 : c > 223 ? 3 : c > 191 ? 2 : 1;
          if (o + h <= r) switch (h) {
            case 1:
              c < 128 && (l = c);
              break;

            case 2:
              128 == (192 & (i = e[o + 1])) && (u = (31 & c) << 6 | 63 & i) > 127 && (l = u);
              break;

            case 3:
              i = e[o + 1], s = e[o + 2], 128 == (192 & i) && 128 == (192 & s) && (u = (15 & c) << 12 | (63 & i) << 6 | 63 & s) > 2047 && (u < 55296 || u > 57343) && (l = u);
              break;

            case 4:
              i = e[o + 1], s = e[o + 2], a = e[o + 3], 128 == (192 & i) && 128 == (192 & s) && 128 == (192 & a) && (u = (15 & c) << 18 | (63 & i) << 12 | (63 & s) << 6 | 63 & a) > 65535 && u < 1114112 && (l = u);
          }
          null === l ? (l = 65533, h = 1) : l > 65535 && (l -= 65536, n.push(l >>> 10 & 1023 | 55296), l = 56320 | 1023 & l), n.push(l), o += h;
        }

        return function (e) {
          var t = e.length;
          if (t <= A) return String.fromCharCode.apply(String, e);
          var r = "",
              n = 0;

          for (; n < t;) r += String.fromCharCode.apply(String, e.slice(n, n += A));

          return r;
        }(n);
      }

      t.Buffer = u, t.SlowBuffer = function (e) {
        +e != e && (e = 0);
        return u.alloc(+e);
      }, t.INSPECT_MAX_BYTES = 50, u.TYPED_ARRAY_SUPPORT = void 0 !== e.TYPED_ARRAY_SUPPORT ? e.TYPED_ARRAY_SUPPORT : function () {
        try {
          var e = new Uint8Array(1);
          return e.__proto__ = {
            __proto__: Uint8Array.prototype,
            foo: function () {
              return 42;
            }
          }, 42 === e.foo() && "function" == typeof e.subarray && 0 === e.subarray(1, 1).byteLength;
        } catch (e) {
          return !1;
        }
      }(), t.kMaxLength = s(), u.poolSize = 8192, u._augment = function (e) {
        return e.__proto__ = u.prototype, e;
      }, u.from = function (e, t, r) {
        return c(null, e, t, r);
      }, u.TYPED_ARRAY_SUPPORT && (u.prototype.__proto__ = Uint8Array.prototype, u.__proto__ = Uint8Array, "undefined" != typeof Symbol && Symbol.species && u[Symbol.species] === u && Object.defineProperty(u, Symbol.species, {
        value: null,
        configurable: !0
      })), u.alloc = function (e, t, r) {
        return function (e, t, r, n) {
          return l(t), t <= 0 ? a(e, t) : void 0 !== r ? "string" == typeof n ? a(e, t).fill(r, n) : a(e, t).fill(r) : a(e, t);
        }(null, e, t, r);
      }, u.allocUnsafe = function (e) {
        return h(null, e);
      }, u.allocUnsafeSlow = function (e) {
        return h(null, e);
      }, u.isBuffer = function (e) {
        return !(null == e || !e._isBuffer);
      }, u.compare = function (e, t) {
        if (!u.isBuffer(e) || !u.isBuffer(t)) throw new TypeError("Arguments must be Buffers");
        if (e === t) return 0;

        for (var r = e.length, n = t.length, o = 0, i = Math.min(r, n); o < i; ++o) if (e[o] !== t[o]) {
          r = e[o], n = t[o];
          break;
        }

        return r < n ? -1 : n < r ? 1 : 0;
      }, u.isEncoding = function (e) {
        switch (String(e).toLowerCase()) {
          case "hex":
          case "utf8":
          case "utf-8":
          case "ascii":
          case "latin1":
          case "binary":
          case "base64":
          case "ucs2":
          case "ucs-2":
          case "utf16le":
          case "utf-16le":
            return !0;

          default:
            return !1;
        }
      }, u.concat = function (e, t) {
        if (!i(e)) throw new TypeError('"list" argument must be an Array of Buffers');
        if (0 === e.length) return u.alloc(0);
        var r;
        if (void 0 === t) for (t = 0, r = 0; r < e.length; ++r) t += e[r].length;
        var n = u.allocUnsafe(t),
            o = 0;

        for (r = 0; r < e.length; ++r) {
          var s = e[r];
          if (!u.isBuffer(s)) throw new TypeError('"list" argument must be an Array of Buffers');
          s.copy(n, o), o += s.length;
        }

        return n;
      }, u.byteLength = p, u.prototype._isBuffer = !0, u.prototype.swap16 = function () {
        var e = this.length;
        if (e % 2 != 0) throw new RangeError("Buffer size must be a multiple of 16-bits");

        for (var t = 0; t < e; t += 2) m(this, t, t + 1);

        return this;
      }, u.prototype.swap32 = function () {
        var e = this.length;
        if (e % 4 != 0) throw new RangeError("Buffer size must be a multiple of 32-bits");

        for (var t = 0; t < e; t += 4) m(this, t, t + 3), m(this, t + 1, t + 2);

        return this;
      }, u.prototype.swap64 = function () {
        var e = this.length;
        if (e % 8 != 0) throw new RangeError("Buffer size must be a multiple of 64-bits");

        for (var t = 0; t < e; t += 8) m(this, t, t + 7), m(this, t + 1, t + 6), m(this, t + 2, t + 5), m(this, t + 3, t + 4);

        return this;
      }, u.prototype.toString = function () {
        var e = 0 | this.length;
        return 0 === e ? "" : 0 === arguments.length ? T(this, 0, e) : function (e, t, r) {
          var n = !1;
          if ((void 0 === t || t < 0) && (t = 0), t > this.length) return "";
          if ((void 0 === r || r > this.length) && (r = this.length), r <= 0) return "";
          if ((r >>>= 0) <= (t >>>= 0)) return "";

          for (e || (e = "utf8");;) switch (e) {
            case "hex":
              return O(this, t, r);

            case "utf8":
            case "utf-8":
              return T(this, t, r);

            case "ascii":
              return R(this, t, r);

            case "latin1":
            case "binary":
              return k(this, t, r);

            case "base64":
              return S(this, t, r);

            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return I(this, t, r);

            default:
              if (n) throw new TypeError("Unknown encoding: " + e);
              e = (e + "").toLowerCase(), n = !0;
          }
        }.apply(this, arguments);
      }, u.prototype.equals = function (e) {
        if (!u.isBuffer(e)) throw new TypeError("Argument must be a Buffer");
        return this === e || 0 === u.compare(this, e);
      }, u.prototype.inspect = function () {
        var e = "",
            r = t.INSPECT_MAX_BYTES;
        return this.length > 0 && (e = this.toString("hex", 0, r).match(/.{2}/g).join(" "), this.length > r && (e += " ... ")), "<Buffer " + e + ">";
      }, u.prototype.compare = function (e, t, r, n, o) {
        if (!u.isBuffer(e)) throw new TypeError("Argument must be a Buffer");
        if (void 0 === t && (t = 0), void 0 === r && (r = e ? e.length : 0), void 0 === n && (n = 0), void 0 === o && (o = this.length), t < 0 || r > e.length || n < 0 || o > this.length) throw new RangeError("out of range index");
        if (n >= o && t >= r) return 0;
        if (n >= o) return -1;
        if (t >= r) return 1;
        if (this === e) return 0;

        for (var i = (o >>>= 0) - (n >>>= 0), s = (r >>>= 0) - (t >>>= 0), a = Math.min(i, s), c = this.slice(n, o), l = e.slice(t, r), h = 0; h < a; ++h) if (c[h] !== l[h]) {
          i = c[h], s = l[h];
          break;
        }

        return i < s ? -1 : s < i ? 1 : 0;
      }, u.prototype.includes = function (e, t, r) {
        return -1 !== this.indexOf(e, t, r);
      }, u.prototype.indexOf = function (e, t, r) {
        return y(this, e, t, r, !0);
      }, u.prototype.lastIndexOf = function (e, t, r) {
        return y(this, e, t, r, !1);
      }, u.prototype.write = function (e, t, r, n) {
        if (void 0 === t) n = "utf8", r = this.length, t = 0;else if (void 0 === r && "string" == typeof t) n = t, r = this.length, t = 0;else {
          if (!isFinite(t)) throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
          t |= 0, isFinite(r) ? (r |= 0, void 0 === n && (n = "utf8")) : (n = r, r = void 0);
        }
        var o = this.length - t;
        if ((void 0 === r || r > o) && (r = o), e.length > 0 && (r < 0 || t < 0) || t > this.length) throw new RangeError("Attempt to write outside buffer bounds");
        n || (n = "utf8");

        for (var i = !1;;) switch (n) {
          case "hex":
            return v(this, e, t, r);

          case "utf8":
          case "utf-8":
            return b(this, e, t, r);

          case "ascii":
            return _(this, e, t, r);

          case "latin1":
          case "binary":
            return w(this, e, t, r);

          case "base64":
            return P(this, e, t, r);

          case "ucs2":
          case "ucs-2":
          case "utf16le":
          case "utf-16le":
            return E(this, e, t, r);

          default:
            if (i) throw new TypeError("Unknown encoding: " + n);
            n = ("" + n).toLowerCase(), i = !0;
        }
      }, u.prototype.toJSON = function () {
        return {
          type: "Buffer",
          data: Array.prototype.slice.call(this._arr || this, 0)
        };
      };
      var A = 4096;

      function R(e, t, r) {
        var n = "";
        r = Math.min(e.length, r);

        for (var o = t; o < r; ++o) n += String.fromCharCode(127 & e[o]);

        return n;
      }

      function k(e, t, r) {
        var n = "";
        r = Math.min(e.length, r);

        for (var o = t; o < r; ++o) n += String.fromCharCode(e[o]);

        return n;
      }

      function O(e, t, r) {
        var n = e.length;
        (!t || t < 0) && (t = 0), (!r || r < 0 || r > n) && (r = n);

        for (var o = "", i = t; i < r; ++i) o += F(e[i]);

        return o;
      }

      function I(e, t, r) {
        for (var n = e.slice(t, r), o = "", i = 0; i < n.length; i += 2) o += String.fromCharCode(n[i] + 256 * n[i + 1]);

        return o;
      }

      function C(e, t, r) {
        if (e % 1 != 0 || e < 0) throw new RangeError("offset is not uint");
        if (e + t > r) throw new RangeError("Trying to access beyond buffer length");
      }

      function M(e, t, r, n, o, i) {
        if (!u.isBuffer(e)) throw new TypeError('"buffer" argument must be a Buffer instance');
        if (t > o || t < i) throw new RangeError('"value" argument is out of bounds');
        if (r + n > e.length) throw new RangeError("Index out of range");
      }

      function N(e, t, r, n) {
        t < 0 && (t = 65535 + t + 1);

        for (var o = 0, i = Math.min(e.length - r, 2); o < i; ++o) e[r + o] = (t & 255 << 8 * (n ? o : 1 - o)) >>> 8 * (n ? o : 1 - o);
      }

      function x(e, t, r, n) {
        t < 0 && (t = 4294967295 + t + 1);

        for (var o = 0, i = Math.min(e.length - r, 4); o < i; ++o) e[r + o] = t >>> 8 * (n ? o : 3 - o) & 255;
      }

      function U(e, t, r, n, o, i) {
        if (r + n > e.length) throw new RangeError("Index out of range");
        if (r < 0) throw new RangeError("Index out of range");
      }

      function j(e, t, r, n, i) {
        return i || U(e, 0, r, 4), o.write(e, t, r, n, 23, 4), r + 4;
      }

      function L(e, t, r, n, i) {
        return i || U(e, 0, r, 8), o.write(e, t, r, n, 52, 8), r + 8;
      }

      u.prototype.slice = function (e, t) {
        var r,
            n = this.length;
        if ((e = ~~e) < 0 ? (e += n) < 0 && (e = 0) : e > n && (e = n), (t = void 0 === t ? n : ~~t) < 0 ? (t += n) < 0 && (t = 0) : t > n && (t = n), t < e && (t = e), u.TYPED_ARRAY_SUPPORT) (r = this.subarray(e, t)).__proto__ = u.prototype;else {
          var o = t - e;
          r = new u(o, void 0);

          for (var i = 0; i < o; ++i) r[i] = this[i + e];
        }
        return r;
      }, u.prototype.readUIntLE = function (e, t, r) {
        e |= 0, t |= 0, r || C(e, t, this.length);

        for (var n = this[e], o = 1, i = 0; ++i < t && (o *= 256);) n += this[e + i] * o;

        return n;
      }, u.prototype.readUIntBE = function (e, t, r) {
        e |= 0, t |= 0, r || C(e, t, this.length);

        for (var n = this[e + --t], o = 1; t > 0 && (o *= 256);) n += this[e + --t] * o;

        return n;
      }, u.prototype.readUInt8 = function (e, t) {
        return t || C(e, 1, this.length), this[e];
      }, u.prototype.readUInt16LE = function (e, t) {
        return t || C(e, 2, this.length), this[e] | this[e + 1] << 8;
      }, u.prototype.readUInt16BE = function (e, t) {
        return t || C(e, 2, this.length), this[e] << 8 | this[e + 1];
      }, u.prototype.readUInt32LE = function (e, t) {
        return t || C(e, 4, this.length), (this[e] | this[e + 1] << 8 | this[e + 2] << 16) + 16777216 * this[e + 3];
      }, u.prototype.readUInt32BE = function (e, t) {
        return t || C(e, 4, this.length), 16777216 * this[e] + (this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3]);
      }, u.prototype.readIntLE = function (e, t, r) {
        e |= 0, t |= 0, r || C(e, t, this.length);

        for (var n = this[e], o = 1, i = 0; ++i < t && (o *= 256);) n += this[e + i] * o;

        return n >= (o *= 128) && (n -= Math.pow(2, 8 * t)), n;
      }, u.prototype.readIntBE = function (e, t, r) {
        e |= 0, t |= 0, r || C(e, t, this.length);

        for (var n = t, o = 1, i = this[e + --n]; n > 0 && (o *= 256);) i += this[e + --n] * o;

        return i >= (o *= 128) && (i -= Math.pow(2, 8 * t)), i;
      }, u.prototype.readInt8 = function (e, t) {
        return t || C(e, 1, this.length), 128 & this[e] ? -1 * (255 - this[e] + 1) : this[e];
      }, u.prototype.readInt16LE = function (e, t) {
        t || C(e, 2, this.length);
        var r = this[e] | this[e + 1] << 8;
        return 32768 & r ? 4294901760 | r : r;
      }, u.prototype.readInt16BE = function (e, t) {
        t || C(e, 2, this.length);
        var r = this[e + 1] | this[e] << 8;
        return 32768 & r ? 4294901760 | r : r;
      }, u.prototype.readInt32LE = function (e, t) {
        return t || C(e, 4, this.length), this[e] | this[e + 1] << 8 | this[e + 2] << 16 | this[e + 3] << 24;
      }, u.prototype.readInt32BE = function (e, t) {
        return t || C(e, 4, this.length), this[e] << 24 | this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3];
      }, u.prototype.readFloatLE = function (e, t) {
        return t || C(e, 4, this.length), o.read(this, e, !0, 23, 4);
      }, u.prototype.readFloatBE = function (e, t) {
        return t || C(e, 4, this.length), o.read(this, e, !1, 23, 4);
      }, u.prototype.readDoubleLE = function (e, t) {
        return t || C(e, 8, this.length), o.read(this, e, !0, 52, 8);
      }, u.prototype.readDoubleBE = function (e, t) {
        return t || C(e, 8, this.length), o.read(this, e, !1, 52, 8);
      }, u.prototype.writeUIntLE = function (e, t, r, n) {
        (e = +e, t |= 0, r |= 0, n) || M(this, e, t, r, Math.pow(2, 8 * r) - 1, 0);
        var o = 1,
            i = 0;

        for (this[t] = 255 & e; ++i < r && (o *= 256);) this[t + i] = e / o & 255;

        return t + r;
      }, u.prototype.writeUIntBE = function (e, t, r, n) {
        (e = +e, t |= 0, r |= 0, n) || M(this, e, t, r, Math.pow(2, 8 * r) - 1, 0);
        var o = r - 1,
            i = 1;

        for (this[t + o] = 255 & e; --o >= 0 && (i *= 256);) this[t + o] = e / i & 255;

        return t + r;
      }, u.prototype.writeUInt8 = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 1, 255, 0), u.TYPED_ARRAY_SUPPORT || (e = Math.floor(e)), this[t] = 255 & e, t + 1;
      }, u.prototype.writeUInt16LE = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 2, 65535, 0), u.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8) : N(this, e, t, !0), t + 2;
      }, u.prototype.writeUInt16BE = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 2, 65535, 0), u.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 8, this[t + 1] = 255 & e) : N(this, e, t, !1), t + 2;
      }, u.prototype.writeUInt32LE = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 4, 4294967295, 0), u.TYPED_ARRAY_SUPPORT ? (this[t + 3] = e >>> 24, this[t + 2] = e >>> 16, this[t + 1] = e >>> 8, this[t] = 255 & e) : x(this, e, t, !0), t + 4;
      }, u.prototype.writeUInt32BE = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 4, 4294967295, 0), u.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 24, this[t + 1] = e >>> 16, this[t + 2] = e >>> 8, this[t + 3] = 255 & e) : x(this, e, t, !1), t + 4;
      }, u.prototype.writeIntLE = function (e, t, r, n) {
        if (e = +e, t |= 0, !n) {
          var o = Math.pow(2, 8 * r - 1);
          M(this, e, t, r, o - 1, -o);
        }

        var i = 0,
            s = 1,
            a = 0;

        for (this[t] = 255 & e; ++i < r && (s *= 256);) e < 0 && 0 === a && 0 !== this[t + i - 1] && (a = 1), this[t + i] = (e / s >> 0) - a & 255;

        return t + r;
      }, u.prototype.writeIntBE = function (e, t, r, n) {
        if (e = +e, t |= 0, !n) {
          var o = Math.pow(2, 8 * r - 1);
          M(this, e, t, r, o - 1, -o);
        }

        var i = r - 1,
            s = 1,
            a = 0;

        for (this[t + i] = 255 & e; --i >= 0 && (s *= 256);) e < 0 && 0 === a && 0 !== this[t + i + 1] && (a = 1), this[t + i] = (e / s >> 0) - a & 255;

        return t + r;
      }, u.prototype.writeInt8 = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 1, 127, -128), u.TYPED_ARRAY_SUPPORT || (e = Math.floor(e)), e < 0 && (e = 255 + e + 1), this[t] = 255 & e, t + 1;
      }, u.prototype.writeInt16LE = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 2, 32767, -32768), u.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8) : N(this, e, t, !0), t + 2;
      }, u.prototype.writeInt16BE = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 2, 32767, -32768), u.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 8, this[t + 1] = 255 & e) : N(this, e, t, !1), t + 2;
      }, u.prototype.writeInt32LE = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 4, 2147483647, -2147483648), u.TYPED_ARRAY_SUPPORT ? (this[t] = 255 & e, this[t + 1] = e >>> 8, this[t + 2] = e >>> 16, this[t + 3] = e >>> 24) : x(this, e, t, !0), t + 4;
      }, u.prototype.writeInt32BE = function (e, t, r) {
        return e = +e, t |= 0, r || M(this, e, t, 4, 2147483647, -2147483648), e < 0 && (e = 4294967295 + e + 1), u.TYPED_ARRAY_SUPPORT ? (this[t] = e >>> 24, this[t + 1] = e >>> 16, this[t + 2] = e >>> 8, this[t + 3] = 255 & e) : x(this, e, t, !1), t + 4;
      }, u.prototype.writeFloatLE = function (e, t, r) {
        return j(this, e, t, !0, r);
      }, u.prototype.writeFloatBE = function (e, t, r) {
        return j(this, e, t, !1, r);
      }, u.prototype.writeDoubleLE = function (e, t, r) {
        return L(this, e, t, !0, r);
      }, u.prototype.writeDoubleBE = function (e, t, r) {
        return L(this, e, t, !1, r);
      }, u.prototype.copy = function (e, t, r, n) {
        if (r || (r = 0), n || 0 === n || (n = this.length), t >= e.length && (t = e.length), t || (t = 0), n > 0 && n < r && (n = r), n === r) return 0;
        if (0 === e.length || 0 === this.length) return 0;
        if (t < 0) throw new RangeError("targetStart out of bounds");
        if (r < 0 || r >= this.length) throw new RangeError("sourceStart out of bounds");
        if (n < 0) throw new RangeError("sourceEnd out of bounds");
        n > this.length && (n = this.length), e.length - t < n - r && (n = e.length - t + r);
        var o,
            i = n - r;
        if (this === e && r < t && t < n) for (o = i - 1; o >= 0; --o) e[o + t] = this[o + r];else if (i < 1e3 || !u.TYPED_ARRAY_SUPPORT) for (o = 0; o < i; ++o) e[o + t] = this[o + r];else Uint8Array.prototype.set.call(e, this.subarray(r, r + i), t);
        return i;
      }, u.prototype.fill = function (e, t, r, n) {
        if ("string" == typeof e) {
          if ("string" == typeof t ? (n = t, t = 0, r = this.length) : "string" == typeof r && (n = r, r = this.length), 1 === e.length) {
            var o = e.charCodeAt(0);
            o < 256 && (e = o);
          }

          if (void 0 !== n && "string" != typeof n) throw new TypeError("encoding must be a string");
          if ("string" == typeof n && !u.isEncoding(n)) throw new TypeError("Unknown encoding: " + n);
        } else "number" == typeof e && (e &= 255);

        if (t < 0 || this.length < t || this.length < r) throw new RangeError("Out of range index");
        if (r <= t) return this;
        var i;
        if (t >>>= 0, r = void 0 === r ? this.length : r >>> 0, e || (e = 0), "number" == typeof e) for (i = t; i < r; ++i) this[i] = e;else {
          var s = u.isBuffer(e) ? e : B(new u(e, n).toString()),
              a = s.length;

          for (i = 0; i < r - t; ++i) this[i + t] = s[i % a];
        }
        return this;
      };
      var D = /[^+\/0-9A-Za-z-_]/g;

      function F(e) {
        return e < 16 ? "0" + e.toString(16) : e.toString(16);
      }

      function B(e, t) {
        var r;
        t = t || 1 / 0;

        for (var n = e.length, o = null, i = [], s = 0; s < n; ++s) {
          if ((r = e.charCodeAt(s)) > 55295 && r < 57344) {
            if (!o) {
              if (r > 56319) {
                (t -= 3) > -1 && i.push(239, 191, 189);
                continue;
              }

              if (s + 1 === n) {
                (t -= 3) > -1 && i.push(239, 191, 189);
                continue;
              }

              o = r;
              continue;
            }

            if (r < 56320) {
              (t -= 3) > -1 && i.push(239, 191, 189), o = r;
              continue;
            }

            r = 65536 + (o - 55296 << 10 | r - 56320);
          } else o && (t -= 3) > -1 && i.push(239, 191, 189);

          if (o = null, r < 128) {
            if ((t -= 1) < 0) break;
            i.push(r);
          } else if (r < 2048) {
            if ((t -= 2) < 0) break;
            i.push(r >> 6 | 192, 63 & r | 128);
          } else if (r < 65536) {
            if ((t -= 3) < 0) break;
            i.push(r >> 12 | 224, r >> 6 & 63 | 128, 63 & r | 128);
          } else {
            if (!(r < 1114112)) throw new Error("Invalid code point");
            if ((t -= 4) < 0) break;
            i.push(r >> 18 | 240, r >> 12 & 63 | 128, r >> 6 & 63 | 128, 63 & r | 128);
          }
        }

        return i;
      }

      function q(e) {
        return n.toByteArray(function (e) {
          if ((e = function (e) {
            return e.trim ? e.trim() : e.replace(/^\s+|\s+$/g, "");
          }(e).replace(D, "")).length < 2) return "";

          for (; e.length % 4 != 0;) e += "=";

          return e;
        }(e));
      }

      function J(e, t, r, n) {
        for (var o = 0; o < n && !(o + r >= t.length || o >= e.length); ++o) t[o + r] = e[o];

        return o;
      }
    }).call(this, r(10));
  }, function (e, t, r) {
    "use strict";

    t.byteLength = function (e) {
      var t = c(e),
          r = t[0],
          n = t[1];
      return 3 * (r + n) / 4 - n;
    }, t.toByteArray = function (e) {
      for (var t, r = c(e), n = r[0], s = r[1], a = new i(function (e, t, r) {
        return 3 * (t + r) / 4 - r;
      }(0, n, s)), u = 0, l = s > 0 ? n - 4 : n, h = 0; h < l; h += 4) t = o[e.charCodeAt(h)] << 18 | o[e.charCodeAt(h + 1)] << 12 | o[e.charCodeAt(h + 2)] << 6 | o[e.charCodeAt(h + 3)], a[u++] = t >> 16 & 255, a[u++] = t >> 8 & 255, a[u++] = 255 & t;

      2 === s && (t = o[e.charCodeAt(h)] << 2 | o[e.charCodeAt(h + 1)] >> 4, a[u++] = 255 & t);
      1 === s && (t = o[e.charCodeAt(h)] << 10 | o[e.charCodeAt(h + 1)] << 4 | o[e.charCodeAt(h + 2)] >> 2, a[u++] = t >> 8 & 255, a[u++] = 255 & t);
      return a;
    }, t.fromByteArray = function (e) {
      for (var t, r = e.length, o = r % 3, i = [], s = 0, a = r - o; s < a; s += 16383) i.push(l(e, s, s + 16383 > a ? a : s + 16383));

      1 === o ? (t = e[r - 1], i.push(n[t >> 2] + n[t << 4 & 63] + "==")) : 2 === o && (t = (e[r - 2] << 8) + e[r - 1], i.push(n[t >> 10] + n[t >> 4 & 63] + n[t << 2 & 63] + "="));
      return i.join("");
    };

    for (var n = [], o = [], i = "undefined" != typeof Uint8Array ? Uint8Array : Array, s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", a = 0, u = s.length; a < u; ++a) n[a] = s[a], o[s.charCodeAt(a)] = a;

    function c(e) {
      var t = e.length;
      if (t % 4 > 0) throw new Error("Invalid string. Length must be a multiple of 4");
      var r = e.indexOf("=");
      return -1 === r && (r = t), [r, r === t ? 0 : 4 - r % 4];
    }

    function l(e, t, r) {
      for (var o, i, s = [], a = t; a < r; a += 3) o = (e[a] << 16 & 16711680) + (e[a + 1] << 8 & 65280) + (255 & e[a + 2]), s.push(n[(i = o) >> 18 & 63] + n[i >> 12 & 63] + n[i >> 6 & 63] + n[63 & i]);

      return s.join("");
    }

    o["-".charCodeAt(0)] = 62, o["_".charCodeAt(0)] = 63;
  }, function (e, t) {
    t.read = function (e, t, r, n, o) {
      var i,
          s,
          a = 8 * o - n - 1,
          u = (1 << a) - 1,
          c = u >> 1,
          l = -7,
          h = r ? o - 1 : 0,
          f = r ? -1 : 1,
          d = e[t + h];

      for (h += f, i = d & (1 << -l) - 1, d >>= -l, l += a; l > 0; i = 256 * i + e[t + h], h += f, l -= 8);

      for (s = i & (1 << -l) - 1, i >>= -l, l += n; l > 0; s = 256 * s + e[t + h], h += f, l -= 8);

      if (0 === i) i = 1 - c;else {
        if (i === u) return s ? NaN : 1 / 0 * (d ? -1 : 1);
        s += Math.pow(2, n), i -= c;
      }
      return (d ? -1 : 1) * s * Math.pow(2, i - n);
    }, t.write = function (e, t, r, n, o, i) {
      var s,
          a,
          u,
          c = 8 * i - o - 1,
          l = (1 << c) - 1,
          h = l >> 1,
          f = 23 === o ? Math.pow(2, -24) - Math.pow(2, -77) : 0,
          d = n ? 0 : i - 1,
          p = n ? 1 : -1,
          m = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0;

      for (t = Math.abs(t), isNaN(t) || t === 1 / 0 ? (a = isNaN(t) ? 1 : 0, s = l) : (s = Math.floor(Math.log(t) / Math.LN2), t * (u = Math.pow(2, -s)) < 1 && (s--, u *= 2), (t += s + h >= 1 ? f / u : f * Math.pow(2, 1 - h)) * u >= 2 && (s++, u /= 2), s + h >= l ? (a = 0, s = l) : s + h >= 1 ? (a = (t * u - 1) * Math.pow(2, o), s += h) : (a = t * Math.pow(2, h - 1) * Math.pow(2, o), s = 0)); o >= 8; e[r + d] = 255 & a, d += p, a /= 256, o -= 8);

      for (s = s << o | a, c += o; c > 0; e[r + d] = 255 & s, d += p, s /= 256, c -= 8);

      e[r + d - p] |= 128 * m;
    };
  }, function (e, t) {
    var r = {}.toString;

    e.exports = Array.isArray || function (e) {
      return "[object Array]" == r.call(e);
    };
  }, function (e, t, r) {
    var n, o, i;
    o = [], void 0 === (i = "function" == typeof (n = function () {
      var e, t, r, n;
      Object.keys || (Object.keys = (e = Object.prototype.hasOwnProperty, t = !{
        toString: null
      }.propertyIsEnumerable("toString"), n = (r = ["toString", "toLocaleString", "valueOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor"]).length, function (o) {
        if ("object" != typeof o && "function" != typeof o || null === o) throw new TypeError("Object.keys called on non-object");
        var i = [];

        for (var s in o) e.call(o, s) && i.push(s);

        if (t) for (var a = 0; a < n; a++) e.call(o, r[a]) && i.push(r[a]);
        return i;
      })), Object.create || (Object.create = function () {
        function e() {}

        return function (t) {
          if (1 !== arguments.length) throw new Error("Object.create implementation only accepts one parameter.");
          return e.prototype = t, new e();
        };
      }()), Array.isArray || (Array.isArray = function (e) {
        return "[object Array]" === Object.prototype.toString.call(e);
      }), Array.prototype.indexOf || (Array.prototype.indexOf = function (e) {
        if (null === this) throw new TypeError();
        var t = Object(this),
            r = t.length >>> 0;
        if (0 === r) return -1;
        var n = 0;
        if (arguments.length > 1 && ((n = Number(arguments[1])) != n ? n = 0 : 0 !== n && n !== 1 / 0 && n !== -1 / 0 && (n = (n > 0 || -1) * Math.floor(Math.abs(n)))), n >= r) return -1;

        for (var o = n >= 0 ? n : Math.max(r - Math.abs(n), 0); o < r; o++) if (o in t && t[o] === e) return o;

        return -1;
      }), Object.isFrozen || (Object.isFrozen = function (e) {
        for (var t = "tv4_test_frozen_key"; e.hasOwnProperty(t);) t += Math.random();

        try {
          return e[t] = !0, delete e[t], !1;
        } catch (e) {
          return !0;
        }
      });
      var o = {
        "+": !0,
        "#": !0,
        ".": !0,
        "/": !0,
        ";": !0,
        "?": !0,
        "&": !0
      },
          i = {
        "*": !0
      };

      function s(e) {
        return encodeURI(e).replace(/%25[0-9][0-9]/g, function (e) {
          return "%" + e.substring(3);
        });
      }

      function a(e) {
        var t = "";
        o[e.charAt(0)] && (t = e.charAt(0), e = e.substring(1));
        var r = "",
            n = "",
            a = !0,
            u = !1,
            c = !1;
        "+" === t ? a = !1 : "." === t ? (n = ".", r = ".") : "/" === t ? (n = "/", r = "/") : "#" === t ? (n = "#", a = !1) : ";" === t ? (n = ";", r = ";", u = !0, c = !0) : "?" === t ? (n = "?", r = "&", u = !0) : "&" === t && (n = "&", r = "&", u = !0);

        for (var l = [], h = e.split(","), f = [], d = {}, p = 0; p < h.length; p++) {
          var m = h[p],
              y = null;

          if (-1 !== m.indexOf(":")) {
            var g = m.split(":");
            m = g[0], y = parseInt(g[1], 10);
          }

          for (var v = {}; i[m.charAt(m.length - 1)];) v[m.charAt(m.length - 1)] = !0, m = m.substring(0, m.length - 1);

          var b = {
            truncate: y,
            name: m,
            suffices: v
          };
          f.push(b), d[m] = b, l.push(m);
        }

        var _ = function (e) {
          for (var t = "", o = 0, i = 0; i < f.length; i++) {
            var l = f[i],
                h = e(l.name);
            if (null == h || Array.isArray(h) && 0 === h.length || "object" == typeof h && 0 === Object.keys(h).length) o++;else if (t += i === o ? n : r || ",", Array.isArray(h)) {
              u && (t += l.name + "=");

              for (var d = 0; d < h.length; d++) d > 0 && (t += l.suffices["*"] && r || ",", l.suffices["*"] && u && (t += l.name + "=")), t += a ? encodeURIComponent(h[d]).replace(/!/g, "%21") : s(h[d]);
            } else if ("object" == typeof h) {
              u && !l.suffices["*"] && (t += l.name + "=");
              var p = !0;

              for (var m in h) p || (t += l.suffices["*"] && r || ","), p = !1, t += a ? encodeURIComponent(m).replace(/!/g, "%21") : s(m), t += l.suffices["*"] ? "=" : ",", t += a ? encodeURIComponent(h[m]).replace(/!/g, "%21") : s(h[m]);
            } else u && (t += l.name, c && "" === h || (t += "=")), null != l.truncate && (h = h.substring(0, l.truncate)), t += a ? encodeURIComponent(h).replace(/!/g, "%21") : s(h);
          }

          return t;
        };

        return _.varNames = l, {
          prefix: n,
          substitution: _
        };
      }

      function u(e) {
        if (!(this instanceof u)) return new u(e);

        for (var t = e.split("{"), r = [t.shift()], n = [], o = [], i = []; t.length > 0;) {
          var s = t.shift(),
              c = s.split("}")[0],
              l = s.substring(c.length + 1),
              h = a(c);
          o.push(h.substitution), n.push(h.prefix), r.push(l), i = i.concat(h.substitution.varNames);
        }

        this.fill = function (e) {
          for (var t = r[0], n = 0; n < o.length; n++) {
            var i = o[n];
            t += i(e), t += r[n + 1];
          }

          return t;
        }, this.varNames = i, this.template = e;
      }

      u.prototype = {
        toString: function () {
          return this.template;
        },
        fillFromObject: function (e) {
          return this.fill(function (t) {
            return e[t];
          });
        }
      };

      var c = function (e, t, r, n, o) {
        if (this.missing = [], this.missingMap = {}, this.formatValidators = e ? Object.create(e.formatValidators) : {}, this.schemas = e ? Object.create(e.schemas) : {}, this.collectMultiple = t, this.errors = [], this.handleError = t ? this.collectError : this.returnError, n && (this.checkRecursive = !0, this.scanned = [], this.scannedFrozen = [], this.scannedFrozenSchemas = [], this.scannedFrozenValidationErrors = [], this.validatedSchemasKey = "tv4_validation_id", this.validationErrorsKey = "tv4_validation_errors_id"), o && (this.trackUnknownProperties = !0, this.knownPropertyPaths = {}, this.unknownPropertyPaths = {}), this.errorReporter = r || g("en"), "string" == typeof this.errorReporter) throw new Error("debug");
        if (this.definedKeywords = {}, e) for (var i in e.definedKeywords) this.definedKeywords[i] = e.definedKeywords[i].slice(0);
      };

      function l(e, t) {
        if (e === t) return !0;

        if (e && t && "object" == typeof e && "object" == typeof t) {
          if (Array.isArray(e) !== Array.isArray(t)) return !1;

          if (Array.isArray(e)) {
            if (e.length !== t.length) return !1;

            for (var r = 0; r < e.length; r++) if (!l(e[r], t[r])) return !1;
          } else {
            var n;

            for (n in e) if (void 0 === t[n] && void 0 !== e[n]) return !1;

            for (n in t) if (void 0 === e[n] && void 0 !== t[n]) return !1;

            for (n in e) if (!l(e[n], t[n])) return !1;
          }

          return !0;
        }

        return !1;
      }

      c.prototype.defineKeyword = function (e, t) {
        this.definedKeywords[e] = this.definedKeywords[e] || [], this.definedKeywords[e].push(t);
      }, c.prototype.createError = function (e, t, r, n, o, i, s) {
        var a = new P(e, t, r, n, o);
        return a.message = this.errorReporter(a, i, s), a;
      }, c.prototype.returnError = function (e) {
        return e;
      }, c.prototype.collectError = function (e) {
        return e && this.errors.push(e), null;
      }, c.prototype.prefixErrors = function (e, t, r) {
        for (var n = e; n < this.errors.length; n++) this.errors[n] = this.errors[n].prefixWith(t, r);

        return this;
      }, c.prototype.banUnknownProperties = function (e, t) {
        for (var r in this.unknownPropertyPaths) {
          var n = this.createError(v.UNKNOWN_PROPERTY, {
            path: r
          }, r, "", null, e, t),
              o = this.handleError(n);
          if (o) return o;
        }

        return null;
      }, c.prototype.addFormat = function (e, t) {
        if ("object" == typeof e) {
          for (var r in e) this.addFormat(r, e[r]);

          return this;
        }

        this.formatValidators[e] = t;
      }, c.prototype.resolveRefs = function (e, t) {
        if (void 0 !== e.$ref) {
          if ((t = t || {})[e.$ref]) return this.createError(v.CIRCULAR_REFERENCE, {
            urls: Object.keys(t).join(", ")
          }, "", "", null, void 0, e);
          t[e.$ref] = !0, e = this.getSchema(e.$ref, t);
        }

        return e;
      }, c.prototype.getSchema = function (e, t) {
        var r;
        if (void 0 !== this.schemas[e]) return r = this.schemas[e], this.resolveRefs(r, t);
        var n = e,
            o = "";

        if (-1 !== e.indexOf("#") && (o = e.substring(e.indexOf("#") + 1), n = e.substring(0, e.indexOf("#"))), "object" == typeof this.schemas[n]) {
          r = this.schemas[n];
          var i = decodeURIComponent(o);
          if ("" === i) return this.resolveRefs(r, t);
          if ("/" !== i.charAt(0)) return;

          for (var s = i.split("/").slice(1), a = 0; a < s.length; a++) {
            var u = s[a].replace(/~1/g, "/").replace(/~0/g, "~");

            if (void 0 === r[u]) {
              r = void 0;
              break;
            }

            r = r[u];
          }

          if (void 0 !== r) return this.resolveRefs(r, t);
        }

        void 0 === this.missing[n] && (this.missing.push(n), this.missing[n] = n, this.missingMap[n] = n);
      }, c.prototype.searchSchemas = function (e, t) {
        if (Array.isArray(e)) for (var r = 0; r < e.length; r++) this.searchSchemas(e[r], t);else if (e && "object" == typeof e) for (var n in "string" == typeof e.id && function (e, t) {
          if (t.substring(0, e.length) === e) {
            var r = t.substring(e.length);
            if (t.length > 0 && "/" === t.charAt(e.length - 1) || "#" === r.charAt(0) || "?" === r.charAt(0)) return !0;
          }

          return !1;
        }(t, e.id) && void 0 === this.schemas[e.id] && (this.schemas[e.id] = e), e) if ("enum" !== n) if ("object" == typeof e[n]) this.searchSchemas(e[n], t);else if ("$ref" === n) {
          var o = m(e[n]);
          o && void 0 === this.schemas[o] && void 0 === this.missingMap[o] && (this.missingMap[o] = o);
        }
      }, c.prototype.addSchema = function (e, t) {
        if ("string" != typeof e || void 0 === t) {
          if ("object" != typeof e || "string" != typeof e.id) return;
          e = (t = e).id;
        }

        e === m(e) + "#" && (e = m(e)), this.schemas[e] = t, delete this.missingMap[e], y(t, e), this.searchSchemas(t, e);
      }, c.prototype.getSchemaMap = function () {
        var e = {};

        for (var t in this.schemas) e[t] = this.schemas[t];

        return e;
      }, c.prototype.getSchemaUris = function (e) {
        var t = [];

        for (var r in this.schemas) e && !e.test(r) || t.push(r);

        return t;
      }, c.prototype.getMissingUris = function (e) {
        var t = [];

        for (var r in this.missingMap) e && !e.test(r) || t.push(r);

        return t;
      }, c.prototype.dropSchemas = function () {
        this.schemas = {}, this.reset();
      }, c.prototype.reset = function () {
        this.missing = [], this.missingMap = {}, this.errors = [];
      }, c.prototype.validateAll = function (e, t, r, n, o) {
        var i;
        if (!(t = this.resolveRefs(t))) return null;
        if (t instanceof P) return this.errors.push(t), t;
        var s,
            a = this.errors.length,
            u = null,
            c = null;

        if (this.checkRecursive && e && "object" == typeof e) {
          if (i = !this.scanned.length, e[this.validatedSchemasKey]) {
            var l = e[this.validatedSchemasKey].indexOf(t);
            if (-1 !== l) return this.errors = this.errors.concat(e[this.validationErrorsKey][l]), null;
          }

          if (Object.isFrozen(e) && -1 !== (s = this.scannedFrozen.indexOf(e))) {
            var h = this.scannedFrozenSchemas[s].indexOf(t);
            if (-1 !== h) return this.errors = this.errors.concat(this.scannedFrozenValidationErrors[s][h]), null;
          }

          if (this.scanned.push(e), Object.isFrozen(e)) -1 === s && (s = this.scannedFrozen.length, this.scannedFrozen.push(e), this.scannedFrozenSchemas.push([])), u = this.scannedFrozenSchemas[s].length, this.scannedFrozenSchemas[s][u] = t, this.scannedFrozenValidationErrors[s][u] = [];else {
            if (!e[this.validatedSchemasKey]) try {
              Object.defineProperty(e, this.validatedSchemasKey, {
                value: [],
                configurable: !0
              }), Object.defineProperty(e, this.validationErrorsKey, {
                value: [],
                configurable: !0
              });
            } catch (t) {
              e[this.validatedSchemasKey] = [], e[this.validationErrorsKey] = [];
            }
            c = e[this.validatedSchemasKey].length, e[this.validatedSchemasKey][c] = t, e[this.validationErrorsKey][c] = [];
          }
        }

        var f = this.errors.length,
            d = this.validateBasic(e, t, o) || this.validateNumeric(e, t, o) || this.validateString(e, t, o) || this.validateArray(e, t, o) || this.validateObject(e, t, o) || this.validateCombinations(e, t, o) || this.validateHypermedia(e, t, o) || this.validateFormat(e, t, o) || this.validateDefinedKeywords(e, t, o) || null;

        if (i) {
          for (; this.scanned.length;) {
            var p = this.scanned.pop();
            delete p[this.validatedSchemasKey];
          }

          this.scannedFrozen = [], this.scannedFrozenSchemas = [];
        }

        if (d || f !== this.errors.length) for (; r && r.length || n && n.length;) {
          var m = r && r.length ? "" + r.pop() : null,
              y = n && n.length ? "" + n.pop() : null;
          d && (d = d.prefixWith(m, y)), this.prefixErrors(f, m, y);
        }
        return null !== u ? this.scannedFrozenValidationErrors[s][u] = this.errors.slice(a) : null !== c && (e[this.validationErrorsKey][c] = this.errors.slice(a)), this.handleError(d);
      }, c.prototype.validateFormat = function (e, t) {
        if ("string" != typeof t.format || !this.formatValidators[t.format]) return null;
        var r = this.formatValidators[t.format].call(null, e, t);
        return "string" == typeof r || "number" == typeof r ? this.createError(v.FORMAT_CUSTOM, {
          message: r
        }, "", "/format", null, e, t) : r && "object" == typeof r ? this.createError(v.FORMAT_CUSTOM, {
          message: r.message || "?"
        }, r.dataPath || "", r.schemaPath || "/format", null, e, t) : null;
      }, c.prototype.validateDefinedKeywords = function (e, t, r) {
        for (var n in this.definedKeywords) if (void 0 !== t[n]) for (var o = this.definedKeywords[n], i = 0; i < o.length; i++) {
          var s = o[i],
              a = s(e, t[n], t, r);
          if ("string" == typeof a || "number" == typeof a) return this.createError(v.KEYWORD_CUSTOM, {
            key: n,
            message: a
          }, "", "", null, e, t).prefixWith(null, n);

          if (a && "object" == typeof a) {
            var u = a.code;

            if ("string" == typeof u) {
              if (!v[u]) throw new Error("Undefined error code (use defineError): " + u);
              u = v[u];
            } else "number" != typeof u && (u = v.KEYWORD_CUSTOM);

            var c = "object" == typeof a.message ? a.message : {
              key: n,
              message: a.message || "?"
            },
                l = a.schemaPath || "/" + n.replace(/~/g, "~0").replace(/\//g, "~1");
            return this.createError(u, c, a.dataPath || null, l, null, e, t);
          }
        }

        return null;
      }, c.prototype.validateBasic = function (e, t, r) {
        var n;
        return (n = this.validateType(e, t, r)) ? n.prefixWith(null, "type") : (n = this.validateEnum(e, t, r)) ? n.prefixWith(null, "type") : null;
      }, c.prototype.validateType = function (e, t) {
        if (void 0 === t.type) return null;
        var r = typeof e;
        null === e ? r = "null" : Array.isArray(e) && (r = "array");
        var n = t.type;
        Array.isArray(n) || (n = [n]);

        for (var o = 0; o < n.length; o++) {
          var i = n[o];
          if (i === r || "integer" === i && "number" === r && e % 1 == 0) return null;
        }

        return this.createError(v.INVALID_TYPE, {
          type: r,
          expected: n.join("/")
        }, "", "", null, e, t);
      }, c.prototype.validateEnum = function (e, t) {
        if (void 0 === t.enum) return null;

        for (var r = 0; r < t.enum.length; r++) {
          var n = t.enum[r];
          if (l(e, n)) return null;
        }

        return this.createError(v.ENUM_MISMATCH, {
          value: "undefined" != typeof JSON ? JSON.stringify(e) : e
        }, "", "", null, e, t);
      }, c.prototype.validateNumeric = function (e, t, r) {
        return this.validateMultipleOf(e, t, r) || this.validateMinMax(e, t, r) || this.validateNaN(e, t, r) || null;
      };
      var h = Math.pow(2, -51),
          f = 1 - h;

      function d(e) {
        var t = String(e).replace(/^\s+|\s+$/g, "").match(/^([^:\/?#]+:)?(\/\/(?:[^:@]*(?::[^:@]*)?@)?(([^:\/?#]*)(?::(\d*))?))?([^?#]*)(\?[^#]*)?(#[\s\S]*)?/);
        return t ? {
          href: t[0] || "",
          protocol: t[1] || "",
          authority: t[2] || "",
          host: t[3] || "",
          hostname: t[4] || "",
          port: t[5] || "",
          pathname: t[6] || "",
          search: t[7] || "",
          hash: t[8] || ""
        } : null;
      }

      function p(e, t) {
        return t = d(t || ""), e = d(e || ""), t && e ? (t.protocol || e.protocol) + (t.protocol || t.authority ? t.authority : e.authority) + (r = t.protocol || t.authority || "/" === t.pathname.charAt(0) ? t.pathname : t.pathname ? (e.authority && !e.pathname ? "/" : "") + e.pathname.slice(0, e.pathname.lastIndexOf("/") + 1) + t.pathname : e.pathname, n = [], r.replace(/^(\.\.?(\/|$))+/, "").replace(/\/(\.(\/|$))+/g, "/").replace(/\/\.\.$/, "/../").replace(/\/?[^\/]*/g, function (e) {
          "/.." === e ? n.pop() : n.push(e);
        }), n.join("").replace(/^\//, "/" === r.charAt(0) ? "/" : "")) + (t.protocol || t.authority || t.pathname ? t.search : t.search || e.search) + t.hash : null;
        var r, n;
      }

      function m(e) {
        return e.split("#")[0];
      }

      function y(e, t) {
        if (e && "object" == typeof e) if (void 0 === t ? t = e.id : "string" == typeof e.id && (t = p(t, e.id), e.id = t), Array.isArray(e)) for (var r = 0; r < e.length; r++) y(e[r], t);else for (var n in "string" == typeof e.$ref && (e.$ref = p(t, e.$ref)), e) "enum" !== n && y(e[n], t);
      }

      function g(e) {
        var t = E[e = e || "en"];
        return function (e) {
          var r = t[e.code] || w[e.code];
          if ("string" != typeof r) return "Unknown error code " + e.code + ": " + JSON.stringify(e.messageParams);
          var n = e.params;
          return r.replace(/\{([^{}]*)\}/g, function (e, t) {
            var r = n[t];
            return "string" == typeof r || "number" == typeof r ? r : e;
          });
        };
      }

      c.prototype.validateMultipleOf = function (e, t) {
        var r = t.multipleOf || t.divisibleBy;
        if (void 0 === r) return null;

        if ("number" == typeof e) {
          var n = e / r % 1;
          if (n >= h && n < f) return this.createError(v.NUMBER_MULTIPLE_OF, {
            value: e,
            multipleOf: r
          }, "", "", null, e, t);
        }

        return null;
      }, c.prototype.validateMinMax = function (e, t) {
        if ("number" != typeof e) return null;

        if (void 0 !== t.minimum) {
          if (e < t.minimum) return this.createError(v.NUMBER_MINIMUM, {
            value: e,
            minimum: t.minimum
          }, "", "/minimum", null, e, t);
          if (t.exclusiveMinimum && e === t.minimum) return this.createError(v.NUMBER_MINIMUM_EXCLUSIVE, {
            value: e,
            minimum: t.minimum
          }, "", "/exclusiveMinimum", null, e, t);
        }

        if (void 0 !== t.maximum) {
          if (e > t.maximum) return this.createError(v.NUMBER_MAXIMUM, {
            value: e,
            maximum: t.maximum
          }, "", "/maximum", null, e, t);
          if (t.exclusiveMaximum && e === t.maximum) return this.createError(v.NUMBER_MAXIMUM_EXCLUSIVE, {
            value: e,
            maximum: t.maximum
          }, "", "/exclusiveMaximum", null, e, t);
        }

        return null;
      }, c.prototype.validateNaN = function (e, t) {
        return "number" != typeof e ? null : !0 === isNaN(e) || e === 1 / 0 || e === -1 / 0 ? this.createError(v.NUMBER_NOT_A_NUMBER, {
          value: e
        }, "", "/type", null, e, t) : null;
      }, c.prototype.validateString = function (e, t, r) {
        return this.validateStringLength(e, t, r) || this.validateStringPattern(e, t, r) || null;
      }, c.prototype.validateStringLength = function (e, t) {
        return "string" != typeof e ? null : void 0 !== t.minLength && e.length < t.minLength ? this.createError(v.STRING_LENGTH_SHORT, {
          length: e.length,
          minimum: t.minLength
        }, "", "/minLength", null, e, t) : void 0 !== t.maxLength && e.length > t.maxLength ? this.createError(v.STRING_LENGTH_LONG, {
          length: e.length,
          maximum: t.maxLength
        }, "", "/maxLength", null, e, t) : null;
      }, c.prototype.validateStringPattern = function (e, t) {
        if ("string" != typeof e || "string" != typeof t.pattern && !(t.pattern instanceof RegExp)) return null;
        var r;
        if (t.pattern instanceof RegExp) r = t.pattern;else {
          var n,
              o = "",
              i = t.pattern.match(/^\/(.+)\/([img]*)$/);
          i ? (n = i[1], o = i[2]) : n = t.pattern, r = new RegExp(n, o);
        }
        return r.test(e) ? null : this.createError(v.STRING_PATTERN, {
          pattern: t.pattern
        }, "", "/pattern", null, e, t);
      }, c.prototype.validateArray = function (e, t, r) {
        return Array.isArray(e) && (this.validateArrayLength(e, t, r) || this.validateArrayUniqueItems(e, t, r) || this.validateArrayItems(e, t, r)) || null;
      }, c.prototype.validateArrayLength = function (e, t) {
        var r;
        return void 0 !== t.minItems && e.length < t.minItems && (r = this.createError(v.ARRAY_LENGTH_SHORT, {
          length: e.length,
          minimum: t.minItems
        }, "", "/minItems", null, e, t), this.handleError(r)) ? r : void 0 !== t.maxItems && e.length > t.maxItems && (r = this.createError(v.ARRAY_LENGTH_LONG, {
          length: e.length,
          maximum: t.maxItems
        }, "", "/maxItems", null, e, t), this.handleError(r)) ? r : null;
      }, c.prototype.validateArrayUniqueItems = function (e, t) {
        if (t.uniqueItems) for (var r = 0; r < e.length; r++) for (var n = r + 1; n < e.length; n++) if (l(e[r], e[n])) {
          var o = this.createError(v.ARRAY_UNIQUE, {
            match1: r,
            match2: n
          }, "", "/uniqueItems", null, e, t);
          if (this.handleError(o)) return o;
        }
        return null;
      }, c.prototype.validateArrayItems = function (e, t, r) {
        if (void 0 === t.items) return null;
        var n, o;

        if (Array.isArray(t.items)) {
          for (o = 0; o < e.length; o++) if (o < t.items.length) {
            if (n = this.validateAll(e[o], t.items[o], [o], ["items", o], r + "/" + o)) return n;
          } else if (void 0 !== t.additionalItems) if ("boolean" == typeof t.additionalItems) {
            if (!t.additionalItems && (n = this.createError(v.ARRAY_ADDITIONAL_ITEMS, {}, "/" + o, "/additionalItems", null, e, t), this.handleError(n))) return n;
          } else if (n = this.validateAll(e[o], t.additionalItems, [o], ["additionalItems"], r + "/" + o)) return n;
        } else for (o = 0; o < e.length; o++) if (n = this.validateAll(e[o], t.items, [o], ["items"], r + "/" + o)) return n;

        return null;
      }, c.prototype.validateObject = function (e, t, r) {
        return "object" != typeof e || null === e || Array.isArray(e) ? null : this.validateObjectMinMaxProperties(e, t, r) || this.validateObjectRequiredProperties(e, t, r) || this.validateObjectProperties(e, t, r) || this.validateObjectDependencies(e, t, r) || null;
      }, c.prototype.validateObjectMinMaxProperties = function (e, t) {
        var r,
            n = Object.keys(e);
        return void 0 !== t.minProperties && n.length < t.minProperties && (r = this.createError(v.OBJECT_PROPERTIES_MINIMUM, {
          propertyCount: n.length,
          minimum: t.minProperties
        }, "", "/minProperties", null, e, t), this.handleError(r)) ? r : void 0 !== t.maxProperties && n.length > t.maxProperties && (r = this.createError(v.OBJECT_PROPERTIES_MAXIMUM, {
          propertyCount: n.length,
          maximum: t.maxProperties
        }, "", "/maxProperties", null, e, t), this.handleError(r)) ? r : null;
      }, c.prototype.validateObjectRequiredProperties = function (e, t) {
        if (void 0 !== t.required) for (var r = 0; r < t.required.length; r++) {
          var n = t.required[r];

          if (void 0 === e[n]) {
            var o = this.createError(v.OBJECT_REQUIRED, {
              key: n
            }, "", "/required/" + r, null, e, t);
            if (this.handleError(o)) return o;
          }
        }
        return null;
      }, c.prototype.validateObjectProperties = function (e, t, r) {
        var n;

        for (var o in e) {
          var i = r + "/" + o.replace(/~/g, "~0").replace(/\//g, "~1"),
              s = !1;
          if (void 0 !== t.properties && void 0 !== t.properties[o] && (s = !0, n = this.validateAll(e[o], t.properties[o], [o], ["properties", o], i))) return n;
          if (void 0 !== t.patternProperties) for (var a in t.patternProperties) {
            var u = new RegExp(a);
            if (u.test(o) && (s = !0, n = this.validateAll(e[o], t.patternProperties[a], [o], ["patternProperties", a], i))) return n;
          }
          if (s) this.trackUnknownProperties && (this.knownPropertyPaths[i] = !0, delete this.unknownPropertyPaths[i]);else if (void 0 !== t.additionalProperties) {
            if (this.trackUnknownProperties && (this.knownPropertyPaths[i] = !0, delete this.unknownPropertyPaths[i]), "boolean" == typeof t.additionalProperties) {
              if (!t.additionalProperties && (n = this.createError(v.OBJECT_ADDITIONAL_PROPERTIES, {
                key: o
              }, "", "/additionalProperties", null, e, t).prefixWith(o, null), this.handleError(n))) return n;
            } else if (n = this.validateAll(e[o], t.additionalProperties, [o], ["additionalProperties"], i)) return n;
          } else this.trackUnknownProperties && !this.knownPropertyPaths[i] && (this.unknownPropertyPaths[i] = !0);
        }

        return null;
      }, c.prototype.validateObjectDependencies = function (e, t, r) {
        var n;
        if (void 0 !== t.dependencies) for (var o in t.dependencies) if (void 0 !== e[o]) {
          var i = t.dependencies[o];

          if ("string" == typeof i) {
            if (void 0 === e[i] && (n = this.createError(v.OBJECT_DEPENDENCY_KEY, {
              key: o,
              missing: i
            }, "", "", null, e, t).prefixWith(null, o).prefixWith(null, "dependencies"), this.handleError(n))) return n;
          } else if (Array.isArray(i)) for (var s = 0; s < i.length; s++) {
            var a = i[s];
            if (void 0 === e[a] && (n = this.createError(v.OBJECT_DEPENDENCY_KEY, {
              key: o,
              missing: a
            }, "", "/" + s, null, e, t).prefixWith(null, o).prefixWith(null, "dependencies"), this.handleError(n))) return n;
          } else if (n = this.validateAll(e, i, [], ["dependencies", o], r)) return n;
        }
        return null;
      }, c.prototype.validateCombinations = function (e, t, r) {
        return this.validateAllOf(e, t, r) || this.validateAnyOf(e, t, r) || this.validateOneOf(e, t, r) || this.validateNot(e, t, r) || null;
      }, c.prototype.validateAllOf = function (e, t, r) {
        if (void 0 === t.allOf) return null;

        for (var n, o = 0; o < t.allOf.length; o++) {
          var i = t.allOf[o];
          if (n = this.validateAll(e, i, [], ["allOf", o], r)) return n;
        }

        return null;
      }, c.prototype.validateAnyOf = function (e, t, r) {
        if (void 0 === t.anyOf) return null;
        var n,
            o,
            i = [],
            s = this.errors.length;
        this.trackUnknownProperties && (n = this.unknownPropertyPaths, o = this.knownPropertyPaths);

        for (var a = !0, u = 0; u < t.anyOf.length; u++) {
          this.trackUnknownProperties && (this.unknownPropertyPaths = {}, this.knownPropertyPaths = {});
          var c = t.anyOf[u],
              l = this.errors.length,
              h = this.validateAll(e, c, [], ["anyOf", u], r);

          if (null === h && l === this.errors.length) {
            if (this.errors = this.errors.slice(0, s), this.trackUnknownProperties) {
              for (var f in this.knownPropertyPaths) o[f] = !0, delete n[f];

              for (var d in this.unknownPropertyPaths) o[d] || (n[d] = !0);

              a = !1;
              continue;
            }

            return null;
          }

          h && i.push(h.prefixWith(null, "" + u).prefixWith(null, "anyOf"));
        }

        return this.trackUnknownProperties && (this.unknownPropertyPaths = n, this.knownPropertyPaths = o), a ? (i = i.concat(this.errors.slice(s)), this.errors = this.errors.slice(0, s), this.createError(v.ANY_OF_MISSING, {}, "", "/anyOf", i, e, t)) : void 0;
      }, c.prototype.validateOneOf = function (e, t, r) {
        if (void 0 === t.oneOf) return null;
        var n,
            o,
            i = null,
            s = [],
            a = this.errors.length;
        this.trackUnknownProperties && (n = this.unknownPropertyPaths, o = this.knownPropertyPaths);

        for (var u = 0; u < t.oneOf.length; u++) {
          this.trackUnknownProperties && (this.unknownPropertyPaths = {}, this.knownPropertyPaths = {});
          var c = t.oneOf[u],
              l = this.errors.length,
              h = this.validateAll(e, c, [], ["oneOf", u], r);

          if (null === h && l === this.errors.length) {
            if (null !== i) return this.errors = this.errors.slice(0, a), this.createError(v.ONE_OF_MULTIPLE, {
              index1: i,
              index2: u
            }, "", "/oneOf", null, e, t);

            if (i = u, this.trackUnknownProperties) {
              for (var f in this.knownPropertyPaths) o[f] = !0, delete n[f];

              for (var d in this.unknownPropertyPaths) o[d] || (n[d] = !0);
            }
          } else h && s.push(h);
        }

        return this.trackUnknownProperties && (this.unknownPropertyPaths = n, this.knownPropertyPaths = o), null === i ? (s = s.concat(this.errors.slice(a)), this.errors = this.errors.slice(0, a), this.createError(v.ONE_OF_MISSING, {}, "", "/oneOf", s, e, t)) : (this.errors = this.errors.slice(0, a), null);
      }, c.prototype.validateNot = function (e, t, r) {
        if (void 0 === t.not) return null;
        var n,
            o,
            i = this.errors.length;
        this.trackUnknownProperties && (n = this.unknownPropertyPaths, o = this.knownPropertyPaths, this.unknownPropertyPaths = {}, this.knownPropertyPaths = {});
        var s = this.validateAll(e, t.not, null, null, r),
            a = this.errors.slice(i);
        return this.errors = this.errors.slice(0, i), this.trackUnknownProperties && (this.unknownPropertyPaths = n, this.knownPropertyPaths = o), null === s && 0 === a.length ? this.createError(v.NOT_PASSED, {}, "", "/not", null, e, t) : null;
      }, c.prototype.validateHypermedia = function (e, t, r) {
        if (!t.links) return null;

        for (var n, o = 0; o < t.links.length; o++) {
          var i = t.links[o];

          if ("describedby" === i.rel) {
            for (var s = new u(i.href), a = !0, c = 0; c < s.varNames.length; c++) if (!(s.varNames[c] in e)) {
              a = !1;
              break;
            }

            if (a) {
              var l = s.fillFromObject(e),
                  h = {
                $ref: l
              };
              if (n = this.validateAll(e, h, [], ["links", o], r)) return n;
            }
          }
        }
      };
      var v = {
        INVALID_TYPE: 0,
        ENUM_MISMATCH: 1,
        ANY_OF_MISSING: 10,
        ONE_OF_MISSING: 11,
        ONE_OF_MULTIPLE: 12,
        NOT_PASSED: 13,
        NUMBER_MULTIPLE_OF: 100,
        NUMBER_MINIMUM: 101,
        NUMBER_MINIMUM_EXCLUSIVE: 102,
        NUMBER_MAXIMUM: 103,
        NUMBER_MAXIMUM_EXCLUSIVE: 104,
        NUMBER_NOT_A_NUMBER: 105,
        STRING_LENGTH_SHORT: 200,
        STRING_LENGTH_LONG: 201,
        STRING_PATTERN: 202,
        OBJECT_PROPERTIES_MINIMUM: 300,
        OBJECT_PROPERTIES_MAXIMUM: 301,
        OBJECT_REQUIRED: 302,
        OBJECT_ADDITIONAL_PROPERTIES: 303,
        OBJECT_DEPENDENCY_KEY: 304,
        ARRAY_LENGTH_SHORT: 400,
        ARRAY_LENGTH_LONG: 401,
        ARRAY_UNIQUE: 402,
        ARRAY_ADDITIONAL_ITEMS: 403,
        FORMAT_CUSTOM: 500,
        KEYWORD_CUSTOM: 501,
        CIRCULAR_REFERENCE: 600,
        UNKNOWN_PROPERTY: 1e3
      },
          b = {};

      for (var _ in v) b[v[_]] = _;

      var w = {
        INVALID_TYPE: "Invalid type: {type} (expected {expected})",
        ENUM_MISMATCH: "No enum match for: {value}",
        ANY_OF_MISSING: 'Data does not match any schemas from "anyOf"',
        ONE_OF_MISSING: 'Data does not match any schemas from "oneOf"',
        ONE_OF_MULTIPLE: 'Data is valid against more than one schema from "oneOf": indices {index1} and {index2}',
        NOT_PASSED: 'Data matches schema from "not"',
        NUMBER_MULTIPLE_OF: "Value {value} is not a multiple of {multipleOf}",
        NUMBER_MINIMUM: "Value {value} is less than minimum {minimum}",
        NUMBER_MINIMUM_EXCLUSIVE: "Value {value} is equal to exclusive minimum {minimum}",
        NUMBER_MAXIMUM: "Value {value} is greater than maximum {maximum}",
        NUMBER_MAXIMUM_EXCLUSIVE: "Value {value} is equal to exclusive maximum {maximum}",
        NUMBER_NOT_A_NUMBER: "Value {value} is not a valid number",
        STRING_LENGTH_SHORT: "String is too short ({length} chars), minimum {minimum}",
        STRING_LENGTH_LONG: "String is too long ({length} chars), maximum {maximum}",
        STRING_PATTERN: "String does not match pattern: {pattern}",
        OBJECT_PROPERTIES_MINIMUM: "Too few properties defined ({propertyCount}), minimum {minimum}",
        OBJECT_PROPERTIES_MAXIMUM: "Too many properties defined ({propertyCount}), maximum {maximum}",
        OBJECT_REQUIRED: "Missing required property: {key}",
        OBJECT_ADDITIONAL_PROPERTIES: "Additional properties not allowed",
        OBJECT_DEPENDENCY_KEY: "Dependency failed - key must exist: {missing} (due to key: {key})",
        ARRAY_LENGTH_SHORT: "Array is too short ({length}), minimum {minimum}",
        ARRAY_LENGTH_LONG: "Array is too long ({length}), maximum {maximum}",
        ARRAY_UNIQUE: "Array items are not unique (indices {match1} and {match2})",
        ARRAY_ADDITIONAL_ITEMS: "Additional items not allowed",
        FORMAT_CUSTOM: "Format validation failed ({message})",
        KEYWORD_CUSTOM: "Keyword failed: {key} ({message})",
        CIRCULAR_REFERENCE: "Circular $refs: {urls}",
        UNKNOWN_PROPERTY: "Unknown property (not in schema)"
      };

      function P(e, t, r, n, o) {
        if (Error.call(this), void 0 === e) throw new Error("No error code supplied: " + n);
        this.message = "", this.params = t, this.code = e, this.dataPath = r || "", this.schemaPath = n || "", this.subErrors = o || null;
        var i = new Error(this.message);
        if (this.stack = i.stack || i.stacktrace, !this.stack) try {
          throw i;
        } catch (i) {
          this.stack = i.stack || i.stacktrace;
        }
      }

      P.prototype = Object.create(Error.prototype), P.prototype.constructor = P, P.prototype.name = "ValidationError", P.prototype.prefixWith = function (e, t) {
        if (null !== e && (e = e.replace(/~/g, "~0").replace(/\//g, "~1"), this.dataPath = "/" + e + this.dataPath), null !== t && (t = t.replace(/~/g, "~0").replace(/\//g, "~1"), this.schemaPath = "/" + t + this.schemaPath), null !== this.subErrors) for (var r = 0; r < this.subErrors.length; r++) this.subErrors[r].prefixWith(e, t);
        return this;
      };

      var E = {},
          S = function e(t) {
        var r,
            n,
            o = new c(),
            i = {
          setErrorReporter: function (e) {
            return "string" == typeof e ? this.language(e) : (n = e, !0);
          },
          addFormat: function () {
            o.addFormat.apply(o, arguments);
          },
          language: function (e) {
            return e ? (E[e] || (e = e.split("-")[0]), !!E[e] && (r = e, e)) : r;
          },
          addLanguage: function (e, t) {
            var r;

            for (r in v) t[r] && !t[v[r]] && (t[v[r]] = t[r]);

            var n = e.split("-")[0];
            if (E[n]) for (r in E[e] = Object.create(E[n]), t) void 0 === E[n][r] && (E[n][r] = t[r]), E[e][r] = t[r];else E[e] = t, E[n] = t;
            return this;
          },
          freshApi: function (t) {
            var r = e();
            return t && r.language(t), r;
          },
          validate: function (e, t, i, s) {
            var a = g(r),
                u = n ? function (e, t, r) {
              return n(e, t, r) || a(e, t, r);
            } : a,
                l = new c(o, !1, u, i, s);
            "string" == typeof t && (t = {
              $ref: t
            }), l.addSchema("", t);
            var h = l.validateAll(e, t, null, null, "");
            return !h && s && (h = l.banUnknownProperties(e, t)), this.error = h, this.missing = l.missing, this.valid = null === h, this.valid;
          },
          validateResult: function () {
            var e = {
              toString: function () {
                return this.valid ? "valid" : this.error.message;
              }
            };
            return this.validate.apply(e, arguments), e;
          },
          validateMultiple: function (e, t, i, s) {
            var a = g(r),
                u = n ? function (e, t, r) {
              return n(e, t, r) || a(e, t, r);
            } : a,
                l = new c(o, !0, u, i, s);
            "string" == typeof t && (t = {
              $ref: t
            }), l.addSchema("", t), l.validateAll(e, t, null, null, ""), s && l.banUnknownProperties(e, t);
            var h = {
              toString: function () {
                return this.valid ? "valid" : this.error.message;
              }
            };
            return h.errors = l.errors, h.missing = l.missing, h.valid = 0 === h.errors.length, h;
          },
          addSchema: function () {
            return o.addSchema.apply(o, arguments);
          },
          getSchema: function () {
            return o.getSchema.apply(o, arguments);
          },
          getSchemaMap: function () {
            return o.getSchemaMap.apply(o, arguments);
          },
          getSchemaUris: function () {
            return o.getSchemaUris.apply(o, arguments);
          },
          getMissingUris: function () {
            return o.getMissingUris.apply(o, arguments);
          },
          dropSchemas: function () {
            o.dropSchemas.apply(o, arguments);
          },
          defineKeyword: function () {
            o.defineKeyword.apply(o, arguments);
          },
          defineError: function (e, t, r) {
            if ("string" != typeof e || !/^[A-Z]+(_[A-Z]+)*$/.test(e)) throw new Error("Code name must be a string in UPPER_CASE_WITH_UNDERSCORES");
            if ("number" != typeof t || t % 1 != 0 || t < 1e4) throw new Error("Code number must be an integer > 10000");
            if (void 0 !== v[e]) throw new Error("Error already defined: " + e + " as " + v[e]);
            if (void 0 !== b[t]) throw new Error("Error code already used: " + b[t] + " as " + t);

            for (var n in v[e] = t, b[t] = e, w[e] = w[t] = r, E) {
              var o = E[n];
              o[e] && (o[t] = o[t] || o[e]);
            }
          },
          reset: function () {
            o.reset(), this.error = null, this.missing = [], this.valid = !0;
          },
          missing: [],
          error: null,
          valid: !0,
          normSchema: y,
          resolveUrl: p,
          getDocumentUri: m,
          errorCodes: v
        };
        return i.language(t || "en"), i;
      }();

      return S.addLanguage("en-gb", w), S.tv4 = S, S;
    }) ? n.apply(t, o) : n) || (e.exports = i);
  }, function (e, t) {
    var r = {
      uris: {},
      schemas: {},
      aliases: {},
      declare: function (e, t, r, n) {
        var o = e + "/" + t;

        if (n.extends) {
          var i,
              s = n.extends.split("/");
          i = 1 === s.length ? e + "/" + s.shift() : s.join("/");
          var a = this.uris[i];
          if (!a) throw "Type '" + o + "' tries to extend unknown schema '" + i + "'";
          n.extends = this.schemas[a];
        }

        this.uris[o] = r, this.aliases[r] = o, this.schemas[r] = n;
      },
      resolveAlias: function (e) {
        return this.uris[e];
      },
      getSchema: function (e) {
        return this.schemas[e];
      },
      inScope: function (e) {
        var t = e.length,
            r = {};

        for (var n in this.uris) if (n.substr(0, t + 1) === e + "/") {
          var o = this.uris[n];
          r[o] = this.schemas[o];
        }

        return r;
      }
    },
        n = function (e) {
      var t = new Error("Schema not found: " + e);
      return t.name = "SchemaNotFound", t;
    };

    n.prototype = Error.prototype, r.SchemaNotFound = n, e.exports = r;
  }, function (e, t) {
    function r(e) {
      this.defaultValue = e, this._canPropagate = !1, this._storage = {}, this._itemsRev = {}, this.activatePropagation();
    }

    r.prototype = {
      get: function (e) {
        e = e.toLowerCase();
        var t = this._storage[e];
        return void 0 === t && (t = this.defaultValue, this._storage[e] = t), t;
      },
      set: function (e, t) {
        return e = e.toLowerCase(), this._storage[e] === t ? t : (this._storage[e] = t, t || delete this._itemsRev[e], this._updateParentFolderItemRev(e, t), this._canPropagate && this._propagate(e), t);
      },
      delete: function (e) {
        return this.set(e, null);
      },
      deactivatePropagation: function () {
        return this._canPropagate = !1, !0;
      },
      activatePropagation: function () {
        return !!this._canPropagate || (this._generateFolderRev("/"), this._canPropagate = !0, !0);
      },
      _hashCode: function (e) {
        var t,
            r = 0;
        if (0 === e.length) return r;

        for (t = 0; t < e.length; t++) r = (r << 5) - r + e.charCodeAt(t), r |= 0;

        return r;
      },
      _generateHash: function (e) {
        var t = e.sort().join("|");
        return "" + this._hashCode(t);
      },
      _updateParentFolderItemRev: function (e, t) {
        if ("/" !== e) {
          var r = this._getParentFolder(e);

          this._itemsRev[r] || (this._itemsRev[r] = {});
          var n = this._itemsRev[r];
          t ? n[e] = t : delete n[e], this._updateParentFolderItemRev(r, this.defaultValue);
        }
      },
      _getParentFolder: function (e) {
        return e.substr(0, e.lastIndexOf("/", e.length - 2) + 1);
      },
      _propagate: function (e) {
        if ("/" !== e) {
          var t = this._getParentFolder(e),
              r = this._itemsRev[t],
              n = [];

          for (var o in r) n.push(r[o]);

          var i = this._generateHash(n);

          this.set(t, i);
        }
      },
      _generateFolderRev: function (e) {
        var t = this._itemsRev[e],
            r = this.defaultValue;

        if (t) {
          var n = [];

          for (var o in t) {
            var i = void 0;
            i = "/" === o.substr(-1) ? this._generateFolderRev(o) : t[o], n.push(i);
          }

          n.length > 0 && (r = this._generateHash(n));
        }

        return this.set(e, r), r;
      }
    }, e.exports = r;
  }, function (e, t, r) {
    var n;
    /*!
     * webfinger.js
     *   version 2.7.0
     *   http://github.com/silverbucket/webfinger.js
     *
     * Developed and Maintained by:
     *   Nick Jennings <nick@silverbucket.net> 2012
     *
     * webfinger.js is released under the AGPL (see LICENSE).
     *
     * You don't have to do anything special to choose one license or the other and you don't
     * have to notify anyone which license you are using.
     * Please see the corresponding license file for details of these licenses.
     * You are free to use, modify and distribute this software, but all copyright
     * information must remain.
     *
     */

    "function" != typeof fetch && "function" != typeof XMLHttpRequest && (XMLHttpRequest = r(26)), function (r) {
      var o = {
        "http://webfist.org/spec/rel": "webfist",
        "http://webfinger.net/rel/avatar": "avatar",
        remotestorage: "remotestorage",
        "http://tools.ietf.org/id/draft-dejong-remotestorage": "remotestorage",
        remoteStorage: "remotestorage",
        "http://www.packetizer.com/rel/share": "share",
        "http://webfinger.net/rel/profile-page": "profile",
        me: "profile",
        vcard: "vcard",
        blog: "blog",
        "http://packetizer.com/rel/blog": "blog",
        "http://schemas.google.com/g/2010#updates-from": "updates",
        "https://camlistore.org/rel/server": "camilstore"
      },
          i = {
        avatar: [],
        remotestorage: [],
        blog: [],
        vcard: [],
        updates: [],
        share: [],
        profile: [],
        webfist: [],
        camlistore: []
      },
          s = ["webfinger", "host-meta", "host-meta.json"];

      function a(e) {
        return e.toString = function () {
          return this.message;
        }, e;
      }

      function u(e) {
        "object" != typeof e && (e = {}), this.config = {
          tls_only: void 0 === e.tls_only || e.tls_only,
          webfist_fallback: void 0 !== e.webfist_fallback && e.webfist_fallback,
          uri_fallback: void 0 !== e.uri_fallback && e.uri_fallback,
          request_timeout: void 0 !== e.request_timeout ? e.request_timeout : 1e4
        };
      }

      u.prototype.__fetchJRD = function (e, t, r) {
        if ("function" == typeof fetch) return this.__fetchJRD_fetch(e, t, r);
        if ("function" == typeof XMLHttpRequest) return this.__fetchJRD_XHR(e, t, r);
        throw new Error("add a polyfill for fetch or XMLHttpRequest");
      }, u.prototype.__fetchJRD_fetch = function (e, t, r) {
        var n,
            o = this;
        "function" == typeof AbortController && (n = new AbortController());
        var i = fetch(e, {
          headers: {
            Accept: "application/jrd+json, application/json"
          },
          signal: n ? n.signal : void 0
        }).then(function (t) {
          if (t.ok) return t.text();
          throw 404 === t.status ? a({
            message: "resource not found",
            url: e,
            status: t.status
          }) : a({
            message: "error during request",
            url: e,
            status: t.status
          });
        }, function (t) {
          throw a({
            message: "error during request",
            url: e,
            status: void 0,
            err: t
          });
        }).then(function (t) {
          if (o.__isValidJSON(t)) return t;
          throw a({
            message: "invalid json",
            url: e,
            status: void 0
          });
        }),
            s = new Promise(function (t, r) {
          setTimeout(function () {
            r(a({
              message: "request timed out",
              url: e,
              status: void 0
            })), n && n.abort();
          }, o.config.request_timeout);
        });
        Promise.race([i, s]).then(function (e) {
          r(e);
        }).catch(function (e) {
          t(e);
        });
      }, u.prototype.__fetchJRD_XHR = function (e, t, r) {
        var n = this,
            o = !1,
            i = new XMLHttpRequest();

        function s() {
          if (!o) {
            if (o = !0, 200 === i.status) return n.__isValidJSON(i.responseText) ? r(i.responseText) : t(a({
              message: "invalid json",
              url: e,
              status: i.status
            }));
            if (404 === i.status) return t(a({
              message: "resource not found",
              url: e,
              status: i.status
            }));

            if (i.status >= 301 && i.status <= 302) {
              var s = i.getResponseHeader("Location");
              return function (e) {
                return "string" == typeof e && "https" === e.split("://")[0];
              }(s) ? u() : t(a({
                message: "no redirect URL found",
                url: e,
                status: i.status
              }));
            }

            return t(a({
              message: "error during request",
              url: e,
              status: i.status
            }));
          }
        }

        function u() {
          i.onreadystatechange = function () {
            4 === i.readyState && s();
          }, i.onload = function () {
            s();
          }, i.ontimeout = function () {
            return t(a({
              message: "request timed out",
              url: e,
              status: i.status
            }));
          }, i.open("GET", e, !0), i.timeout = n.config.request_timeout, i.setRequestHeader("Accept", "application/jrd+json, application/json"), i.send();
        }

        return u();
      }, u.prototype.__isValidJSON = function (e) {
        try {
          JSON.parse(e);
        } catch (e) {
          return !1;
        }

        return !0;
      }, u.prototype.__isLocalhost = function (e) {
        return /^localhost(\.localdomain)?(\:[0-9]+)?$/.test(e);
      }, u.prototype.__processJRD = function (e, t, r, n) {
        var s = JSON.parse(t);
        if ("object" != typeof s || "object" != typeof s.links) return void 0 !== s.error ? r(a({
          message: s.error,
          request: e
        })) : r(a({
          message: "unknown response from server",
          request: e
        }));
        var u = s.links;
        Array.isArray(u) || (u = []);
        var c = {
          object: s,
          json: t,
          idx: {}
        };
        c.idx.properties = {
          name: void 0
        }, c.idx.links = JSON.parse(JSON.stringify(i)), u.map(function (e, t) {
          if (o.hasOwnProperty(e.rel) && c.idx.links[o[e.rel]]) {
            var r = {};
            Object.keys(e).map(function (t, n) {
              r[t] = e[t];
            }), c.idx.links[o[e.rel]].push(r);
          }
        });
        var l = JSON.parse(t).properties;

        for (var h in l) l.hasOwnProperty(h) && "http://packetizer.com/ns/name" === h && (c.idx.properties.name = l[h]);

        return n(c);
      }, u.prototype.lookup = function (e, t) {
        if ("string" != typeof e) throw new Error("first parameter must be a user address");
        if ("function" != typeof t) throw new Error("second parameter must be a callback");
        var r = this,
            n = "";
        n = e.indexOf("://") > -1 ? e.replace(/ /g, "").split("/")[2] : e.replace(/ /g, "").split("@")[1];
        var o = 0,
            i = "https";

        function a() {
          var t = "";
          return e.split("://")[1] || (t = "acct:"), i + "://" + n + "/.well-known/" + s[o] + "?resource=" + t + e;
        }

        function u(e) {
          if (r.config.uri_fallback && "webfist.org" !== n && o !== s.length - 1) return o += 1, c();
          if (!r.config.tls_only && "https" === i) return o = 0, i = "http", c();
          if (!r.config.webfist_fallback || "webfist.org" === n) return t(e);
          o = 0, i = "http", n = "webfist.org";
          var u = a();

          r.__fetchJRD(u, t, function (e) {
            r.__processJRD(u, e, t, function (e) {
              "object" == typeof e.idx.links.webfist && "string" == typeof e.idx.links.webfist[0].href && r.__fetchJRD(e.idx.links.webfist[0].href, t, function (e) {
                r.__processJRD(u, e, t, function (e) {
                  return t(null, t);
                });
              });
            });
          });
        }

        function c() {
          var e = a();

          r.__fetchJRD(e, u, function (n) {
            r.__processJRD(e, n, t, function (e) {
              t(null, e);
            });
          });
        }

        return r.__isLocalhost(n) && (i = "http"), setTimeout(c, 0);
      }, u.prototype.lookupLink = function (e, t, r) {
        if (!i.hasOwnProperty(t)) return r("unsupported rel " + t);
        this.lookup(e, function (e, n) {
          var o = n.idx.links[t];
          return e ? r(e) : 0 === o.length ? r('no links found with rel="' + t + '"') : r(null, o[0]);
        });
      }, void 0 === (n = function () {
        return u;
      }.apply(t, [])) || (e.exports = n);
    }();
  }, function (e, t) {
    e.exports = XMLHttpRequest;
  }, function (e, t, r) {
    "use strict";

    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o = r(0),
        i = r(1),
        s = r(28),
        a = r(3),
        u = {
      features: [],
      featuresDone: 0,
      readyFired: !1,
      loadFeatures: function () {
        var e = this;

        for (var t in this.features = [], this.featuresDone = 0, this.readyFired = !1, this.featureModules = {
          WireClient: r(6),
          Dropbox: r(11),
          GoogleDrive: r(13),
          Access: r(15),
          Discover: r(14),
          Authorize: r(4),
          BaseClient: r(5),
          Env: r(12)
        }, a.cache && o.extend(this.featureModules, {
          Caching: r(16),
          IndexedDB: r(29),
          LocalStorage: r(30),
          InMemoryStorage: r(31),
          Sync: r(7)
        }), a.disableFeatures.forEach(function (t) {
          e.featureModules[t] && delete e.featureModules[t];
        }), this._allLoaded = !1, this.featureModules) this.loadFeature(t);
      },
      hasFeature: function (e) {
        for (var t = this.features.length - 1; t >= 0; t--) if (this.features[t].name === e) return this.features[t].supported;

        return !1;
      },
      loadFeature: function (e) {
        var t = this,
            r = this.featureModules[e],
            o = !r._rs_supported || r._rs_supported();

        i("[RemoteStorage] [FEATURE ".concat(e, "] initializing ...")), "object" === n(o) ? o.then(function () {
          t.featureSupported(e, !0), t.initFeature(e);
        }, function () {
          t.featureSupported(e, !1);
        }) : "boolean" == typeof o ? (this.featureSupported(e, o), o && this.initFeature(e)) : this.featureSupported(e, !1);
      },
      initFeature: function (e) {
        var t,
            r = this,
            o = this.featureModules[e];

        try {
          t = o._rs_init(this);
        } catch (t) {
          return void this.featureFailed(e, t);
        }

        "object" === n(t) && "function" == typeof t.then ? t.then(function () {
          r.featureInitialized(e);
        }, function (t) {
          r.featureFailed(e, t);
        }) : this.featureInitialized(e);
      },
      featureFailed: function (e, t) {
        i("[RemoteStorage] [FEATURE ".concat(e, "] initialization failed (").concat(t, ")")), this.featureDone();
      },
      featureSupported: function (e, t) {
        i("[RemoteStorage] [FEATURE ".concat(e, "]  ").concat(t ? "" : " not", " supported")), t || this.featureDone();
      },
      featureInitialized: function (e) {
        i("[RemoteStorage] [FEATURE ".concat(e, "] initialized.")), this.features.push({
          name: e,
          init: this.featureModules[e]._rs_init,
          supported: !0,
          cleanup: this.featureModules[e]._rs_cleanup
        }), this.featureDone();
      },
      featureDone: function () {
        this.featuresDone++, this.featuresDone === Object.keys(this.featureModules).length && setTimeout(this.featuresLoaded.bind(this), 0);
      },
      _setCachingModule: function () {
        var e = this;
        ["IndexedDB", "LocalStorage", "InMemoryStorage"].some(function (t) {
          if (e.features.some(function (e) {
            return e.name === t;
          })) return e.features.local = e.featureModules[t], !0;
        });
      },
      _fireReady: function () {
        try {
          this.readyFired || (this._emit("ready"), this.readyFired = !0);
        } catch (e) {
          console.error("'ready' failed: ", e, e.stack), this._emit("error", e);
        }
      },
      featuresLoaded: function () {
        var e = this;
        i("[REMOTESTORAGE] All features loaded !"), this._setCachingModule(), this.local = a.cache && this.features.local && new this.features.local(), this.local && this.remote ? (this._setGPD(s, this), this._bindChange(this.local)) : this.remote && this._setGPD(this.remote, this.remote), this.remote && (this.remote.on("connected", function () {
          e._fireReady(), e._emit("connected");
        }), this.remote.on("not-connected", function () {
          e._fireReady(), e._emit("not-connected");
        }), this.remote.connected && (this._fireReady(), this._emit("connected")), this.hasFeature("Authorize") || this.remote.stopWaitingForToken()), this._collectCleanupFunctions();

        try {
          this._allLoaded = !0, this._emit("features-loaded");
        } catch (e) {
          o.logError(e), this._emit("error", e);
        }

        this._processPending();
      },
      _collectCleanupFunctions: function () {
        this._cleanups = [];

        for (var e = 0; e < this.features.length; e++) {
          var t = this.features[e].cleanup;
          "function" == typeof t && this._cleanups.push(t);
        }
      }
    };
    e.exports = u;
  }, function (e, t, r) {
    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o = r(1);
    var i = {
      get: function (e, t) {
        if (this.local) {
          if (void 0 === t) t = "object" === n((r = this).remote) && r.remote.connected && r.remote.online ? 2 * r.getSyncInterval() : (o("Not setting default maxAge, because remote is offline or not connected"), !1);else if ("number" != typeof t && !1 !== t) return Promise.reject("Argument 'maxAge' must be 'false' or a number");
          return this.local.get(e, t, this.sync.queueGetRequest.bind(this.sync));
        }

        return this.remote.get(e);
        var r;
      },
      put: function (e, t, r) {
        return function (e) {
          return "dropbox" === this.backend && e.match(/^\/public\/.*[^\/]$/);
        }.bind(this)(e) ? i._wrapBusyDone.call(this, this.remote.put(e, t, r)) : this.local ? this.local.put(e, t, r) : i._wrapBusyDone.call(this, this.remote.put(e, t, r));
      },
      delete: function (e) {
        return this.local ? this.local.delete(e) : i._wrapBusyDone.call(this, this.remote.delete(e));
      },
      _wrapBusyDone: function (e) {
        var t = this;
        return this._emit("wire-busy"), e.then(function (e) {
          return t._emit("wire-done", {
            success: !0
          }), Promise.resolve(e);
        }, function (e) {
          return t._emit("wire-done", {
            success: !1
          }), Promise.reject(e);
        });
      }
    };
    e.exports = i;
  }, function (e, t, r) {
    function n(e) {
      return (n = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
        return typeof e;
      } : function (e) {
        return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
      })(e);
    }

    var o,
        i = r(1),
        s = r(8),
        a = r(2),
        u = r(0),
        c = function (e) {
      this.db = e || o, this.db ? (s(this), a(this, "change", "local-events-done"), this.getsRunning = 0, this.putsRunning = 0, this.changesQueued = {}, this.changesRunning = {}) : i("[IndexedDB] Failed to open DB");
    };

    c.prototype = {
      getNodes: function (e) {
        for (var t = [], r = {}, n = 0, o = e.length; n < o; n++) void 0 !== this.changesQueued[e[n]] ? r[e[n]] = u.deepClone(this.changesQueued[e[n]] || void 0) : void 0 !== this.changesRunning[e[n]] ? r[e[n]] = u.deepClone(this.changesRunning[e[n]] || void 0) : t.push(e[n]);

        return t.length > 0 ? this.getNodesFromDb(t).then(function (e) {
          for (var t in r) e[t] = r[t];

          return e;
        }) : Promise.resolve(r);
      },
      setNodes: function (e) {
        for (var t in e) this.changesQueued[t] = e[t] || !1;

        return this.maybeFlush(), Promise.resolve();
      },
      maybeFlush: function () {
        0 === this.putsRunning ? this.flushChangesQueued() : this.commitSlownessWarning || (this.commitSlownessWarning = setInterval(function () {
          console.warn("WARNING: waited more than 10 seconds for previous commit to finish");
        }, 1e4));
      },
      flushChangesQueued: function () {
        this.commitSlownessWarning && (clearInterval(this.commitSlownessWarning), this.commitSlownessWarning = null), Object.keys(this.changesQueued).length > 0 && (this.changesRunning = this.changesQueued, this.changesQueued = {}, this.setNodesInDb(this.changesRunning).then(this.flushChangesQueued.bind(this)));
      },
      getNodesFromDb: function (e) {
        var t = this;
        return new Promise(function (r, n) {
          var o = t.db.transaction(["nodes"], "readonly"),
              i = o.objectStore("nodes"),
              s = {};
          t.getsRunning++, e.map(function (e) {
            i.get(e).onsuccess = function (t) {
              s[e] = t.target.result;
            };
          }), o.oncomplete = function () {
            r(s), this.getsRunning--;
          }.bind(t), o.onerror = o.onabort = function () {
            n("get transaction error/abort"), this.getsRunning--;
          }.bind(t);
        });
      },
      setNodesInDb: function (e) {
        var t = this;
        return new Promise(function (r, o) {
          var s = t.db.transaction(["nodes"], "readwrite"),
              a = s.objectStore("nodes"),
              u = new Date().getTime();

          for (var c in t.putsRunning++, i("[IndexedDB] Starting put", e, t.putsRunning), e) {
            var l = e[c];
            if ("object" === n(l)) try {
              a.put(l);
            } catch (e) {
              throw i("[IndexedDB] Error while putting", l, e), e;
            } else try {
              a.delete(c);
            } catch (e) {
              throw i("[IndexedDB] Error while removing", a, l, e), e;
            }
          }

          s.oncomplete = function () {
            this.putsRunning--, i("[IndexedDB] Finished put", e, this.putsRunning, new Date().getTime() - u + "ms"), r();
          }.bind(t), s.onerror = function () {
            this.putsRunning--, o("transaction error");
          }.bind(t), s.onabort = function () {
            o("transaction abort"), this.putsRunning--;
          }.bind(t);
        });
      },
      reset: function (e) {
        var t = this,
            r = this.db.name;
        this.db.close(), c.clean(this.db.name, function () {
          c.open(r, function (r, n) {
            r ? i("[IndexedDB] Error while resetting local storage", r) : t.db = n, "function" == typeof e && e(self);
          });
        });
      },
      forAllNodes: function (e) {
        var t = this;
        return new Promise(function (r) {
          t.db.transaction(["nodes"], "readonly").objectStore("nodes").openCursor().onsuccess = function (n) {
            var o = n.target.result;
            o ? (e(t.migrate(o.value)), o.continue()) : r();
          };
        });
      },
      closeDB: function () {
        0 === this.putsRunning ? this.db.close() : setTimeout(this.closeDB.bind(this), 100);
      }
    }, c.open = function (e, t) {
      var r = setTimeout(function () {
        t("timeout trying to open db");
      }, 1e4);

      try {
        var n = indexedDB.open(e, 2);
        n.onerror = function () {
          i("[IndexedDB] Opening DB failed", n), clearTimeout(r), t(n.error);
        }, n.onupgradeneeded = function (e) {
          var t = n.result;
          i("[IndexedDB] Upgrade: from ", e.oldVersion, " to ", e.newVersion), 1 !== e.oldVersion && (i("[IndexedDB] Creating object store: nodes"), t.createObjectStore("nodes", {
            keyPath: "path"
          })), i("[IndexedDB] Creating object store: changes"), t.createObjectStore("changes", {
            keyPath: "path"
          });
        }, n.onsuccess = function () {
          clearTimeout(r);
          var o = n.result;
          if (!o.objectStoreNames.contains("nodes") || !o.objectStoreNames.contains("changes")) return i("[IndexedDB] Missing object store. Resetting the database."), void c.clean(e, function () {
            c.open(e, t);
          });
          t(null, n.result);
        };
      } catch (n) {
        i("[IndexedDB] Failed to open database: " + n), i("[IndexedDB] Resetting database and trying again."), clearTimeout(r), c.clean(e, function () {
          c.open(e, t);
        });
      }
    }, c.clean = function (e, t) {
      var r = indexedDB.deleteDatabase(e);
      r.onsuccess = function () {
        i("[IndexedDB] Done removing DB"), t();
      }, r.onerror = r.onabort = function (t) {
        console.error('Failed to remove database "' + e + '"', t);
      };
    }, c._rs_init = function (e) {
      return new Promise(function (t, r) {
        c.open("remotestorage", function (n, i) {
          n ? r(n) : (o = i, i.onerror = function () {
            e._emit("error", n);
          }, t());
        });
      });
    }, c._rs_supported = function () {
      return new Promise(function (e, t) {
        var r = u.getGlobalContext(),
            n = !1;
        if ("undefined" != typeof navigator && navigator.userAgent.match(/Android (2|3|4\.[0-3])/) && (navigator.userAgent.match(/Chrome|Firefox/) || (n = !0)), "indexedDB" in r && !n) try {
          var o = indexedDB.open("rs-check");
          o.onerror = function () {
            t();
          }, o.onsuccess = function () {
            o.result.close(), indexedDB.deleteDatabase("rs-check"), e();
          };
        } catch (e) {
          t();
        } else t();
      });
    }, c._rs_cleanup = function (e) {
      return new Promise(function (t) {
        e.local && e.local.closeDB(), c.clean("remotestorage", t);
      });
    }, e.exports = c;
  }, function (e, t, r) {
    var n = r(8),
        o = r(1),
        i = r(2),
        s = r(0),
        a = "remotestorage:cache:nodes:",
        u = "remotestorage:cache:changes:",
        c = function () {
      n(this), o("[LocalStorage] Registering events"), i(this, "change", "local-events-done");
    };

    function l(e) {
      return e.substr(0, a.length) === a || e.substr(0, u.length) === u;
    }

    c.prototype = {
      getNodes: function (e) {
        for (var t = {}, r = 0, n = e.length; r < n; r++) try {
          t[e[r]] = JSON.parse(localStorage[a + e[r]]);
        } catch (n) {
          t[e[r]] = void 0;
        }

        return Promise.resolve(t);
      },
      setNodes: function (e) {
        for (var t in e) localStorage[a + t] = JSON.stringify(e[t]);

        return Promise.resolve();
      },
      forAllNodes: function (e) {
        for (var t, r = 0, n = localStorage.length; r < n; r++) if (localStorage.key(r).substr(0, a.length) === a) {
          try {
            t = this.migrate(JSON.parse(localStorage[localStorage.key(r)]));
          } catch (e) {
            t = void 0;
          }

          t && e(t);
        }

        return Promise.resolve();
      }
    }, c._rs_init = function () {}, c._rs_supported = function () {
      return s.localStorageAvailable();
    }, c._rs_cleanup = function () {
      for (var e = [], t = 0, r = localStorage.length; t < r; t++) {
        var n = localStorage.key(t);
        l(n) && e.push(n);
      }

      e.forEach(function (e) {
        o("[LocalStorage] Removing", e), delete localStorage[e];
      });
    }, e.exports = c;
  }, function (e, t, r) {
    var n = r(2),
        o = r(1),
        i = r(8),
        s = function () {
      i(this), o("[InMemoryStorage] Registering events"), n(this, "change", "local-events-done"), this._storage = {};
    };

    s.prototype = {
      getNodes: function (e) {
        for (var t = {}, r = 0, n = e.length; r < n; r++) t[e[r]] = this._storage[e[r]];

        return Promise.resolve(t);
      },
      setNodes: function (e) {
        for (var t in e) void 0 === e[t] ? delete this._storage[t] : this._storage[t] = e[t];

        return Promise.resolve();
      },
      forAllNodes: function (e) {
        for (var t in this._storage) e(this.migrate(this._storage[t]));

        return Promise.resolve();
      }
    }, s._rs_init = function () {}, s._rs_supported = function () {
      return !0;
    }, s._rs_cleanup = function () {}, e.exports = s;
  }, function (e, t, r) {
    var n = r(5),
        o = r(9);
    o.prototype.addModule = function (e) {
      var t = e.name,
          r = e.builder;

      if (Object.defineProperty(this, t, {
        configurable: !0,
        get: function () {
          var e = this._loadModule(t, r);

          return Object.defineProperty(this, t, {
            value: e
          }), e;
        }
      }), -1 !== t.indexOf("-")) {
        var n = t.replace(/\-[a-z]/g, function (e) {
          return e[1].toUpperCase();
        });
        Object.defineProperty(this, n, {
          get: function () {
            return this[t];
          }
        });
      }
    }, o.prototype._loadModule = function (e, t) {
      if (t) return t(new n(this, "/" + e + "/"), new n(this, "/public/" + e + "/")).exports;
      throw "Unknown module: " + e;
    };
  }]);
});
},{}],"src/lib/remotestorage-module-notes.js":[function(require,module,exports) {
/**
 * File: notes
 *
 * Maintainer: - Jorin Vogel <hi@jorin.in>
 * Version: -    0.2.1
 *
 * This module stores lists of notes.
 * A note has the fields title, content and lastEdited.
 *
 * This module is used by Litewrite.
 * 
 * 
 * Modified for Carnet - Sylvain Vinet
 */
function Notes(privateClient, publicClient) {
  // Schema
  privateClient.declareType('note', {
    description: 'A text note',
    type: 'object',
    '$schema': 'http://json-schema.org/draft-03/schema#',
    additionalProperties: true,
    properties: {
      id: {
        type: 'string',
        required: true
      },
      title: {
        type: 'string',
        required: true
      },
      content: {
        type: 'string',
        required: true,
        default: ''
      },
      lastEdited: {
        type: 'string',
        required: true
      },
      etiquettes: {
        type: 'array',
        required: false,
        default: []
      }
    }
  });
  var notesModule = {
    /**
     * Method: privateList
     *
     * List all private notes.
     *
     * Parameters:
     *
     *   path - a pathstring where to scope the client to.
     *
     * Returns:
     *   A privateClient scoped to the given path
     *    and extended with the listMethods.
     *   It also supports all <BaseClient methods at http://remotestoragejs.com/doc/code/files/baseclient-js.html>
     */
    privateList: function privateList(path) {
      return Object.assign(privateClient.scope(path), listMethods); //must check if path end with '/'
    },

    /**
     * Method: publicList
     *
     * List all public notes.
     *
     * Parameters:
     *
     *   path - a pathstring where to scope the client to.
     *
     * Returns:
     *   A publicClient scoped to the given path
     *    and extended with the listMethods.
     *   It also supports all <BaseClient methods at http://remotestoragejs.com/doc/code/files/baseclient-js.html>
     */
    publicList: function publicList(path) {
      return Object.assign(publicClient.scope(path), listMethods);
    }
  };
  /**
   * Class: listMethods
   *
   */

  var listMethods = {
    /**
     * Method: add
     *
     * Create a new note
     *
     * Parameters:
     *   doc - the note data to store as JSON object.
     *
     * Returns:
     *   A promise, which will be fulfilled with the created note as JSON object.
     *   The created note also contains the newly created id property.
     */
    add: function add(doc) {
      var id = doc.id;
      return this.set(id, doc);
    },

    /**
     * Method: set
     *
     * Update or create a note for a specified id.
     *
     * Parameters:
     *   id  - the id the note is at.
     *   doc - the note data to store as JSON object.
     *
     * Returns:
     *   A promise, which will be fulfilled with the updated note.
     */
    set: function set(id, doc) {
      return this.storeObject('note', id, doc).then(function () {
        return doc;
      }).catch(function (err) {
        return console.log("erreur dans client.set", err);
      });
    },

    /**
     * Method: get
     *
     * Get a note.
     *
     * Parameters:
     *   id - the id of the note you want to get.
     *
     * Returns:
     *   A promise, which will be fulfilled with the note as JSON object.
     */
    get: function get(id) {
      return this.getObject(id).then(function (obj) {
        return obj || {};
      });
    },

    /**
     * Method: addRaw
     *
     * Store a raw note of the specified contentType at shared/.
     *
     * Parameters:
     *   contentType - the content type of the data (like 'text/html').
     *   data - the raw data to store.
     *
     * Returns:
     *   A promise, which will be fulfilled with the path of the added note.
     */
    addRaw: function addRaw(contentType, data) {
      var id = new Date().toJSON();
      var path = 'shared/' + id;
      var url = this.getItemURL(path);
      return this.storeFile(contentType, path, data).then(function () {
        return url;
      });
    },

    /**
     * Method: setRaw
     *
     * Store a raw doccument of the specified contentType at shared/.
     *
     * Parameters:
     *   id - id of the note to update
     *   contentType - the content type of the data (like 'text/html').
     *   data - the raw data to store.
     *
     * Returns:
     *   A promise, which will be fulfilled with the path of the added note.
     */
    setRaw: function setRaw(id, contentType, data) {
      var path = 'shared/' + id;
      return this.storeFile(contentType, path, data);
    }
  };
  return {
    exports: notesModule
  };
}

module.exports = {
  name: 'documents',
  builder: Notes
};
},{}],"node_modules/remotestorage-widget/build/widget.js":[function(require,module,exports) {
var define;
!function(n,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports.Widget=t():n.Widget=t()}(window,function(){return function(n){var t={};function e(l){if(t[l])return t[l].exports;var i=t[l]={i:l,l:!1,exports:{}};return n[l].call(i.exports,i,i.exports,e),i.l=!0,i.exports}return e.m=n,e.c=t,e.d=function(n,t,l){e.o(n,t)||Object.defineProperty(n,t,{enumerable:!0,get:l})},e.r=function(n){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(n,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(n,"__esModule",{value:!0})},e.t=function(n,t){if(1&t&&(n=e(n)),8&t)return n;if(4&t&&"object"==typeof n&&n&&n.__esModule)return n;var l=Object.create(null);if(e.r(l),Object.defineProperty(l,"default",{enumerable:!0,value:n}),2&t&&"string"!=typeof n)for(var i in n)e.d(l,i,function(t){return n[t]}.bind(null,i));return l},e.n=function(n){var t=n&&n.__esModule?function(){return n.default}:function(){return n};return e.d(t,"a",t),t},e.o=function(n,t){return Object.prototype.hasOwnProperty.call(n,t)},e.p="",e(e.s=0)}([function(n,t,e){n.exports=e(1)},function(n,t,e){"use strict";var l=function(n){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:{};if(this.rs=n,this.leaveOpen=!!t.leaveOpen&&t.leaveOpen,this.autoCloseAfter=t.autoCloseAfter?t.autoCloseAfter:1500,this.skipInitial=!!t.skipInitial&&t.skipInitial,this.logging=!!t.logging&&t.logging,t.hasOwnProperty("modalBackdrop")){if("boolean"!=typeof t.modalBackdrop&&"onlySmallScreens"!==t.modalBackdrop)throw'options.modalBackdrop has to be true/false or "onlySmallScreens"';this.modalBackdrop=t.modalBackdrop}else this.modalBackdrop="onlySmallScreens";this.active=!1,this.online=!1,this.closed=!1,this.lastSynced=null,this.lastSyncedUpdateLoop=null};l.prototype={log:function(){if(this.logging){for(var n,t=arguments.length,e=Array(t),l=0;l<t;l++)e[l]=arguments[l];(n=console).debug.apply(n,["[RS-WIDGET] "].concat(e))}},eventHandler:function(n,t){var e=this;switch(this.log("EVENT: ",n),n){case"ready":this.setState(this.state);break;case"sync-req-done":this.rsSyncButton.classList.add("rs-rotate");break;case"sync-done":this.rsSyncButton.classList.remove("rs-rotate"),this.rsWidget.classList.contains("rs-state-unauthorized")||!this.rs.remote.online?this.updateLastSyncedOutput():this.rs.remote.online&&(this.lastSynced=new Date,document.querySelector(".rs-box-connected .rs-sub-headline").innerHTML="Synced just now"),!this.closed&&this.shouldCloseWhenSyncDone&&setTimeout(this.close.bind(this),this.autoCloseAfter);break;case"disconnected":this.active=!1,this.setOnline(),this.setBackendClass(),this.open(),this.setInitialState();break;case"connected":this.active=!0,this.online=!0,this.rs.hasFeature("Sync")?(this.shouldCloseWhenSyncDone=!0,this.rs.on("sync-req-done",function(){return e.eventHandler("sync-req-done")}),this.rs.on("sync-done",function(){return e.eventHandler("sync-done")})):(this.rsSyncButton.classList.add("rs-hidden"),setTimeout(this.close.bind(this),this.autoCloseAfter));var l=this.rs.remote.userAddress;this.rsConnectedUser.innerHTML=l,this.setBackendClass(this.rs.backend),this.rsConnectedLabel.textContent="Connected",this.setState("connected");break;case"network-offline":this.setOffline();break;case"network-online":this.setOnline();break;case"error":this.setBackendClass(this.rs.backend),"DiscoveryError"===t.name?this.handleDiscoveryError(t):"SyncError"===t.name?this.handleSyncError(t):"Unauthorized"===t.name?this.handleUnauthorized(t):console.debug("Encountered unhandled error",t)}},setState:function(n){if(n){this.log("Setting state ",n);var t=document.querySelector(".rs-box.rs-selected");t&&t.classList.remove("rs-selected");var e=document.querySelector(".rs-box.rs-box-"+n);e&&e.classList.add("rs-selected");var l=this.rsWidget.className.match(/rs-state-\S+/g)[0];this.rsWidget.classList.remove(l),this.rsWidget.classList.add("rs-state-"+(n||this.state)),this.state=n}},setInitialState:function(){this.skipInitial?this.showChooseOrSignIn():this.setState("initial")},createHtmlTemplate:function(){var n=document.createElement("div"),t=document.createElement("style");return t.innerHTML=e(2),n.id="remotestorage-widget",n.innerHTML=e(3),n.appendChild(t),n},createBackdropHtml:function(){var n=document.createElement("div");n.classList.add("remotestorage-widget-modal-backdrop"),document.body.appendChild(n)},setModalClass:function(){if(this.modalBackdrop){if("onlySmallScreens"===this.modalBackdrop&&!this.isSmallScreen())return;this.rsWidget.classList.add("rs-modal")}},setupElements:function(){this.rsWidget=document.querySelector(".rs-widget"),this.rsBackdrop=document.querySelector(".remotestorage-widget-modal-backdrop"),this.rsInitial=document.querySelector(".rs-box-initial"),this.rsChoose=document.querySelector(".rs-box-choose"),this.rsConnected=document.querySelector(".rs-box-connected"),this.rsSignIn=document.querySelector(".rs-box-sign-in"),this.rsConnectedLabel=document.querySelector(".rs-box-connected .rs-sub-headline"),this.rsChooseRemoteStorageButton=document.querySelector("button.rs-choose-rs"),this.rsChooseDropboxButton=document.querySelector("button.rs-choose-dropbox"),this.rsChooseGoogleDriveButton=document.querySelector("button.rs-choose-googledrive"),this.rsErrorBox=document.querySelector(".rs-box-error .rs-error-message"),this.rs.apiKeys.hasOwnProperty("googledrive")||this.rsChooseGoogleDriveButton.parentNode.removeChild(this.rsChooseGoogleDriveButton),this.rs.apiKeys.hasOwnProperty("dropbox")||this.rsChooseDropboxButton.parentNode.removeChild(this.rsChooseDropboxButton),this.rsSignInForm=document.querySelector(".rs-sign-in-form"),this.rsConnectButton=document.querySelector(".rs-connect"),this.rsDisconnectButton=document.querySelector(".rs-disconnect"),this.rsSyncButton=document.querySelector(".rs-sync"),this.rsLogo=document.querySelector(".rs-widget-icon"),this.rsErrorReconnectLink=document.querySelector(".rs-box-error a.rs-reconnect"),this.rsErrorDisconnectButton=document.querySelector(".rs-box-error button.rs-disconnect"),this.rsConnectedUser=document.querySelector(".rs-connected-text h1.rs-user")},setupHandlers:function(){var n=this;this.rs.on("connected",function(){return n.eventHandler("connected")}),this.rs.on("ready",function(){return n.eventHandler("ready")}),this.rs.on("disconnected",function(){return n.eventHandler("disconnected")}),this.rs.on("network-online",function(){return n.eventHandler("network-online")}),this.rs.on("network-offline",function(){return n.eventHandler("network-offline")}),this.rs.on("error",function(t){return n.eventHandler("error",t)}),this.setEventListeners(),this.setClickHandlers()},attach:function(n){this.createBackdropHtml();var t=this.createHtmlTemplate(),e=void 0;if(n){if(e=document.getElementById(n),!parent)throw'Failed to find target DOM element with id="'+n+'"'}else e=document.body;e.appendChild(t),this.setupElements(),this.setupHandlers(),this.setInitialState(),this.setModalClass()},setEventListeners:function(){var n=this;this.rsSignInForm.addEventListener("submit",function(t){t.preventDefault();var e=document.querySelector("input[name=rs-user-address]").value;n.rsConnectButton.disabled=!0,n.rs.connect(e)})},showChooseOrSignIn:function(){this.rsWidget.classList.contains("rs-modal")&&(this.rsBackdrop.style.display="block",this.rsBackdrop.classList.add("visible")),this.rs.apiKeys&&Object.keys(this.rs.apiKeys).length>0?this.setState("choose"):this.setState("sign-in")},setClickHandlers:function(){var n=this;this.rsInitial.addEventListener("click",function(){return n.showChooseOrSignIn()}),this.rsChooseRemoteStorageButton.addEventListener("click",function(){return n.setState("sign-in")}),this.rsChooseDropboxButton.addEventListener("click",function(){return n.rs.dropbox.connect()}),this.rsChooseGoogleDriveButton.addEventListener("click",function(){return n.rs.googledrive.connect()}),this.rsDisconnectButton.addEventListener("click",function(){return n.rs.disconnect()}),this.rsErrorReconnectLink.addEventListener("click",function(){return n.rs.reconnect()}),this.rsErrorDisconnectButton.addEventListener("click",function(){return n.rs.disconnect()}),this.rs.hasFeature("Sync")&&this.rsSyncButton.addEventListener("click",function(){n.rsSyncButton.classList.contains("rs-rotate")?(n.rs.stopSync(),n.rsSyncButton.classList.remove("rs-rotate")):(n.rs.startSync(),n.rsSyncButton.classList.add("rs-rotate"))}),document.addEventListener("click",function(){return n.close()}),this.rsWidget.addEventListener("click",function(n){return n.stopPropagation()}),this.rsLogo.addEventListener("click",function(){return n.toggle()})},toggle:function(){this.closed?this.open():"initial"===this.state?this.showChooseOrSignIn():this.close()},open:function(){this.closed=!1,this.rsWidget.classList.remove("rs-closed"),this.shouldCloseWhenSyncDone=!1},close:function(){var n=this;"error"!==this.state&&(!this.leaveOpen&&this.active?(this.closed=!0,this.rsWidget.classList.add("rs-closed")):this.active?this.setState("connected"):this.setInitialState(),this.rsWidget.classList.contains("rs-modal")&&(this.rsBackdrop.classList.remove("visible"),setTimeout(function(){n.rsBackdrop.style.display="none"},300)))},setOffline:function(){this.online&&(this.rsWidget.classList.add("rs-offline"),this.rsConnectedLabel.textContent="Offline",this.online=!1)},setOnline:function(){this.online||(this.rsWidget.classList.remove("rs-offline"),this.active&&(this.rsConnectedLabel.textContent="Connected")),this.online=!0},setBackendClass:function(n){this.rsWidget.classList.remove("rs-backend-remotestorage"),this.rsWidget.classList.remove("rs-backend-dropbox"),this.rsWidget.classList.remove("rs-backend-googledrive"),n&&this.rsWidget.classList.add("rs-backend-"+n)},showErrorBox:function(n){this.rsErrorBox.innerHTML=n,this.setState("error")},hideErrorBox:function(){this.rsErrorBox.innerHTML="",this.close()},handleDiscoveryError:function(n){var t=document.querySelector(".rs-sign-in-error");t.innerHTML=n.message,t.classList.remove("rs-hidden"),t.classList.add("rs-visible"),this.rsConnectButton.disabled=!1},handleSyncError:function(){this.open(),this.showErrorBox("App sync error")},handleUnauthorized:function(n){n.code&&"access_denied"===n.code?this.rs.disconnect():(this.open(),this.showErrorBox(n.message+" "),this.rsErrorBox.appendChild(this.rsErrorReconnectLink),this.rsErrorReconnectLink.classList.remove("rs-hidden"))},updateLastSyncedOutput:function(){if(this.lastSynced){var n=new Date,t=Math.round((n.getTime()-this.lastSynced.getTime())/1e3);document.querySelector(".rs-box-connected .rs-sub-headline").innerHTML="Synced "+t+" seconds ago"}},isSmallScreen:function(){return window.innerWidth<421}},n.exports=l},function(n,t){n.exports='#remotestorage-widget {\n  z-index: 21000000;\n  position: fixed;\n}\n\n.rs-widget {\n  box-sizing: border-box;\n  overflow: hidden;\n  max-width: 350px;\n  padding: 10px;\n  margin: 10px;\n  border-radius: 3px;\n  background-color: #fff;\n  box-shadow: 0 1px 2px 0 rgba(0,0,0,0.1), 0 3px 8px 0 rgba(0,0,0,0.2);\n  font-family: arial, sans-serif;\n  font-size: 16px;\n  color: #333;\n  will-change: max-height, height, width, opacity, max-width, background, box-shadow;\n  transition-property: width, height, opacity, max-width, max-height, background, box-shadow;\n  transition-duration: 300ms;\n}\n\n.rs-widget * {\n  box-sizing: border-box;\n}\n\n.rs-widget .rs-hidden {\n  display: none;\n}\n\n.rs-box {\n  overflow: hidden;\n  will-change: height;\n  transition-property: height, width, max-height;\n  transition-duration: 300ms;\n  transition-timing-function: ease-in;\n  opacity: 0;\n  max-height: 0px;\n}\n\n.rs-box.rs-selected {\n  opacity: 1;\n  max-height: 420px;\n}\n\n/* Main logo */\n.rs-main-logo {\n  float: left;\n  height: 36px;\n  width: 36px;\n  margin-top: 1px;\n  margin-right: 0.625em;\n  transition: margin-left 300ms ease-out, transform 300ms ease-out;\n  cursor: pointer;\n}\n.rs-widget .rs-backend-remotestorage svg#rs-main-logo-remotestorage,\n{\n  display: normal;\n}\n.rs-widget[class*="rs-backend-"]:not(.rs-backend-remotestorage) svg#rs-main-logo-remotestorage {\n  display: none;\n}\n.rs-widget.rs-backend-dropbox svg#rs-main-logo-dropbox {\n  display: normal;\n}\n.rs-widget:not(.rs-backend-dropbox) svg#rs-main-logo-dropbox {\n  display: none;\n}\n.rs-widget.rs-backend-googledrive svg#rs-main-logo-googledrive {\n  display: normal;\n}\n.rs-widget:not(.rs-backend-googledrive) svg#rs-main-logo-googledrive {\n  display: none;\n}\n\npolygon.rs-logo-shape {\n  fill: #FF4B03;\n}\n\npolygon.rs-logo-shape,\n#rs-main-logo-dropbox path,\n#rs-main-logo-googledrive path {\n  transition-property: fill;\n  transition-duration: 0.5s;\n}\n\n.rs-offline polygon.rs-logo-shape,\n.rs-offline #rs-main-logo-dropbox path,\n.rs-offline #rs-main-logo-googledrive path {\n  fill: #888;\n  transition-property: fill;\n  transition-duration: 0.5s;\n}\n\n/* Hide everything except logo when connected and clicked outside of box */\n.rs-closed {\n  max-width: 56px;\n  background-color: transparent;\n  box-shadow: none;\n  opacity: 0.5;\n\n  transition: max-height 100ms ease-out 0ms, max-width 300ms ease-out 300ms, background 300ms ease-in 200ms, opacity 300ms ease 200ms; \n}\n\n.rs-closed:hover {\n  cursor: pointer;\n  opacity: 1;\n}\n\n.rs-box-initial {\n  transition-duration: 0ms;\n}\n\n.rs-box-initial:hover {\n  cursor: pointer;\n}\n\n.rs-widget a {\n  color: #0093cc;\n}\n\n/* HEADLINE */\n.rs-small-headline {\n  font-size: 1em;\n  font-weight: bold;\n  margin: 0;\n  margin-bottom: 2px;\n  height: 1.2em;\n  word-break: break-all;\n  overflow: hidden;\n  line-height: 1em;\n}\n\n.rs-sub-headline {\n  word-break: break-all;\n  overflow: hidden;\n  color: #666;\n  font-size: 0.92em;\n  height: 1.2em;\n}\n.rs-big-headline {\n  font-size: 1.625em;\n  font-weight: normal;\n  text-align: center;\n  margin-top: 20px;\n  margin-bottom: 20px;\n}\n\n/* BUTTONS  */\n.rs-button {\n  font: inherit;\n  color: inherit;\n  background-color: transparent;\n  border: 1px solid #dcdcdc;\n  border-radius: 3px;\n  cursor: pointer;\n}\n.rs-button-small {\n  padding: 0.5em 0.6em;\n  margin-left: 0.3em;\n  transition: border-color 300ms ease-out;\n}\n.rs-button-wrap {\n  margin-top: 10px;\n}\n\n.rs-button-wrap img,\n.rs-button-wrap svg {\n  float: left;\n  margin-right: 0.6em;\n  width: 40px;\n  height: 40px;\n}\n\n.rs-button-big {\n  padding: 15px 10px;\n  margin-bottom: 10px;\n  display: block;\n  width: 100%;\n  text-align: left;\n  transition: box-shadow 200ms;\n}\n.rs-button-big > div {\n  font-size: 1.125em;\n  padding: 10px 0;\n}\n.rs-button-big:hover {\n  box-shadow: 0 1px 2px 0 rgba(0,0,0,0.1), 0 3px 8px 0 rgba(0,0,0,0.2);\n}\n.rs-button-big:active {\n  background-color: #eee;\n  box-shadow: 0 1px 2px 0 rgba(0,0,0,0.1), 0 3px 8px 0 rgba(0,0,0,0.2);\n}\n.rs-button-big:last-child {\n  margin-bottom: 0;\n}\n\n.rs-content {\n  padding: 0 10px 10px 10px;\n}\n\n\n.rs-state-choose .rs-main-logo,\n.rs-state-sign-in .rs-main-logo {\n  margin-left: 45%;\n  float: none;\n}\n\n.rs-sign-in-form input[type=text] {\n  padding: 15px 10px;\n  display: block;\n  width: 100%;\n  font: inherit;\n  height: 52px;\n  border: 1px solid #aaa;\n  border-radius: 0;\n  box-shadow: none;\n}\n.rs-sign-in-form input[type=submit] {\n  padding: 15px 10px;\n  margin-top: 20px;\n  margin-bottom: 15px;\n  display: block;\n  width: 100%;\n  border: none;\n  border-radius: 3px;\n  background-color: #3fb34f;\n  font: inherit;\n  color: #fff;\n  transition: box-shadow 200ms, background-color 200ms;\n}\n.rs-sign-in-form input[type=submit]:hover {\n  cursor: pointer;\n  background-color: #4BCB5D;\n  box-shadow: 0 1px 2px 0 rgba(0,0,0,0.1), 0 3px 8px 0 rgba(0,0,0,0.2);\n}\n.rs-sign-in-form input[type=submit]:active {\n  background-color: #3fb34f;\n}\n.rs-sign-in-form input[type=submit]:disabled,\n.rs-sign-in-form input[type=submit]:disabled:hover {\n  background-color: #aaa;\n}\n\n.rs-sign-in-error.rs-hidden,\n.rs-box-error.rs-hidden {\n  height: 0;\n}\n\n.rs-sign-in-error.rs-visible,\n.rs-box-error.rs-visible {\n  height: auto;\n  border-radius: 3px;\n  padding: 0.5em 0.5em;\n  margin-top: 0.5em;\n  text-align: center;\n  background-color: rgba(255,0,0,0.1);\n  color: darkred;\n}\n\n.rs-box-error {\n  display: flex;\n  flex-direction: row;\n}\n\n.rs-box-error .rs-error-message {\n  flex: auto;\n}\n\n /*Choose provider box */\n.rs-box-choose {\n  text-align: center;\n  overflow: hidden;\n}\n\n.rs-box-choose p {\n  margin-top: 0;\n  margin-bottom: 20px;\n  line-height: 1.4em;\n}\n\n/*Connected box */\n.rs-box-connected {\n  display: flex;\n  flex-direction: row;\n  height: 40px;\n  transition: height 0s;\n}\n.rs-connected-text {\n  flex: auto;\n  min-width: 0;\n}\n.rs-box-connected .rs-user {\n  font-weight: bold;\n  text-overflow: ellipsis;\n  overflow: hidden;\n  word-break: keep-all;\n}\n.rs-connected-buttons, .rs-error-buttons {\n  flex: none;\n}\n.rs-disconnect:hover {\n  border-color: #FF2D2D;\n}\n.rs-disconnect:hover .rs-icon{\n  fill: #FF2D2D;\n}\n.rs-sync:hover {\n  border-color: #FFBB0C;\n}\n.rs-sync:hover .rs-icon {\n  fill: #FFBB0C;\n}\n.rs-sync.rs-rotate {\n  border-color: #FFBB0C;\n}\n.rs-sync.rs-rotate .rs-icon {\n  fill: #FFBB0C;\n  animation: rs-spin 1s linear infinite;\n}\n@keyframes rs-spin { 100% { transform: rotate(360deg); transform:rotate(360deg); } }\n\n/* Floating widget styles (top right corner) */\n.rs-floating {\n  position: fixed;\n  top: 0;\n  right: 0;\n}\n\n\n/* Small/mobile screens */\n@media screen and (max-width: 420px) {\n  .rs-widget {\n    font-size: 100%;\n    transition: all 300ms ease-out;\n    max-width: 400px;\n  }\n  .rs-floating {\n    position: relative;\n    top: auto;\n    right: auto\n  }\n  .rs-closed {\n    max-width: 56px;\n  }\n  .rs-state-choose,\n  .rs-state-sign-in {\n    position: fixed;\n    top: 0;\n    left: 0;\n    right: 0;\n    max-width: 100%;\n  }\n}\n\n/* remove dotted outline border on Firefox */\n.rs-widget a:focus,\n.rs-widget a:active,\n.rs-widget button:focus,\n.rs-widget input:focus {\n  outline:none;\n}\n.rs-widget button::-moz-focus-inner,\n.rs-widget input[type="button"]::-moz-focus-inner,\n.rs-widget input[type="submit"]::-moz-focus-inner {\n  border:0;\n}\n\n/* prevent rounded buttons on mobile Safari */\n.rs-widget input[type="button"],\n.rs-widget input[type="submit"] {\n  -webkit-appearance: none;\n}\n\n.remotestorage-widget-modal-backdrop {\n  display: none;\n  z-index: 20000000;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.5);\n  opacity: 0;\n  transition: opacity 0.3s linear;\n}\n\n.remotestorage-widget-modal-backdrop.visible {\n  opacity: 1;\n  transition: opacity 0.3s linear;\n}\n'},function(n,t){n.exports='\x3c!--\n  rs-state-initial\n  rs-state-choose\n  rs-state-sign-in\n  rs-state-connected\n  rs-state-error\n\n  rs-offline\n  rs-closed\n  rs-modal\n  rs-floating        - TODO - will make it fixed in the right corner\n--\x3e\n\n\x3c!-- RS WIDGET, a class named rs-state-<state> indicate\n its current state --\x3e\n<div class="rs-widget rs-state-initial">\n\n  <div class="rs-widget-icon">\n    <svg class="rs-main-logo" id="rs-main-logo-remotestorage" xmlns="http://www.w3.org/2000/svg" version="1.1"\n         xml:space="preserve" width="0.739008in" height="0.853339in"\n         style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"\n         viewBox="0 0 739 853" xmlns:xlink="http://www.w3.org/1999/xlink">\n      <g>\n        <polygon class="rs-logo-shape" points="370,754 0,542 0,640 185,747 370,853 554,747 739,640 739,525 739,525 739,476 739,427 739,378 653,427 370,589 86,427 86,427 86,361 185,418 370,524 554,418 653,361 739,311 739,213 739,213 554,107 370,0 185,107 58,180 144,230 228,181 370,100 511,181 652,263 370,425 87,263 87,263 0,213 0,213 0,311 0,378 0,427 0,476 86,525 185,582 370,689 554,582 653,525 653,590 653,592 "/>\n      </g>\n    </svg>\n    <svg class="rs-main-logo" id="rs-main-logo-dropbox" width="40" height="36"\n         xml:space="preserve" stroke-miterlimit="1.4142"\n         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 104 97"\n                                            stroke-linejoin="round" version="1.1" clip-rule="evenodd">\n      <path d="m30.691 0l-30.691 20.039 21.221 16.994 30.946-19.108-21.476-17.925z" fill="#007ee5"/>\n      <path d="m0 54.028l30.691 20.039 21.476-17.926-30.945-19.108-21.222 16.995z" fill="#007ee5"/>\n      <path d="m52.167 56.142l21.477 17.926 30.696-20.039-21.227-16.995-30.946 19.108z" fill="#007ee5"/>\n      <path d="m104.34 20.039l-30.696-20.039-21.477 17.925 30.946 19.108 21.227-16.994z" fill="#007ee5"/>\n      <path d="m52.23 59.998l-21.538 17.873-9.218-6.018v6.747l30.756 18.443 30.756-18.443v-6.747l-9.216 6.018-21.54-17.873z" fill="#007ee5"/>\n    </svg>\n    <svg class="rs-main-logo" id="rs-main-logo-googledrive"\n         width="40" height="40" version="1.1"\n         stroke-miterlimit="1.4142" xml:space="preserve"\n         xmlns="http://www.w3.org/2000/svg" width="100%" stroke-linejoin="round"\n                                                         clip-rule="evenodd" viewBox="0 0 511 442" height="100%">\n      <path d="m166.23 0.10955l0.44-0.009998 0.04 0.49-0.56 0.02 0.08-0.5z" fill="#0ba25e"/>\n      <path d="m164.52 3.3896c0.39-0.56 1.15-1.68 1.54-2.24l0.7-0.11c0.52 6.34 2.38 12.45 3.78 18.62 6.02 26.81 12.1 53.6 18.43 80.33 4.15 18.33 8.78 36.57 12.58 54.97 3.3 11.39 5.07 23.14 8.26 34.55 2.06 10.64 5.29 21.02 7.02 31.72-0.7 1.17-1.4 2.35-2.11 3.52-0.17 0.27-0.53 0.83-0.7 1.11l-0.26 0.45c-0.04 0.08-0.13 0.25-0.18 0.33-0.39 0.72-1.17 2.15-1.56 2.86-0.07 0.13-0.22 0.39-0.3 0.52l-0.71 1.26c-0.09 0.15-0.26 0.44-0.34 0.59l-0.83 1.45c-0.04 0.08-0.13 0.24-0.18 0.32l-1.03 1.8c-0.09 0.15-0.27 0.47-0.36 0.63-0.1 0.17-0.3 0.5-0.4 0.67l-1.01 1.74c-0.08 0.13-0.24 0.41-0.32 0.54l-0.62 1.05c-0.1 0.18-0.32 0.55-0.42 0.73-0.4 0.69-1.19 2.07-1.59 2.77-0.07 0.12-0.22 0.37-0.29 0.5l-0.63 1.05c-0.1 0.19-0.32 0.55-0.43 0.74-0.37 0.65-1.13 1.94-1.5 2.59-0.09 0.15-0.26 0.46-0.35 0.61l-0.75 1.35c-0.07 0.12-0.21 0.37-0.28 0.49l-0.77 1.35-0.32 0.56c-0.4 0.66-1.18 1.99-1.57 2.66-0.09 0.15-0.27 0.46-0.36 0.62l-0.71 1.28c-0.07 0.12-0.2 0.35-0.26 0.47l-0.82 1.42c-0.07 0.13-0.22 0.38-0.29 0.5-0.42 0.72-1.25 2.15-1.67 2.87-0.07 0.13-0.23 0.39-0.31 0.52l-0.6 0.99c-0.1 0.18-0.31 0.53-0.41 0.71-0.38 0.66-1.13 1.97-1.51 2.63-0.06 0.11-0.2 0.35-0.26 0.46l-0.82 1.46c-0.06 0.11-0.18 0.32-0.24 0.43l-0.84 1.47c-0.07 0.11-0.21 0.35-0.28 0.46-0.39 0.66-1.18 1.96-1.57 2.62-0.11 0.17-0.31 0.53-0.42 0.71l-0.59 1.1c-0.08 0.14-0.23 0.42-0.3 0.55l-1.03 1.78c-0.08 0.15-0.26 0.45-0.34 0.6-0.09 0.14-0.25 0.43-0.33 0.57-1.69 2.85-3.22 5.79-5.09 8.53-10.65 17.37-20.45 35.28-30.76 52.86-16.74 28.97-33.46 57.94-50.2 86.91-2.51 4.51-5.37 8.83-7.53 13.54-0.11 0.16-0.33 0.48-0.44 0.64-4.01-5.78-7.11-12.16-10.78-18.16-16.74-28.99-33.49-57.97-50.22-86.97-9.3-16.31-18.99-32.42-28.04-48.88 35.84-61.85 71.52-123.81 107.29-185.7 0.03-0.06 0.1-0.17 0.13-0.23l0.98-1.68c0.04-0.07 0.12-0.21 0.17-0.28l0.99-1.69c0.09-0.15 0.27-0.47 0.37-0.62 0.1-0.18 0.3-0.53 0.4-0.71l0.99-1.76 0.28-0.48 0.74-1.18c0.13-0.19 0.39-0.58 0.52-0.78 0.36-0.54 1.09-1.63 1.46-2.17 0.04-0.18 0.11-0.53 0.15-0.7l0.87-1.67 0.16-0.28 1-1.68c0.1-0.16 0.3-0.5 0.4-0.67 0.11-0.19 0.33-0.58 0.45-0.78l0.84-1.47c0.09-0.16 0.27-0.47 0.36-0.62l0.7-1.23c0.08-0.13 0.24-0.41 0.32-0.55 0.39-0.67 1.17-2.01 1.57-2.68l0.32-0.56 0.73-1.33c0.06-0.1 0.18-0.32 0.24-0.42l0.96-1.68c0.12-0.2 0.37-0.61 0.5-0.81l0.65-0.98c0.14-0.2 0.41-0.59 0.55-0.78 0.12-0.2 0.37-0.6 0.5-0.8l0.54-1.04c0.18-0.39 0.55-1.17 0.73-1.56l0.55-0.77c0.1-0.14 0.29-0.41 0.39-0.55 0.37-0.31 1.12-0.95 1.49-1.27l-0.07-1.17c0.06-0.14 0.2-0.42 0.27-0.56l0.85-1.69c0.07-0.12 0.21-0.37 0.28-0.49l0.75-1.22c0.14-0.2 0.41-0.6 0.55-0.8 0.34-0.56 1.03-1.66 1.37-2.21 0.1-0.2 0.28-0.59 0.38-0.79l0.53-1.15c0.16-0.3 0.5-0.91 0.67-1.22l0.66-0.94c0.14-0.19 0.42-0.57 0.56-0.76 0.12-0.2 0.37-0.6 0.49-0.8l0.49-1.03c0.11-0.23 0.34-0.7 0.45-0.93l0.86-1.66c0.13-0.2 0.38-0.62 0.5-0.83l0.66-0.98c0.14-0.2 0.42-0.58 0.56-0.78 0.13-0.2 0.38-0.61 0.5-0.81l0.51-1.04c0.15-0.29 0.44-0.86 0.58-1.15l0.68-1.12c0.12-0.19 0.36-0.57 0.49-0.76 0.36-0.56 1.09-1.68 1.46-2.23 0.04-0.17 0.13-0.51 0.17-0.68l0.87-1.66c0.03-0.07 0.11-0.2 0.15-0.27l1-1.71c0.1-0.17 0.29-0.5 0.39-0.67 0.1-0.16 0.29-0.5 0.39-0.66l1-1.75c0.04-0.07 0.12-0.21 0.16-0.29l0.94-1.63c0.05-0.07 0.14-0.22 0.18-0.3l1.01-1.7c0.09-0.16 0.27-0.46 0.36-0.62 0.1-0.17 0.3-0.52 0.4-0.7l0.96-1.69c0.06-0.11 0.18-0.32 0.24-0.42l0.81-1.31c0.13-0.19 0.39-0.58 0.51-0.77 0.37-0.55 1.09-1.65 1.46-2.2 0.03-0.17 0.11-0.51 0.15-0.68l0.85-1.63c0.07-0.12 0.22-0.37 0.29-0.5l0.76-1.19c0.14-0.19 0.4-0.58 0.54-0.78 0.35-0.56 1.05-1.67 1.4-2.23 0.05-0.16 0.14-0.48 0.19-0.64l0.88-1.65c0.04-0.08 0.12-0.22 0.16-0.3l1.02-1.7204c0.08-0.15 0.26-0.45 0.35-0.6 0.09-0.17 0.27-0.49 0.36-0.65l1-1.78c0.05-0.07 0.13-0.22 0.17-0.29l0.95-1.61c0.03-0.05 0.1-0.16 0.13-0.21z" fill="#0ba25e"/>\n      <path id="#fccd48ff" fill="#fccd48" d="m166.67 0.099552c32.48-0.15 64.97-0.02 97.46-0.07 26.88 0.099998 53.78-0.21 80.65 0.15 0.53 1.02 1.07 2.03 1.64 3.04 0.08 0.13 0.23 0.41 0.31 0.55 2.6 4.53 5.24 9.0404 7.81 13.58 0.05 0.08 0.13 0.23 0.18 0.3 4.98 8.55 9.94 17.11 14.83 25.71 0.04 0.07 0.13 0.21 0.17 0.29 0.89 1.53 1.81 3.06 2.72 4.6 0.08 0.13 0.23 0.4 0.31 0.54 1.25 2.2 2.51 4.39 3.78 6.58 0.05 0.07 0.13 0.21 0.17 0.29 1.27 2.18 2.52 4.38 3.78 6.58 0.08 0.13 0.23 0.39 0.31 0.52 1.27 2.14 2.5 4.3 3.72 6.46 0.08 0.13 0.23 0.4 0.31 0.53 11.94 20.62 23.85 41.25 35.75 61.9-1.36 5.99 1.93 11.95 0.1 17.97-1.11 16.45-7.32 32.46-17.56 45.38-3.33 3.61-0.1 8.62-0.46 12.9-3.59 4.35-8.21 7.6-12.57 11.11 0.01 3.28 0.02 6.56-0.13 9.85-3.89 2.57-8.35 4.06-12.3 6.54-0.42 1.33-0.85 2.67-1.26 4.02-3.17 1.5-6.56 2.47-9.73 3.99-12.55-3.6-24.97-7.61-37.49-11.32-11.65-3.85-23.58-6.87-35.11-11.11l-0.66-1.18c-0.06-0.09-0.16-0.27-0.21-0.36l-0.95-1.61-0.4-0.68c-0.09-0.15-0.25-0.44-0.34-0.59-0.71-1.26-1.36-2.55-1.94-3.87l-0.06-0.13-0.07-0.1c-0.44-0.7-1.33-2.11-1.78-2.81-0.05-0.08-0.15-0.24-0.2-0.33l-0.29-0.46c-0.17-0.29-0.52-0.86-0.69-1.15-0.43-0.73-1.3-2.19-1.73-2.92-0.11-0.18-0.32-0.55-0.43-0.73l-0.61-1.04c-0.08-0.14-0.24-0.41-0.32-0.55l-0.97-1.67c-0.04-0.07-0.13-0.21-0.17-0.29l-1.02-1.75c-0.09-0.16-0.27-0.48-0.36-0.63-0.1-0.18-0.31-0.55-0.41-0.73l-0.83-1.46c-0.1-0.17-0.3-0.53-0.4-0.7l-0.64-1.13c-0.09-0.15-0.26-0.45-0.35-0.6-0.39-0.72-1.18-2.15-1.58-2.87-0.04-0.08-0.14-0.25-0.18-0.33l-0.27-0.47c-0.19-0.28-0.56-0.84-0.75-1.12-0.99-1.66-1.92-3.36-2.81-5.07-0.05-0.08-0.14-0.25-0.18-0.33l-0.26-0.46c-0.19-0.27-0.57-0.81-0.76-1.07-0.44-0.79-1.32-2.37-1.76-3.16-0.05-0.08-0.15-0.25-0.19-0.34l-0.27-0.48c-0.2-0.33-0.59-0.98-0.79-1.31l-0.86-1.45c-0.08-0.14-0.25-0.42-0.34-0.57-0.38-0.66-1.15-1.99-1.54-2.65-0.09-0.15-0.26-0.46-0.35-0.62-0.68-1.13-1.33-2.27-1.97-3.41-0.05-0.09-0.16-0.27-0.21-0.36l-0.95-1.63c-0.1-0.16-0.29-0.48-0.38-0.65-0.09-0.15-0.26-0.45-0.35-0.6l-1.01-1.76c-0.05-0.08-0.14-0.25-0.19-0.33l-0.82-1.47c-0.07-0.12-0.2-0.35-0.27-0.47-0.38-0.67-1.15-2-1.53-2.67-0.08-0.14-0.24-0.41-0.32-0.54-0.62-1.03-1.24-2.07-1.84-3.1-27.73-48.12-55.5-96.21-83.29-144.3-1.74-3.0004-3.32-6.1204-5.45-8.8804l-0.05-0.45005-0.04-0.49z"/>\n      <path d="m166.15 0.60955l0.56-0.02 0.05 0.45005-0.7 0.11 0.09-0.54005z" fill="#089156"/>\n      <path d="m166.76 1.0396c2.13 2.76 3.71 5.88 5.45 8.88 27.79 48.09 55.56 96.18 83.29 144.3-12.84 22.36-25.78 44.67-38.67 67.01-1.73-10.7-4.96-21.08-7.02-31.72-3.19-11.41-4.96-23.16-8.26-34.55-3.8-18.4-8.43-36.64-12.58-54.97-6.33-26.73-12.41-53.52-18.43-80.33-1.4-6.17-3.26-12.28-3.78-18.62z" fill="#089156"/>\n      <path d="m164.39 3.5996c0.03-0.05 0.1-0.16 0.13-0.21-0.03 0.05-0.1 0.16-0.13 0.21z" fill="#089156"/>\n      <path d="m163.27 5.4996c0.05-0.07 0.13-0.22 0.17-0.29-0.04 0.07-0.12 0.22-0.17 0.29z" fill="#089156"/>\n      <path d="m161.91 7.9296c0.09-0.17 0.27-0.49 0.36-0.65-0.09 0.16-0.27 0.48-0.36 0.65z" fill="#089156"/>\n      <path d="m161.56 8.5296c0.08-0.15 0.26-0.45 0.35-0.6-0.09 0.15-0.27 0.45-0.35 0.6z" fill="#089156"/>\n      <path d="m160.38 10.55c0.04-0.08 0.12-0.22 0.16-0.3-0.04 0.08-0.12 0.22-0.16 0.3z" fill="#089156"/>\n      <path d="m159.31 12.84c0.05-0.16 0.14-0.48 0.19-0.64-0.05 0.16-0.14 0.48-0.19 0.64z" fill="#089156"/>\n      <path d="m157.91 15.07c0.35-0.56 1.05-1.67 1.4-2.23-0.35 0.56-1.05 1.67-1.4 2.23z" fill="#089156"/>\n      <path d="m157.37 15.85c0.14-0.19 0.4-0.58 0.54-0.78-0.14 0.2-0.4 0.59-0.54 0.78z" fill="#089156"/>\n      <path d="m156.32 17.54c0.07-0.12 0.22-0.37 0.29-0.5-0.07 0.13-0.22 0.38-0.29 0.5z" fill="#089156"/>\n      <path d="m155.32 19.85c0.03-0.17 0.11-0.51 0.15-0.68-0.04 0.17-0.12 0.51-0.15 0.68z" fill="#089156"/>\n      <path d="m153.86 22.05c0.37-0.55 1.09-1.65 1.46-2.2-0.37 0.55-1.09 1.65-1.46 2.2z" fill="#089156"/>\n      <path d="m153.35 22.82c0.13-0.19 0.39-0.58 0.51-0.77-0.12 0.19-0.38 0.58-0.51 0.77z" fill="#089156"/>\n      <path d="m152.3 24.55c0.06-0.11 0.18-0.32 0.24-0.42-0.06 0.1-0.18 0.31-0.24 0.42z" fill="#089156"/>\n      <path d="m150.94 26.94c0.1-0.17 0.3-0.52 0.4-0.7-0.1 0.18-0.3 0.53-0.4 0.7z" fill="#089156"/>\n      <path d="m150.58 27.56c0.09-0.16 0.27-0.46 0.36-0.62-0.09 0.16-0.27 0.46-0.36 0.62z" fill="#089156"/>\n      <path d="m149.39 29.56c0.05-0.07 0.14-0.22 0.18-0.3-0.04 0.08-0.13 0.23-0.18 0.3z" fill="#089156"/>\n      <path d="m148.29 31.48c0.04-0.07 0.12-0.21 0.16-0.29-0.04 0.08-0.12 0.22-0.16 0.29z" fill="#089156"/>\n      <path d="m146.9 33.89c0.1-0.16 0.29-0.5 0.39-0.66-0.1 0.16-0.29 0.5-0.39 0.66z" fill="#089156"/>\n      <path d="m146.51 34.56c0.1-0.17 0.29-0.5 0.39-0.67-0.1 0.17-0.29 0.5-0.39 0.67z" fill="#089156"/>\n      <path d="m145.36 36.54c0.03-0.07 0.11-0.2 0.15-0.27-0.04 0.07-0.12 0.2-0.15 0.27z" fill="#089156"/>\n      <path d="m144.32 38.88c0.04-0.17 0.13-0.51 0.17-0.68-0.04 0.17-0.13 0.51-0.17 0.68z" fill="#089156"/>\n      <path d="m142.86 41.11c0.36-0.56 1.09-1.68 1.46-2.23-0.37 0.55-1.1 1.67-1.46 2.23z" fill="#089156"/>\n      <path d="m142.37 41.87c0.12-0.19 0.36-0.57 0.49-0.76-0.13 0.19-0.37 0.57-0.49 0.76z" fill="#089156"/>\n      <path d="m141.11 44.14c0.15-0.29 0.44-0.86 0.58-1.15-0.14 0.29-0.43 0.86-0.58 1.15z" fill="#089156"/>\n      <path d="m140.1 45.99c0.13-0.2 0.38-0.61 0.5-0.81-0.12 0.2-0.37 0.61-0.5 0.81z" fill="#089156"/>\n      <path d="m139.54 46.77c0.14-0.2 0.42-0.58 0.56-0.78-0.14 0.2-0.42 0.58-0.56 0.78z" fill="#089156"/>\n      <path d="m138.38 48.58c0.13-0.2 0.38-0.62 0.5-0.83-0.12 0.21-0.37 0.63-0.5 0.83z" fill="#089156"/>\n      <path d="m137.07 51.17c0.11-0.23 0.34-0.7 0.45-0.93-0.11 0.23-0.34 0.7-0.45 0.93z" fill="#089156"/>\n      <path d="m136.09 53c0.12-0.2 0.37-0.6 0.49-0.8-0.12 0.2-0.37 0.6-0.49 0.8z" fill="#089156"/>\n      <path d="m135.53 53.76c0.14-0.19 0.42-0.57 0.56-0.76-0.14 0.19-0.42 0.57-0.56 0.76z" fill="#089156"/>\n      <path d="m134.2 55.92c0.16-0.3 0.5-0.91 0.67-1.22-0.17 0.31-0.51 0.92-0.67 1.22z" fill="#089156"/>\n      <path d="m133.29 57.86c0.1-0.2 0.28-0.59 0.38-0.79-0.1 0.2-0.28 0.59-0.38 0.79z" fill="#089156"/>\n      <path d="m131.92 60.07c0.34-0.56 1.03-1.66 1.37-2.21-0.34 0.55-1.03 1.65-1.37 2.21z" fill="#089156"/>\n      <path d="m131.37 60.87c0.14-0.2 0.41-0.6 0.55-0.8-0.14 0.2-0.41 0.6-0.55 0.8z" fill="#089156"/>\n      <path d="m130.34 62.58c0.07-0.12 0.21-0.37 0.28-0.49-0.07 0.12-0.21 0.37-0.28 0.49z" fill="#089156"/>\n      <path d="m129.22 64.83c0.06-0.14 0.2-0.42 0.27-0.56-0.07 0.14-0.21 0.42-0.27 0.56z" fill="#089156"/>\n      <path d="m127.8 67.27c0.35-0.61 1.06-1.83 1.42-2.44l0.07 1.17c-0.37 0.32-1.12 0.96-1.49 1.27z" fill="#089156"/>\n      <path d="m127.41 67.82c0.1-0.14 0.29-0.41 0.39-0.55-0.1 0.14-0.29 0.41-0.39 0.55z" fill="#089156"/>\n      <path d="m126.13 70.15c0.18-0.39 0.55-1.17 0.73-1.56-0.18 0.39-0.55 1.17-0.73 1.56z" fill="#089156"/>\n      <path d="m125.09 71.99c0.12-0.2 0.37-0.6 0.5-0.8-0.13 0.2-0.38 0.6-0.5 0.8z" fill="#089156"/>\n      <path d="m124.54 72.77c0.14-0.2 0.41-0.59 0.55-0.78-0.14 0.19-0.41 0.58-0.55 0.78z" fill="#089156"/>\n      <path d="m123.39 74.56c0.12-0.2 0.37-0.61 0.5-0.81-0.13 0.2-0.38 0.61-0.5 0.81z" fill="#089156"/>\n      <path d="m122.19 76.66c0.06-0.1 0.18-0.32 0.24-0.42-0.06 0.1-0.18 0.32-0.24 0.42z" fill="#089156"/>\n      <path d="m121.14 78.55l0.32-0.56-0.32 0.56z" fill="#089156"/>\n      <path d="m119.25 81.78c0.08-0.13 0.24-0.41 0.32-0.55-0.08 0.14-0.24 0.42-0.32 0.55z" fill="#089156"/>\n      <path d="m118.19 83.63c0.09-0.16 0.27-0.47 0.36-0.62-0.09 0.15-0.27 0.46-0.36 0.62z" fill="#089156"/>\n      <path d="m116.9 85.88c0.11-0.19 0.33-0.58 0.45-0.78-0.12 0.2-0.34 0.59-0.45 0.78z" fill="#089156"/>\n      <path d="m116.5 86.55c0.1-0.16 0.3-0.5 0.4-0.67-0.1 0.17-0.3 0.51-0.4 0.67z" fill="#089156"/>\n      <path d="m115.34 88.51l0.16-0.28-0.16 0.28z" fill="#089156"/>\n      <path d="m114.32 90.88c0.04-0.18 0.11-0.53 0.15-0.7-0.04 0.17-0.11 0.52-0.15 0.7z" fill="#089156"/>\n      <path d="m112.86 93.05c0.36-0.54 1.09-1.63 1.46-2.17-0.37 0.54-1.1 1.63-1.46 2.17z" fill="#089156"/>\n      <path d="m112.34 93.83c0.13-0.19 0.39-0.58 0.52-0.78-0.13 0.2-0.39 0.59-0.52 0.78z" fill="#089156"/>\n      <path d="m111.32 95.49l0.28-0.48-0.28 0.48z" fill="#089156"/>\n      <path d="m109.93 97.96c0.1-0.18 0.3-0.53 0.4-0.71-0.1 0.18-0.3 0.53-0.4 0.71z" fill="#089156"/>\n      <path d="m109.56 98.58c0.09-0.15 0.27-0.47 0.37-0.62-0.1 0.15-0.28 0.47-0.37 0.62z" fill="#089156"/>\n      <path d="m108.4 100.55c0.04-0.07 0.12-0.21 0.17-0.28-0.05 0.07-0.13 0.21-0.17 0.28z" fill="#089156"/>\n      <path d="m107.29 102.46c0.03-0.06 0.1-0.17 0.13-0.23-0.03 0.06-0.1 0.17-0.13 0.23z" fill="#089156"/>\n      <path d="m214.02 225.86c0.17-0.28 0.53-0.84 0.7-1.11-0.17 0.27-0.53 0.83-0.7 1.11z" fill="#089156"/>\n      <path d="m213.58 226.64c0.05-0.08 0.14-0.25 0.18-0.33-0.04 0.08-0.13 0.25-0.18 0.33z" fill="#089156"/>\n      <path d="m211.72 230.02c0.08-0.13 0.23-0.39 0.3-0.52-0.07 0.13-0.22 0.39-0.3 0.52z" fill="#089156"/>\n      <path d="m210.67 231.87c0.08-0.15 0.25-0.44 0.34-0.59-0.09 0.15-0.26 0.44-0.34 0.59z" fill="#089156"/>\n      <path d="m209.66 233.64c0.05-0.08 0.14-0.24 0.18-0.32-0.04 0.08-0.13 0.24-0.18 0.32z" fill="#089156"/>\n      <path d="m208.27 236.07c0.09-0.16 0.27-0.48 0.36-0.63-0.09 0.15-0.27 0.47-0.36 0.63z" fill="#089156"/>\n      <path d="m207.87 236.74c0.1-0.17 0.3-0.5 0.4-0.67-0.1 0.17-0.3 0.5-0.4 0.67z" fill="#089156"/>\n      <path d="m206.54 239.02c0.08-0.13 0.24-0.41 0.32-0.54-0.08 0.13-0.24 0.41-0.32 0.54z" fill="#089156"/>\n      <path d="m205.5 240.8c0.1-0.18 0.32-0.55 0.42-0.73-0.1 0.18-0.32 0.55-0.42 0.73z" fill="#089156"/>\n      <path d="m203.62 244.07c0.07-0.13 0.22-0.38 0.29-0.5-0.07 0.12-0.22 0.37-0.29 0.5z" fill="#089156"/>\n      <path d="m202.56 245.86c0.11-0.19 0.33-0.55 0.43-0.74-0.1 0.19-0.32 0.55-0.43 0.74z" fill="#089156"/>\n      <path d="m200.71 249.06c0.09-0.15 0.26-0.46 0.35-0.61-0.09 0.15-0.26 0.46-0.35 0.61z" fill="#089156"/>\n      <path d="m199.68 250.9c0.07-0.12 0.21-0.37 0.28-0.49-0.07 0.12-0.21 0.37-0.28 0.49z" fill="#089156"/>\n      <path d="m198.59 252.81l0.32-0.56-0.32 0.56z" fill="#089156"/>\n      <path d="m196.66 256.09c0.09-0.16 0.27-0.47 0.36-0.62-0.09 0.15-0.27 0.46-0.36 0.62z" fill="#089156"/>\n      <path d="m195.69 257.84c0.06-0.12 0.19-0.35 0.26-0.47-0.07 0.12-0.2 0.35-0.26 0.47z" fill="#089156"/>\n      <path d="m194.58 259.76c0.07-0.12 0.22-0.37 0.29-0.5-0.07 0.13-0.22 0.38-0.29 0.5z" fill="#089156"/>\n      <path d="m192.6 263.15c0.08-0.13 0.24-0.39 0.31-0.52-0.07 0.13-0.23 0.39-0.31 0.52z" fill="#089156"/>\n      <path d="m191.59 264.85c0.1-0.18 0.31-0.53 0.41-0.71-0.1 0.18-0.31 0.53-0.41 0.71z" fill="#089156"/>\n      <path d="m189.82 267.94c0.06-0.11 0.2-0.35 0.26-0.46-0.06 0.11-0.2 0.35-0.26 0.46z" fill="#089156"/>\n      <path d="m188.76 269.83c0.06-0.11 0.18-0.32 0.24-0.43-0.06 0.11-0.18 0.32-0.24 0.43z" fill="#089156"/>\n      <path d="m187.64 271.76c0.07-0.11 0.21-0.35 0.28-0.46-0.07 0.11-0.21 0.35-0.28 0.46z" fill="#089156"/>\n      <path d="m185.65 275.09c0.11-0.18 0.31-0.54 0.42-0.71-0.11 0.17-0.31 0.53-0.42 0.71z" fill="#089156"/>\n      <path d="m184.76 276.74c0.07-0.13 0.22-0.41 0.3-0.55-0.08 0.14-0.23 0.42-0.3 0.55z" fill="#089156"/>\n      <path d="m183.39 279.12c0.08-0.15 0.26-0.45 0.34-0.6-0.08 0.15-0.26 0.45-0.34 0.6z" fill="#089156"/>\n      <path d="m183.06 279.69c0.08-0.14 0.24-0.43 0.33-0.57-0.09 0.14-0.25 0.43-0.33 0.57z" fill="#089156"/>\n      <path d="m346.42 3.2196c0.08 0.13 0.23 0.41 0.31 0.55-0.08-0.14-0.23-0.42-0.31-0.55z" fill="#10985b"/>\n      <path d="m354.54 17.35c0.05 0.08 0.13 0.23 0.18 0.3-0.05-0.07-0.13-0.22-0.18-0.3z" fill="#10985b"/>\n      <path d="m369.55 43.36c0.04 0.07 0.13 0.21 0.17 0.29-0.04-0.08-0.13-0.22-0.17-0.29z" fill="#10985b"/>\n      <path d="m372.44 48.25c0.08 0.13 0.23 0.4 0.31 0.54-0.08-0.14-0.23-0.41-0.31-0.54z" fill="#10985b"/>\n      <path d="m376.53 55.37c0.05 0.07 0.13 0.21 0.17 0.29-0.04-0.08-0.12-0.22-0.17-0.29z" fill="#10985b"/>\n      <path d="m380.48 62.24c0.08 0.13 0.23 0.39 0.31 0.52-0.08-0.13-0.23-0.39-0.31-0.52z" fill="#10985b"/>\n      <path d="m384.51 69.22c0.08 0.13 0.23 0.4 0.31 0.53-0.08-0.13-0.23-0.4-0.31-0.53z" fill="#10985b"/>\n      <path d="m420.57 131.65c17.39 30.13 34.89 60.21 52.14 90.43 0.08 0.13 0.24 0.41 0.32 0.54 0.4 0.66 1.2 1.97 1.59 2.62 0.08 0.13 0.24 0.4 0.32 0.53l0.8 1.37c0.05 0.09 0.15 0.28 0.21 0.37l1 1.74c0.1 0.17 0.29 0.52 0.39 0.69 0.09 0.15 0.27 0.46 0.36 0.62l0.98 1.7 0.16 0.28 0.98 1.68c0.04 0.07 0.12 0.2 0.16 0.27l0.98 1.7c0.1 0.17 0.29 0.5 0.39 0.67 0.09 0.17 0.28 0.5 0.37 0.66l1.01 1.78c0.14 0.35 0.42 1.03 0.56 1.37l0.28 0.55 0.13 0.29 0.22 0.29 0.59 0.89c0.21 0.33 0.62 0.99 0.83 1.32 0.12 0.2 0.38 0.6 0.5 0.8l0.66 1.05c0.2 0.45 0.6 1.36 0.8 1.81l0.3 0.54 0.11 0.21 0.09 0.13c0.44 0.65 1.31 1.96 1.75 2.61 0.11 0.18 0.33 0.54 0.45 0.72l0.6 1c0.09 0.16 0.27 0.47 0.36 0.62l1.01 1.77c0.09 0.16 0.27 0.48 0.36 0.64s0.27 0.47 0.36 0.63l1 1.71c0.04 0.08 0.13 0.23 0.18 0.3l0.91 1.6c0.06 0.1 0.18 0.31 0.24 0.41l0.92 1.61c0.11 0.18 0.32 0.55 0.43 0.73 0.09 0.16 0.28 0.48 0.37 0.64 0.74 1.23 1.45 2.48 2.11 3.76 0.1 0.21 0.3 0.63 0.39 0.84l0.46 1.04c0.12 0.2 0.36 0.61 0.47 0.81 0.15 0.2 0.44 0.58 0.59 0.77l0.69 0.98c0.12 0.19 0.36 0.57 0.49 0.77l1.01 1.75c0.09 0.16 0.27 0.48 0.36 0.64s0.27 0.47 0.37 0.63l1 1.72c0.04 0.08 0.12 0.22 0.16 0.29l0.95 1.66c0.04 0.07 0.12 0.2 0.16 0.27l0.97 1.69c0.1 0.17 0.3 0.51 0.4 0.69 0.1 0.17 0.3 0.51 0.39 0.68l1 1.72c0.04 0.07 0.11 0.21 0.15 0.28l1.06 1.98-0.01 0.9c-25.02-7.19-49.64-15.72-74.73-22.65-22.77-7.35-45.75-14.04-68.52-21.35 3.17-1.52 6.56-2.49 9.73-3.99 0.41-1.35 0.84-2.69 1.26-4.02 3.95-2.48 8.41-3.97 12.3-6.54 0.15-3.29 0.14-6.57 0.13-9.85 4.36-3.51 8.98-6.76 12.57-11.11 0.36-4.28-2.87-9.29 0.46-12.9 10.24-12.92 16.45-28.93 17.56-45.38 1.83-6.02-1.46-11.98-0.1-17.97z" fill="#f9c941"/>\n      <path d="m509.91 287.41l0.52 0.02 0.29 0.76-0.59 0.04-0.22-0.82z" fill="#f9c941"/>\n      <path d="m257.34 157.32c0.08 0.13 0.24 0.4 0.32 0.54-0.08-0.14-0.24-0.41-0.32-0.54z" fill="#e3b73a"/>\n      <path d="m259.19 160.53c0.07 0.12 0.2 0.35 0.27 0.47-0.07-0.12-0.2-0.35-0.27-0.47z" fill="#e3b73a"/>\n      <path d="m260.28 162.47c0.05 0.08 0.14 0.25 0.19 0.33-0.05-0.08-0.14-0.25-0.19-0.33z" fill="#e3b73a"/>\n      <path d="m261.48 164.56c0.09 0.15 0.26 0.45 0.35 0.6-0.09-0.15-0.26-0.45-0.35-0.6z" fill="#e3b73a"/>\n      <path d="m261.83 165.16c0.09 0.17 0.28 0.49 0.38 0.65-0.1-0.16-0.29-0.48-0.38-0.65z" fill="#e3b73a"/>\n      <path d="m263.16 167.44c0.05 0.09 0.16 0.27 0.21 0.36-0.05-0.09-0.16-0.27-0.21-0.36z" fill="#e3b73a"/>\n      <path d="m265.34 171.21c0.09 0.16 0.26 0.47 0.35 0.62-0.09-0.15-0.26-0.46-0.35-0.62z" fill="#e3b73a"/>\n      <path d="m267.23 174.48c0.09 0.15 0.26 0.43 0.34 0.57-0.08-0.14-0.25-0.42-0.34-0.57z" fill="#e3b73a"/>\n      <path d="m268.43 176.5c0.2 0.33 0.59 0.98 0.79 1.31-0.2-0.33-0.59-0.98-0.79-1.31z" fill="#e3b73a"/>\n      <path d="m269.49 178.29c0.04 0.09 0.14 0.26 0.19 0.34-0.05-0.08-0.15-0.25-0.19-0.34z" fill="#e3b73a"/>\n      <path d="m271.44 181.79c0.19 0.26 0.57 0.8 0.76 1.07-0.19-0.27-0.57-0.81-0.76-1.07z" fill="#e3b73a"/>\n      <path d="m272.46 183.32c0.04 0.08 0.13 0.25 0.18 0.33-0.05-0.08-0.14-0.25-0.18-0.33z" fill="#e3b73a"/>\n      <path d="m275.45 188.72c0.19 0.28 0.56 0.84 0.75 1.12-0.19-0.28-0.56-0.84-0.75-1.12z" fill="#e3b73a"/>\n      <path d="m276.47 190.31c0.04 0.08 0.14 0.25 0.18 0.33-0.04-0.08-0.14-0.25-0.18-0.33z" fill="#e3b73a"/>\n      <path d="m278.23 193.51c0.09 0.15 0.26 0.45 0.35 0.6-0.09-0.15-0.26-0.45-0.35-0.6z" fill="#e3b73a"/>\n      <path d="m279.22 195.24c0.1 0.17 0.3 0.53 0.4 0.7-0.1-0.17-0.3-0.53-0.4-0.7z" fill="#e3b73a"/>\n      <path d="m280.45 197.4c0.1 0.18 0.31 0.55 0.41 0.73-0.1-0.18-0.31-0.55-0.41-0.73z" fill="#e3b73a"/>\n      <path d="m280.86 198.13c0.09 0.15 0.27 0.47 0.36 0.63-0.09-0.16-0.27-0.48-0.36-0.63z" fill="#e3b73a"/>\n      <path d="m282.24 200.51c0.04 0.08 0.13 0.22 0.17 0.29-0.04-0.07-0.13-0.21-0.17-0.29z" fill="#e3b73a"/>\n      <path d="m283.38 202.47c0.08 0.14 0.24 0.41 0.32 0.55-0.08-0.14-0.24-0.41-0.32-0.55z" fill="#e3b73a"/>\n      <path d="m284.31 204.06c0.11 0.18 0.32 0.55 0.43 0.73-0.11-0.18-0.32-0.55-0.43-0.73z" fill="#e3b73a"/>\n      <path d="m286.47 207.71c0.17 0.29 0.52 0.86 0.69 1.15-0.17-0.29-0.52-0.86-0.69-1.15z" fill="#e3b73a"/>\n      <path d="m287.45 209.32c0.05 0.09 0.15 0.25 0.2 0.33-0.05-0.08-0.15-0.24-0.2-0.33z" fill="#e3b73a"/>\n      <path d="m289.43 212.46l0.07 0.1 0.06 0.13c-0.03-0.06-0.1-0.17-0.13-0.23z" fill="#e3b73a"/>\n      <path d="m291.5 216.56c0.09 0.15 0.25 0.44 0.34 0.59-0.09-0.15-0.25-0.44-0.34-0.59z" fill="#e3b73a"/>\n      <path d="m291.84 217.15l0.4 0.68-0.4-0.68z" fill="#e3b73a"/>\n      <path d="m293.19 219.44c0.05 0.09 0.15 0.27 0.21 0.36-0.06-0.09-0.16-0.27-0.21-0.36z" fill="#e3b73a"/>\n      <path d="m294.06 220.98c11.53 4.24 23.46 7.26 35.11 11.11 12.52 3.71 24.94 7.72 37.49 11.32 22.77 7.31 45.75 14 68.52 21.35-0.34 4.87 0.62 9.86-0.59 14.64-0.93 4.65-6.49 5.38-8.78 9.09-30.77 0.01-61.53 0.1-92.29-0.03l-0.66-0.05c-12.71-22.6-25.93-44.92-38.8-67.43z" fill="#e3b73a"/>\n      <path d="m472.71 222.08c0.08 0.13 0.24 0.41 0.32 0.54-0.08-0.13-0.24-0.41-0.32-0.54z" fill="#e8b835"/>\n      <path d="m474.62 225.24c0.08 0.13 0.24 0.4 0.32 0.53-0.08-0.13-0.24-0.4-0.32-0.53z" fill="#e8b835"/>\n      <path d="m475.74 227.14c0.05 0.09 0.15 0.28 0.21 0.37-0.06-0.09-0.16-0.28-0.21-0.37z" fill="#e8b835"/>\n      <path d="m476.95 229.25c0.1 0.17 0.29 0.52 0.39 0.69-0.1-0.17-0.29-0.52-0.39-0.69z" fill="#e8b835"/>\n      <path d="m477.34 229.94c0.09 0.15 0.27 0.46 0.36 0.62-0.09-0.16-0.27-0.47-0.36-0.62z" fill="#e8b835"/>\n      <path d="m478.68 232.26l0.16 0.28-0.16-0.28z" fill="#e8b835"/>\n      <path d="m479.82 234.22c0.04 0.07 0.12 0.2 0.16 0.27-0.04-0.07-0.12-0.2-0.16-0.27z" fill="#e8b835"/>\n      <path d="m480.96 236.19c0.1 0.17 0.29 0.5 0.39 0.67-0.1-0.17-0.29-0.5-0.39-0.67z" fill="#e8b835"/>\n      <path d="m481.35 236.86c0.09 0.17 0.28 0.5 0.37 0.66-0.09-0.16-0.28-0.49-0.37-0.66z" fill="#e8b835"/>\n      <path d="m482.73 239.3c0.14 0.35 0.42 1.03 0.56 1.37-0.14-0.34-0.42-1.02-0.56-1.37z" fill="#e8b835"/>\n      <path d="m483.57 241.22c0.09 0.15 0.26 0.44 0.35 0.58l-0.22-0.29-0.13-0.29z" fill="#e8b835"/>\n      <path d="m484.51 242.69c0.21 0.33 0.62 0.99 0.83 1.32-0.21-0.33-0.62-0.99-0.83-1.32z" fill="#e8b835"/>\n      <path d="m485.34 244.01c0.12 0.2 0.38 0.6 0.5 0.8-0.12-0.2-0.38-0.6-0.5-0.8z" fill="#e8b835"/>\n      <path d="m486.5 245.86c0.2 0.45 0.6 1.36 0.8 1.81-0.2-0.45-0.6-1.36-0.8-1.81z" fill="#e8b835"/>\n      <path d="m487.6 248.21c0.05 0.09 0.15 0.26 0.2 0.34l-0.09-0.13-0.11-0.21z" fill="#e8b835"/>\n      <path d="m489.55 251.16c0.11 0.18 0.33 0.54 0.45 0.72-0.12-0.18-0.34-0.54-0.45-0.72z" fill="#e8b835"/>\n      <path d="m490.6 252.88c0.09 0.16 0.27 0.47 0.36 0.62-0.09-0.15-0.27-0.46-0.36-0.62z" fill="#e8b835"/>\n      <path d="m491.97 255.27c0.09 0.16 0.27 0.48 0.36 0.64-0.09-0.16-0.27-0.48-0.36-0.64z" fill="#e8b835"/>\n      <path d="m492.33 255.91c0.09 0.16 0.27 0.47 0.36 0.63-0.09-0.16-0.27-0.47-0.36-0.63z" fill="#e8b835"/>\n      <path d="m493.69 258.25c0.04 0.08 0.13 0.23 0.18 0.3-0.05-0.07-0.14-0.22-0.18-0.3z" fill="#e8b835"/>\n      <path d="m494.78 260.15c0.06 0.1 0.18 0.31 0.24 0.41-0.06-0.1-0.18-0.31-0.24-0.41z" fill="#e8b835"/>\n      <path d="m495.94 262.17c0.11 0.18 0.32 0.55 0.43 0.73-0.11-0.18-0.32-0.55-0.43-0.73z" fill="#e8b835"/>\n      <path d="m496.37 262.9c0.09 0.16 0.28 0.48 0.37 0.64-0.09-0.16-0.28-0.48-0.37-0.64z" fill="#e8b835"/>\n      <path d="m435.18 264.76c25.09 6.93 49.71 15.46 74.73 22.65l0.22 0.82c-28.09 0.64-56.22 0.11-84.32 0.26 2.29-3.71 7.85-4.44 8.78-9.09 1.21-4.78 0.25-9.77 0.59-14.64z" fill="#e8b835"/>\n      <path d="m498.85 267.3c0.1 0.21 0.3 0.63 0.39 0.84-0.09-0.21-0.29-0.63-0.39-0.84z" fill="#e8b835"/>\n      <path d="m499.7 269.18c0.12 0.2 0.36 0.61 0.47 0.81-0.11-0.2-0.35-0.61-0.47-0.81z" fill="#e8b835"/>\n      <path d="m500.17 269.99c0.15 0.2 0.44 0.58 0.59 0.77-0.15-0.19-0.44-0.57-0.59-0.77z" fill="#e8b835"/>\n      <path d="m501.45 271.74c0.12 0.19 0.36 0.57 0.49 0.77-0.13-0.2-0.37-0.58-0.49-0.77z" fill="#e8b835"/>\n      <path d="m502.95 274.26c0.09 0.16 0.27 0.48 0.36 0.64-0.09-0.16-0.27-0.48-0.36-0.64z" fill="#e8b835"/>\n      <path d="m503.31 274.9c0.09 0.16 0.27 0.47 0.37 0.63-0.1-0.16-0.28-0.47-0.37-0.63z" fill="#e8b835"/>\n      <path d="m504.68 277.25c0.04 0.08 0.12 0.22 0.16 0.29-0.04-0.07-0.12-0.21-0.16-0.29z" fill="#e8b835"/>\n      <path d="m505.79 279.2c0.04 0.07 0.12 0.2 0.16 0.27-0.04-0.07-0.12-0.2-0.16-0.27z" fill="#e8b835"/>\n      <path d="m506.92 281.16c0.1 0.17 0.3 0.51 0.4 0.69-0.1-0.18-0.3-0.52-0.4-0.69z" fill="#e8b835"/>\n      <path d="m507.32 281.85c0.1 0.17 0.3 0.51 0.39 0.68-0.09-0.17-0.29-0.51-0.39-0.68z" fill="#e8b835"/>\n      <path d="m508.71 284.25c0.04 0.07 0.11 0.21 0.15 0.28-0.04-0.07-0.11-0.21-0.15-0.28z" fill="#e8b835"/>\n      <path d="m509.92 286.51c0.13 0.23 0.38 0.69 0.51 0.92l-0.52-0.02 0.01-0.9z" fill="#e8b835"/>\n      <path d="m147.21 341.08c10.31-17.58 20.11-35.49 30.76-52.86 51.63 0.19 103.27-0.29 154.89 0.19l0.66 0.05c-26.11 0.84-52.29-0.22-78.4 0.44-4.98 3.21-8.3 8.41-13.07 11.91-4.43 3.23-7.69 7.73-11.92 11.18-5.93 4.37-10.34 10.43-16.36 14.71-6.96 7.37-14.99 13.58-22.01 20.91-5.26 4.04-9.66 9.03-14.56 13.48-5.39 3.91-9.52 9.21-14.56 13.53-0.13 0.11-0.38 0.33-0.51 0.45-0.27 0.21-0.8 0.65-1.07 0.86-0.13 0.11-0.37 0.31-0.5 0.42-3.97 3.07-7.01 7.12-10.91 10.27-0.13 0.11-0.38 0.32-0.5 0.42-0.13 0.11-0.38 0.32-0.51 0.42-5.69 4.67-10.25 10.58-16.22 14.93-6.01 5.96-12.79 11.08-18.5 17.36-8.44 6.94-15.74 15.13-24.44 21.78 2.16-4.71 5.02-9.03 7.53-13.54 16.74-28.97 33.46-57.94 50.2-86.91z" fill="#296ad9"/>\n      <path d="m89.04 442.17c0.11-0.16 0.33-0.48 0.44-0.64l-0.03 0.65-0.41-0.01z" fill="#296ad9"/>\n      <path id="#2a71e9ff" fill="#2a71e9" d="m333.52 288.46c30.76 0.13 61.52 0.04 92.29 0.03 28.1-0.15 56.23 0.38 84.32-0.26l0.59-0.04 0.4 0.11v0.08c-29.66 51.27-59.23 102.6-88.89 153.88-110.93-0.03-221.86 0.1-332.78-0.08l0.03-0.65c8.7-6.65 16-14.84 24.44-21.78 5.71-6.28 12.49-11.4 18.5-17.36 5.97-4.35 10.53-10.26 16.22-14.93 0.13-0.1 0.38-0.31 0.51-0.42 0.12-0.1 0.37-0.31 0.5-0.42 3.9-3.15 6.94-7.2 10.91-10.27 0.13-0.11 0.37-0.31 0.5-0.42 0.27-0.21 0.8-0.65 1.07-0.86 0.13-0.12 0.38-0.34 0.51-0.45 5.04-4.32 9.17-9.62 14.56-13.53 4.9-4.45 9.3-9.44 14.56-13.48 7.02-7.33 15.05-13.54 22.01-20.91 6.02-4.28 10.43-10.34 16.36-14.71 4.23-3.45 7.49-7.95 11.92-11.18 4.77-3.5 8.09-8.7 13.07-11.91 26.11-0.66 52.29 0.4 78.4-0.44z"/>\n      <path d="m162.13 375.07c0.13-0.12 0.38-0.34 0.51-0.45-0.13 0.11-0.38 0.33-0.51 0.45z" fill="#286ee6"/>\n      <path d="m161.06 375.93c0.27-0.21 0.8-0.65 1.07-0.86-0.27 0.21-0.8 0.65-1.07 0.86z" fill="#286ee6"/>\n      <path d="m160.56 376.35c0.13-0.11 0.37-0.31 0.5-0.42-0.13 0.11-0.37 0.31-0.5 0.42z" fill="#286ee6"/>\n      <path d="m149.15 387.04c0.12-0.1 0.37-0.31 0.5-0.42-0.13 0.11-0.38 0.32-0.5 0.42z" fill="#286ee6"/>\n      <path d="m148.64 387.46c0.13-0.1 0.38-0.31 0.51-0.42-0.13 0.11-0.38 0.32-0.51 0.42z" fill="#286ee6"/>\n    </svg>\n  </div>\n\n  <div class="rs-box rs-box-initial">\n    <h3 class="rs-small-headline">Connect your storage</h3>\n    <span class="rs-sub-headline">To sync data with your account</span>\n  </div>\n\n  <div class="rs-box rs-box-connected">\n    <div class="rs-connected-text">\n      <h1 class="rs-user rs-small-headline">user@provider.com</h1>\n      <span class="rs-sub-headline">Connected</span>\n    </div>\n    <div class="rs-connected-buttons">\n      <button class="rs-button rs-button-small rs-sync" title="Sync now">\n        <svg class="rs-icon rs-loop-icon" xml:space="preserve" version="1.1"\n             x="0px" y="0px" height="16" width="16" viewBox="0 0 512 512"\n             style="enable-background:new 0 0 512 512" xmlns="http://www.w3.org/2000/svg">\n          <path d="m273.4 300.5l-0.3 58c48.9-8.2 86.3-51 86.3-102.5 0-15.9-3.6-31-10-44.5-2.8-5.8-6-11.3-9.8-16.5l47.1-43.5c1.1 1.3 2.1 2.7 3.1 4 20.9 28 33.2 62.8 33.2 100.5v3.7c-1.5 71.5-47.6 132-111.4 154.6-12.3 4.3-25.2 7.3-38.5 8.7l-0.1 57-76.2-67-26.2-23 44.4-38.7 58.4-50.8z"/>\n          <path d="m89 252.3c1.6-72.1 48.3-133 112.9-155.2 11.7-4 24-6.8 36.8-8.1l0.1-57 76.1 66.9 26.2 23.1-44.3 38.6-58.4 50.9 0.2-57.9c-48.8 8.3-86 51.1-86 102.4 0 16 3.6 31.1 10.1 44.7 2.7 5.8 6 11.2 9.7 16.3l-47 43.6c-1.3-1.6-2.6-3.3-3.8-5-20.5-27.9-32.6-62.3-32.6-99.6v-3.7z"/>\n        </svg>\n      </button>\n      <button class="rs-button rs-button-small rs-disconnect" title="Disconnect">\n        <svg class="rs-icon rs-power-icon" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="16" height="16"\n          style="enable-background:new 0 0 512 512" xml:space="preserve"\n          viewBox="0 0 512 512" version="1.1">\n          <path d="m256 256c-17.7 0-32-14.3-32-32v-160c0-17.7 14.3-32 32-32s32 14.3 32 32v160c0 17.7-14.3 32-32 32z"/>\n          <path d="m379 68.8c-5-3-10.8-4.8-17-4.8-17.7 0-32 14.3-32 32 0 6.2 1.8 12 4.8 16.9 2 3.2 4.6 6.1 7.6 8.4 1.2 0.9 2.4 1.7 3.7 2.5 8.1 5.6 15.8 11.9 23 19.1 30.3 30.2 46.9 70.4 46.9 113.1s-16.6 82.9-46.9 113.1c-30.2 30.3-70.4 46.9-113.1 46.9s-82.9-16.6-113.1-46.9c-30.3-30.2-46.9-70.4-46.9-113.1s16.6-82.9 46.9-113.1c7.1-7.1 14.8-13.5 22.9-19 1.4-0.8 2.6-1.6 3.9-2.6 3-2.3 5.5-5.1 7.5-8.3 3.1-4.9 4.8-10.7 4.8-16.9 0-17.7-14.3-32-32-32-6.2 0-12 1.8-16.9 4.8l-0.1-0.1c-60.8 40-101 108.9-101 187.2 0 123.7 100.3 224 224 224s224-100.3 224-224c0-78.3-40.2-147.2-101-187.2z"/>\n        </svg>\n      </button>\n    </div>\n  </div>\n\n  <div class="rs-box rs-box-error">\n    <div class="rs-error-message"></div>\n    <div class="rs-error-buttons">\n      <a href="#" class="rs-reconnect rs-hidden">Renew</a>\n      <button class="rs-button rs-button-small rs-disconnect" title="Disconnect">\n        <svg class="rs-icon rs-power-icon" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="16" height="16"\n          style="enable-background:new 0 0 512 512" xml:space="preserve"\n          viewBox="0 0 512 512" version="1.1">\n          <path d="m256 256c-17.7 0-32-14.3-32-32v-160c0-17.7 14.3-32 32-32s32 14.3 32 32v160c0 17.7-14.3 32-32 32z"/>\n          <path d="m379 68.8c-5-3-10.8-4.8-17-4.8-17.7 0-32 14.3-32 32 0 6.2 1.8 12 4.8 16.9 2 3.2 4.6 6.1 7.6 8.4 1.2 0.9 2.4 1.7 3.7 2.5 8.1 5.6 15.8 11.9 23 19.1 30.3 30.2 46.9 70.4 46.9 113.1s-16.6 82.9-46.9 113.1c-30.2 30.3-70.4 46.9-113.1 46.9s-82.9-16.6-113.1-46.9c-30.3-30.2-46.9-70.4-46.9-113.1s16.6-82.9 46.9-113.1c7.1-7.1 14.8-13.5 22.9-19 1.4-0.8 2.6-1.6 3.9-2.6 3-2.3 5.5-5.1 7.5-8.3 3.1-4.9 4.8-10.7 4.8-16.9 0-17.7-14.3-32-32-32-6.2 0-12 1.8-16.9 4.8l-0.1-0.1c-60.8 40-101 108.9-101 187.2 0 123.7 100.3 224 224 224s224-100.3 224-224c0-78.3-40.2-147.2-101-187.2z"/>\n        </svg>\n      </button>\n    </div>\n  </div>\n\n  <div class="rs-box rs-box-choose">\n    <div class="rs-content">\n      <h1 class="rs-big-headline">Connect your storage</h1>\n      <p class="rs-short-desc">\n        This app allows you to sync data with a storage of your choice.\n        <a class="rs-help" href="https://remotestorage.io/" target="_blank">Read more</a>\n      </p>\n      <div class="rs-button-wrap">\n        <button class="rs-button rs-button-big rs-choose-rs">\n          <svg class="rs-logo" fill-rule="evenodd" height="40" width="40"\n               xmlns="http://www.w3.org/2000/svg" version="1.1"\n               style="shape-rendering:geometricPrecision;image-rendering:optimizeQuality;text-rendering:geometricPrecision"\n               clip-rule="evenodd" xml:space="preserve" viewBox="0 0 739 853">\n            <g>\n              <polygon class="rs-logo-shape" points="370 754 0 542 0 640 185 747 370 853 554 747 739 640 739 525 739 525 739 476 739 427 739 378 653 427 370 589 86 427 86 427 86 361 185 418 370 524 554 418 653 361 739 311 739 213 739 213 554 107 370 0 185 107 58 180 144 230 228 181 370 100 511 181 652 263 370 425 87 263 87 263 0 213 0 213 0 311 0 378 0 427 0 476 86 525 185 582 370 689 554 582 653 525 653 590 653 592" />\n            </g>\n          </svg>\n          <div>RemoteStorage</div>\n        </button>\n        <button class="rs-button rs-button-big rs-choose-dropbox">\n          <svg class="dropbox-logo" width="40" height="40"\n               xml:space="preserve" stroke-miterlimit="1.4142"\n               xmlns="http://www.w3.org/2000/svg" viewBox="0 0 104 97"\n               stroke-linejoin="round" version="1.1" clip-rule="evenodd">\n            <path d="m30.691 0l-30.691 20.039 21.221 16.994 30.946-19.108-21.476-17.925z" fill="#007ee5"/>\n            <path d="m0 54.028l30.691 20.039 21.476-17.926-30.945-19.108-21.222 16.995z" fill="#007ee5"/>\n            <path d="m52.167 56.142l21.477 17.926 30.696-20.039-21.227-16.995-30.946 19.108z" fill="#007ee5"/>\n            <path d="m104.34 20.039l-30.696-20.039-21.477 17.925 30.946 19.108 21.227-16.994z" fill="#007ee5"/>\n            <path d="m52.23 59.998l-21.538 17.873-9.218-6.018v6.747l30.756 18.443 30.756-18.443v-6.747l-9.216 6.018-21.54-17.873z" fill="#007ee5"/>\n          </svg>\n          <div>Dropbox</div>\n        </button>\n        <button class="rs-button rs-button-big rs-choose-googledrive">\n          <svg class="googledrive-logo" width="40" height="40" version="1.1"\n                                                          stroke-miterlimit="1.4142" xml:space="preserve"\n                                                          xmlns="http://www.w3.org/2000/svg" width="100%" stroke-linejoin="round"\n                                                                                                          clip-rule="evenodd" viewBox="0 0 511 442" height="100%">\n            <path d="m166.23 0.10955l0.44-0.009998 0.04 0.49-0.56 0.02 0.08-0.5z" fill="#0ba25e"/>\n            <path d="m164.52 3.3896c0.39-0.56 1.15-1.68 1.54-2.24l0.7-0.11c0.52 6.34 2.38 12.45 3.78 18.62 6.02 26.81 12.1 53.6 18.43 80.33 4.15 18.33 8.78 36.57 12.58 54.97 3.3 11.39 5.07 23.14 8.26 34.55 2.06 10.64 5.29 21.02 7.02 31.72-0.7 1.17-1.4 2.35-2.11 3.52-0.17 0.27-0.53 0.83-0.7 1.11l-0.26 0.45c-0.04 0.08-0.13 0.25-0.18 0.33-0.39 0.72-1.17 2.15-1.56 2.86-0.07 0.13-0.22 0.39-0.3 0.52l-0.71 1.26c-0.09 0.15-0.26 0.44-0.34 0.59l-0.83 1.45c-0.04 0.08-0.13 0.24-0.18 0.32l-1.03 1.8c-0.09 0.15-0.27 0.47-0.36 0.63-0.1 0.17-0.3 0.5-0.4 0.67l-1.01 1.74c-0.08 0.13-0.24 0.41-0.32 0.54l-0.62 1.05c-0.1 0.18-0.32 0.55-0.42 0.73-0.4 0.69-1.19 2.07-1.59 2.77-0.07 0.12-0.22 0.37-0.29 0.5l-0.63 1.05c-0.1 0.19-0.32 0.55-0.43 0.74-0.37 0.65-1.13 1.94-1.5 2.59-0.09 0.15-0.26 0.46-0.35 0.61l-0.75 1.35c-0.07 0.12-0.21 0.37-0.28 0.49l-0.77 1.35-0.32 0.56c-0.4 0.66-1.18 1.99-1.57 2.66-0.09 0.15-0.27 0.46-0.36 0.62l-0.71 1.28c-0.07 0.12-0.2 0.35-0.26 0.47l-0.82 1.42c-0.07 0.13-0.22 0.38-0.29 0.5-0.42 0.72-1.25 2.15-1.67 2.87-0.07 0.13-0.23 0.39-0.31 0.52l-0.6 0.99c-0.1 0.18-0.31 0.53-0.41 0.71-0.38 0.66-1.13 1.97-1.51 2.63-0.06 0.11-0.2 0.35-0.26 0.46l-0.82 1.46c-0.06 0.11-0.18 0.32-0.24 0.43l-0.84 1.47c-0.07 0.11-0.21 0.35-0.28 0.46-0.39 0.66-1.18 1.96-1.57 2.62-0.11 0.17-0.31 0.53-0.42 0.71l-0.59 1.1c-0.08 0.14-0.23 0.42-0.3 0.55l-1.03 1.78c-0.08 0.15-0.26 0.45-0.34 0.6-0.09 0.14-0.25 0.43-0.33 0.57-1.69 2.85-3.22 5.79-5.09 8.53-10.65 17.37-20.45 35.28-30.76 52.86-16.74 28.97-33.46 57.94-50.2 86.91-2.51 4.51-5.37 8.83-7.53 13.54-0.11 0.16-0.33 0.48-0.44 0.64-4.01-5.78-7.11-12.16-10.78-18.16-16.74-28.99-33.49-57.97-50.22-86.97-9.3-16.31-18.99-32.42-28.04-48.88 35.84-61.85 71.52-123.81 107.29-185.7 0.03-0.06 0.1-0.17 0.13-0.23l0.98-1.68c0.04-0.07 0.12-0.21 0.17-0.28l0.99-1.69c0.09-0.15 0.27-0.47 0.37-0.62 0.1-0.18 0.3-0.53 0.4-0.71l0.99-1.76 0.28-0.48 0.74-1.18c0.13-0.19 0.39-0.58 0.52-0.78 0.36-0.54 1.09-1.63 1.46-2.17 0.04-0.18 0.11-0.53 0.15-0.7l0.87-1.67 0.16-0.28 1-1.68c0.1-0.16 0.3-0.5 0.4-0.67 0.11-0.19 0.33-0.58 0.45-0.78l0.84-1.47c0.09-0.16 0.27-0.47 0.36-0.62l0.7-1.23c0.08-0.13 0.24-0.41 0.32-0.55 0.39-0.67 1.17-2.01 1.57-2.68l0.32-0.56 0.73-1.33c0.06-0.1 0.18-0.32 0.24-0.42l0.96-1.68c0.12-0.2 0.37-0.61 0.5-0.81l0.65-0.98c0.14-0.2 0.41-0.59 0.55-0.78 0.12-0.2 0.37-0.6 0.5-0.8l0.54-1.04c0.18-0.39 0.55-1.17 0.73-1.56l0.55-0.77c0.1-0.14 0.29-0.41 0.39-0.55 0.37-0.31 1.12-0.95 1.49-1.27l-0.07-1.17c0.06-0.14 0.2-0.42 0.27-0.56l0.85-1.69c0.07-0.12 0.21-0.37 0.28-0.49l0.75-1.22c0.14-0.2 0.41-0.6 0.55-0.8 0.34-0.56 1.03-1.66 1.37-2.21 0.1-0.2 0.28-0.59 0.38-0.79l0.53-1.15c0.16-0.3 0.5-0.91 0.67-1.22l0.66-0.94c0.14-0.19 0.42-0.57 0.56-0.76 0.12-0.2 0.37-0.6 0.49-0.8l0.49-1.03c0.11-0.23 0.34-0.7 0.45-0.93l0.86-1.66c0.13-0.2 0.38-0.62 0.5-0.83l0.66-0.98c0.14-0.2 0.42-0.58 0.56-0.78 0.13-0.2 0.38-0.61 0.5-0.81l0.51-1.04c0.15-0.29 0.44-0.86 0.58-1.15l0.68-1.12c0.12-0.19 0.36-0.57 0.49-0.76 0.36-0.56 1.09-1.68 1.46-2.23 0.04-0.17 0.13-0.51 0.17-0.68l0.87-1.66c0.03-0.07 0.11-0.2 0.15-0.27l1-1.71c0.1-0.17 0.29-0.5 0.39-0.67 0.1-0.16 0.29-0.5 0.39-0.66l1-1.75c0.04-0.07 0.12-0.21 0.16-0.29l0.94-1.63c0.05-0.07 0.14-0.22 0.18-0.3l1.01-1.7c0.09-0.16 0.27-0.46 0.36-0.62 0.1-0.17 0.3-0.52 0.4-0.7l0.96-1.69c0.06-0.11 0.18-0.32 0.24-0.42l0.81-1.31c0.13-0.19 0.39-0.58 0.51-0.77 0.37-0.55 1.09-1.65 1.46-2.2 0.03-0.17 0.11-0.51 0.15-0.68l0.85-1.63c0.07-0.12 0.22-0.37 0.29-0.5l0.76-1.19c0.14-0.19 0.4-0.58 0.54-0.78 0.35-0.56 1.05-1.67 1.4-2.23 0.05-0.16 0.14-0.48 0.19-0.64l0.88-1.65c0.04-0.08 0.12-0.22 0.16-0.3l1.02-1.7204c0.08-0.15 0.26-0.45 0.35-0.6 0.09-0.17 0.27-0.49 0.36-0.65l1-1.78c0.05-0.07 0.13-0.22 0.17-0.29l0.95-1.61c0.03-0.05 0.1-0.16 0.13-0.21z" fill="#0ba25e"/>\n            <path id="#fccd48ff" fill="#fccd48" d="m166.67 0.099552c32.48-0.15 64.97-0.02 97.46-0.07 26.88 0.099998 53.78-0.21 80.65 0.15 0.53 1.02 1.07 2.03 1.64 3.04 0.08 0.13 0.23 0.41 0.31 0.55 2.6 4.53 5.24 9.0404 7.81 13.58 0.05 0.08 0.13 0.23 0.18 0.3 4.98 8.55 9.94 17.11 14.83 25.71 0.04 0.07 0.13 0.21 0.17 0.29 0.89 1.53 1.81 3.06 2.72 4.6 0.08 0.13 0.23 0.4 0.31 0.54 1.25 2.2 2.51 4.39 3.78 6.58 0.05 0.07 0.13 0.21 0.17 0.29 1.27 2.18 2.52 4.38 3.78 6.58 0.08 0.13 0.23 0.39 0.31 0.52 1.27 2.14 2.5 4.3 3.72 6.46 0.08 0.13 0.23 0.4 0.31 0.53 11.94 20.62 23.85 41.25 35.75 61.9-1.36 5.99 1.93 11.95 0.1 17.97-1.11 16.45-7.32 32.46-17.56 45.38-3.33 3.61-0.1 8.62-0.46 12.9-3.59 4.35-8.21 7.6-12.57 11.11 0.01 3.28 0.02 6.56-0.13 9.85-3.89 2.57-8.35 4.06-12.3 6.54-0.42 1.33-0.85 2.67-1.26 4.02-3.17 1.5-6.56 2.47-9.73 3.99-12.55-3.6-24.97-7.61-37.49-11.32-11.65-3.85-23.58-6.87-35.11-11.11l-0.66-1.18c-0.06-0.09-0.16-0.27-0.21-0.36l-0.95-1.61-0.4-0.68c-0.09-0.15-0.25-0.44-0.34-0.59-0.71-1.26-1.36-2.55-1.94-3.87l-0.06-0.13-0.07-0.1c-0.44-0.7-1.33-2.11-1.78-2.81-0.05-0.08-0.15-0.24-0.2-0.33l-0.29-0.46c-0.17-0.29-0.52-0.86-0.69-1.15-0.43-0.73-1.3-2.19-1.73-2.92-0.11-0.18-0.32-0.55-0.43-0.73l-0.61-1.04c-0.08-0.14-0.24-0.41-0.32-0.55l-0.97-1.67c-0.04-0.07-0.13-0.21-0.17-0.29l-1.02-1.75c-0.09-0.16-0.27-0.48-0.36-0.63-0.1-0.18-0.31-0.55-0.41-0.73l-0.83-1.46c-0.1-0.17-0.3-0.53-0.4-0.7l-0.64-1.13c-0.09-0.15-0.26-0.45-0.35-0.6-0.39-0.72-1.18-2.15-1.58-2.87-0.04-0.08-0.14-0.25-0.18-0.33l-0.27-0.47c-0.19-0.28-0.56-0.84-0.75-1.12-0.99-1.66-1.92-3.36-2.81-5.07-0.05-0.08-0.14-0.25-0.18-0.33l-0.26-0.46c-0.19-0.27-0.57-0.81-0.76-1.07-0.44-0.79-1.32-2.37-1.76-3.16-0.05-0.08-0.15-0.25-0.19-0.34l-0.27-0.48c-0.2-0.33-0.59-0.98-0.79-1.31l-0.86-1.45c-0.08-0.14-0.25-0.42-0.34-0.57-0.38-0.66-1.15-1.99-1.54-2.65-0.09-0.15-0.26-0.46-0.35-0.62-0.68-1.13-1.33-2.27-1.97-3.41-0.05-0.09-0.16-0.27-0.21-0.36l-0.95-1.63c-0.1-0.16-0.29-0.48-0.38-0.65-0.09-0.15-0.26-0.45-0.35-0.6l-1.01-1.76c-0.05-0.08-0.14-0.25-0.19-0.33l-0.82-1.47c-0.07-0.12-0.2-0.35-0.27-0.47-0.38-0.67-1.15-2-1.53-2.67-0.08-0.14-0.24-0.41-0.32-0.54-0.62-1.03-1.24-2.07-1.84-3.1-27.73-48.12-55.5-96.21-83.29-144.3-1.74-3.0004-3.32-6.1204-5.45-8.8804l-0.05-0.45005-0.04-0.49z"/>\n            <path d="m166.15 0.60955l0.56-0.02 0.05 0.45005-0.7 0.11 0.09-0.54005z" fill="#089156"/>\n            <path d="m166.76 1.0396c2.13 2.76 3.71 5.88 5.45 8.88 27.79 48.09 55.56 96.18 83.29 144.3-12.84 22.36-25.78 44.67-38.67 67.01-1.73-10.7-4.96-21.08-7.02-31.72-3.19-11.41-4.96-23.16-8.26-34.55-3.8-18.4-8.43-36.64-12.58-54.97-6.33-26.73-12.41-53.52-18.43-80.33-1.4-6.17-3.26-12.28-3.78-18.62z" fill="#089156"/>\n            <path d="m164.39 3.5996c0.03-0.05 0.1-0.16 0.13-0.21-0.03 0.05-0.1 0.16-0.13 0.21z" fill="#089156"/>\n            <path d="m163.27 5.4996c0.05-0.07 0.13-0.22 0.17-0.29-0.04 0.07-0.12 0.22-0.17 0.29z" fill="#089156"/>\n            <path d="m161.91 7.9296c0.09-0.17 0.27-0.49 0.36-0.65-0.09 0.16-0.27 0.48-0.36 0.65z" fill="#089156"/>\n            <path d="m161.56 8.5296c0.08-0.15 0.26-0.45 0.35-0.6-0.09 0.15-0.27 0.45-0.35 0.6z" fill="#089156"/>\n            <path d="m160.38 10.55c0.04-0.08 0.12-0.22 0.16-0.3-0.04 0.08-0.12 0.22-0.16 0.3z" fill="#089156"/>\n            <path d="m159.31 12.84c0.05-0.16 0.14-0.48 0.19-0.64-0.05 0.16-0.14 0.48-0.19 0.64z" fill="#089156"/>\n            <path d="m157.91 15.07c0.35-0.56 1.05-1.67 1.4-2.23-0.35 0.56-1.05 1.67-1.4 2.23z" fill="#089156"/>\n            <path d="m157.37 15.85c0.14-0.19 0.4-0.58 0.54-0.78-0.14 0.2-0.4 0.59-0.54 0.78z" fill="#089156"/>\n            <path d="m156.32 17.54c0.07-0.12 0.22-0.37 0.29-0.5-0.07 0.13-0.22 0.38-0.29 0.5z" fill="#089156"/>\n            <path d="m155.32 19.85c0.03-0.17 0.11-0.51 0.15-0.68-0.04 0.17-0.12 0.51-0.15 0.68z" fill="#089156"/>\n            <path d="m153.86 22.05c0.37-0.55 1.09-1.65 1.46-2.2-0.37 0.55-1.09 1.65-1.46 2.2z" fill="#089156"/>\n            <path d="m153.35 22.82c0.13-0.19 0.39-0.58 0.51-0.77-0.12 0.19-0.38 0.58-0.51 0.77z" fill="#089156"/>\n            <path d="m152.3 24.55c0.06-0.11 0.18-0.32 0.24-0.42-0.06 0.1-0.18 0.31-0.24 0.42z" fill="#089156"/>\n            <path d="m150.94 26.94c0.1-0.17 0.3-0.52 0.4-0.7-0.1 0.18-0.3 0.53-0.4 0.7z" fill="#089156"/>\n            <path d="m150.58 27.56c0.09-0.16 0.27-0.46 0.36-0.62-0.09 0.16-0.27 0.46-0.36 0.62z" fill="#089156"/>\n            <path d="m149.39 29.56c0.05-0.07 0.14-0.22 0.18-0.3-0.04 0.08-0.13 0.23-0.18 0.3z" fill="#089156"/>\n            <path d="m148.29 31.48c0.04-0.07 0.12-0.21 0.16-0.29-0.04 0.08-0.12 0.22-0.16 0.29z" fill="#089156"/>\n            <path d="m146.9 33.89c0.1-0.16 0.29-0.5 0.39-0.66-0.1 0.16-0.29 0.5-0.39 0.66z" fill="#089156"/>\n            <path d="m146.51 34.56c0.1-0.17 0.29-0.5 0.39-0.67-0.1 0.17-0.29 0.5-0.39 0.67z" fill="#089156"/>\n            <path d="m145.36 36.54c0.03-0.07 0.11-0.2 0.15-0.27-0.04 0.07-0.12 0.2-0.15 0.27z" fill="#089156"/>\n            <path d="m144.32 38.88c0.04-0.17 0.13-0.51 0.17-0.68-0.04 0.17-0.13 0.51-0.17 0.68z" fill="#089156"/>\n            <path d="m142.86 41.11c0.36-0.56 1.09-1.68 1.46-2.23-0.37 0.55-1.1 1.67-1.46 2.23z" fill="#089156"/>\n            <path d="m142.37 41.87c0.12-0.19 0.36-0.57 0.49-0.76-0.13 0.19-0.37 0.57-0.49 0.76z" fill="#089156"/>\n            <path d="m141.11 44.14c0.15-0.29 0.44-0.86 0.58-1.15-0.14 0.29-0.43 0.86-0.58 1.15z" fill="#089156"/>\n            <path d="m140.1 45.99c0.13-0.2 0.38-0.61 0.5-0.81-0.12 0.2-0.37 0.61-0.5 0.81z" fill="#089156"/>\n            <path d="m139.54 46.77c0.14-0.2 0.42-0.58 0.56-0.78-0.14 0.2-0.42 0.58-0.56 0.78z" fill="#089156"/>\n            <path d="m138.38 48.58c0.13-0.2 0.38-0.62 0.5-0.83-0.12 0.21-0.37 0.63-0.5 0.83z" fill="#089156"/>\n            <path d="m137.07 51.17c0.11-0.23 0.34-0.7 0.45-0.93-0.11 0.23-0.34 0.7-0.45 0.93z" fill="#089156"/>\n            <path d="m136.09 53c0.12-0.2 0.37-0.6 0.49-0.8-0.12 0.2-0.37 0.6-0.49 0.8z" fill="#089156"/>\n            <path d="m135.53 53.76c0.14-0.19 0.42-0.57 0.56-0.76-0.14 0.19-0.42 0.57-0.56 0.76z" fill="#089156"/>\n            <path d="m134.2 55.92c0.16-0.3 0.5-0.91 0.67-1.22-0.17 0.31-0.51 0.92-0.67 1.22z" fill="#089156"/>\n            <path d="m133.29 57.86c0.1-0.2 0.28-0.59 0.38-0.79-0.1 0.2-0.28 0.59-0.38 0.79z" fill="#089156"/>\n            <path d="m131.92 60.07c0.34-0.56 1.03-1.66 1.37-2.21-0.34 0.55-1.03 1.65-1.37 2.21z" fill="#089156"/>\n            <path d="m131.37 60.87c0.14-0.2 0.41-0.6 0.55-0.8-0.14 0.2-0.41 0.6-0.55 0.8z" fill="#089156"/>\n            <path d="m130.34 62.58c0.07-0.12 0.21-0.37 0.28-0.49-0.07 0.12-0.21 0.37-0.28 0.49z" fill="#089156"/>\n            <path d="m129.22 64.83c0.06-0.14 0.2-0.42 0.27-0.56-0.07 0.14-0.21 0.42-0.27 0.56z" fill="#089156"/>\n            <path d="m127.8 67.27c0.35-0.61 1.06-1.83 1.42-2.44l0.07 1.17c-0.37 0.32-1.12 0.96-1.49 1.27z" fill="#089156"/>\n            <path d="m127.41 67.82c0.1-0.14 0.29-0.41 0.39-0.55-0.1 0.14-0.29 0.41-0.39 0.55z" fill="#089156"/>\n            <path d="m126.13 70.15c0.18-0.39 0.55-1.17 0.73-1.56-0.18 0.39-0.55 1.17-0.73 1.56z" fill="#089156"/>\n            <path d="m125.09 71.99c0.12-0.2 0.37-0.6 0.5-0.8-0.13 0.2-0.38 0.6-0.5 0.8z" fill="#089156"/>\n            <path d="m124.54 72.77c0.14-0.2 0.41-0.59 0.55-0.78-0.14 0.19-0.41 0.58-0.55 0.78z" fill="#089156"/>\n            <path d="m123.39 74.56c0.12-0.2 0.37-0.61 0.5-0.81-0.13 0.2-0.38 0.61-0.5 0.81z" fill="#089156"/>\n            <path d="m122.19 76.66c0.06-0.1 0.18-0.32 0.24-0.42-0.06 0.1-0.18 0.32-0.24 0.42z" fill="#089156"/>\n            <path d="m121.14 78.55l0.32-0.56-0.32 0.56z" fill="#089156"/>\n            <path d="m119.25 81.78c0.08-0.13 0.24-0.41 0.32-0.55-0.08 0.14-0.24 0.42-0.32 0.55z" fill="#089156"/>\n            <path d="m118.19 83.63c0.09-0.16 0.27-0.47 0.36-0.62-0.09 0.15-0.27 0.46-0.36 0.62z" fill="#089156"/>\n            <path d="m116.9 85.88c0.11-0.19 0.33-0.58 0.45-0.78-0.12 0.2-0.34 0.59-0.45 0.78z" fill="#089156"/>\n            <path d="m116.5 86.55c0.1-0.16 0.3-0.5 0.4-0.67-0.1 0.17-0.3 0.51-0.4 0.67z" fill="#089156"/>\n            <path d="m115.34 88.51l0.16-0.28-0.16 0.28z" fill="#089156"/>\n            <path d="m114.32 90.88c0.04-0.18 0.11-0.53 0.15-0.7-0.04 0.17-0.11 0.52-0.15 0.7z" fill="#089156"/>\n            <path d="m112.86 93.05c0.36-0.54 1.09-1.63 1.46-2.17-0.37 0.54-1.1 1.63-1.46 2.17z" fill="#089156"/>\n            <path d="m112.34 93.83c0.13-0.19 0.39-0.58 0.52-0.78-0.13 0.2-0.39 0.59-0.52 0.78z" fill="#089156"/>\n            <path d="m111.32 95.49l0.28-0.48-0.28 0.48z" fill="#089156"/>\n            <path d="m109.93 97.96c0.1-0.18 0.3-0.53 0.4-0.71-0.1 0.18-0.3 0.53-0.4 0.71z" fill="#089156"/>\n            <path d="m109.56 98.58c0.09-0.15 0.27-0.47 0.37-0.62-0.1 0.15-0.28 0.47-0.37 0.62z" fill="#089156"/>\n            <path d="m108.4 100.55c0.04-0.07 0.12-0.21 0.17-0.28-0.05 0.07-0.13 0.21-0.17 0.28z" fill="#089156"/>\n            <path d="m107.29 102.46c0.03-0.06 0.1-0.17 0.13-0.23-0.03 0.06-0.1 0.17-0.13 0.23z" fill="#089156"/>\n            <path d="m214.02 225.86c0.17-0.28 0.53-0.84 0.7-1.11-0.17 0.27-0.53 0.83-0.7 1.11z" fill="#089156"/>\n            <path d="m213.58 226.64c0.05-0.08 0.14-0.25 0.18-0.33-0.04 0.08-0.13 0.25-0.18 0.33z" fill="#089156"/>\n            <path d="m211.72 230.02c0.08-0.13 0.23-0.39 0.3-0.52-0.07 0.13-0.22 0.39-0.3 0.52z" fill="#089156"/>\n            <path d="m210.67 231.87c0.08-0.15 0.25-0.44 0.34-0.59-0.09 0.15-0.26 0.44-0.34 0.59z" fill="#089156"/>\n            <path d="m209.66 233.64c0.05-0.08 0.14-0.24 0.18-0.32-0.04 0.08-0.13 0.24-0.18 0.32z" fill="#089156"/>\n            <path d="m208.27 236.07c0.09-0.16 0.27-0.48 0.36-0.63-0.09 0.15-0.27 0.47-0.36 0.63z" fill="#089156"/>\n            <path d="m207.87 236.74c0.1-0.17 0.3-0.5 0.4-0.67-0.1 0.17-0.3 0.5-0.4 0.67z" fill="#089156"/>\n            <path d="m206.54 239.02c0.08-0.13 0.24-0.41 0.32-0.54-0.08 0.13-0.24 0.41-0.32 0.54z" fill="#089156"/>\n            <path d="m205.5 240.8c0.1-0.18 0.32-0.55 0.42-0.73-0.1 0.18-0.32 0.55-0.42 0.73z" fill="#089156"/>\n            <path d="m203.62 244.07c0.07-0.13 0.22-0.38 0.29-0.5-0.07 0.12-0.22 0.37-0.29 0.5z" fill="#089156"/>\n            <path d="m202.56 245.86c0.11-0.19 0.33-0.55 0.43-0.74-0.1 0.19-0.32 0.55-0.43 0.74z" fill="#089156"/>\n            <path d="m200.71 249.06c0.09-0.15 0.26-0.46 0.35-0.61-0.09 0.15-0.26 0.46-0.35 0.61z" fill="#089156"/>\n            <path d="m199.68 250.9c0.07-0.12 0.21-0.37 0.28-0.49-0.07 0.12-0.21 0.37-0.28 0.49z" fill="#089156"/>\n            <path d="m198.59 252.81l0.32-0.56-0.32 0.56z" fill="#089156"/>\n            <path d="m196.66 256.09c0.09-0.16 0.27-0.47 0.36-0.62-0.09 0.15-0.27 0.46-0.36 0.62z" fill="#089156"/>\n            <path d="m195.69 257.84c0.06-0.12 0.19-0.35 0.26-0.47-0.07 0.12-0.2 0.35-0.26 0.47z" fill="#089156"/>\n            <path d="m194.58 259.76c0.07-0.12 0.22-0.37 0.29-0.5-0.07 0.13-0.22 0.38-0.29 0.5z" fill="#089156"/>\n            <path d="m192.6 263.15c0.08-0.13 0.24-0.39 0.31-0.52-0.07 0.13-0.23 0.39-0.31 0.52z" fill="#089156"/>\n            <path d="m191.59 264.85c0.1-0.18 0.31-0.53 0.41-0.71-0.1 0.18-0.31 0.53-0.41 0.71z" fill="#089156"/>\n            <path d="m189.82 267.94c0.06-0.11 0.2-0.35 0.26-0.46-0.06 0.11-0.2 0.35-0.26 0.46z" fill="#089156"/>\n            <path d="m188.76 269.83c0.06-0.11 0.18-0.32 0.24-0.43-0.06 0.11-0.18 0.32-0.24 0.43z" fill="#089156"/>\n            <path d="m187.64 271.76c0.07-0.11 0.21-0.35 0.28-0.46-0.07 0.11-0.21 0.35-0.28 0.46z" fill="#089156"/>\n            <path d="m185.65 275.09c0.11-0.18 0.31-0.54 0.42-0.71-0.11 0.17-0.31 0.53-0.42 0.71z" fill="#089156"/>\n            <path d="m184.76 276.74c0.07-0.13 0.22-0.41 0.3-0.55-0.08 0.14-0.23 0.42-0.3 0.55z" fill="#089156"/>\n            <path d="m183.39 279.12c0.08-0.15 0.26-0.45 0.34-0.6-0.08 0.15-0.26 0.45-0.34 0.6z" fill="#089156"/>\n            <path d="m183.06 279.69c0.08-0.14 0.24-0.43 0.33-0.57-0.09 0.14-0.25 0.43-0.33 0.57z" fill="#089156"/>\n            <path d="m346.42 3.2196c0.08 0.13 0.23 0.41 0.31 0.55-0.08-0.14-0.23-0.42-0.31-0.55z" fill="#10985b"/>\n            <path d="m354.54 17.35c0.05 0.08 0.13 0.23 0.18 0.3-0.05-0.07-0.13-0.22-0.18-0.3z" fill="#10985b"/>\n            <path d="m369.55 43.36c0.04 0.07 0.13 0.21 0.17 0.29-0.04-0.08-0.13-0.22-0.17-0.29z" fill="#10985b"/>\n            <path d="m372.44 48.25c0.08 0.13 0.23 0.4 0.31 0.54-0.08-0.14-0.23-0.41-0.31-0.54z" fill="#10985b"/>\n            <path d="m376.53 55.37c0.05 0.07 0.13 0.21 0.17 0.29-0.04-0.08-0.12-0.22-0.17-0.29z" fill="#10985b"/>\n            <path d="m380.48 62.24c0.08 0.13 0.23 0.39 0.31 0.52-0.08-0.13-0.23-0.39-0.31-0.52z" fill="#10985b"/>\n            <path d="m384.51 69.22c0.08 0.13 0.23 0.4 0.31 0.53-0.08-0.13-0.23-0.4-0.31-0.53z" fill="#10985b"/>\n            <path d="m420.57 131.65c17.39 30.13 34.89 60.21 52.14 90.43 0.08 0.13 0.24 0.41 0.32 0.54 0.4 0.66 1.2 1.97 1.59 2.62 0.08 0.13 0.24 0.4 0.32 0.53l0.8 1.37c0.05 0.09 0.15 0.28 0.21 0.37l1 1.74c0.1 0.17 0.29 0.52 0.39 0.69 0.09 0.15 0.27 0.46 0.36 0.62l0.98 1.7 0.16 0.28 0.98 1.68c0.04 0.07 0.12 0.2 0.16 0.27l0.98 1.7c0.1 0.17 0.29 0.5 0.39 0.67 0.09 0.17 0.28 0.5 0.37 0.66l1.01 1.78c0.14 0.35 0.42 1.03 0.56 1.37l0.28 0.55 0.13 0.29 0.22 0.29 0.59 0.89c0.21 0.33 0.62 0.99 0.83 1.32 0.12 0.2 0.38 0.6 0.5 0.8l0.66 1.05c0.2 0.45 0.6 1.36 0.8 1.81l0.3 0.54 0.11 0.21 0.09 0.13c0.44 0.65 1.31 1.96 1.75 2.61 0.11 0.18 0.33 0.54 0.45 0.72l0.6 1c0.09 0.16 0.27 0.47 0.36 0.62l1.01 1.77c0.09 0.16 0.27 0.48 0.36 0.64s0.27 0.47 0.36 0.63l1 1.71c0.04 0.08 0.13 0.23 0.18 0.3l0.91 1.6c0.06 0.1 0.18 0.31 0.24 0.41l0.92 1.61c0.11 0.18 0.32 0.55 0.43 0.73 0.09 0.16 0.28 0.48 0.37 0.64 0.74 1.23 1.45 2.48 2.11 3.76 0.1 0.21 0.3 0.63 0.39 0.84l0.46 1.04c0.12 0.2 0.36 0.61 0.47 0.81 0.15 0.2 0.44 0.58 0.59 0.77l0.69 0.98c0.12 0.19 0.36 0.57 0.49 0.77l1.01 1.75c0.09 0.16 0.27 0.48 0.36 0.64s0.27 0.47 0.37 0.63l1 1.72c0.04 0.08 0.12 0.22 0.16 0.29l0.95 1.66c0.04 0.07 0.12 0.2 0.16 0.27l0.97 1.69c0.1 0.17 0.3 0.51 0.4 0.69 0.1 0.17 0.3 0.51 0.39 0.68l1 1.72c0.04 0.07 0.11 0.21 0.15 0.28l1.06 1.98-0.01 0.9c-25.02-7.19-49.64-15.72-74.73-22.65-22.77-7.35-45.75-14.04-68.52-21.35 3.17-1.52 6.56-2.49 9.73-3.99 0.41-1.35 0.84-2.69 1.26-4.02 3.95-2.48 8.41-3.97 12.3-6.54 0.15-3.29 0.14-6.57 0.13-9.85 4.36-3.51 8.98-6.76 12.57-11.11 0.36-4.28-2.87-9.29 0.46-12.9 10.24-12.92 16.45-28.93 17.56-45.38 1.83-6.02-1.46-11.98-0.1-17.97z" fill="#f9c941"/>\n            <path d="m509.91 287.41l0.52 0.02 0.29 0.76-0.59 0.04-0.22-0.82z" fill="#f9c941"/>\n            <path d="m257.34 157.32c0.08 0.13 0.24 0.4 0.32 0.54-0.08-0.14-0.24-0.41-0.32-0.54z" fill="#e3b73a"/>\n            <path d="m259.19 160.53c0.07 0.12 0.2 0.35 0.27 0.47-0.07-0.12-0.2-0.35-0.27-0.47z" fill="#e3b73a"/>\n            <path d="m260.28 162.47c0.05 0.08 0.14 0.25 0.19 0.33-0.05-0.08-0.14-0.25-0.19-0.33z" fill="#e3b73a"/>\n            <path d="m261.48 164.56c0.09 0.15 0.26 0.45 0.35 0.6-0.09-0.15-0.26-0.45-0.35-0.6z" fill="#e3b73a"/>\n            <path d="m261.83 165.16c0.09 0.17 0.28 0.49 0.38 0.65-0.1-0.16-0.29-0.48-0.38-0.65z" fill="#e3b73a"/>\n            <path d="m263.16 167.44c0.05 0.09 0.16 0.27 0.21 0.36-0.05-0.09-0.16-0.27-0.21-0.36z" fill="#e3b73a"/>\n            <path d="m265.34 171.21c0.09 0.16 0.26 0.47 0.35 0.62-0.09-0.15-0.26-0.46-0.35-0.62z" fill="#e3b73a"/>\n            <path d="m267.23 174.48c0.09 0.15 0.26 0.43 0.34 0.57-0.08-0.14-0.25-0.42-0.34-0.57z" fill="#e3b73a"/>\n            <path d="m268.43 176.5c0.2 0.33 0.59 0.98 0.79 1.31-0.2-0.33-0.59-0.98-0.79-1.31z" fill="#e3b73a"/>\n            <path d="m269.49 178.29c0.04 0.09 0.14 0.26 0.19 0.34-0.05-0.08-0.15-0.25-0.19-0.34z" fill="#e3b73a"/>\n            <path d="m271.44 181.79c0.19 0.26 0.57 0.8 0.76 1.07-0.19-0.27-0.57-0.81-0.76-1.07z" fill="#e3b73a"/>\n            <path d="m272.46 183.32c0.04 0.08 0.13 0.25 0.18 0.33-0.05-0.08-0.14-0.25-0.18-0.33z" fill="#e3b73a"/>\n            <path d="m275.45 188.72c0.19 0.28 0.56 0.84 0.75 1.12-0.19-0.28-0.56-0.84-0.75-1.12z" fill="#e3b73a"/>\n            <path d="m276.47 190.31c0.04 0.08 0.14 0.25 0.18 0.33-0.04-0.08-0.14-0.25-0.18-0.33z" fill="#e3b73a"/>\n            <path d="m278.23 193.51c0.09 0.15 0.26 0.45 0.35 0.6-0.09-0.15-0.26-0.45-0.35-0.6z" fill="#e3b73a"/>\n            <path d="m279.22 195.24c0.1 0.17 0.3 0.53 0.4 0.7-0.1-0.17-0.3-0.53-0.4-0.7z" fill="#e3b73a"/>\n            <path d="m280.45 197.4c0.1 0.18 0.31 0.55 0.41 0.73-0.1-0.18-0.31-0.55-0.41-0.73z" fill="#e3b73a"/>\n            <path d="m280.86 198.13c0.09 0.15 0.27 0.47 0.36 0.63-0.09-0.16-0.27-0.48-0.36-0.63z" fill="#e3b73a"/>\n            <path d="m282.24 200.51c0.04 0.08 0.13 0.22 0.17 0.29-0.04-0.07-0.13-0.21-0.17-0.29z" fill="#e3b73a"/>\n            <path d="m283.38 202.47c0.08 0.14 0.24 0.41 0.32 0.55-0.08-0.14-0.24-0.41-0.32-0.55z" fill="#e3b73a"/>\n            <path d="m284.31 204.06c0.11 0.18 0.32 0.55 0.43 0.73-0.11-0.18-0.32-0.55-0.43-0.73z" fill="#e3b73a"/>\n            <path d="m286.47 207.71c0.17 0.29 0.52 0.86 0.69 1.15-0.17-0.29-0.52-0.86-0.69-1.15z" fill="#e3b73a"/>\n            <path d="m287.45 209.32c0.05 0.09 0.15 0.25 0.2 0.33-0.05-0.08-0.15-0.24-0.2-0.33z" fill="#e3b73a"/>\n            <path d="m289.43 212.46l0.07 0.1 0.06 0.13c-0.03-0.06-0.1-0.17-0.13-0.23z" fill="#e3b73a"/>\n            <path d="m291.5 216.56c0.09 0.15 0.25 0.44 0.34 0.59-0.09-0.15-0.25-0.44-0.34-0.59z" fill="#e3b73a"/>\n            <path d="m291.84 217.15l0.4 0.68-0.4-0.68z" fill="#e3b73a"/>\n            <path d="m293.19 219.44c0.05 0.09 0.15 0.27 0.21 0.36-0.06-0.09-0.16-0.27-0.21-0.36z" fill="#e3b73a"/>\n            <path d="m294.06 220.98c11.53 4.24 23.46 7.26 35.11 11.11 12.52 3.71 24.94 7.72 37.49 11.32 22.77 7.31 45.75 14 68.52 21.35-0.34 4.87 0.62 9.86-0.59 14.64-0.93 4.65-6.49 5.38-8.78 9.09-30.77 0.01-61.53 0.1-92.29-0.03l-0.66-0.05c-12.71-22.6-25.93-44.92-38.8-67.43z" fill="#e3b73a"/>\n            <path d="m472.71 222.08c0.08 0.13 0.24 0.41 0.32 0.54-0.08-0.13-0.24-0.41-0.32-0.54z" fill="#e8b835"/>\n            <path d="m474.62 225.24c0.08 0.13 0.24 0.4 0.32 0.53-0.08-0.13-0.24-0.4-0.32-0.53z" fill="#e8b835"/>\n            <path d="m475.74 227.14c0.05 0.09 0.15 0.28 0.21 0.37-0.06-0.09-0.16-0.28-0.21-0.37z" fill="#e8b835"/>\n            <path d="m476.95 229.25c0.1 0.17 0.29 0.52 0.39 0.69-0.1-0.17-0.29-0.52-0.39-0.69z" fill="#e8b835"/>\n            <path d="m477.34 229.94c0.09 0.15 0.27 0.46 0.36 0.62-0.09-0.16-0.27-0.47-0.36-0.62z" fill="#e8b835"/>\n            <path d="m478.68 232.26l0.16 0.28-0.16-0.28z" fill="#e8b835"/>\n            <path d="m479.82 234.22c0.04 0.07 0.12 0.2 0.16 0.27-0.04-0.07-0.12-0.2-0.16-0.27z" fill="#e8b835"/>\n            <path d="m480.96 236.19c0.1 0.17 0.29 0.5 0.39 0.67-0.1-0.17-0.29-0.5-0.39-0.67z" fill="#e8b835"/>\n            <path d="m481.35 236.86c0.09 0.17 0.28 0.5 0.37 0.66-0.09-0.16-0.28-0.49-0.37-0.66z" fill="#e8b835"/>\n            <path d="m482.73 239.3c0.14 0.35 0.42 1.03 0.56 1.37-0.14-0.34-0.42-1.02-0.56-1.37z" fill="#e8b835"/>\n            <path d="m483.57 241.22c0.09 0.15 0.26 0.44 0.35 0.58l-0.22-0.29-0.13-0.29z" fill="#e8b835"/>\n            <path d="m484.51 242.69c0.21 0.33 0.62 0.99 0.83 1.32-0.21-0.33-0.62-0.99-0.83-1.32z" fill="#e8b835"/>\n            <path d="m485.34 244.01c0.12 0.2 0.38 0.6 0.5 0.8-0.12-0.2-0.38-0.6-0.5-0.8z" fill="#e8b835"/>\n            <path d="m486.5 245.86c0.2 0.45 0.6 1.36 0.8 1.81-0.2-0.45-0.6-1.36-0.8-1.81z" fill="#e8b835"/>\n            <path d="m487.6 248.21c0.05 0.09 0.15 0.26 0.2 0.34l-0.09-0.13-0.11-0.21z" fill="#e8b835"/>\n            <path d="m489.55 251.16c0.11 0.18 0.33 0.54 0.45 0.72-0.12-0.18-0.34-0.54-0.45-0.72z" fill="#e8b835"/>\n            <path d="m490.6 252.88c0.09 0.16 0.27 0.47 0.36 0.62-0.09-0.15-0.27-0.46-0.36-0.62z" fill="#e8b835"/>\n            <path d="m491.97 255.27c0.09 0.16 0.27 0.48 0.36 0.64-0.09-0.16-0.27-0.48-0.36-0.64z" fill="#e8b835"/>\n            <path d="m492.33 255.91c0.09 0.16 0.27 0.47 0.36 0.63-0.09-0.16-0.27-0.47-0.36-0.63z" fill="#e8b835"/>\n            <path d="m493.69 258.25c0.04 0.08 0.13 0.23 0.18 0.3-0.05-0.07-0.14-0.22-0.18-0.3z" fill="#e8b835"/>\n            <path d="m494.78 260.15c0.06 0.1 0.18 0.31 0.24 0.41-0.06-0.1-0.18-0.31-0.24-0.41z" fill="#e8b835"/>\n            <path d="m495.94 262.17c0.11 0.18 0.32 0.55 0.43 0.73-0.11-0.18-0.32-0.55-0.43-0.73z" fill="#e8b835"/>\n            <path d="m496.37 262.9c0.09 0.16 0.28 0.48 0.37 0.64-0.09-0.16-0.28-0.48-0.37-0.64z" fill="#e8b835"/>\n            <path d="m435.18 264.76c25.09 6.93 49.71 15.46 74.73 22.65l0.22 0.82c-28.09 0.64-56.22 0.11-84.32 0.26 2.29-3.71 7.85-4.44 8.78-9.09 1.21-4.78 0.25-9.77 0.59-14.64z" fill="#e8b835"/>\n            <path d="m498.85 267.3c0.1 0.21 0.3 0.63 0.39 0.84-0.09-0.21-0.29-0.63-0.39-0.84z" fill="#e8b835"/>\n            <path d="m499.7 269.18c0.12 0.2 0.36 0.61 0.47 0.81-0.11-0.2-0.35-0.61-0.47-0.81z" fill="#e8b835"/>\n            <path d="m500.17 269.99c0.15 0.2 0.44 0.58 0.59 0.77-0.15-0.19-0.44-0.57-0.59-0.77z" fill="#e8b835"/>\n            <path d="m501.45 271.74c0.12 0.19 0.36 0.57 0.49 0.77-0.13-0.2-0.37-0.58-0.49-0.77z" fill="#e8b835"/>\n            <path d="m502.95 274.26c0.09 0.16 0.27 0.48 0.36 0.64-0.09-0.16-0.27-0.48-0.36-0.64z" fill="#e8b835"/>\n            <path d="m503.31 274.9c0.09 0.16 0.27 0.47 0.37 0.63-0.1-0.16-0.28-0.47-0.37-0.63z" fill="#e8b835"/>\n            <path d="m504.68 277.25c0.04 0.08 0.12 0.22 0.16 0.29-0.04-0.07-0.12-0.21-0.16-0.29z" fill="#e8b835"/>\n            <path d="m505.79 279.2c0.04 0.07 0.12 0.2 0.16 0.27-0.04-0.07-0.12-0.2-0.16-0.27z" fill="#e8b835"/>\n            <path d="m506.92 281.16c0.1 0.17 0.3 0.51 0.4 0.69-0.1-0.18-0.3-0.52-0.4-0.69z" fill="#e8b835"/>\n            <path d="m507.32 281.85c0.1 0.17 0.3 0.51 0.39 0.68-0.09-0.17-0.29-0.51-0.39-0.68z" fill="#e8b835"/>\n            <path d="m508.71 284.25c0.04 0.07 0.11 0.21 0.15 0.28-0.04-0.07-0.11-0.21-0.15-0.28z" fill="#e8b835"/>\n            <path d="m509.92 286.51c0.13 0.23 0.38 0.69 0.51 0.92l-0.52-0.02 0.01-0.9z" fill="#e8b835"/>\n            <path d="m147.21 341.08c10.31-17.58 20.11-35.49 30.76-52.86 51.63 0.19 103.27-0.29 154.89 0.19l0.66 0.05c-26.11 0.84-52.29-0.22-78.4 0.44-4.98 3.21-8.3 8.41-13.07 11.91-4.43 3.23-7.69 7.73-11.92 11.18-5.93 4.37-10.34 10.43-16.36 14.71-6.96 7.37-14.99 13.58-22.01 20.91-5.26 4.04-9.66 9.03-14.56 13.48-5.39 3.91-9.52 9.21-14.56 13.53-0.13 0.11-0.38 0.33-0.51 0.45-0.27 0.21-0.8 0.65-1.07 0.86-0.13 0.11-0.37 0.31-0.5 0.42-3.97 3.07-7.01 7.12-10.91 10.27-0.13 0.11-0.38 0.32-0.5 0.42-0.13 0.11-0.38 0.32-0.51 0.42-5.69 4.67-10.25 10.58-16.22 14.93-6.01 5.96-12.79 11.08-18.5 17.36-8.44 6.94-15.74 15.13-24.44 21.78 2.16-4.71 5.02-9.03 7.53-13.54 16.74-28.97 33.46-57.94 50.2-86.91z" fill="#296ad9"/>\n            <path d="m89.04 442.17c0.11-0.16 0.33-0.48 0.44-0.64l-0.03 0.65-0.41-0.01z" fill="#296ad9"/>\n            <path id="#2a71e9ff" fill="#2a71e9" d="m333.52 288.46c30.76 0.13 61.52 0.04 92.29 0.03 28.1-0.15 56.23 0.38 84.32-0.26l0.59-0.04 0.4 0.11v0.08c-29.66 51.27-59.23 102.6-88.89 153.88-110.93-0.03-221.86 0.1-332.78-0.08l0.03-0.65c8.7-6.65 16-14.84 24.44-21.78 5.71-6.28 12.49-11.4 18.5-17.36 5.97-4.35 10.53-10.26 16.22-14.93 0.13-0.1 0.38-0.31 0.51-0.42 0.12-0.1 0.37-0.31 0.5-0.42 3.9-3.15 6.94-7.2 10.91-10.27 0.13-0.11 0.37-0.31 0.5-0.42 0.27-0.21 0.8-0.65 1.07-0.86 0.13-0.12 0.38-0.34 0.51-0.45 5.04-4.32 9.17-9.62 14.56-13.53 4.9-4.45 9.3-9.44 14.56-13.48 7.02-7.33 15.05-13.54 22.01-20.91 6.02-4.28 10.43-10.34 16.36-14.71 4.23-3.45 7.49-7.95 11.92-11.18 4.77-3.5 8.09-8.7 13.07-11.91 26.11-0.66 52.29 0.4 78.4-0.44z"/>\n            <path d="m162.13 375.07c0.13-0.12 0.38-0.34 0.51-0.45-0.13 0.11-0.38 0.33-0.51 0.45z" fill="#286ee6"/>\n            <path d="m161.06 375.93c0.27-0.21 0.8-0.65 1.07-0.86-0.27 0.21-0.8 0.65-1.07 0.86z" fill="#286ee6"/>\n            <path d="m160.56 376.35c0.13-0.11 0.37-0.31 0.5-0.42-0.13 0.11-0.37 0.31-0.5 0.42z" fill="#286ee6"/>\n            <path d="m149.15 387.04c0.12-0.1 0.37-0.31 0.5-0.42-0.13 0.11-0.38 0.32-0.5 0.42z" fill="#286ee6"/>\n            <path d="m148.64 387.46c0.13-0.1 0.38-0.31 0.51-0.42-0.13 0.11-0.38 0.32-0.51 0.42z" fill="#286ee6"/>\n          </svg>\n          <div>Google Drive</div>\n        </button>\n      </div>\n    </div>\n  </div>\n\n  <div class="rs-box rs-box-sign-in">\n    <div class="rs-content">\n      <form name="rs-sign-in-form" class="rs-sign-in-form">\n        <h1 class="rs-big-headline">Connect your storage</h1>\n        <input type="text" name="rs-user-address" placeholder="user@provider.com" autocapitalize="off">\n        <div class="rs-sign-in-error rs-hidden"></div>\n        <input type="submit" class="rs-connect" value="Connect">\n        <a href="https://remotestorage.io/get/" class="rs-help" target="_blank">Need help?</a>\n      </form>\n    </div>\n  </div>\n</div>\n'}])});

},{}],"src/models/carnet.js":[function(require,module,exports) {
var m = require("mithril");

var debounce = require('lodash.debounce');

var fs = require("file-saver"); //var rs = require('../lib/storage')


var RemoteStorage = require('remotestoragejs');

var storageNotes = require('../lib/remotestorage-module-notes');

var Widget = require('remotestorage-widget');

var rs = new RemoteStorage({
  cache: true,
  changeEvents: {
    local: true,
    window: false,
    remote: true,
    conflict: true
  },
  cordovaRedirectUri: undefined,
  logging: false,
  modules: [storageNotes]
}); //new Widget(rs).attach('remotestorage-connect')
//console.log("initiate remoteStorage");

rs.access.claim('documents', 'rw'); //console.log("initiate2 remoteStorage");

rs.caching.enable('/documents/notes/', 'ALL');
var widget = new Widget(rs, {
  autoCloseAfter: 2000
});
rs.on('connected', function () {
  var userAddress = rs.remote.userAddress;
  console.log("".concat(userAddress, " connected their remote storage."));
});
rs.on('network-offline', function () {
  console.log("We're offline now.");
});
rs.on('network-online', function () {
  console.log("Hooray, we're back online.");
}); //console.log("rs")
//console.log(rs)
//de stackoverflow

function auto_grow(element) {
  element.style.height = "5px";
  element.style.height = element.scrollHeight + 20 + "px";
}

var Carnet = {
  store: rs.documents.privateList('notes-nl/'),
  notes: [],
  note: {},
  selectionne: "",
  encharge: true,
  effModal: '',
  actMenu: false,
  noteVide: function noteVide() {
    return {
      title: "",
      content: "",
      id: new Date().toJSON(),
      lastEdited: new Date().toJSON(),
      etiquettes: []
    };
  },
  viewNote: "editeur",
  //parmi liste/editeur/lecteur,
  chargement: function chargement() {
    Carnet.store.getAll('').then(function (reponse) {
      Carnet.notes = Object.values(reponse).filter(function (it) {
        return it.id;
      });
      Carnet.encharge = false; //console.log("chargé!!");

      m.redraw();
    }).catch(function (err) {
      console.log("erreur de chargement");
      console.log(err);
    });
  },
  allerNote: function allerNote(id) {
    m.route.set("/note/:id", {
      id: id
    });
  },
  ajout: function ajout() {
    var nnote = Carnet.noteVide(); //console.log("affichage store:")
    //console.log(Carnet.store)
    //console.log("fin store:")

    Carnet.store.add(nnote);
    Carnet.notes.push(nnote);
    Carnet.store.getListing('').then(function (listing) {
      return console.log(listing);
    }); //console.log(Carnet.notes);
  },
  retirer: function retirer() {
    //alert("effacement irréversible")
    Carnet.notes = Carnet.notes.filter(function (n) {
      return n.id != Carnet.note.id;
    });
    Carnet.toggleModal();
    Carnet.store.remove(Carnet.note.id); //m(m.route.Link, {href: "#/liste"});

    m.route.set('#/liste');
  },
  toggleModal: function toggleModal() {
    console.log("toggleModal");

    if (Carnet.effModal == '') {
      Carnet.effModal = '.is-active';
    } else {
      Carnet.effModal = '';
    }

    m.redraw();
  },
  edit: function edit(id, txt) {
    Carnet.note[id] = txt;
    Carnet.note.lastEdited = new Date().toJSON();
    debounce(function () {
      return Carnet.store.set(Carnet.note.id, Carnet.note);
    }, 1000)(); //Carnet.store.set(Carnet.note._id,Carnet.note);
    //var element = document.getElementById("contenu_editeur");
    //var c = parseInt(element.cols);
    //element.rows = (2+Carnet.note.content.split('\n').reduce((acc,str) => acc+Math.ceil((1+str.length)/c),0)).toString();
    //console.log(c,element.rows)
  },
  editSel: function editSel(e, titre) {
    Carnet.selectionne = titre.trim();

    if (e.keyCode == 13) {
      console.log("keycode 13");
      e.target.value = "";
      Carnet.ajoutAvecTitre();
      Carnet.chargement();
    }
  },
  chargeNote: function chargeNote(id) {
    Carnet.store.get(id).then(function (reponse) {
      Carnet.note = reponse;
      Carnet.encharge = false; //console.log("chargé!!");

      m.redraw();
    }).catch(function (err) {
      console.log("erreur de chargement"); //console.log(err);
    });

    if (Carnet.notes.length == 0) {
      Carnet.encharge = true;
    } else {
      //console.log("note charge:",id,Carnet.notes)
      Carnet.note = Carnet.notes.find(function (n) {
        return n.id == id;
      }); //console.log("chargé ?",Carnet.note)
    }
  },
  vue: function vue() {
    if (Carnet.viewNote == 'editeur') {
      Carnet.viewNote = 'lecteur';
    } else {
      Carnet.viewNote = 'editeur';
    } //m.redraw();

  },
  ajoutEtiq: function ajoutEtiq(e, tag) {
    //console.log(e.keyCode)
    if (e.keyCode == 13) {
      //console.log("keycode 13")
      var mesetiquettes = new Set(Carnet.note.etiquettes.concat(tag.split(',').map(function (mot) {
        return mot.trim();
      }).filter(function (mot) {
        return mot.length > 0;
      })));
      Carnet.note.etiquettes = Array.from(mesetiquettes);
      e.target.value = "";
      Carnet.store.set(Carnet.note.id, Carnet.note);
      Carnet.toggleEtiq();
      Carnet.chargement();
    }
  },
  effaceEtiq: function effaceEtiq(tag) {
    Carnet.note.etiquettes = Carnet.note.etiquettes.filter(function (t) {
      return t !== tag;
    });
  },
  toggleEtiq: function toggleEtiq() {
    Carnet.actMenu = !Carnet.actMenu;
  },
  selection: function selection() {
    var selection = window.getSelection();

    if (selection.toString().length > 0) {
      str = selection.toString().trim();
      Carnet.selectionne = str[0].toUpperCase() + str.slice(1);
    } //console.log(Carnet.selectionne)
    //console.log(Carnet.selectionne.length)
    //alert(Carnet.selectionne);

  },
  ajoutAvecTitre: function ajoutAvecTitre() {
    var nvlleNote = Carnet.noteVide(); //console.log("affichage store:")
    //console.log(Carnet.store)
    //console.log("fin store:")
    //console.log(nvlleNote)
    //var selection = window.getSelection();

    if (Carnet.selectionne.length == 0) {//popup pour un titre
      //console.log(Document.activeElement)
      // Carnet.selectionne = HTMLInputElement.setSelectionRange().toString();
    } else {
      nvlleNote.title = Carnet.selectionne;
      Carnet.selectionne = "";
    }

    var doc = Carnet.store.add(nvlleNote); //console.log(Carnet.notes)
    //Carnet.chargeNote(doc.id)
    //Carnet.notes.push(nvlleNote);

    Carnet.chargement();
  },
  download: function download() {
    console.log("fun download called on note", Carnet.note);
    var taglist = "[" + Carnet.note.etiquettes.map(function (it) {
      return '"' + it + '"';
    }).toString() + "]";
    var entete = "---\n" + "title: " + Carnet.note.title + "\ndate: " + Carnet.note.id + "\nmodified: " + Carnet.note.lastEdited + "\ntags:" + taglist + "\n---\n";
    var blob = new Blob([entete + Carnet.note.content], {
      type: "text/plain;charset=utf-8"
    });
    fs.saveAs(blob, Carnet.note.id + ".md");
  }
};
widget.attach("widget"); //widget.close();

Carnet.store.on('change', function (evt) {
  Carnet.chargement();
  console.log('data was added, updated, or removed:', evt);
});
Carnet.chargement();
/*// Applied globally on all textareas with the "autoExpand" class
$(document)
    .one('focus.autoExpand', 'textarea.autoExpand', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input.autoExpand', 'textarea.autoExpand', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 16);
        this.rows = minRows + rows;
    });
*/

module.exports = Carnet;
},{"mithril":"node_modules/mithril/index.js","lodash.debounce":"node_modules/lodash.debounce/index.js","file-saver":"node_modules/file-saver/dist/FileSaver.min.js","remotestoragejs":"node_modules/remotestoragejs/release/remotestorage.js","../lib/remotestorage-module-notes":"src/lib/remotestorage-module-notes.js","remotestorage-widget":"node_modules/remotestorage-widget/build/widget.js"}],"node_modules/lodash.flatten/index.js":[function(require,module,exports) {
var global = arguments[3];
/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]';

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/**
 * Appends the elements of `values` to `array`.
 *
 * @private
 * @param {Array} array The array to modify.
 * @param {Array} values The values to append.
 * @returns {Array} Returns `array`.
 */
function arrayPush(array, values) {
  var index = -1,
      length = values.length,
      offset = array.length;

  while (++index < length) {
    array[offset + index] = values[index];
  }
  return array;
}

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/** Built-in value references. */
var Symbol = root.Symbol,
    propertyIsEnumerable = objectProto.propertyIsEnumerable,
    spreadableSymbol = Symbol ? Symbol.isConcatSpreadable : undefined;

/**
 * The base implementation of `_.flatten` with support for restricting flattening.
 *
 * @private
 * @param {Array} array The array to flatten.
 * @param {number} depth The maximum recursion depth.
 * @param {boolean} [predicate=isFlattenable] The function invoked per iteration.
 * @param {boolean} [isStrict] Restrict to values that pass `predicate` checks.
 * @param {Array} [result=[]] The initial result value.
 * @returns {Array} Returns the new flattened array.
 */
function baseFlatten(array, depth, predicate, isStrict, result) {
  var index = -1,
      length = array.length;

  predicate || (predicate = isFlattenable);
  result || (result = []);

  while (++index < length) {
    var value = array[index];
    if (depth > 0 && predicate(value)) {
      if (depth > 1) {
        // Recursively flatten arrays (susceptible to call stack limits).
        baseFlatten(value, depth - 1, predicate, isStrict, result);
      } else {
        arrayPush(result, value);
      }
    } else if (!isStrict) {
      result[result.length] = value;
    }
  }
  return result;
}

/**
 * Checks if `value` is a flattenable `arguments` object or array.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is flattenable, else `false`.
 */
function isFlattenable(value) {
  return isArray(value) || isArguments(value) ||
    !!(spreadableSymbol && value && value[spreadableSymbol]);
}

/**
 * Flattens `array` a single level deep.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {Array} array The array to flatten.
 * @returns {Array} Returns the new flattened array.
 * @example
 *
 * _.flatten([1, [2, [3, [4]], 5]]);
 * // => [1, 2, [3, [4]], 5]
 */
function flatten(array) {
  var length = array ? array.length : 0;
  return length ? baseFlatten(array, 1) : [];
}

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
function isArguments(value) {
  // Safari 8.1 makes `arguments.callee` enumerable in strict mode.
  return isArrayLikeObject(value) && hasOwnProperty.call(value, 'callee') &&
    (!propertyIsEnumerable.call(value, 'callee') || objectToString.call(value) == argsTag);
}

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

/**
 * This method is like `_.isArrayLike` except that it also checks if `value`
 * is an object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array-like object,
 *  else `false`.
 * @example
 *
 * _.isArrayLikeObject([1, 2, 3]);
 * // => true
 *
 * _.isArrayLikeObject(document.body.children);
 * // => true
 *
 * _.isArrayLikeObject('abc');
 * // => false
 *
 * _.isArrayLikeObject(_.noop);
 * // => false
 */
function isArrayLikeObject(value) {
  return isObjectLike(value) && isArrayLike(value);
}

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 8-9 which returns 'object' for typed array and other constructors.
  var tag = isObject(value) ? objectToString.call(value) : '';
  return tag == funcTag || tag == genTag;
}

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

module.exports = flatten;

},{}],"src/views/Etiquette.js":[function(require,module,exports) {
//import marked from "marked";
var m = require("mithril");

var Carnet = require("../models/carnet");

var flatten = require('lodash.flatten');

function Etiquette() {
  return {
    view: function view(vnode) {
      //console.log(vnode);
      //console.log("note:",Carnet.note)
      var etiquettes;
      var boutonEfface = false;

      if (Carnet.note.id) {
        etiquettes = Carnet.note.etiquettes;
        boutonEfface = true;
      } else {
        var etiquettesSet = new Set(flatten(Carnet.notes.map(function (n) {
          return n.etiquettes;
        })));
        etiquettes = Array.from(etiquettesSet);
      }

      return m("#etiquettes.tags", etiquettes.map(function (tag) {
        return m("span.is-light", m("a.is-size-7", {
          href: "#/tag/" + tag
        }, tag), m("button.delete.is-small" + (boutonEfface ? "" : ".is-hidden"), {
          onclick: function onclick() {
            return Carnet.effaceEtiq(tag);
          }
        }));
      }));
    }
  };
}

module.exports = Etiquette;
},{"mithril":"node_modules/mithril/index.js","../models/carnet":"src/models/carnet.js","lodash.flatten":"node_modules/lodash.flatten/index.js"}],"src/views/Barre.js":[function(require,module,exports) {
var m = require("mithril");

var Carnet = require("../models/carnet");

var Etiquette = require("./Etiquette");

function Barre() {
  return {
    view: function view(vnode) {
      //console.log('Barre')
      //console.log(vnode.attrs)
      var vueMenu;

      if (Carnet.viewNote == "lecteur") {
        vueMenu = "Editer";
      } else {
        vueMenu = "Voir";
      }

      var menuList = [{
        val: "Carnet",
        attr: {
          href: "#/liste",
          style: "font-variant: small-caps;"
        },
        id: "a.is-size-4.has-text-weight-bold.has-text-centered"
      }, {
        id: ".level#nvellenote.is-mobile",
        attr: {},
        val: [m(".level-left", {
          style: "width:85%;"
        }, m("input.input.is-size-6.is-primary", {
          oninput: function oninput(e) {
            Carnet.selectionne = e.target.value;
          },
          value: Carnet.selectionne,
          placeholder: "Nouvelle note"
        })), m(".level.right", {
          style: "width:15%;"
        }, m(".button", {
          onclick: Carnet.ajoutAvecTitre
        }, m("span.icon", m("ion-icon", {
          name: "add-circle"
        }))))]
      }, {
        id: "#etiquettes",
        attr: {},
        val: m(Etiquette, vnode.attrs)
      }
      /*,
      {
          val: vueMenu,
          id: "a",
          attr: {onclick:Carnet.vue},
          vue: "note"
      },
      {
          val: "Effacer",
          id: "a",
          attr: {onclick:()=>{Carnet.effmodal=".is-active";}},
          vue: "note"
      }*/
      ];

      if (!vnode.attrs.id) {
        menuList = menuList.filter(function (m) {
          return m.vue != "note";
        });
      }

      return m("aside.menu", {}, m("ul.menu-list", menuList.map(function (item) {
        return m("li", m(item.id, item.attr, item.val));
      }) //m("li.has-text-right",m(Etiquette,vnode.attrs)),
      ));
    }
  };
}

module.exports = Barre;
},{"mithril":"node_modules/mithril/index.js","../models/carnet":"src/models/carnet.js","./Etiquette":"src/views/Etiquette.js"}],"src/views/Layout.js":[function(require,module,exports) {
var m = require("mithril");

var Barre = require("./Barre");

var Carnet = require("../models/carnet");

function Layout() {
  return {
    view: function view(vnode) {
      return m("#layout.columns.is-widescreen", {
        onmouseup: Carnet.selection
      }, m(".column.is-2", m(Barre, vnode.attrs)), m("#note.column.is-10", vnode.children));
    }
  };
}

module.exports = Layout;
},{"mithril":"node_modules/mithril/index.js","./Barre":"src/views/Barre.js","../models/carnet":"src/models/carnet.js"}],"src/views/Liste.js":[function(require,module,exports) {
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var m = require("mithril"); //import marked from "marked";


var Carnet = require("../models/carnet");

var Etiquette = require("./Etiquette");

function titleOrContent(note) {
  if (note.title) {
    return note.title;
  } else {
    var ind = note.content.indexOf('\n');

    if (ind == -1) {
      ind = 60;
    }

    return note.content.substr(0, ind);
  }
}

function formatDate(jsondate) {
  var date = new Date(jsondate);
  var dateTimeFormat = new Intl.DateTimeFormat('fr', {
    year: 'numeric',
    month: 'short',
    day: '2-digit'
  });

  var _dateTimeFormat$forma = dateTimeFormat.formatToParts(date),
      _dateTimeFormat$forma2 = _slicedToArray(_dateTimeFormat$forma, 5),
      jour = _dateTimeFormat$forma2[0].value,
      mois = _dateTimeFormat$forma2[2].value,
      annee = _dateTimeFormat$forma2[4].value;

  return "".concat(jour, " ").concat(mois, " ").concat(annee);
}

function Liste() {
  return {
    oninit: function oninit() {
      Carnet.chargement();
    },
    view: function view(vnode) {
      var notesFiltrees;
      Carnet.note = {}; //console.log(vnode.attrs)

      if (vnode.attrs.etiq) {
        notesFiltrees = Carnet.notes.filter(function (x) {
          return x.etiquettes.includes(vnode.attrs.etiq);
        });
      } else {
        notesFiltrees = Carnet.notes.slice();
      } //console.log(sort((a,b)=>Date.parse(b.modif)-Date.parse(a.modif),Carnet.notes ));


      return m("main.columns", m(".column.is-three-quarters", notesFiltrees.sort(function (a, b) {
        return Date.parse(b.lastEdited) - Date.parse(a.lastEdited);
      }).map(function (note) {
        return m(".box", {
          onclick: function onclick() {
            return Carnet.allerNote(note.id);
          }
        }, m("p.is-size-5.has-text-weight-semibold", titleOrContent(note)), m("p", formatDate(note.id)));
      })) //m(".column.is-one-quarter",m(Etiquette))
      );
    }
  };
}

module.exports = Liste;
},{"mithril":"node_modules/mithril/index.js","../models/carnet":"src/models/carnet.js","./Etiquette":"src/views/Etiquette.js"}],"src/views/Editeur.js":[function(require,module,exports) {
var m = require("mithril"); //var throttle = require('lodash.throttle');
//import marked from "marked";


var Carnet = require("../models/carnet"); //var autosize = require("autosize");


function calculDuNombreDeLigne() {
  var elt = document.getElementById("editor");

  if (elt) {
    var scrollWidth = elt.scrollWidth;
  } else {
    var scrollWidth = Math.ceil(document.scrollWidth * 10 / 12);
  }

  var lignes = Carnet.note.content.split(/\n/g);
  var nb = lignes.reduce(function (acc, s) {
    return acc + Math.ceil(s.length / scrollWidth * 9);
  }, 0); //magic number

  return nb;
}

function Editeur() {
  return {
    view: function view() {
      //console.log("editeur")
      //console.log((Carnet.note.content.match(/\n/g)||[]).length+1)
      return m('.box#editor.column', m("input.input.is-size-5", {
        oninput: function oninput(e) {
          Carnet.edit("title", e.target.value);
        },
        value: Carnet.note.title,
        placeholder: "Titre"
      }), m("textarea.textarea#contenu_editeur.has-fixed-size.autoExpand", {
        oninput: function oninput(e) {
          Carnet.edit("content", e.target.value);
        },
        value: Carnet.note.content,
        placeholder: "Texte" //rows: calculDuNombreDeLigne(),

      }));
    }
  };
}

module.exports = Editeur;
},{"mithril":"node_modules/mithril/index.js","../models/carnet":"src/models/carnet.js"}],"node_modules/dompurify/dist/purify.js":[function(require,module,exports) {
var define;
var global = arguments[3];
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.DOMPurify = factory());
}(this, (function () { 'use strict';

var freeze$1 = Object.freeze || function (x) {
  return x;
};

var html = freeze$1(['a', 'abbr', 'acronym', 'address', 'area', 'article', 'aside', 'audio', 'b', 'bdi', 'bdo', 'big', 'blink', 'blockquote', 'body', 'br', 'button', 'canvas', 'caption', 'center', 'cite', 'code', 'col', 'colgroup', 'content', 'data', 'datalist', 'dd', 'decorator', 'del', 'details', 'dfn', 'dir', 'div', 'dl', 'dt', 'element', 'em', 'fieldset', 'figcaption', 'figure', 'font', 'footer', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head', 'header', 'hgroup', 'hr', 'html', 'i', 'img', 'input', 'ins', 'kbd', 'label', 'legend', 'li', 'main', 'map', 'mark', 'marquee', 'menu', 'menuitem', 'meter', 'nav', 'nobr', 'ol', 'optgroup', 'option', 'output', 'p', 'pre', 'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'section', 'select', 'shadow', 'small', 'source', 'spacer', 'span', 'strike', 'strong', 'style', 'sub', 'summary', 'sup', 'table', 'tbody', 'td', 'template', 'textarea', 'tfoot', 'th', 'thead', 'time', 'tr', 'track', 'tt', 'u', 'ul', 'var', 'video', 'wbr']);

// SVG
var svg = freeze$1(['svg', 'a', 'altglyph', 'altglyphdef', 'altglyphitem', 'animatecolor', 'animatemotion', 'animatetransform', 'audio', 'canvas', 'circle', 'clippath', 'defs', 'desc', 'ellipse', 'filter', 'font', 'g', 'glyph', 'glyphref', 'hkern', 'image', 'line', 'lineargradient', 'marker', 'mask', 'metadata', 'mpath', 'path', 'pattern', 'polygon', 'polyline', 'radialgradient', 'rect', 'stop', 'style', 'switch', 'symbol', 'text', 'textpath', 'title', 'tref', 'tspan', 'video', 'view', 'vkern']);

var svgFilters = freeze$1(['feBlend', 'feColorMatrix', 'feComponentTransfer', 'feComposite', 'feConvolveMatrix', 'feDiffuseLighting', 'feDisplacementMap', 'feDistantLight', 'feFlood', 'feFuncA', 'feFuncB', 'feFuncG', 'feFuncR', 'feGaussianBlur', 'feMerge', 'feMergeNode', 'feMorphology', 'feOffset', 'fePointLight', 'feSpecularLighting', 'feSpotLight', 'feTile', 'feTurbulence']);

var mathMl = freeze$1(['math', 'menclose', 'merror', 'mfenced', 'mfrac', 'mglyph', 'mi', 'mlabeledtr', 'mmultiscripts', 'mn', 'mo', 'mover', 'mpadded', 'mphantom', 'mroot', 'mrow', 'ms', 'mspace', 'msqrt', 'mstyle', 'msub', 'msup', 'msubsup', 'mtable', 'mtd', 'mtext', 'mtr', 'munder', 'munderover']);

var text = freeze$1(['#text']);

var freeze$2 = Object.freeze || function (x) {
  return x;
};

var html$1 = freeze$2(['accept', 'action', 'align', 'alt', 'autocomplete', 'background', 'bgcolor', 'border', 'cellpadding', 'cellspacing', 'checked', 'cite', 'class', 'clear', 'color', 'cols', 'colspan', 'controls', 'coords', 'crossorigin', 'datetime', 'default', 'dir', 'disabled', 'download', 'enctype', 'face', 'for', 'headers', 'height', 'hidden', 'high', 'href', 'hreflang', 'id', 'integrity', 'ismap', 'label', 'lang', 'list', 'loop', 'low', 'max', 'maxlength', 'media', 'method', 'min', 'minlength', 'multiple', 'name', 'noshade', 'novalidate', 'nowrap', 'open', 'optimum', 'pattern', 'placeholder', 'poster', 'preload', 'pubdate', 'radiogroup', 'readonly', 'rel', 'required', 'rev', 'reversed', 'role', 'rows', 'rowspan', 'spellcheck', 'scope', 'selected', 'shape', 'size', 'sizes', 'span', 'srclang', 'start', 'src', 'srcset', 'step', 'style', 'summary', 'tabindex', 'title', 'type', 'usemap', 'valign', 'value', 'width', 'xmlns']);

var svg$1 = freeze$2(['accent-height', 'accumulate', 'additive', 'alignment-baseline', 'ascent', 'attributename', 'attributetype', 'azimuth', 'basefrequency', 'baseline-shift', 'begin', 'bias', 'by', 'class', 'clip', 'clip-path', 'clip-rule', 'color', 'color-interpolation', 'color-interpolation-filters', 'color-profile', 'color-rendering', 'cx', 'cy', 'd', 'dx', 'dy', 'diffuseconstant', 'direction', 'display', 'divisor', 'dur', 'edgemode', 'elevation', 'end', 'fill', 'fill-opacity', 'fill-rule', 'filter', 'filterunits', 'flood-color', 'flood-opacity', 'font-family', 'font-size', 'font-size-adjust', 'font-stretch', 'font-style', 'font-variant', 'font-weight', 'fx', 'fy', 'g1', 'g2', 'glyph-name', 'glyphref', 'gradientunits', 'gradienttransform', 'height', 'href', 'id', 'image-rendering', 'in', 'in2', 'k', 'k1', 'k2', 'k3', 'k4', 'kerning', 'keypoints', 'keysplines', 'keytimes', 'lang', 'lengthadjust', 'letter-spacing', 'kernelmatrix', 'kernelunitlength', 'lighting-color', 'local', 'marker-end', 'marker-mid', 'marker-start', 'markerheight', 'markerunits', 'markerwidth', 'maskcontentunits', 'maskunits', 'max', 'mask', 'media', 'method', 'mode', 'min', 'name', 'numoctaves', 'offset', 'operator', 'opacity', 'order', 'orient', 'orientation', 'origin', 'overflow', 'paint-order', 'path', 'pathlength', 'patterncontentunits', 'patterntransform', 'patternunits', 'points', 'preservealpha', 'preserveaspectratio', 'primitiveunits', 'r', 'rx', 'ry', 'radius', 'refx', 'refy', 'repeatcount', 'repeatdur', 'restart', 'result', 'rotate', 'scale', 'seed', 'shape-rendering', 'specularconstant', 'specularexponent', 'spreadmethod', 'stddeviation', 'stitchtiles', 'stop-color', 'stop-opacity', 'stroke-dasharray', 'stroke-dashoffset', 'stroke-linecap', 'stroke-linejoin', 'stroke-miterlimit', 'stroke-opacity', 'stroke', 'stroke-width', 'style', 'surfacescale', 'tabindex', 'targetx', 'targety', 'transform', 'text-anchor', 'text-decoration', 'text-rendering', 'textlength', 'type', 'u1', 'u2', 'unicode', 'values', 'viewbox', 'visibility', 'version', 'vert-adv-y', 'vert-origin-x', 'vert-origin-y', 'width', 'word-spacing', 'wrap', 'writing-mode', 'xchannelselector', 'ychannelselector', 'x', 'x1', 'x2', 'xmlns', 'y', 'y1', 'y2', 'z', 'zoomandpan']);

var mathMl$1 = freeze$2(['accent', 'accentunder', 'align', 'bevelled', 'close', 'columnsalign', 'columnlines', 'columnspan', 'denomalign', 'depth', 'dir', 'display', 'displaystyle', 'encoding', 'fence', 'frame', 'height', 'href', 'id', 'largeop', 'length', 'linethickness', 'lspace', 'lquote', 'mathbackground', 'mathcolor', 'mathsize', 'mathvariant', 'maxsize', 'minsize', 'movablelimits', 'notation', 'numalign', 'open', 'rowalign', 'rowlines', 'rowspacing', 'rowspan', 'rspace', 'rquote', 'scriptlevel', 'scriptminsize', 'scriptsizemultiplier', 'selection', 'separator', 'separators', 'stretchy', 'subscriptshift', 'supscriptshift', 'symmetric', 'voffset', 'width', 'xmlns']);

var xml = freeze$2(['xlink:href', 'xml:id', 'xlink:title', 'xml:space', 'xmlns:xlink']);

var hasOwnProperty = Object.hasOwnProperty;
var setPrototypeOf = Object.setPrototypeOf;

var _ref$1 = typeof Reflect !== 'undefined' && Reflect;
var apply$1 = _ref$1.apply;

if (!apply$1) {
  apply$1 = function apply(fun, thisValue, args) {
    return fun.apply(thisValue, args);
  };
}

/* Add properties to a lookup table */
function addToSet(set, array) {
  if (setPrototypeOf) {
    // Make 'in' and truthy checks like Boolean(set.constructor)
    // independent of any properties defined on Object.prototype.
    // Prevent prototype setters from intercepting set as a this value.
    setPrototypeOf(set, null);
  }

  var l = array.length;
  while (l--) {
    var element = array[l];
    if (typeof element === 'string') {
      var lcElement = element.toLowerCase();
      if (lcElement !== element) {
        // Config presets (e.g. tags.js, attrs.js) are immutable.
        if (!Object.isFrozen(array)) {
          array[l] = lcElement;
        }

        element = lcElement;
      }
    }

    set[element] = true;
  }

  return set;
}

/* Shallow clone an object */
function clone(object) {
  var newObject = {};

  var property = void 0;
  for (property in object) {
    if (apply$1(hasOwnProperty, object, [property])) {
      newObject[property] = object[property];
    }
  }

  return newObject;
}

var seal = Object.seal || function (x) {
  return x;
};

var MUSTACHE_EXPR = seal(/\{\{[\s\S]*|[\s\S]*\}\}/gm); // Specify template detection regex for SAFE_FOR_TEMPLATES mode
var ERB_EXPR = seal(/<%[\s\S]*|[\s\S]*%>/gm);
var DATA_ATTR = seal(/^data-[\-\w.\u00B7-\uFFFF]/); // eslint-disable-line no-useless-escape
var ARIA_ATTR = seal(/^aria-[\-\w]+$/); // eslint-disable-line no-useless-escape
var IS_ALLOWED_URI = seal(/^(?:(?:(?:f|ht)tps?|mailto|tel|callto|cid|xmpp):|[^a-z]|[a-z+.\-]+(?:[^a-z+.\-:]|$))/i // eslint-disable-line no-useless-escape
);
var IS_SCRIPT_OR_DATA = seal(/^(?:\w+script|data):/i);
var ATTR_WHITESPACE = seal(/[\u0000-\u0020\u00A0\u1680\u180E\u2000-\u2029\u205f\u3000]/g // eslint-disable-line no-control-regex
);

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var _ref = typeof Reflect !== 'undefined' && Reflect;
var apply = _ref.apply;

var arraySlice = Array.prototype.slice;
var freeze = Object.freeze;

var getGlobal = function getGlobal() {
  return typeof window === 'undefined' ? null : window;
};

if (!apply) {
  apply = function apply(fun, thisValue, args) {
    return fun.apply(thisValue, args);
  };
}

/**
 * Creates a no-op policy for internal use only.
 * Don't export this function outside this module!
 * @param {?TrustedTypePolicyFactory} trustedTypes The policy factory.
 * @param {Document} document The document object (to determine policy name suffix)
 * @return {?TrustedTypePolicy} The policy created (or null, if Trusted Types
 * are not supported).
 */
var _createTrustedTypesPolicy = function _createTrustedTypesPolicy(trustedTypes, document) {
  if ((typeof trustedTypes === 'undefined' ? 'undefined' : _typeof(trustedTypes)) !== 'object' || typeof trustedTypes.createPolicy !== 'function') {
    return null;
  }

  // Allow the callers to control the unique policy name
  // by adding a data-tt-policy-suffix to the script element with the DOMPurify.
  // Policy creation with duplicate names throws in Trusted Types.
  var suffix = null;
  var ATTR_NAME = 'data-tt-policy-suffix';
  if (document.currentScript && document.currentScript.hasAttribute(ATTR_NAME)) {
    suffix = document.currentScript.getAttribute(ATTR_NAME);
  }

  var policyName = 'dompurify' + (suffix ? '#' + suffix : '');

  try {
    return trustedTypes.createPolicy(policyName, {
      createHTML: function createHTML(html$$1) {
        return html$$1;
      }
    });
  } catch (error) {
    // Policy creation failed (most likely another DOMPurify script has
    // already run). Skip creating the policy, as this will only cause errors
    // if TT are enforced.
    console.warn('TrustedTypes policy ' + policyName + ' could not be created.');
    return null;
  }
};

function createDOMPurify() {
  var window = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : getGlobal();

  var DOMPurify = function DOMPurify(root) {
    return createDOMPurify(root);
  };

  /**
   * Version label, exposed for easier checks
   * if DOMPurify is up to date or not
   */
  DOMPurify.version = '2.0.7';

  /**
   * Array of elements that DOMPurify removed during sanitation.
   * Empty if nothing was removed.
   */
  DOMPurify.removed = [];

  if (!window || !window.document || window.document.nodeType !== 9) {
    // Not running in a browser, provide a factory function
    // so that you can pass your own Window
    DOMPurify.isSupported = false;

    return DOMPurify;
  }

  var originalDocument = window.document;
  var useDOMParser = false;
  var removeTitle = false;

  var document = window.document;
  var DocumentFragment = window.DocumentFragment,
      HTMLTemplateElement = window.HTMLTemplateElement,
      Node = window.Node,
      NodeFilter = window.NodeFilter,
      _window$NamedNodeMap = window.NamedNodeMap,
      NamedNodeMap = _window$NamedNodeMap === undefined ? window.NamedNodeMap || window.MozNamedAttrMap : _window$NamedNodeMap,
      Text = window.Text,
      Comment = window.Comment,
      DOMParser = window.DOMParser,
      TrustedTypes = window.TrustedTypes;

  // As per issue #47, the web-components registry is inherited by a
  // new document created via createHTMLDocument. As per the spec
  // (http://w3c.github.io/webcomponents/spec/custom/#creating-and-passing-registries)
  // a new empty registry is used when creating a template contents owner
  // document, so we use that as our parent document to ensure nothing
  // is inherited.

  if (typeof HTMLTemplateElement === 'function') {
    var template = document.createElement('template');
    if (template.content && template.content.ownerDocument) {
      document = template.content.ownerDocument;
    }
  }

  var trustedTypesPolicy = _createTrustedTypesPolicy(TrustedTypes, originalDocument);
  var emptyHTML = trustedTypesPolicy ? trustedTypesPolicy.createHTML('') : '';

  var _document = document,
      implementation = _document.implementation,
      createNodeIterator = _document.createNodeIterator,
      getElementsByTagName = _document.getElementsByTagName,
      createDocumentFragment = _document.createDocumentFragment;
  var importNode = originalDocument.importNode;


  var hooks = {};

  /**
   * Expose whether this browser supports running the full DOMPurify.
   */
  DOMPurify.isSupported = implementation && typeof implementation.createHTMLDocument !== 'undefined' && document.documentMode !== 9;

  var MUSTACHE_EXPR$$1 = MUSTACHE_EXPR,
      ERB_EXPR$$1 = ERB_EXPR,
      DATA_ATTR$$1 = DATA_ATTR,
      ARIA_ATTR$$1 = ARIA_ATTR,
      IS_SCRIPT_OR_DATA$$1 = IS_SCRIPT_OR_DATA,
      ATTR_WHITESPACE$$1 = ATTR_WHITESPACE;
  var IS_ALLOWED_URI$$1 = IS_ALLOWED_URI;

  /**
   * We consider the elements and attributes below to be safe. Ideally
   * don't add any new ones but feel free to remove unwanted ones.
   */

  /* allowed element names */

  var ALLOWED_TAGS = null;
  var DEFAULT_ALLOWED_TAGS = addToSet({}, [].concat(_toConsumableArray(html), _toConsumableArray(svg), _toConsumableArray(svgFilters), _toConsumableArray(mathMl), _toConsumableArray(text)));

  /* Allowed attribute names */
  var ALLOWED_ATTR = null;
  var DEFAULT_ALLOWED_ATTR = addToSet({}, [].concat(_toConsumableArray(html$1), _toConsumableArray(svg$1), _toConsumableArray(mathMl$1), _toConsumableArray(xml)));

  /* Explicitly forbidden tags (overrides ALLOWED_TAGS/ADD_TAGS) */
  var FORBID_TAGS = null;

  /* Explicitly forbidden attributes (overrides ALLOWED_ATTR/ADD_ATTR) */
  var FORBID_ATTR = null;

  /* Decide if ARIA attributes are okay */
  var ALLOW_ARIA_ATTR = true;

  /* Decide if custom data attributes are okay */
  var ALLOW_DATA_ATTR = true;

  /* Decide if unknown protocols are okay */
  var ALLOW_UNKNOWN_PROTOCOLS = false;

  /* Output should be safe for jQuery's $() factory? */
  var SAFE_FOR_JQUERY = false;

  /* Output should be safe for common template engines.
   * This means, DOMPurify removes data attributes, mustaches and ERB
   */
  var SAFE_FOR_TEMPLATES = false;

  /* Decide if document with <html>... should be returned */
  var WHOLE_DOCUMENT = false;

  /* Track whether config is already set on this instance of DOMPurify. */
  var SET_CONFIG = false;

  /* Decide if all elements (e.g. style, script) must be children of
   * document.body. By default, browsers might move them to document.head */
  var FORCE_BODY = false;

  /* Decide if a DOM `HTMLBodyElement` should be returned, instead of a html
   * string (or a TrustedHTML object if Trusted Types are supported).
   * If `WHOLE_DOCUMENT` is enabled a `HTMLHtmlElement` will be returned instead
   */
  var RETURN_DOM = false;

  /* Decide if a DOM `DocumentFragment` should be returned, instead of a html
   * string  (or a TrustedHTML object if Trusted Types are supported) */
  var RETURN_DOM_FRAGMENT = false;

  /* If `RETURN_DOM` or `RETURN_DOM_FRAGMENT` is enabled, decide if the returned DOM
   * `Node` is imported into the current `Document`. If this flag is not enabled the
   * `Node` will belong (its ownerDocument) to a fresh `HTMLDocument`, created by
   * DOMPurify. */
  var RETURN_DOM_IMPORT = false;

  /* Try to return a Trusted Type object instead of a string, retrun a string in
   * case Trusted Types are not supported  */
  var RETURN_TRUSTED_TYPE = false;

  /* Output should be free from DOM clobbering attacks? */
  var SANITIZE_DOM = true;

  /* Keep element content when removing element? */
  var KEEP_CONTENT = true;

  /* If a `Node` is passed to sanitize(), then performs sanitization in-place instead
   * of importing it into a new Document and returning a sanitized copy */
  var IN_PLACE = false;

  /* Allow usage of profiles like html, svg and mathMl */
  var USE_PROFILES = {};

  /* Tags to ignore content of when KEEP_CONTENT is true */
  var FORBID_CONTENTS = addToSet({}, ['annotation-xml', 'audio', 'colgroup', 'desc', 'foreignobject', 'head', 'iframe', 'math', 'mi', 'mn', 'mo', 'ms', 'mtext', 'noembed', 'noframes', 'plaintext', 'script', 'style', 'svg', 'template', 'thead', 'title', 'video', 'xmp']);

  /* Tags that are safe for data: URIs */
  var DATA_URI_TAGS = addToSet({}, ['audio', 'video', 'img', 'source', 'image']);

  /* Attributes safe for values like "javascript:" */
  var URI_SAFE_ATTRIBUTES = null;
  var DEFAULT_URI_SAFE_ATTRIBUTES = addToSet({}, ['alt', 'class', 'for', 'id', 'label', 'name', 'pattern', 'placeholder', 'summary', 'title', 'value', 'style', 'xmlns']);

  /* Keep a reference to config to pass to hooks */
  var CONFIG = null;

  /* Ideally, do not touch anything below this line */
  /* ______________________________________________ */

  var formElement = document.createElement('form');

  /**
   * _parseConfig
   *
   * @param  {Object} cfg optional config literal
   */
  // eslint-disable-next-line complexity
  var _parseConfig = function _parseConfig(cfg) {
    if (CONFIG && CONFIG === cfg) {
      return;
    }

    /* Shield configuration object from tampering */
    if (!cfg || (typeof cfg === 'undefined' ? 'undefined' : _typeof(cfg)) !== 'object') {
      cfg = {};
    }

    /* Set configuration parameters */
    ALLOWED_TAGS = 'ALLOWED_TAGS' in cfg ? addToSet({}, cfg.ALLOWED_TAGS) : DEFAULT_ALLOWED_TAGS;
    ALLOWED_ATTR = 'ALLOWED_ATTR' in cfg ? addToSet({}, cfg.ALLOWED_ATTR) : DEFAULT_ALLOWED_ATTR;
    URI_SAFE_ATTRIBUTES = 'ADD_URI_SAFE_ATTR' in cfg ? addToSet(clone(DEFAULT_URI_SAFE_ATTRIBUTES), cfg.ADD_URI_SAFE_ATTR) : DEFAULT_URI_SAFE_ATTRIBUTES;
    FORBID_TAGS = 'FORBID_TAGS' in cfg ? addToSet({}, cfg.FORBID_TAGS) : {};
    FORBID_ATTR = 'FORBID_ATTR' in cfg ? addToSet({}, cfg.FORBID_ATTR) : {};
    USE_PROFILES = 'USE_PROFILES' in cfg ? cfg.USE_PROFILES : false;
    ALLOW_ARIA_ATTR = cfg.ALLOW_ARIA_ATTR !== false; // Default true
    ALLOW_DATA_ATTR = cfg.ALLOW_DATA_ATTR !== false; // Default true
    ALLOW_UNKNOWN_PROTOCOLS = cfg.ALLOW_UNKNOWN_PROTOCOLS || false; // Default false
    SAFE_FOR_JQUERY = cfg.SAFE_FOR_JQUERY || false; // Default false
    SAFE_FOR_TEMPLATES = cfg.SAFE_FOR_TEMPLATES || false; // Default false
    WHOLE_DOCUMENT = cfg.WHOLE_DOCUMENT || false; // Default false
    RETURN_DOM = cfg.RETURN_DOM || false; // Default false
    RETURN_DOM_FRAGMENT = cfg.RETURN_DOM_FRAGMENT || false; // Default false
    RETURN_DOM_IMPORT = cfg.RETURN_DOM_IMPORT || false; // Default false
    RETURN_TRUSTED_TYPE = cfg.RETURN_TRUSTED_TYPE || false; // Default false
    FORCE_BODY = cfg.FORCE_BODY || false; // Default false
    SANITIZE_DOM = cfg.SANITIZE_DOM !== false; // Default true
    KEEP_CONTENT = cfg.KEEP_CONTENT !== false; // Default true
    IN_PLACE = cfg.IN_PLACE || false; // Default false

    IS_ALLOWED_URI$$1 = cfg.ALLOWED_URI_REGEXP || IS_ALLOWED_URI$$1;

    if (SAFE_FOR_TEMPLATES) {
      ALLOW_DATA_ATTR = false;
    }

    if (RETURN_DOM_FRAGMENT) {
      RETURN_DOM = true;
    }

    /* Parse profile info */
    if (USE_PROFILES) {
      ALLOWED_TAGS = addToSet({}, [].concat(_toConsumableArray(text)));
      ALLOWED_ATTR = [];
      if (USE_PROFILES.html === true) {
        addToSet(ALLOWED_TAGS, html);
        addToSet(ALLOWED_ATTR, html$1);
      }

      if (USE_PROFILES.svg === true) {
        addToSet(ALLOWED_TAGS, svg);
        addToSet(ALLOWED_ATTR, svg$1);
        addToSet(ALLOWED_ATTR, xml);
      }

      if (USE_PROFILES.svgFilters === true) {
        addToSet(ALLOWED_TAGS, svgFilters);
        addToSet(ALLOWED_ATTR, svg$1);
        addToSet(ALLOWED_ATTR, xml);
      }

      if (USE_PROFILES.mathMl === true) {
        addToSet(ALLOWED_TAGS, mathMl);
        addToSet(ALLOWED_ATTR, mathMl$1);
        addToSet(ALLOWED_ATTR, xml);
      }
    }

    /* Merge configuration parameters */
    if (cfg.ADD_TAGS) {
      if (ALLOWED_TAGS === DEFAULT_ALLOWED_TAGS) {
        ALLOWED_TAGS = clone(ALLOWED_TAGS);
      }

      addToSet(ALLOWED_TAGS, cfg.ADD_TAGS);
    }

    if (cfg.ADD_ATTR) {
      if (ALLOWED_ATTR === DEFAULT_ALLOWED_ATTR) {
        ALLOWED_ATTR = clone(ALLOWED_ATTR);
      }

      addToSet(ALLOWED_ATTR, cfg.ADD_ATTR);
    }

    if (cfg.ADD_URI_SAFE_ATTR) {
      addToSet(URI_SAFE_ATTRIBUTES, cfg.ADD_URI_SAFE_ATTR);
    }

    /* Add #text in case KEEP_CONTENT is set to true */
    if (KEEP_CONTENT) {
      ALLOWED_TAGS['#text'] = true;
    }

    /* Add html, head and body to ALLOWED_TAGS in case WHOLE_DOCUMENT is true */
    if (WHOLE_DOCUMENT) {
      addToSet(ALLOWED_TAGS, ['html', 'head', 'body']);
    }

    /* Add tbody to ALLOWED_TAGS in case tables are permitted, see #286, #365 */
    if (ALLOWED_TAGS.table) {
      addToSet(ALLOWED_TAGS, ['tbody']);
      delete FORBID_TAGS.tbody;
    }

    // Prevent further manipulation of configuration.
    // Not available in IE8, Safari 5, etc.
    if (freeze) {
      freeze(cfg);
    }

    CONFIG = cfg;
  };

  /**
   * _forceRemove
   *
   * @param  {Node} node a DOM node
   */
  var _forceRemove = function _forceRemove(node) {
    DOMPurify.removed.push({ element: node });
    try {
      node.parentNode.removeChild(node);
    } catch (error) {
      node.outerHTML = emptyHTML;
    }
  };

  /**
   * _removeAttribute
   *
   * @param  {String} name an Attribute name
   * @param  {Node} node a DOM node
   */
  var _removeAttribute = function _removeAttribute(name, node) {
    try {
      DOMPurify.removed.push({
        attribute: node.getAttributeNode(name),
        from: node
      });
    } catch (error) {
      DOMPurify.removed.push({
        attribute: null,
        from: node
      });
    }

    node.removeAttribute(name);
  };

  /**
   * _initDocument
   *
   * @param  {String} dirty a string of dirty markup
   * @return {Document} a DOM, filled with the dirty markup
   */
  var _initDocument = function _initDocument(dirty) {
    /* Create a HTML document */
    var doc = void 0;
    var leadingWhitespace = void 0;

    if (FORCE_BODY) {
      dirty = '<remove></remove>' + dirty;
    } else {
      /* If FORCE_BODY isn't used, leading whitespace needs to be preserved manually */
      var matches = dirty.match(/^[\s]+/);
      leadingWhitespace = matches && matches[0];
      if (leadingWhitespace) {
        dirty = dirty.slice(leadingWhitespace.length);
      }
    }

    /* Use DOMParser to workaround Firefox bug (see comment below) */
    if (useDOMParser) {
      try {
        doc = new DOMParser().parseFromString(dirty, 'text/html');
      } catch (error) {}
    }

    /* Remove title to fix a mXSS bug in older MS Edge */
    if (removeTitle) {
      addToSet(FORBID_TAGS, ['title']);
    }

    /* Otherwise use createHTMLDocument, because DOMParser is unsafe in
    Safari (see comment below) */
    if (!doc || !doc.documentElement) {
      doc = implementation.createHTMLDocument('');
      var _doc = doc,
          body = _doc.body;

      body.parentNode.removeChild(body.parentNode.firstElementChild);
      body.outerHTML = trustedTypesPolicy ? trustedTypesPolicy.createHTML(dirty) : dirty;
    }

    if (dirty && leadingWhitespace) {
      doc.body.insertBefore(document.createTextNode(leadingWhitespace), doc.body.childNodes[0] || null);
    }

    /* Work on whole document or just its body */
    return getElementsByTagName.call(doc, WHOLE_DOCUMENT ? 'html' : 'body')[0];
  };

  // Firefox uses a different parser for innerHTML rather than
  // DOMParser (see https://bugzilla.mozilla.org/show_bug.cgi?id=1205631)
  // which means that you *must* use DOMParser, otherwise the output may
  // not be safe if used in a document.write context later.
  //
  // So we feature detect the Firefox bug and use the DOMParser if necessary.
  //
  // Chrome 77 and other versions ship an mXSS bug that caused a bypass to
  // happen. We now check for the mXSS trigger and react accordingly.
  if (DOMPurify.isSupported) {
    (function () {
      try {
        var doc = _initDocument('<svg><p><textarea><img src="</textarea><img src=x abc=1//">');
        if (doc.querySelector('svg img')) {
          useDOMParser = true;
        }
      } catch (error) {}
    })();

    (function () {
      try {
        var doc = _initDocument('<x/><title>&lt;/title&gt;&lt;img&gt;');
        if (/<\/title/.test(doc.querySelector('title').innerHTML)) {
          removeTitle = true;
        }
      } catch (error) {}
    })();
  }

  /**
   * _createIterator
   *
   * @param  {Document} root document/fragment to create iterator for
   * @return {Iterator} iterator instance
   */
  var _createIterator = function _createIterator(root) {
    return createNodeIterator.call(root.ownerDocument || root, root, NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_COMMENT | NodeFilter.SHOW_TEXT, function () {
      return NodeFilter.FILTER_ACCEPT;
    }, false);
  };

  /**
   * _isClobbered
   *
   * @param  {Node} elm element to check for clobbering attacks
   * @return {Boolean} true if clobbered, false if safe
   */
  var _isClobbered = function _isClobbered(elm) {
    if (elm instanceof Text || elm instanceof Comment) {
      return false;
    }

    if (typeof elm.nodeName !== 'string' || typeof elm.textContent !== 'string' || typeof elm.removeChild !== 'function' || !(elm.attributes instanceof NamedNodeMap) || typeof elm.removeAttribute !== 'function' || typeof elm.setAttribute !== 'function' || typeof elm.namespaceURI !== 'string') {
      return true;
    }

    return false;
  };

  /**
   * _isNode
   *
   * @param  {Node} obj object to check whether it's a DOM node
   * @return {Boolean} true is object is a DOM node
   */
  var _isNode = function _isNode(obj) {
    return (typeof Node === 'undefined' ? 'undefined' : _typeof(Node)) === 'object' ? obj instanceof Node : obj && (typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) === 'object' && typeof obj.nodeType === 'number' && typeof obj.nodeName === 'string';
  };

  /**
   * _executeHook
   * Execute user configurable hooks
   *
   * @param  {String} entryPoint  Name of the hook's entry point
   * @param  {Node} currentNode node to work on with the hook
   * @param  {Object} data additional hook parameters
   */
  var _executeHook = function _executeHook(entryPoint, currentNode, data) {
    if (!hooks[entryPoint]) {
      return;
    }

    hooks[entryPoint].forEach(function (hook) {
      hook.call(DOMPurify, currentNode, data, CONFIG);
    });
  };

  /**
   * _sanitizeElements
   *
   * @protect nodeName
   * @protect textContent
   * @protect removeChild
   *
   * @param   {Node} currentNode to check for permission to exist
   * @return  {Boolean} true if node was killed, false if left alive
   */
  // eslint-disable-next-line complexity
  var _sanitizeElements = function _sanitizeElements(currentNode) {
    var content = void 0;

    /* Execute a hook if present */
    _executeHook('beforeSanitizeElements', currentNode, null);

    /* Check if element is clobbered or can clobber */
    if (_isClobbered(currentNode)) {
      _forceRemove(currentNode);
      return true;
    }

    /* Now let's check the element's type and name */
    var tagName = currentNode.nodeName.toLowerCase();

    /* Execute a hook if present */
    _executeHook('uponSanitizeElement', currentNode, {
      tagName: tagName,
      allowedTags: ALLOWED_TAGS
    });

    /* Take care of an mXSS pattern using p, br inside svg, math */
    if ((tagName === 'svg' || tagName === 'math') && currentNode.querySelectorAll('p, br').length !== 0) {
      _forceRemove(currentNode);
      return true;
    }

    /* Remove element if anything forbids its presence */
    if (!ALLOWED_TAGS[tagName] || FORBID_TAGS[tagName]) {
      /* Keep content except for black-listed elements */
      if (KEEP_CONTENT && !FORBID_CONTENTS[tagName] && typeof currentNode.insertAdjacentHTML === 'function') {
        try {
          var htmlToInsert = currentNode.innerHTML;
          currentNode.insertAdjacentHTML('AfterEnd', trustedTypesPolicy ? trustedTypesPolicy.createHTML(htmlToInsert) : htmlToInsert);
        } catch (error) {}
      }

      _forceRemove(currentNode);
      return true;
    }

    /* Remove in case a noscript/noembed XSS is suspected */
    if (tagName === 'noscript' && /<\/noscript/i.test(currentNode.innerHTML)) {
      _forceRemove(currentNode);
      return true;
    }

    if (tagName === 'noembed' && /<\/noembed/i.test(currentNode.innerHTML)) {
      _forceRemove(currentNode);
      return true;
    }

    /* Convert markup to cover jQuery behavior */
    if (SAFE_FOR_JQUERY && !currentNode.firstElementChild && (!currentNode.content || !currentNode.content.firstElementChild) && /</g.test(currentNode.textContent)) {
      DOMPurify.removed.push({ element: currentNode.cloneNode() });
      if (currentNode.innerHTML) {
        currentNode.innerHTML = currentNode.innerHTML.replace(/</g, '&lt;');
      } else {
        currentNode.innerHTML = currentNode.textContent.replace(/</g, '&lt;');
      }
    }

    /* Sanitize element content to be template-safe */
    if (SAFE_FOR_TEMPLATES && currentNode.nodeType === 3) {
      /* Get the element's text content */
      content = currentNode.textContent;
      content = content.replace(MUSTACHE_EXPR$$1, ' ');
      content = content.replace(ERB_EXPR$$1, ' ');
      if (currentNode.textContent !== content) {
        DOMPurify.removed.push({ element: currentNode.cloneNode() });
        currentNode.textContent = content;
      }
    }

    /* Execute a hook if present */
    _executeHook('afterSanitizeElements', currentNode, null);

    return false;
  };

  /**
   * _isValidAttribute
   *
   * @param  {string} lcTag Lowercase tag name of containing element.
   * @param  {string} lcName Lowercase attribute name.
   * @param  {string} value Attribute value.
   * @return {Boolean} Returns true if `value` is valid, otherwise false.
   */
  // eslint-disable-next-line complexity
  var _isValidAttribute = function _isValidAttribute(lcTag, lcName, value) {
    /* Make sure attribute cannot clobber */
    if (SANITIZE_DOM && (lcName === 'id' || lcName === 'name') && (value in document || value in formElement)) {
      return false;
    }

    /* Allow valid data-* attributes: At least one character after "-"
        (https://html.spec.whatwg.org/multipage/dom.html#embedding-custom-non-visible-data-with-the-data-*-attributes)
        XML-compatible (https://html.spec.whatwg.org/multipage/infrastructure.html#xml-compatible and http://www.w3.org/TR/xml/#d0e804)
        We don't need to check the value; it's always URI safe. */
    if (ALLOW_DATA_ATTR && DATA_ATTR$$1.test(lcName)) {
      // This attribute is safe
    } else if (ALLOW_ARIA_ATTR && ARIA_ATTR$$1.test(lcName)) {
      // This attribute is safe
      /* Otherwise, check the name is permitted */
    } else if (!ALLOWED_ATTR[lcName] || FORBID_ATTR[lcName]) {
      return false;

      /* Check value is safe. First, is attr inert? If so, is safe */
    } else if (URI_SAFE_ATTRIBUTES[lcName]) {
      // This attribute is safe
      /* Check no script, data or unknown possibly unsafe URI
        unless we know URI values are safe for that attribute */
    } else if (IS_ALLOWED_URI$$1.test(value.replace(ATTR_WHITESPACE$$1, ''))) {
      // This attribute is safe
      /* Keep image data URIs alive if src/xlink:href is allowed */
      /* Further prevent gadget XSS for dynamically built script tags */
    } else if ((lcName === 'src' || lcName === 'xlink:href' || lcName === 'href') && lcTag !== 'script' && value.indexOf('data:') === 0 && DATA_URI_TAGS[lcTag]) {
      // This attribute is safe
      /* Allow unknown protocols: This provides support for links that
        are handled by protocol handlers which may be unknown ahead of
        time, e.g. fb:, spotify: */
    } else if (ALLOW_UNKNOWN_PROTOCOLS && !IS_SCRIPT_OR_DATA$$1.test(value.replace(ATTR_WHITESPACE$$1, ''))) {
      // This attribute is safe
      /* Check for binary attributes */
      // eslint-disable-next-line no-negated-condition
    } else if (!value) {
      // Binary attributes are safe at this point
      /* Anything else, presume unsafe, do not add it back */
    } else {
      return false;
    }

    return true;
  };

  /**
   * _sanitizeAttributes
   *
   * @protect attributes
   * @protect nodeName
   * @protect removeAttribute
   * @protect setAttribute
   *
   * @param  {Node} currentNode to sanitize
   */
  // eslint-disable-next-line complexity
  var _sanitizeAttributes = function _sanitizeAttributes(currentNode) {
    var attr = void 0;
    var value = void 0;
    var lcName = void 0;
    var idAttr = void 0;
    var l = void 0;
    /* Execute a hook if present */
    _executeHook('beforeSanitizeAttributes', currentNode, null);

    var attributes = currentNode.attributes;

    /* Check if we have attributes; if not we might have a text node */

    if (!attributes) {
      return;
    }

    var hookEvent = {
      attrName: '',
      attrValue: '',
      keepAttr: true,
      allowedAttributes: ALLOWED_ATTR
    };
    l = attributes.length;

    /* Go backwards over all attributes; safely remove bad ones */
    while (l--) {
      attr = attributes[l];
      var _attr = attr,
          name = _attr.name,
          namespaceURI = _attr.namespaceURI;

      value = attr.value.trim();
      lcName = name.toLowerCase();

      /* Execute a hook if present */
      hookEvent.attrName = lcName;
      hookEvent.attrValue = value;
      hookEvent.keepAttr = true;
      _executeHook('uponSanitizeAttribute', currentNode, hookEvent);
      value = hookEvent.attrValue;

      /* Remove attribute */
      // Safari (iOS + Mac), last tested v8.0.5, crashes if you try to
      // remove a "name" attribute from an <img> tag that has an "id"
      // attribute at the time.
      if (lcName === 'name' && currentNode.nodeName === 'IMG' && attributes.id) {
        idAttr = attributes.id;
        attributes = apply(arraySlice, attributes, []);
        _removeAttribute('id', currentNode);
        _removeAttribute(name, currentNode);
        if (attributes.indexOf(idAttr) > l) {
          currentNode.setAttribute('id', idAttr.value);
        }
      } else if (
      // This works around a bug in Safari, where input[type=file]
      // cannot be dynamically set after type has been removed
      currentNode.nodeName === 'INPUT' && lcName === 'type' && value === 'file' && hookEvent.keepAttr && (ALLOWED_ATTR[lcName] || !FORBID_ATTR[lcName])) {
        continue;
      } else {
        // This avoids a crash in Safari v9.0 with double-ids.
        // The trick is to first set the id to be empty and then to
        // remove the attribute
        if (name === 'id') {
          currentNode.setAttribute(name, '');
        }

        _removeAttribute(name, currentNode);
      }

      /* Did the hooks approve of the attribute? */
      if (!hookEvent.keepAttr) {
        continue;
      }

      /* Take care of an mXSS pattern using namespace switches */
      if (/svg|math/i.test(currentNode.namespaceURI) && new RegExp('</(' + Object.keys(FORBID_CONTENTS).join('|') + ')', 'i').test(value)) {
        _removeAttribute(name, currentNode);
        continue;
      }

      /* Sanitize attribute content to be template-safe */
      if (SAFE_FOR_TEMPLATES) {
        value = value.replace(MUSTACHE_EXPR$$1, ' ');
        value = value.replace(ERB_EXPR$$1, ' ');
      }

      /* Is `value` valid for this attribute? */
      var lcTag = currentNode.nodeName.toLowerCase();
      if (!_isValidAttribute(lcTag, lcName, value)) {
        continue;
      }

      /* Handle invalid data-* attribute set by try-catching it */
      try {
        if (namespaceURI) {
          currentNode.setAttributeNS(namespaceURI, name, value);
        } else {
          /* Fallback to setAttribute() for browser-unrecognized namespaces e.g. "x-schema". */
          currentNode.setAttribute(name, value);
        }

        DOMPurify.removed.pop();
      } catch (error) {}
    }

    /* Execute a hook if present */
    _executeHook('afterSanitizeAttributes', currentNode, null);
  };

  /**
   * _sanitizeShadowDOM
   *
   * @param  {DocumentFragment} fragment to iterate over recursively
   */
  var _sanitizeShadowDOM = function _sanitizeShadowDOM(fragment) {
    var shadowNode = void 0;
    var shadowIterator = _createIterator(fragment);

    /* Execute a hook if present */
    _executeHook('beforeSanitizeShadowDOM', fragment, null);

    while (shadowNode = shadowIterator.nextNode()) {
      /* Execute a hook if present */
      _executeHook('uponSanitizeShadowNode', shadowNode, null);

      /* Sanitize tags and elements */
      if (_sanitizeElements(shadowNode)) {
        continue;
      }

      /* Deep shadow DOM detected */
      if (shadowNode.content instanceof DocumentFragment) {
        _sanitizeShadowDOM(shadowNode.content);
      }

      /* Check attributes, sanitize if necessary */
      _sanitizeAttributes(shadowNode);
    }

    /* Execute a hook if present */
    _executeHook('afterSanitizeShadowDOM', fragment, null);
  };

  /**
   * Sanitize
   * Public method providing core sanitation functionality
   *
   * @param {String|Node} dirty string or DOM node
   * @param {Object} configuration object
   */
  // eslint-disable-next-line complexity
  DOMPurify.sanitize = function (dirty, cfg) {
    var body = void 0;
    var importedNode = void 0;
    var currentNode = void 0;
    var oldNode = void 0;
    var returnNode = void 0;
    /* Make sure we have a string to sanitize.
      DO NOT return early, as this will return the wrong type if
      the user has requested a DOM object rather than a string */
    if (!dirty) {
      dirty = '<!-->';
    }

    /* Stringify, in case dirty is an object */
    if (typeof dirty !== 'string' && !_isNode(dirty)) {
      // eslint-disable-next-line no-negated-condition
      if (typeof dirty.toString !== 'function') {
        throw new TypeError('toString is not a function');
      } else {
        dirty = dirty.toString();
        if (typeof dirty !== 'string') {
          throw new TypeError('dirty is not a string, aborting');
        }
      }
    }

    /* Check we can run. Otherwise fall back or ignore */
    if (!DOMPurify.isSupported) {
      if (_typeof(window.toStaticHTML) === 'object' || typeof window.toStaticHTML === 'function') {
        if (typeof dirty === 'string') {
          return window.toStaticHTML(dirty);
        }

        if (_isNode(dirty)) {
          return window.toStaticHTML(dirty.outerHTML);
        }
      }

      return dirty;
    }

    /* Assign config vars */
    if (!SET_CONFIG) {
      _parseConfig(cfg);
    }

    /* Clean up removed elements */
    DOMPurify.removed = [];

    if (IN_PLACE) {
      /* No special handling necessary for in-place sanitization */
    } else if (dirty instanceof Node) {
      /* If dirty is a DOM element, append to an empty document to avoid
         elements being stripped by the parser */
      body = _initDocument('<!-->');
      importedNode = body.ownerDocument.importNode(dirty, true);
      if (importedNode.nodeType === 1 && importedNode.nodeName === 'BODY') {
        /* Node is already a body, use as is */
        body = importedNode;
      } else if (importedNode.nodeName === 'HTML') {
        body = importedNode;
      } else {
        // eslint-disable-next-line unicorn/prefer-node-append
        body.appendChild(importedNode);
      }
    } else {
      /* Exit directly if we have nothing to do */
      if (!RETURN_DOM && !SAFE_FOR_TEMPLATES && !WHOLE_DOCUMENT && RETURN_TRUSTED_TYPE && dirty.indexOf('<') === -1) {
        return trustedTypesPolicy ? trustedTypesPolicy.createHTML(dirty) : dirty;
      }

      /* Initialize the document to work on */
      body = _initDocument(dirty);

      /* Check we have a DOM node from the data */
      if (!body) {
        return RETURN_DOM ? null : emptyHTML;
      }
    }

    /* Remove first element node (ours) if FORCE_BODY is set */
    if (body && FORCE_BODY) {
      _forceRemove(body.firstChild);
    }

    /* Get node iterator */
    var nodeIterator = _createIterator(IN_PLACE ? dirty : body);

    /* Now start iterating over the created document */
    while (currentNode = nodeIterator.nextNode()) {
      /* Fix IE's strange behavior with manipulated textNodes #89 */
      if (currentNode.nodeType === 3 && currentNode === oldNode) {
        continue;
      }

      /* Sanitize tags and elements */
      if (_sanitizeElements(currentNode)) {
        continue;
      }

      /* Shadow DOM detected, sanitize it */
      if (currentNode.content instanceof DocumentFragment) {
        _sanitizeShadowDOM(currentNode.content);
      }

      /* Check attributes, sanitize if necessary */
      _sanitizeAttributes(currentNode);

      oldNode = currentNode;
    }

    oldNode = null;

    /* If we sanitized `dirty` in-place, return it. */
    if (IN_PLACE) {
      return dirty;
    }

    /* Return sanitized string or DOM */
    if (RETURN_DOM) {
      if (RETURN_DOM_FRAGMENT) {
        returnNode = createDocumentFragment.call(body.ownerDocument);

        while (body.firstChild) {
          // eslint-disable-next-line unicorn/prefer-node-append
          returnNode.appendChild(body.firstChild);
        }
      } else {
        returnNode = body;
      }

      if (RETURN_DOM_IMPORT) {
        /* AdoptNode() is not used because internal state is not reset
               (e.g. the past names map of a HTMLFormElement), this is safe
               in theory but we would rather not risk another attack vector.
               The state that is cloned by importNode() is explicitly defined
               by the specs. */
        returnNode = importNode.call(originalDocument, returnNode, true);
      }

      return returnNode;
    }

    var serializedHTML = WHOLE_DOCUMENT ? body.outerHTML : body.innerHTML;

    /* Sanitize final string template-safe */
    if (SAFE_FOR_TEMPLATES) {
      serializedHTML = serializedHTML.replace(MUSTACHE_EXPR$$1, ' ');
      serializedHTML = serializedHTML.replace(ERB_EXPR$$1, ' ');
    }

    return trustedTypesPolicy && RETURN_TRUSTED_TYPE ? trustedTypesPolicy.createHTML(serializedHTML) : serializedHTML;
  };

  /**
   * Public method to set the configuration once
   * setConfig
   *
   * @param {Object} cfg configuration object
   */
  DOMPurify.setConfig = function (cfg) {
    _parseConfig(cfg);
    SET_CONFIG = true;
  };

  /**
   * Public method to remove the configuration
   * clearConfig
   *
   */
  DOMPurify.clearConfig = function () {
    CONFIG = null;
    SET_CONFIG = false;
  };

  /**
   * Public method to check if an attribute value is valid.
   * Uses last set config, if any. Otherwise, uses config defaults.
   * isValidAttribute
   *
   * @param  {string} tag Tag name of containing element.
   * @param  {string} attr Attribute name.
   * @param  {string} value Attribute value.
   * @return {Boolean} Returns true if `value` is valid. Otherwise, returns false.
   */
  DOMPurify.isValidAttribute = function (tag, attr, value) {
    /* Initialize shared config vars if necessary. */
    if (!CONFIG) {
      _parseConfig({});
    }

    var lcTag = tag.toLowerCase();
    var lcName = attr.toLowerCase();
    return _isValidAttribute(lcTag, lcName, value);
  };

  /**
   * AddHook
   * Public method to add DOMPurify hooks
   *
   * @param {String} entryPoint entry point for the hook to add
   * @param {Function} hookFunction function to execute
   */
  DOMPurify.addHook = function (entryPoint, hookFunction) {
    if (typeof hookFunction !== 'function') {
      return;
    }

    hooks[entryPoint] = hooks[entryPoint] || [];
    hooks[entryPoint].push(hookFunction);
  };

  /**
   * RemoveHook
   * Public method to remove a DOMPurify hook at a given entryPoint
   * (pops it from the stack of hooks if more are present)
   *
   * @param {String} entryPoint entry point for the hook to remove
   */
  DOMPurify.removeHook = function (entryPoint) {
    if (hooks[entryPoint]) {
      hooks[entryPoint].pop();
    }
  };

  /**
   * RemoveHooks
   * Public method to remove all DOMPurify hooks at a given entryPoint
   *
   * @param  {String} entryPoint entry point for the hooks to remove
   */
  DOMPurify.removeHooks = function (entryPoint) {
    if (hooks[entryPoint]) {
      hooks[entryPoint] = [];
    }
  };

  /**
   * RemoveAllHooks
   * Public method to remove all DOMPurify hooks
   *
   */
  DOMPurify.removeAllHooks = function () {
    hooks = {};
  };

  return DOMPurify;
}

var purify = createDOMPurify();

return purify;

})));


},{}],"node_modules/marked/lib/marked.js":[function(require,module,exports) {
var define;
var global = arguments[3];
/**
 * marked - a markdown parser
 * Copyright (c) 2011-2018, Christopher Jeffrey. (MIT Licensed)
 * https://github.com/markedjs/marked
 */
;

(function (root) {
  'use strict';
  /**
   * Block-Level Grammar
   */

  var block = {
    newline: /^\n+/,
    code: /^( {4}[^\n]+\n*)+/,
    fences: /^ {0,3}(`{3,}|~{3,})([^`~\n]*)\n(?:|([\s\S]*?)\n)(?: {0,3}\1[~`]* *(?:\n+|$)|$)/,
    hr: /^ {0,3}((?:- *){3,}|(?:_ *){3,}|(?:\* *){3,})(?:\n+|$)/,
    heading: /^ {0,3}(#{1,6}) +([^\n]*?)(?: +#+)? *(?:\n+|$)/,
    blockquote: /^( {0,3}> ?(paragraph|[^\n]*)(?:\n|$))+/,
    list: /^( {0,3})(bull) [\s\S]+?(?:hr|def|\n{2,}(?! )(?!\1bull )\n*|\s*$)/,
    html: '^ {0,3}(?:' // optional indentation
    + '<(script|pre|style)[\\s>][\\s\\S]*?(?:</\\1>[^\\n]*\\n+|$)' // (1)
    + '|comment[^\\n]*(\\n+|$)' // (2)
    + '|<\\?[\\s\\S]*?\\?>\\n*' // (3)
    + '|<![A-Z][\\s\\S]*?>\\n*' // (4)
    + '|<!\\[CDATA\\[[\\s\\S]*?\\]\\]>\\n*' // (5)
    + '|</?(tag)(?: +|\\n|/?>)[\\s\\S]*?(?:\\n{2,}|$)' // (6)
    + '|<(?!script|pre|style)([a-z][\\w-]*)(?:attribute)*? */?>(?=[ \\t]*(?:\\n|$))[\\s\\S]*?(?:\\n{2,}|$)' // (7) open tag
    + '|</(?!script|pre|style)[a-z][\\w-]*\\s*>(?=[ \\t]*(?:\\n|$))[\\s\\S]*?(?:\\n{2,}|$)' // (7) closing tag
    + ')',
    def: /^ {0,3}\[(label)\]: *\n? *<?([^\s>]+)>?(?:(?: +\n? *| *\n *)(title))? *(?:\n+|$)/,
    nptable: noop,
    table: noop,
    lheading: /^([^\n]+)\n {0,3}(=+|-+) *(?:\n+|$)/,
    // regex template, placeholders will be replaced according to different paragraph
    // interruption rules of commonmark and the original markdown spec:
    _paragraph: /^([^\n]+(?:\n(?!hr|heading|lheading|blockquote|fences|list|html)[^\n]+)*)/,
    text: /^[^\n]+/
  };
  block._label = /(?!\s*\])(?:\\[\[\]]|[^\[\]])+/;
  block._title = /(?:"(?:\\"?|[^"\\])*"|'[^'\n]*(?:\n[^'\n]+)*\n?'|\([^()]*\))/;
  block.def = edit(block.def).replace('label', block._label).replace('title', block._title).getRegex();
  block.bullet = /(?:[*+-]|\d{1,9}\.)/;
  block.item = /^( *)(bull) ?[^\n]*(?:\n(?!\1bull ?)[^\n]*)*/;
  block.item = edit(block.item, 'gm').replace(/bull/g, block.bullet).getRegex();
  block.list = edit(block.list).replace(/bull/g, block.bullet).replace('hr', '\\n+(?=\\1?(?:(?:- *){3,}|(?:_ *){3,}|(?:\\* *){3,})(?:\\n+|$))').replace('def', '\\n+(?=' + block.def.source + ')').getRegex();
  block._tag = 'address|article|aside|base|basefont|blockquote|body|caption' + '|center|col|colgroup|dd|details|dialog|dir|div|dl|dt|fieldset|figcaption' + '|figure|footer|form|frame|frameset|h[1-6]|head|header|hr|html|iframe' + '|legend|li|link|main|menu|menuitem|meta|nav|noframes|ol|optgroup|option' + '|p|param|section|source|summary|table|tbody|td|tfoot|th|thead|title|tr' + '|track|ul';
  block._comment = /<!--(?!-?>)[\s\S]*?-->/;
  block.html = edit(block.html, 'i').replace('comment', block._comment).replace('tag', block._tag).replace('attribute', / +[a-zA-Z:_][\w.:-]*(?: *= *"[^"\n]*"| *= *'[^'\n]*'| *= *[^\s"'=<>`]+)?/).getRegex();
  block.paragraph = edit(block._paragraph).replace('hr', block.hr).replace('heading', ' {0,3}#{1,6} +').replace('|lheading', '') // setex headings don't interrupt commonmark paragraphs
  .replace('blockquote', ' {0,3}>').replace('fences', ' {0,3}(?:`{3,}|~{3,})[^`\\n]*\\n').replace('list', ' {0,3}(?:[*+-]|1[.)]) ') // only lists starting from 1 can interrupt
  .replace('html', '</?(?:tag)(?: +|\\n|/?>)|<(?:script|pre|style|!--)').replace('tag', block._tag) // pars can be interrupted by type (6) html blocks
  .getRegex();
  block.blockquote = edit(block.blockquote).replace('paragraph', block.paragraph).getRegex();
  /**
   * Normal Block Grammar
   */

  block.normal = merge({}, block);
  /**
   * GFM Block Grammar
   */

  block.gfm = merge({}, block.normal, {
    nptable: /^ *([^|\n ].*\|.*)\n *([-:]+ *\|[-| :]*)(?:\n((?:.*[^>\n ].*(?:\n|$))*)\n*|$)/,
    table: /^ *\|(.+)\n *\|?( *[-:]+[-| :]*)(?:\n((?: *[^>\n ].*(?:\n|$))*)\n*|$)/
  });
  /**
   * Pedantic grammar (original John Gruber's loose markdown specification)
   */

  block.pedantic = merge({}, block.normal, {
    html: edit('^ *(?:comment *(?:\\n|\\s*$)' + '|<(tag)[\\s\\S]+?</\\1> *(?:\\n{2,}|\\s*$)' // closed tag
    + '|<tag(?:"[^"]*"|\'[^\']*\'|\\s[^\'"/>\\s]*)*?/?> *(?:\\n{2,}|\\s*$))').replace('comment', block._comment).replace(/tag/g, '(?!(?:' + 'a|em|strong|small|s|cite|q|dfn|abbr|data|time|code|var|samp|kbd|sub' + '|sup|i|b|u|mark|ruby|rt|rp|bdi|bdo|span|br|wbr|ins|del|img)' + '\\b)\\w+(?!:|[^\\w\\s@]*@)\\b').getRegex(),
    def: /^ *\[([^\]]+)\]: *<?([^\s>]+)>?(?: +(["(][^\n]+[")]))? *(?:\n+|$)/,
    heading: /^ *(#{1,6}) *([^\n]+?) *(?:#+ *)?(?:\n+|$)/,
    fences: noop,
    // fences not supported
    paragraph: edit(block.normal._paragraph).replace('hr', block.hr).replace('heading', ' *#{1,6} *[^\n]').replace('lheading', block.lheading).replace('blockquote', ' {0,3}>').replace('|fences', '').replace('|list', '').replace('|html', '').getRegex()
  });
  /**
   * Block Lexer
   */

  function Lexer(options) {
    this.tokens = [];
    this.tokens.links = Object.create(null);
    this.options = options || marked.defaults;
    this.rules = block.normal;

    if (this.options.pedantic) {
      this.rules = block.pedantic;
    } else if (this.options.gfm) {
      this.rules = block.gfm;
    }
  }
  /**
   * Expose Block Rules
   */


  Lexer.rules = block;
  /**
   * Static Lex Method
   */

  Lexer.lex = function (src, options) {
    var lexer = new Lexer(options);
    return lexer.lex(src);
  };
  /**
   * Preprocessing
   */


  Lexer.prototype.lex = function (src) {
    src = src.replace(/\r\n|\r/g, '\n').replace(/\t/g, '    ').replace(/\u00a0/g, ' ').replace(/\u2424/g, '\n');
    return this.token(src, true);
  };
  /**
   * Lexing
   */


  Lexer.prototype.token = function (src, top) {
    src = src.replace(/^ +$/gm, '');
    var next, loose, cap, bull, b, item, listStart, listItems, t, space, i, tag, l, isordered, istask, ischecked;

    while (src) {
      // newline
      if (cap = this.rules.newline.exec(src)) {
        src = src.substring(cap[0].length);

        if (cap[0].length > 1) {
          this.tokens.push({
            type: 'space'
          });
        }
      } // code


      if (cap = this.rules.code.exec(src)) {
        var lastToken = this.tokens[this.tokens.length - 1];
        src = src.substring(cap[0].length); // An indented code block cannot interrupt a paragraph.

        if (lastToken && lastToken.type === 'paragraph') {
          lastToken.text += '\n' + cap[0].trimRight();
        } else {
          cap = cap[0].replace(/^ {4}/gm, '');
          this.tokens.push({
            type: 'code',
            codeBlockStyle: 'indented',
            text: !this.options.pedantic ? rtrim(cap, '\n') : cap
          });
        }

        continue;
      } // fences


      if (cap = this.rules.fences.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'code',
          lang: cap[2] ? cap[2].trim() : cap[2],
          text: cap[3] || ''
        });
        continue;
      } // heading


      if (cap = this.rules.heading.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'heading',
          depth: cap[1].length,
          text: cap[2]
        });
        continue;
      } // table no leading pipe (gfm)


      if (cap = this.rules.nptable.exec(src)) {
        item = {
          type: 'table',
          header: splitCells(cap[1].replace(/^ *| *\| *$/g, '')),
          align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
          cells: cap[3] ? cap[3].replace(/\n$/, '').split('\n') : []
        };

        if (item.header.length === item.align.length) {
          src = src.substring(cap[0].length);

          for (i = 0; i < item.align.length; i++) {
            if (/^ *-+: *$/.test(item.align[i])) {
              item.align[i] = 'right';
            } else if (/^ *:-+: *$/.test(item.align[i])) {
              item.align[i] = 'center';
            } else if (/^ *:-+ *$/.test(item.align[i])) {
              item.align[i] = 'left';
            } else {
              item.align[i] = null;
            }
          }

          for (i = 0; i < item.cells.length; i++) {
            item.cells[i] = splitCells(item.cells[i], item.header.length);
          }

          this.tokens.push(item);
          continue;
        }
      } // hr


      if (cap = this.rules.hr.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'hr'
        });
        continue;
      } // blockquote


      if (cap = this.rules.blockquote.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'blockquote_start'
        });
        cap = cap[0].replace(/^ *> ?/gm, ''); // Pass `top` to keep the current
        // "toplevel" state. This is exactly
        // how markdown.pl works.

        this.token(cap, top);
        this.tokens.push({
          type: 'blockquote_end'
        });
        continue;
      } // list


      if (cap = this.rules.list.exec(src)) {
        src = src.substring(cap[0].length);
        bull = cap[2];
        isordered = bull.length > 1;
        listStart = {
          type: 'list_start',
          ordered: isordered,
          start: isordered ? +bull : '',
          loose: false
        };
        this.tokens.push(listStart); // Get each top-level item.

        cap = cap[0].match(this.rules.item);
        listItems = [];
        next = false;
        l = cap.length;
        i = 0;

        for (; i < l; i++) {
          item = cap[i]; // Remove the list item's bullet
          // so it is seen as the next token.

          space = item.length;
          item = item.replace(/^ *([*+-]|\d+\.) */, ''); // Outdent whatever the
          // list item contains. Hacky.

          if (~item.indexOf('\n ')) {
            space -= item.length;
            item = !this.options.pedantic ? item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '') : item.replace(/^ {1,4}/gm, '');
          } // Determine whether the next list item belongs here.
          // Backpedal if it does not belong in this list.


          if (i !== l - 1) {
            b = block.bullet.exec(cap[i + 1])[0];

            if (bull.length > 1 ? b.length === 1 : b.length > 1 || this.options.smartLists && b !== bull) {
              src = cap.slice(i + 1).join('\n') + src;
              i = l - 1;
            }
          } // Determine whether item is loose or not.
          // Use: /(^|\n)(?! )[^\n]+\n\n(?!\s*$)/
          // for discount behavior.


          loose = next || /\n\n(?!\s*$)/.test(item);

          if (i !== l - 1) {
            next = item.charAt(item.length - 1) === '\n';
            if (!loose) loose = next;
          }

          if (loose) {
            listStart.loose = true;
          } // Check for task list items


          istask = /^\[[ xX]\] /.test(item);
          ischecked = undefined;

          if (istask) {
            ischecked = item[1] !== ' ';
            item = item.replace(/^\[[ xX]\] +/, '');
          }

          t = {
            type: 'list_item_start',
            task: istask,
            checked: ischecked,
            loose: loose
          };
          listItems.push(t);
          this.tokens.push(t); // Recurse.

          this.token(item, false);
          this.tokens.push({
            type: 'list_item_end'
          });
        }

        if (listStart.loose) {
          l = listItems.length;
          i = 0;

          for (; i < l; i++) {
            listItems[i].loose = true;
          }
        }

        this.tokens.push({
          type: 'list_end'
        });
        continue;
      } // html


      if (cap = this.rules.html.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: this.options.sanitize ? 'paragraph' : 'html',
          pre: !this.options.sanitizer && (cap[1] === 'pre' || cap[1] === 'script' || cap[1] === 'style'),
          text: this.options.sanitize ? this.options.sanitizer ? this.options.sanitizer(cap[0]) : escape(cap[0]) : cap[0]
        });
        continue;
      } // def


      if (top && (cap = this.rules.def.exec(src))) {
        src = src.substring(cap[0].length);
        if (cap[3]) cap[3] = cap[3].substring(1, cap[3].length - 1);
        tag = cap[1].toLowerCase().replace(/\s+/g, ' ');

        if (!this.tokens.links[tag]) {
          this.tokens.links[tag] = {
            href: cap[2],
            title: cap[3]
          };
        }

        continue;
      } // table (gfm)


      if (cap = this.rules.table.exec(src)) {
        item = {
          type: 'table',
          header: splitCells(cap[1].replace(/^ *| *\| *$/g, '')),
          align: cap[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
          cells: cap[3] ? cap[3].replace(/\n$/, '').split('\n') : []
        };

        if (item.header.length === item.align.length) {
          src = src.substring(cap[0].length);

          for (i = 0; i < item.align.length; i++) {
            if (/^ *-+: *$/.test(item.align[i])) {
              item.align[i] = 'right';
            } else if (/^ *:-+: *$/.test(item.align[i])) {
              item.align[i] = 'center';
            } else if (/^ *:-+ *$/.test(item.align[i])) {
              item.align[i] = 'left';
            } else {
              item.align[i] = null;
            }
          }

          for (i = 0; i < item.cells.length; i++) {
            item.cells[i] = splitCells(item.cells[i].replace(/^ *\| *| *\| *$/g, ''), item.header.length);
          }

          this.tokens.push(item);
          continue;
        }
      } // lheading


      if (cap = this.rules.lheading.exec(src)) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'heading',
          depth: cap[2].charAt(0) === '=' ? 1 : 2,
          text: cap[1]
        });
        continue;
      } // top-level paragraph


      if (top && (cap = this.rules.paragraph.exec(src))) {
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'paragraph',
          text: cap[1].charAt(cap[1].length - 1) === '\n' ? cap[1].slice(0, -1) : cap[1]
        });
        continue;
      } // text


      if (cap = this.rules.text.exec(src)) {
        // Top-level should never reach here.
        src = src.substring(cap[0].length);
        this.tokens.push({
          type: 'text',
          text: cap[0]
        });
        continue;
      }

      if (src) {
        throw new Error('Infinite loop on byte: ' + src.charCodeAt(0));
      }
    }

    return this.tokens;
  };
  /**
   * Inline-Level Grammar
   */


  var inline = {
    escape: /^\\([!"#$%&'()*+,\-./:;<=>?@\[\]\\^_`{|}~])/,
    autolink: /^<(scheme:[^\s\x00-\x1f<>]*|email)>/,
    url: noop,
    tag: '^comment' + '|^</[a-zA-Z][\\w:-]*\\s*>' // self-closing tag
    + '|^<[a-zA-Z][\\w-]*(?:attribute)*?\\s*/?>' // open tag
    + '|^<\\?[\\s\\S]*?\\?>' // processing instruction, e.g. <?php ?>
    + '|^<![a-zA-Z]+\\s[\\s\\S]*?>' // declaration, e.g. <!DOCTYPE html>
    + '|^<!\\[CDATA\\[[\\s\\S]*?\\]\\]>',
    // CDATA section
    link: /^!?\[(label)\]\(\s*(href)(?:\s+(title))?\s*\)/,
    reflink: /^!?\[(label)\]\[(?!\s*\])((?:\\[\[\]]?|[^\[\]\\])+)\]/,
    nolink: /^!?\[(?!\s*\])((?:\[[^\[\]]*\]|\\[\[\]]|[^\[\]])*)\](?:\[\])?/,
    strong: /^__([^\s_])__(?!_)|^\*\*([^\s*])\*\*(?!\*)|^__([^\s][\s\S]*?[^\s])__(?!_)|^\*\*([^\s][\s\S]*?[^\s])\*\*(?!\*)/,
    em: /^_([^\s_])_(?!_)|^\*([^\s*<\[])\*(?!\*)|^_([^\s<][\s\S]*?[^\s_])_(?!_|[^\spunctuation])|^_([^\s_<][\s\S]*?[^\s])_(?!_|[^\spunctuation])|^\*([^\s<"][\s\S]*?[^\s\*])\*(?!\*|[^\spunctuation])|^\*([^\s*"<\[][\s\S]*?[^\s])\*(?!\*)/,
    code: /^(`+)([^`]|[^`][\s\S]*?[^`])\1(?!`)/,
    br: /^( {2,}|\\)\n(?!\s*$)/,
    del: noop,
    text: /^(`+|[^`])(?:[\s\S]*?(?:(?=[\\<!\[`*]|\b_|$)|[^ ](?= {2,}\n))|(?= {2,}\n))/
  }; // list of punctuation marks from common mark spec
  // without ` and ] to workaround Rule 17 (inline code blocks/links)

  inline._punctuation = '!"#$%&\'()*+,\\-./:;<=>?@\\[^_{|}~';
  inline.em = edit(inline.em).replace(/punctuation/g, inline._punctuation).getRegex();
  inline._escapes = /\\([!"#$%&'()*+,\-./:;<=>?@\[\]\\^_`{|}~])/g;
  inline._scheme = /[a-zA-Z][a-zA-Z0-9+.-]{1,31}/;
  inline._email = /[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+(@)[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+(?![-_])/;
  inline.autolink = edit(inline.autolink).replace('scheme', inline._scheme).replace('email', inline._email).getRegex();
  inline._attribute = /\s+[a-zA-Z:_][\w.:-]*(?:\s*=\s*"[^"]*"|\s*=\s*'[^']*'|\s*=\s*[^\s"'=<>`]+)?/;
  inline.tag = edit(inline.tag).replace('comment', block._comment).replace('attribute', inline._attribute).getRegex();
  inline._label = /(?:\[[^\[\]]*\]|\\.|`[^`]*`|[^\[\]\\`])*?/;
  inline._href = /<(?:\\[<>]?|[^\s<>\\])*>|[^\s\x00-\x1f]*/;
  inline._title = /"(?:\\"?|[^"\\])*"|'(?:\\'?|[^'\\])*'|\((?:\\\)?|[^)\\])*\)/;
  inline.link = edit(inline.link).replace('label', inline._label).replace('href', inline._href).replace('title', inline._title).getRegex();
  inline.reflink = edit(inline.reflink).replace('label', inline._label).getRegex();
  /**
   * Normal Inline Grammar
   */

  inline.normal = merge({}, inline);
  /**
   * Pedantic Inline Grammar
   */

  inline.pedantic = merge({}, inline.normal, {
    strong: /^__(?=\S)([\s\S]*?\S)__(?!_)|^\*\*(?=\S)([\s\S]*?\S)\*\*(?!\*)/,
    em: /^_(?=\S)([\s\S]*?\S)_(?!_)|^\*(?=\S)([\s\S]*?\S)\*(?!\*)/,
    link: edit(/^!?\[(label)\]\((.*?)\)/).replace('label', inline._label).getRegex(),
    reflink: edit(/^!?\[(label)\]\s*\[([^\]]*)\]/).replace('label', inline._label).getRegex()
  });
  /**
   * GFM Inline Grammar
   */

  inline.gfm = merge({}, inline.normal, {
    escape: edit(inline.escape).replace('])', '~|])').getRegex(),
    _extended_email: /[A-Za-z0-9._+-]+(@)[a-zA-Z0-9-_]+(?:\.[a-zA-Z0-9-_]*[a-zA-Z0-9])+(?![-_])/,
    url: /^((?:ftp|https?):\/\/|www\.)(?:[a-zA-Z0-9\-]+\.?)+[^\s<]*|^email/,
    _backpedal: /(?:[^?!.,:;*_~()&]+|\([^)]*\)|&(?![a-zA-Z0-9]+;$)|[?!.,:;*_~)]+(?!$))+/,
    del: /^~+(?=\S)([\s\S]*?\S)~+/,
    text: /^(`+|[^`])(?:[\s\S]*?(?:(?=[\\<!\[`*~]|\b_|https?:\/\/|ftp:\/\/|www\.|$)|[^ ](?= {2,}\n)|[^a-zA-Z0-9.!#$%&'*+\/=?_`{\|}~-](?=[a-zA-Z0-9.!#$%&'*+\/=?_`{\|}~-]+@))|(?= {2,}\n|[a-zA-Z0-9.!#$%&'*+\/=?_`{\|}~-]+@))/
  });
  inline.gfm.url = edit(inline.gfm.url, 'i').replace('email', inline.gfm._extended_email).getRegex();
  /**
   * GFM + Line Breaks Inline Grammar
   */

  inline.breaks = merge({}, inline.gfm, {
    br: edit(inline.br).replace('{2,}', '*').getRegex(),
    text: edit(inline.gfm.text).replace('\\b_', '\\b_| {2,}\\n').replace(/\{2,\}/g, '*').getRegex()
  });
  /**
   * Inline Lexer & Compiler
   */

  function InlineLexer(links, options) {
    this.options = options || marked.defaults;
    this.links = links;
    this.rules = inline.normal;
    this.renderer = this.options.renderer || new Renderer();
    this.renderer.options = this.options;

    if (!this.links) {
      throw new Error('Tokens array requires a `links` property.');
    }

    if (this.options.pedantic) {
      this.rules = inline.pedantic;
    } else if (this.options.gfm) {
      if (this.options.breaks) {
        this.rules = inline.breaks;
      } else {
        this.rules = inline.gfm;
      }
    }
  }
  /**
   * Expose Inline Rules
   */


  InlineLexer.rules = inline;
  /**
   * Static Lexing/Compiling Method
   */

  InlineLexer.output = function (src, links, options) {
    var inline = new InlineLexer(links, options);
    return inline.output(src);
  };
  /**
   * Lexing/Compiling
   */


  InlineLexer.prototype.output = function (src) {
    var out = '',
        link,
        text,
        href,
        title,
        cap,
        prevCapZero;

    while (src) {
      // escape
      if (cap = this.rules.escape.exec(src)) {
        src = src.substring(cap[0].length);
        out += escape(cap[1]);
        continue;
      } // tag


      if (cap = this.rules.tag.exec(src)) {
        if (!this.inLink && /^<a /i.test(cap[0])) {
          this.inLink = true;
        } else if (this.inLink && /^<\/a>/i.test(cap[0])) {
          this.inLink = false;
        }

        if (!this.inRawBlock && /^<(pre|code|kbd|script)(\s|>)/i.test(cap[0])) {
          this.inRawBlock = true;
        } else if (this.inRawBlock && /^<\/(pre|code|kbd|script)(\s|>)/i.test(cap[0])) {
          this.inRawBlock = false;
        }

        src = src.substring(cap[0].length);
        out += this.options.sanitize ? this.options.sanitizer ? this.options.sanitizer(cap[0]) : escape(cap[0]) : cap[0];
        continue;
      } // link


      if (cap = this.rules.link.exec(src)) {
        var lastParenIndex = findClosingBracket(cap[2], '()');

        if (lastParenIndex > -1) {
          var linkLen = 4 + cap[1].length + lastParenIndex;
          cap[2] = cap[2].substring(0, lastParenIndex);
          cap[0] = cap[0].substring(0, linkLen).trim();
          cap[3] = '';
        }

        src = src.substring(cap[0].length);
        this.inLink = true;
        href = cap[2];

        if (this.options.pedantic) {
          link = /^([^'"]*[^\s])\s+(['"])(.*)\2/.exec(href);

          if (link) {
            href = link[1];
            title = link[3];
          } else {
            title = '';
          }
        } else {
          title = cap[3] ? cap[3].slice(1, -1) : '';
        }

        href = href.trim().replace(/^<([\s\S]*)>$/, '$1');
        out += this.outputLink(cap, {
          href: InlineLexer.escapes(href),
          title: InlineLexer.escapes(title)
        });
        this.inLink = false;
        continue;
      } // reflink, nolink


      if ((cap = this.rules.reflink.exec(src)) || (cap = this.rules.nolink.exec(src))) {
        src = src.substring(cap[0].length);
        link = (cap[2] || cap[1]).replace(/\s+/g, ' ');
        link = this.links[link.toLowerCase()];

        if (!link || !link.href) {
          out += cap[0].charAt(0);
          src = cap[0].substring(1) + src;
          continue;
        }

        this.inLink = true;
        out += this.outputLink(cap, link);
        this.inLink = false;
        continue;
      } // strong


      if (cap = this.rules.strong.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.strong(this.output(cap[4] || cap[3] || cap[2] || cap[1]));
        continue;
      } // em


      if (cap = this.rules.em.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.em(this.output(cap[6] || cap[5] || cap[4] || cap[3] || cap[2] || cap[1]));
        continue;
      } // code


      if (cap = this.rules.code.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.codespan(escape(cap[2].trim(), true));
        continue;
      } // br


      if (cap = this.rules.br.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.br();
        continue;
      } // del (gfm)


      if (cap = this.rules.del.exec(src)) {
        src = src.substring(cap[0].length);
        out += this.renderer.del(this.output(cap[1]));
        continue;
      } // autolink


      if (cap = this.rules.autolink.exec(src)) {
        src = src.substring(cap[0].length);

        if (cap[2] === '@') {
          text = escape(this.mangle(cap[1]));
          href = 'mailto:' + text;
        } else {
          text = escape(cap[1]);
          href = text;
        }

        out += this.renderer.link(href, null, text);
        continue;
      } // url (gfm)


      if (!this.inLink && (cap = this.rules.url.exec(src))) {
        if (cap[2] === '@') {
          text = escape(cap[0]);
          href = 'mailto:' + text;
        } else {
          // do extended autolink path validation
          do {
            prevCapZero = cap[0];
            cap[0] = this.rules._backpedal.exec(cap[0])[0];
          } while (prevCapZero !== cap[0]);

          text = escape(cap[0]);

          if (cap[1] === 'www.') {
            href = 'http://' + text;
          } else {
            href = text;
          }
        }

        src = src.substring(cap[0].length);
        out += this.renderer.link(href, null, text);
        continue;
      } // text


      if (cap = this.rules.text.exec(src)) {
        src = src.substring(cap[0].length);

        if (this.inRawBlock) {
          out += this.renderer.text(this.options.sanitize ? this.options.sanitizer ? this.options.sanitizer(cap[0]) : escape(cap[0]) : cap[0]);
        } else {
          out += this.renderer.text(escape(this.smartypants(cap[0])));
        }

        continue;
      }

      if (src) {
        throw new Error('Infinite loop on byte: ' + src.charCodeAt(0));
      }
    }

    return out;
  };

  InlineLexer.escapes = function (text) {
    return text ? text.replace(InlineLexer.rules._escapes, '$1') : text;
  };
  /**
   * Compile Link
   */


  InlineLexer.prototype.outputLink = function (cap, link) {
    var href = link.href,
        title = link.title ? escape(link.title) : null;
    return cap[0].charAt(0) !== '!' ? this.renderer.link(href, title, this.output(cap[1])) : this.renderer.image(href, title, escape(cap[1]));
  };
  /**
   * Smartypants Transformations
   */


  InlineLexer.prototype.smartypants = function (text) {
    if (!this.options.smartypants) return text;
    return text // em-dashes
    .replace(/---/g, '\u2014') // en-dashes
    .replace(/--/g, '\u2013') // opening singles
    .replace(/(^|[-\u2014/(\[{"\s])'/g, '$1\u2018') // closing singles & apostrophes
    .replace(/'/g, '\u2019') // opening doubles
    .replace(/(^|[-\u2014/(\[{\u2018\s])"/g, '$1\u201c') // closing doubles
    .replace(/"/g, '\u201d') // ellipses
    .replace(/\.{3}/g, '\u2026');
  };
  /**
   * Mangle Links
   */


  InlineLexer.prototype.mangle = function (text) {
    if (!this.options.mangle) return text;
    var out = '',
        l = text.length,
        i = 0,
        ch;

    for (; i < l; i++) {
      ch = text.charCodeAt(i);

      if (Math.random() > 0.5) {
        ch = 'x' + ch.toString(16);
      }

      out += '&#' + ch + ';';
    }

    return out;
  };
  /**
   * Renderer
   */


  function Renderer(options) {
    this.options = options || marked.defaults;
  }

  Renderer.prototype.code = function (code, infostring, escaped) {
    var lang = (infostring || '').match(/\S*/)[0];

    if (this.options.highlight) {
      var out = this.options.highlight(code, lang);

      if (out != null && out !== code) {
        escaped = true;
        code = out;
      }
    }

    if (!lang) {
      return '<pre><code>' + (escaped ? code : escape(code, true)) + '</code></pre>';
    }

    return '<pre><code class="' + this.options.langPrefix + escape(lang, true) + '">' + (escaped ? code : escape(code, true)) + '</code></pre>\n';
  };

  Renderer.prototype.blockquote = function (quote) {
    return '<blockquote>\n' + quote + '</blockquote>\n';
  };

  Renderer.prototype.html = function (html) {
    return html;
  };

  Renderer.prototype.heading = function (text, level, raw, slugger) {
    if (this.options.headerIds) {
      return '<h' + level + ' id="' + this.options.headerPrefix + slugger.slug(raw) + '">' + text + '</h' + level + '>\n';
    } // ignore IDs


    return '<h' + level + '>' + text + '</h' + level + '>\n';
  };

  Renderer.prototype.hr = function () {
    return this.options.xhtml ? '<hr/>\n' : '<hr>\n';
  };

  Renderer.prototype.list = function (body, ordered, start) {
    var type = ordered ? 'ol' : 'ul',
        startatt = ordered && start !== 1 ? ' start="' + start + '"' : '';
    return '<' + type + startatt + '>\n' + body + '</' + type + '>\n';
  };

  Renderer.prototype.listitem = function (text) {
    return '<li>' + text + '</li>\n';
  };

  Renderer.prototype.checkbox = function (checked) {
    return '<input ' + (checked ? 'checked="" ' : '') + 'disabled="" type="checkbox"' + (this.options.xhtml ? ' /' : '') + '> ';
  };

  Renderer.prototype.paragraph = function (text) {
    return '<p>' + text + '</p>\n';
  };

  Renderer.prototype.table = function (header, body) {
    if (body) body = '<tbody>' + body + '</tbody>';
    return '<table>\n' + '<thead>\n' + header + '</thead>\n' + body + '</table>\n';
  };

  Renderer.prototype.tablerow = function (content) {
    return '<tr>\n' + content + '</tr>\n';
  };

  Renderer.prototype.tablecell = function (content, flags) {
    var type = flags.header ? 'th' : 'td';
    var tag = flags.align ? '<' + type + ' align="' + flags.align + '">' : '<' + type + '>';
    return tag + content + '</' + type + '>\n';
  }; // span level renderer


  Renderer.prototype.strong = function (text) {
    return '<strong>' + text + '</strong>';
  };

  Renderer.prototype.em = function (text) {
    return '<em>' + text + '</em>';
  };

  Renderer.prototype.codespan = function (text) {
    return '<code>' + text + '</code>';
  };

  Renderer.prototype.br = function () {
    return this.options.xhtml ? '<br/>' : '<br>';
  };

  Renderer.prototype.del = function (text) {
    return '<del>' + text + '</del>';
  };

  Renderer.prototype.link = function (href, title, text) {
    href = cleanUrl(this.options.sanitize, this.options.baseUrl, href);

    if (href === null) {
      return text;
    }

    var out = '<a href="' + escape(href) + '"';

    if (title) {
      out += ' title="' + title + '"';
    }

    out += '>' + text + '</a>';
    return out;
  };

  Renderer.prototype.image = function (href, title, text) {
    href = cleanUrl(this.options.sanitize, this.options.baseUrl, href);

    if (href === null) {
      return text;
    }

    var out = '<img src="' + href + '" alt="' + text + '"';

    if (title) {
      out += ' title="' + title + '"';
    }

    out += this.options.xhtml ? '/>' : '>';
    return out;
  };

  Renderer.prototype.text = function (text) {
    return text;
  };
  /**
   * TextRenderer
   * returns only the textual part of the token
   */


  function TextRenderer() {} // no need for block level renderers


  TextRenderer.prototype.strong = TextRenderer.prototype.em = TextRenderer.prototype.codespan = TextRenderer.prototype.del = TextRenderer.prototype.text = function (text) {
    return text;
  };

  TextRenderer.prototype.link = TextRenderer.prototype.image = function (href, title, text) {
    return '' + text;
  };

  TextRenderer.prototype.br = function () {
    return '';
  };
  /**
   * Parsing & Compiling
   */


  function Parser(options) {
    this.tokens = [];
    this.token = null;
    this.options = options || marked.defaults;
    this.options.renderer = this.options.renderer || new Renderer();
    this.renderer = this.options.renderer;
    this.renderer.options = this.options;
    this.slugger = new Slugger();
  }
  /**
   * Static Parse Method
   */


  Parser.parse = function (src, options) {
    var parser = new Parser(options);
    return parser.parse(src);
  };
  /**
   * Parse Loop
   */


  Parser.prototype.parse = function (src) {
    this.inline = new InlineLexer(src.links, this.options); // use an InlineLexer with a TextRenderer to extract pure text

    this.inlineText = new InlineLexer(src.links, merge({}, this.options, {
      renderer: new TextRenderer()
    }));
    this.tokens = src.reverse();
    var out = '';

    while (this.next()) {
      out += this.tok();
    }

    return out;
  };
  /**
   * Next Token
   */


  Parser.prototype.next = function () {
    this.token = this.tokens.pop();
    return this.token;
  };
  /**
   * Preview Next Token
   */


  Parser.prototype.peek = function () {
    return this.tokens[this.tokens.length - 1] || 0;
  };
  /**
   * Parse Text Tokens
   */


  Parser.prototype.parseText = function () {
    var body = this.token.text;

    while (this.peek().type === 'text') {
      body += '\n' + this.next().text;
    }

    return this.inline.output(body);
  };
  /**
   * Parse Current Token
   */


  Parser.prototype.tok = function () {
    switch (this.token.type) {
      case 'space':
        {
          return '';
        }

      case 'hr':
        {
          return this.renderer.hr();
        }

      case 'heading':
        {
          return this.renderer.heading(this.inline.output(this.token.text), this.token.depth, unescape(this.inlineText.output(this.token.text)), this.slugger);
        }

      case 'code':
        {
          return this.renderer.code(this.token.text, this.token.lang, this.token.escaped);
        }

      case 'table':
        {
          var header = '',
              body = '',
              i,
              row,
              cell,
              j; // header

          cell = '';

          for (i = 0; i < this.token.header.length; i++) {
            cell += this.renderer.tablecell(this.inline.output(this.token.header[i]), {
              header: true,
              align: this.token.align[i]
            });
          }

          header += this.renderer.tablerow(cell);

          for (i = 0; i < this.token.cells.length; i++) {
            row = this.token.cells[i];
            cell = '';

            for (j = 0; j < row.length; j++) {
              cell += this.renderer.tablecell(this.inline.output(row[j]), {
                header: false,
                align: this.token.align[j]
              });
            }

            body += this.renderer.tablerow(cell);
          }

          return this.renderer.table(header, body);
        }

      case 'blockquote_start':
        {
          body = '';

          while (this.next().type !== 'blockquote_end') {
            body += this.tok();
          }

          return this.renderer.blockquote(body);
        }

      case 'list_start':
        {
          body = '';
          var ordered = this.token.ordered,
              start = this.token.start;

          while (this.next().type !== 'list_end') {
            body += this.tok();
          }

          return this.renderer.list(body, ordered, start);
        }

      case 'list_item_start':
        {
          body = '';
          var loose = this.token.loose;
          var checked = this.token.checked;
          var task = this.token.task;

          if (this.token.task) {
            body += this.renderer.checkbox(checked);
          }

          while (this.next().type !== 'list_item_end') {
            body += !loose && this.token.type === 'text' ? this.parseText() : this.tok();
          }

          return this.renderer.listitem(body, task, checked);
        }

      case 'html':
        {
          // TODO parse inline content if parameter markdown=1
          return this.renderer.html(this.token.text);
        }

      case 'paragraph':
        {
          return this.renderer.paragraph(this.inline.output(this.token.text));
        }

      case 'text':
        {
          return this.renderer.paragraph(this.parseText());
        }

      default:
        {
          var errMsg = 'Token with "' + this.token.type + '" type was not found.';

          if (this.options.silent) {
            console.log(errMsg);
          } else {
            throw new Error(errMsg);
          }
        }
    }
  };
  /**
   * Slugger generates header id
   */


  function Slugger() {
    this.seen = {};
  }
  /**
   * Convert string to unique id
   */


  Slugger.prototype.slug = function (value) {
    var slug = value.toLowerCase().trim().replace(/[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,./:;<=>?@[\]^`{|}~]/g, '').replace(/\s/g, '-');

    if (this.seen.hasOwnProperty(slug)) {
      var originalSlug = slug;

      do {
        this.seen[originalSlug]++;
        slug = originalSlug + '-' + this.seen[originalSlug];
      } while (this.seen.hasOwnProperty(slug));
    }

    this.seen[slug] = 0;
    return slug;
  };
  /**
   * Helpers
   */


  function escape(html, encode) {
    if (encode) {
      if (escape.escapeTest.test(html)) {
        return html.replace(escape.escapeReplace, function (ch) {
          return escape.replacements[ch];
        });
      }
    } else {
      if (escape.escapeTestNoEncode.test(html)) {
        return html.replace(escape.escapeReplaceNoEncode, function (ch) {
          return escape.replacements[ch];
        });
      }
    }

    return html;
  }

  escape.escapeTest = /[&<>"']/;
  escape.escapeReplace = /[&<>"']/g;
  escape.replacements = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;'
  };
  escape.escapeTestNoEncode = /[<>"']|&(?!#?\w+;)/;
  escape.escapeReplaceNoEncode = /[<>"']|&(?!#?\w+;)/g;

  function unescape(html) {
    // explicitly match decimal, hex, and named HTML entities
    return html.replace(/&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/ig, function (_, n) {
      n = n.toLowerCase();
      if (n === 'colon') return ':';

      if (n.charAt(0) === '#') {
        return n.charAt(1) === 'x' ? String.fromCharCode(parseInt(n.substring(2), 16)) : String.fromCharCode(+n.substring(1));
      }

      return '';
    });
  }

  function edit(regex, opt) {
    regex = regex.source || regex;
    opt = opt || '';
    return {
      replace: function (name, val) {
        val = val.source || val;
        val = val.replace(/(^|[^\[])\^/g, '$1');
        regex = regex.replace(name, val);
        return this;
      },
      getRegex: function () {
        return new RegExp(regex, opt);
      }
    };
  }

  function cleanUrl(sanitize, base, href) {
    if (sanitize) {
      try {
        var prot = decodeURIComponent(unescape(href)).replace(/[^\w:]/g, '').toLowerCase();
      } catch (e) {
        return null;
      }

      if (prot.indexOf('javascript:') === 0 || prot.indexOf('vbscript:') === 0 || prot.indexOf('data:') === 0) {
        return null;
      }
    }

    if (base && !originIndependentUrl.test(href)) {
      href = resolveUrl(base, href);
    }

    try {
      href = encodeURI(href).replace(/%25/g, '%');
    } catch (e) {
      return null;
    }

    return href;
  }

  function resolveUrl(base, href) {
    if (!baseUrls[' ' + base]) {
      // we can ignore everything in base after the last slash of its path component,
      // but we might need to add _that_
      // https://tools.ietf.org/html/rfc3986#section-3
      if (/^[^:]+:\/*[^/]*$/.test(base)) {
        baseUrls[' ' + base] = base + '/';
      } else {
        baseUrls[' ' + base] = rtrim(base, '/', true);
      }
    }

    base = baseUrls[' ' + base];

    if (href.slice(0, 2) === '//') {
      return base.replace(/:[\s\S]*/, ':') + href;
    } else if (href.charAt(0) === '/') {
      return base.replace(/(:\/*[^/]*)[\s\S]*/, '$1') + href;
    } else {
      return base + href;
    }
  }

  var baseUrls = {};
  var originIndependentUrl = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i;

  function noop() {}

  noop.exec = noop;

  function merge(obj) {
    var i = 1,
        target,
        key;

    for (; i < arguments.length; i++) {
      target = arguments[i];

      for (key in target) {
        if (Object.prototype.hasOwnProperty.call(target, key)) {
          obj[key] = target[key];
        }
      }
    }

    return obj;
  }

  function splitCells(tableRow, count) {
    // ensure that every cell-delimiting pipe has a space
    // before it to distinguish it from an escaped pipe
    var row = tableRow.replace(/\|/g, function (match, offset, str) {
      var escaped = false,
          curr = offset;

      while (--curr >= 0 && str[curr] === '\\') escaped = !escaped;

      if (escaped) {
        // odd number of slashes means | is escaped
        // so we leave it alone
        return '|';
      } else {
        // add space before unescaped |
        return ' |';
      }
    }),
        cells = row.split(/ \|/),
        i = 0;

    if (cells.length > count) {
      cells.splice(count);
    } else {
      while (cells.length < count) cells.push('');
    }

    for (; i < cells.length; i++) {
      // leading or trailing whitespace is ignored per the gfm spec
      cells[i] = cells[i].trim().replace(/\\\|/g, '|');
    }

    return cells;
  } // Remove trailing 'c's. Equivalent to str.replace(/c*$/, '').
  // /c*$/ is vulnerable to REDOS.
  // invert: Remove suffix of non-c chars instead. Default falsey.


  function rtrim(str, c, invert) {
    if (str.length === 0) {
      return '';
    } // Length of suffix matching the invert condition.


    var suffLen = 0; // Step left until we fail to match the invert condition.

    while (suffLen < str.length) {
      var currChar = str.charAt(str.length - suffLen - 1);

      if (currChar === c && !invert) {
        suffLen++;
      } else if (currChar !== c && invert) {
        suffLen++;
      } else {
        break;
      }
    }

    return str.substr(0, str.length - suffLen);
  }

  function findClosingBracket(str, b) {
    if (str.indexOf(b[1]) === -1) {
      return -1;
    }

    var level = 0;

    for (var i = 0; i < str.length; i++) {
      if (str[i] === '\\') {
        i++;
      } else if (str[i] === b[0]) {
        level++;
      } else if (str[i] === b[1]) {
        level--;

        if (level < 0) {
          return i;
        }
      }
    }

    return -1;
  }

  function checkSanitizeDeprecation(opt) {
    if (opt && opt.sanitize && !opt.silent) {
      console.warn('marked(): sanitize and sanitizer parameters are deprecated since version 0.7.0, should not be used and will be removed in the future. Read more here: https://marked.js.org/#/USING_ADVANCED.md#options');
    }
  }
  /**
   * Marked
   */


  function marked(src, opt, callback) {
    // throw error in case of non string input
    if (typeof src === 'undefined' || src === null) {
      throw new Error('marked(): input parameter is undefined or null');
    }

    if (typeof src !== 'string') {
      throw new Error('marked(): input parameter is of type ' + Object.prototype.toString.call(src) + ', string expected');
    }

    if (callback || typeof opt === 'function') {
      if (!callback) {
        callback = opt;
        opt = null;
      }

      opt = merge({}, marked.defaults, opt || {});
      checkSanitizeDeprecation(opt);
      var highlight = opt.highlight,
          tokens,
          pending,
          i = 0;

      try {
        tokens = Lexer.lex(src, opt);
      } catch (e) {
        return callback(e);
      }

      pending = tokens.length;

      var done = function (err) {
        if (err) {
          opt.highlight = highlight;
          return callback(err);
        }

        var out;

        try {
          out = Parser.parse(tokens, opt);
        } catch (e) {
          err = e;
        }

        opt.highlight = highlight;
        return err ? callback(err) : callback(null, out);
      };

      if (!highlight || highlight.length < 3) {
        return done();
      }

      delete opt.highlight;
      if (!pending) return done();

      for (; i < tokens.length; i++) {
        (function (token) {
          if (token.type !== 'code') {
            return --pending || done();
          }

          return highlight(token.text, token.lang, function (err, code) {
            if (err) return done(err);

            if (code == null || code === token.text) {
              return --pending || done();
            }

            token.text = code;
            token.escaped = true;
            --pending || done();
          });
        })(tokens[i]);
      }

      return;
    }

    try {
      if (opt) opt = merge({}, marked.defaults, opt);
      checkSanitizeDeprecation(opt);
      return Parser.parse(Lexer.lex(src, opt), opt);
    } catch (e) {
      e.message += '\nPlease report this to https://github.com/markedjs/marked.';

      if ((opt || marked.defaults).silent) {
        return '<p>An error occurred:</p><pre>' + escape(e.message + '', true) + '</pre>';
      }

      throw e;
    }
  }
  /**
   * Options
   */


  marked.options = marked.setOptions = function (opt) {
    merge(marked.defaults, opt);
    return marked;
  };

  marked.getDefaults = function () {
    return {
      baseUrl: null,
      breaks: false,
      gfm: true,
      headerIds: true,
      headerPrefix: '',
      highlight: null,
      langPrefix: 'language-',
      mangle: true,
      pedantic: false,
      renderer: new Renderer(),
      sanitize: false,
      sanitizer: null,
      silent: false,
      smartLists: false,
      smartypants: false,
      xhtml: false
    };
  };

  marked.defaults = marked.getDefaults();
  /**
   * Expose
   */

  marked.Parser = Parser;
  marked.parser = Parser.parse;
  marked.Renderer = Renderer;
  marked.TextRenderer = TextRenderer;
  marked.Lexer = Lexer;
  marked.lexer = Lexer.lex;
  marked.InlineLexer = InlineLexer;
  marked.inlineLexer = InlineLexer.output;
  marked.Slugger = Slugger;
  marked.parse = marked;

  if (typeof module !== 'undefined' && typeof exports === 'object') {
    module.exports = marked;
  } else if (typeof define === 'function' && define.amd) {
    define(function () {
      return marked;
    });
  } else {
    root.marked = marked;
  }
})(this || (typeof window !== 'undefined' ? window : global));
},{}],"src/views/Lecteur.js":[function(require,module,exports) {
var m = require("mithril");

var purify = require("dompurify"); //ar marked = require("marked");
// Create reference instance


var marked = require('marked'); // Get reference


var renderer = new marked.Renderer(); // Override function for Bulma headings

renderer.heading = function (text, level) {
  var escapedText = text.toLowerCase().replace(/[^\w]+/g, '-');
  var size = Math.min(parseInt(level) + 2, 6);
  return "\n          <h".concat(level, " id=").concat(text, " class=\"title is-").concat(size.toString(), "\">\n            ").concat(text, "\n          </h").concat(level, ">");
};

var Carnet = require("../models/carnet");

function creationLienTitre(texte, titres) {
  titres = titres.filter(function (t) {
    return t[1].length > 0;
  }).sort(function (a, b) {
    return b.length - a.length;
  });
  var decoupe = texte.split(/(\[[^\]]*\]\([^\)]*\))/);
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = titres[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      titre = _step.value;

      if (titre[1].toLowerCase() !== Carnet.note.title.toLowerCase()) {
        var regx = new RegExp(titre[1], "giu");
        var nvdecoupe = [];
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
          for (var _iterator2 = decoupe[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            morceau = _step2.value;

            if (morceau[0] !== "[") {
              var tranches = morceau.replace(regx, function (cor, p) {
                return "[" + cor + "](#/note/" + titre[0] + ")";
              }).split(/(\[[^\]]*\]\([^\)]*\))/);
              nvdecoupe = nvdecoupe.concat(tranches);
            } else {
              nvdecoupe.push(morceau);
            }
          }
        } catch (err) {
          _didIteratorError2 = true;
          _iteratorError2 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
              _iterator2.return();
            }
          } finally {
            if (_didIteratorError2) {
              throw _iteratorError2;
            }
          }
        }

        decoupe = nvdecoupe;
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return decoupe.join("");
}

function Lecteur() {
  //console.log("lecteur")
  return {
    oninit: function oninit() {},
    view: function view() {
      var texte = Carnet.note.content;
      var texteLie = creationLienTitre(texte, Carnet.notes.map(function (n) {
        return [n.id, n.title];
      })); //console.log(texteLie)

      var md_texte = purify.sanitize(marked("# " + Carnet.note.title + "\n\n" + texteLie, {
        renderer: renderer
      }));
      return m('div#viewer.box.column', {}, m(".content", m.trust(md_texte)));
    }
  };
}

module.exports = Lecteur;
},{"mithril":"node_modules/mithril/index.js","dompurify":"node_modules/dompurify/dist/purify.js","marked":"node_modules/marked/lib/marked.js","../models/carnet":"src/models/carnet.js"}],"src/views/MenuNote.js":[function(require,module,exports) {
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var m = require("mithril"); //import marked from "marked";


var Carnet = require("../models/carnet");

var Etiquette = require("./Etiquette");

function MenuNote() {
  return {
    oninit: function oninit(vnode) {},
    view: function view() {
      var _m;

      var vueMenu;
      var actMenu = false;

      if (Carnet.viewNote == "lecteur") {
        vueMenu = "Editer";
        vueIcon = "create";
      } else {
        vueMenu = "Voir";
        vueIcon = "eye";
      }

      var drop = Carnet.actMenu ? ".dropdown.is-active" : ".dropdown";
      return m("#stickymenu.menu.column.is-narrow", m("ul.menu-list", m("li.is-inline-mobile", m(".button.is-inline-block", {
        onclick: Carnet.vue
      }, m("span.icon", m("ion-icon", {
        name: vueIcon
      })))
      /*,m("span",vueMenu)*/
      ), // Voir/Editer
      m("li.is-inline-mobile", m(".button.is-inline-block", {
        onclick: Carnet.toggleModal
      }, m("span.icon", m("ion-icon", {
        name: "trash"
      })))
      /*,m("span","Effacer")*/
      ), // Trash
      m("li.is-inline-mobile", m(".button.is-inline-block", {
        onclick: Carnet.download
      }, m("span.icon", m("ion-icon", {
        name: "download"
      })))
      /*,m("span",vueMenu)*/
      ), // Telecharger
      m("li.is-inline-mobile", m(drop, m(".dropdown-trigger", m("button.button", (_m = {}, _defineProperty(_m, 'aria-haspopup', "true"), _defineProperty(_m, "onclick", Carnet.toggleEtiq), _m), m("span.icon", m("ion-icon", {
        name: "pricetag"
      })))), m(".dropdown-menu", m(".dropdown-content", m(".dropdown-item", m("input.input.nvlle-etiq.is-primary", {
        placeholder: "etiquette",
        onkeypress: function onkeypress(e) {
          return Carnet.ajoutEtiq(e, e.target.value);
        }
      })))))), m("#effdialog.modal.is-clipped" + Carnet.effModal, m(".modal-background", ""), m(".modal-content", m(".box", m("p", "Êtes-vous sûr de vouloir effacer cette note ? C'est irréversible."), m("button.button", {
        onclick: Carnet.retirer,
        href: "#/liste"
      }, "Effacer"), m("button.button", {
        onclick: Carnet.toggleModal
      }, "Annuler"))))));
    }
  };
}

module.exports = MenuNote;
},{"mithril":"node_modules/mithril/index.js","../models/carnet":"src/models/carnet.js","./Etiquette":"src/views/Etiquette.js"}],"src/views/Note.js":[function(require,module,exports) {
var m = require("mithril"); //import marked from "marked";


var Carnet = require("../models/carnet");

var Editeur = require("./Editeur");

var Lecteur = require("./Lecteur");

var MenuNote = require("./MenuNote");

var Etiquette = require("./Etiquette");

function Note() {
  return {
    oninit: function oninit(vnode) {
      //console.log(vnode.attrs.id);
      Carnet.chargeNote(vnode.attrs.id);

      if (Carnet.note.title != "" && Carnet.note.content != "") {
        Carnet.viewNote = "lecteur";
      } else {
        Carnet.viewNote = "editeur";
      }
    },
    onupdate: function onupdate(vnode) {
      //console.log(vnode.attrs.id);
      if (vnode.attrs.id != Carnet.note.id) {
        Carnet.chargeNote(vnode.attrs.id);
      }
    },
    view: function view() {
      var note = "#main.columns";
      var conteneur = "#conteneur.column.is-10.columns";
      var menu = ".column.is-2";

      if (Carnet.encharge) {
        return m(note, m(conteneur, m("p.is-size-4", "Chargement en cours")) //m(menu,m(Etiquette))
        );
      } else {
        if (Carnet.viewNote == "editeur") {
          return m(note, m(conteneur, m(MenuNote), m(Editeur)) //m(menu,m(Etiquette))
          );
        } else {
          return m(note, m(conteneur, m(MenuNote), m(Lecteur)) //m(menu,m(Etiquette))
          );
        }
      }
    }
  };
}

module.exports = Note;
},{"mithril":"node_modules/mithril/index.js","../models/carnet":"src/models/carnet.js","./Editeur":"src/views/Editeur.js","./Lecteur":"src/views/Lecteur.js","./MenuNote":"src/views/MenuNote.js","./Etiquette":"src/views/Etiquette.js"}],"node_modules/parcel-bundler/src/builtins/bundle-url.js":[function(require,module,exports) {
var bundleURL = null;

function getBundleURLCached() {
  if (!bundleURL) {
    bundleURL = getBundleURL();
  }

  return bundleURL;
}

function getBundleURL() {
  // Attempt to find the URL of the current script and use that as the base URL
  try {
    throw new Error();
  } catch (err) {
    var matches = ('' + err.stack).match(/(https?|file|ftp|chrome-extension|moz-extension):\/\/[^)\n]+/g);

    if (matches) {
      return getBaseURL(matches[0]);
    }
  }

  return '/';
}

function getBaseURL(url) {
  return ('' + url).replace(/^((?:https?|file|ftp|chrome-extension|moz-extension):\/\/.+)\/[^/]+$/, '$1') + '/';
}

exports.getBundleURL = getBundleURLCached;
exports.getBaseURL = getBaseURL;
},{}],"node_modules/parcel-bundler/src/builtins/css-loader.js":[function(require,module,exports) {
var bundle = require('./bundle-url');

function updateLink(link) {
  var newLink = link.cloneNode();

  newLink.onload = function () {
    link.remove();
  };

  newLink.href = link.href.split('?')[0] + '?' + Date.now();
  link.parentNode.insertBefore(newLink, link.nextSibling);
}

var cssTimeout = null;

function reloadCSS() {
  if (cssTimeout) {
    return;
  }

  cssTimeout = setTimeout(function () {
    var links = document.querySelectorAll('link[rel="stylesheet"]');

    for (var i = 0; i < links.length; i++) {
      if (bundle.getBaseURL(links[i].href) === bundle.getBundleURL()) {
        updateLink(links[i]);
      }
    }

    cssTimeout = null;
  }, 50);
}

module.exports = reloadCSS;
},{"./bundle-url":"node_modules/parcel-bundler/src/builtins/bundle-url.js"}],"src/mystyles.scss":[function(require,module,exports) {
var reloadCSS = require('_css_loader');

module.hot.dispose(reloadCSS);
module.hot.accept(reloadCSS);
},{"_css_loader":"node_modules/parcel-bundler/src/builtins/css-loader.js"}],"src/index.js":[function(require,module,exports) {
var m = require("mithril");

var Layout = require("./views/Layout");

var Liste = require("./views/Liste");

var Note = require("./views/Note");

require('./mystyles.scss'); //require('./scss/flavors/mini-custom.scss');
//console.log("index.js chargé !!!")


m.route(document.getElementById("app"), "/liste", {
  "/liste": {
    render: function render() {
      return m(Layout, m(Liste));
    }
  },
  "/note/:id": {
    render: function render(vnode) {
      return m(Layout, vnode.attrs, m(Note, vnode.attrs));
    }
  },
  "/tag/:etiq": {
    render: function render(vnode) {
      return m(Layout, m(Liste, vnode.attrs));
    }
  }
});
},{"mithril":"node_modules/mithril/index.js","./views/Layout":"src/views/Layout.js","./views/Liste":"src/views/Liste.js","./views/Note":"src/views/Note.js","./mystyles.scss":"src/mystyles.scss"}],"node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "44269" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["node_modules/parcel-bundler/src/builtins/hmr-runtime.js","src/index.js"], null)
//# sourceMappingURL=/src.a2b27638.js.map