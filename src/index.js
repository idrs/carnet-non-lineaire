var m = require("mithril");
var Layout = require("./views/Layout")
var Liste = require("./views/Liste")

var Note =require("./views/Note")

require('./mystyles.scss');
//require('./scss/flavors/mini-custom.scss');

//console.log("index.js chargé !!!")

m.route(document.getElementById("app"), "/liste", {
    "/liste": {
        render: function() {
            return m(Layout, m(Liste))
        }
    },
    "/note/:id": {
        render: function(vnode) {
            return m(Layout,vnode.attrs, m(Note,vnode.attrs))
        }
    },
    "/tag/:etiq": {
        render: function(vnode) {
            return m(Layout, m(Liste,vnode.attrs))
        }
    },
})

