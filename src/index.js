var m = require("mithril");
var Layout = require("./views/Layout")
var Liste = require("./views/Liste")
var ListeTag = require("./views/ListeTag")

var Note =require("./views/Note")
var Menu = require("./views/Menu")

require('./mystyles.scss');
//require('./scss/flavors/mini-custom.scss');

console.log("index.js chargé !!!")

m.route(document.getElementById("app"), "/liste", {
    "/liste": {
        render: function() {
            return m(Layout, m(Liste),m(Menu))
        }
    },
    "/:id": {
        render: function(vnode) {
            return m(Layout,vnode.attrs, m(Note,vnode.attrs),m(Menu,vnode.attrs))
        }
    },
    "/tag/:etiq": {
        render: function(vnode) {
            return m(Layout, m(Liste,vnode.attrs),m(Menu))
        }
    },
})