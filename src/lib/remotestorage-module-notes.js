/**
 * File: notes
 *
 * Maintainer: - Jorin Vogel <hi@jorin.in>
 * Version: -    0.2.1
 *
 * This module stores lists of notes.
 * A note has the fields title, content and lastEdited.
 *
 * This module is used by Litewrite.
 * 
 * 
 * Modified for Carnet - Sylvain Vinet
 */


function Notes (privateClient, publicClient) {
  // Schema
  privateClient.declareType('note', {
    description: 'A text note',
    type: 'object',
    '$schema': 'http://json-schema.org/draft-03/schema#',
    additionalProperties: true,
    properties: {
      id: {
        type: 'string',
        required: true
      },
      title: {
        type: 'string',
        required: true
      },
      content: {
        type: 'string',
        required: true,
        default: ''
      },
      lastEdited: {
        type: 'string',
        required: true
      },
      etiquettes: {
        type: 'array',
        required: false,
        default:[]
      },
    }
  })

  var notesModule = {

    /**
     * Method: privateList
     *
     * List all private notes.
     *
     * Parameters:
     *
     *   path - a pathstring where to scope the client to.
     *
     * Returns:
     *   A privateClient scoped to the given path
     *    and extended with the listMethods.
     *   It also supports all <BaseClient methods at http://remotestoragejs.com/doc/code/files/baseclient-js.html>
     */
    privateList: function (path) {
      return Object.assign(privateClient.scope(path), listMethods) //must check if path end with '/'
    },

    /**
     * Method: publicList
     *
     * List all public notes.
     *
     * Parameters:
     *
     *   path - a pathstring where to scope the client to.
     *
     * Returns:
     *   A publicClient scoped to the given path
     *    and extended with the listMethods.
     *   It also supports all <BaseClient methods at http://remotestoragejs.com/doc/code/files/baseclient-js.html>
     */
    publicList: function (path) {
      return Object.assign(publicClient.scope(path), listMethods)
    }

  }

  /**
   * Class: listMethods
   *
   */
  var listMethods = {

    /**
     * Method: add
     *
     * Create a new note
     *
     * Parameters:
     *   doc - the note data to store as JSON object.
     *
     * Returns:
     *   A promise, which will be fulfilled with the created note as JSON object.
     *   The created note also contains the newly created id property.
     */
    add: function (doc) {
      var id = doc.id
      return this.set(id, doc)
    },

    /**
     * Method: set
     *
     * Update or create a note for a specified id.
     *
     * Parameters:
     *   id  - the id the note is at.
     *   doc - the note data to store as JSON object.
     *
     * Returns:
     *   A promise, which will be fulfilled with the updated note.
     */
    set: function (id, doc) {
      return this.storeObject('note', id, doc).then(function () {
        return doc
      }).catch(err => console.log("erreur dans client.set",err))
    },

    /**
     * Method: get
     *
     * Get a note.
     *
     * Parameters:
     *   id - the id of the note you want to get.
     *
     * Returns:
     *   A promise, which will be fulfilled with the note as JSON object.
     */
    get: function (id) {
      return this.getObject(id).then(function (obj) {
        return obj || {}
      })
    },

    /**
     * Method: addRaw
     *
     * Store a raw note of the specified contentType at shared/.
     *
     * Parameters:
     *   contentType - the content type of the data (like 'text/html').
     *   data - the raw data to store.
     *
     * Returns:
     *   A promise, which will be fulfilled with the path of the added note.
     */
    addRaw: function (contentType, data) {
      var id = new Date().toJSON();
      var path = 'shared/' + id
      var url = this.getItemURL(path)
      return this.storeFile(contentType, path, data).then(function () {
        return url
      })
    },

    /**
     * Method: setRaw
     *
     * Store a raw doccument of the specified contentType at shared/.
     *
     * Parameters:
     *   id - id of the note to update
     *   contentType - the content type of the data (like 'text/html').
     *   data - the raw data to store.
     *
     * Returns:
     *   A promise, which will be fulfilled with the path of the added note.
     */
    setRaw: function (id, contentType, data) {
      var path = 'shared/' + id
      return this.storeFile(contentType, path, data)
    }

  }

  return {
    exports: notesModule
  }
}

module.exports = {
  name: 'documents',
  builder: Notes
}
