var m = require( "mithril");
var debounce = require('lodash.debounce');

//var rs = require('../lib/storage')


var RemoteStorage = require('remotestoragejs');
var storageNotes = require('../lib/remotestorage-module-notes');

var Widget = require('remotestorage-widget');


var rs = new RemoteStorage({ modules: [storageNotes] });

//new Widget(rs).attach('remotestorage-connect')
console.log("initiate remoteStorage");
rs.access.claim('notes', 'rw')
console.log("initiate2 remoteStorage");
rs.caching.enable('/notes/')

const widget = new Widget(rs, { autoCloseAfter: 2000 });

rs.on('connected', () => {
    const userAddress = rs.remote.userAddress;
    console.debug(`${userAddress} connected their remote storage.`);
  })
  
rs.on('network-offline', () => {
    console.debug(`We're offline now.`);
  })
  
rs.on('network-online', () => {
    console.debug(`Hooray, we're back online.`);
  })

console.log("rs")
console.log(rs)

var Carnet = {
    store: rs.notes.privateList(''),
    notes: [],
    note: {},
    selectionne: "",
    encharge: true,
    noteVide: function(){
        return {
        titre: "",
        texte: "",
        _id: new Date().toJSON(),
        modif: new Date().toJSON(),
        etiquettes:[]
        };
    },
    viewNote: "editeur", //parmi liste/editeur/lecteur,
    chargement: function() {
        Carnet.store.getAll('').then(reponse => { 
            Carnet.notes = Object.values(reponse).filter(it => it._id); 
            Carnet.encharge=false;
            console.log("chargé!!");
            m.redraw();
        }).catch(err=>{
            console.log("erreur de chargement");
            console.log(err);
        });
    },
    ajout: function(){
        var nnote = Carnet.noteVide();
        console.log("affichage store:")
        console.log(Carnet.store)
        console.log("fin store:")
        Carnet.store.add(nnote)
        Carnet.notes.push(nnote);
        Carnet.store.getListing('')
  .then(listing => console.log(listing));
        //console.log(Carnet.notes);
    },
    retirer: function(){
        alert("effacement irréversible")
        Carnet.notes = Carnet.notes.filter(n=>n._id!=Carnet.note._id);
        Carnet.store.remove(Carnet.note._id);
    },
    edit: function(id,txt){

        Carnet.note[id] = txt;
        Carnet.note.modif = new Date().toJSON();
        debounce(()=>Carnet.store.set(Carnet.note._id,Carnet.note),500)();
        //Carnet.store.set(Carnet.note._id,Carnet.note);
    
    },
    chargeNote: function(id){
        Carnet.note = Carnet.notes.find(n => n._id == id);
    },
    vue: function(){
        if(Carnet.viewNote == 'editeur'){
            Carnet.viewNote = 'lecteur';
        }
        else{
            Carnet.viewNote = 'editeur';
        }
        //m.redraw();
    },
    ajoutEtiq: function(e,tag){
        //console.log(e.keyCode)
        if(e.keyCode == 13){
            //console.log("keycode 13")
            Carnet.note.etiquettes= Carnet.note.etiquettes.concat(tag.split(',').map(mot=>mot.trim()).filter(mot=>mot.length>0));
            e.target.value = ""
            Carnet.store.set(Carnet.note._id,Carnet.note);
        }
    },
    selection: function(){
        Carnet.selectionne = window.getSelection();
        if(Carnet.selectionne.toString().length==0){
            //console.log(Document.activeElement)
           // Carnet.selectionne = HTMLInputElement.setSelectionRange().toString();
        }
        console.log(Carnet.selectionne)
        console.log(Carnet.selectionne.length)
        alert(Carnet.selectionne);
    },
    ajoutAvecTitre: function(){
        var nvlleNote = Carnet.noteVide();
        console.log("affichage store:")
        console.log(Carnet.store)
        console.log("fin store:")
        
        var selection = window.getSelection();
        if(selection.toString().length==0){

            //popup pour un titre
            //console.log(Document.activeElement)
           // Carnet.selectionne = HTMLInputElement.setSelectionRange().toString();
        }
        else {
            nvlleNote.titre = selection.toString();
        }
        Carnet.store.add(nvlleNote)
        Carnet.notes.push(nvlleNote);
        Carnet.store.getListing('')
        .then(listing => console.log(listing));
    }
}

Carnet.chargement();
Carnet.store.on('change', function (evt) {
    console.log('data was added, updated, or removed:', evt)
  });
widget.attach("widget");
widget.close();

module.exports = Carnet