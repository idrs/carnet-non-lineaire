var m = require( "mithril");
var debounce = require('lodash.debounce');
var fs = require("file-saver");

//var rs = require('../lib/storage')


var RemoteStorage = require('remotestoragejs');
var storageNotes = require('../lib/remotestorage-module-notes');

var Widget = require('remotestorage-widget');


var rs = new RemoteStorage({
    cache: true,
    changeEvents: {
      local:    true,
      window:   false,
      remote:   true,
      conflict: true
    },
    cordovaRedirectUri: undefined,
    logging: false,
    modules: [storageNotes] 
});

//new Widget(rs).attach('remotestorage-connect')
//console.log("initiate remoteStorage");
rs.access.claim('documents', 'rw')
//console.log("initiate2 remoteStorage");
rs.caching.enable('/documents/notes/','ALL')

const widget = new Widget(rs, { autoCloseAfter: 2000 });

rs.on('connected', () => {
    const userAddress = rs.remote.userAddress;
    console.log(`${userAddress} connected their remote storage.`);
  })
  
rs.on('network-offline', () => {
    console.log(`We're offline now.`);
  })
  
rs.on('network-online', () => {
    console.log(`Hooray, we're back online.`);
  })

//console.log("rs")
//console.log(rs)
//de stackoverflow
function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+20+"px";
}

var Carnet = {
    store: rs.documents.privateList('notes-nl/'),
    notes: [],
    note: {},
    selectionne: "",
    encharge: true,
    effModal: '',
    actMenu: false,
    noteVide: function(){
        return {
        title: "",
        content: "",
        id: new Date().toJSON(),
        lastEdited: new Date().toJSON(),
        etiquettes:[]
        };
    },
    viewNote: "editeur", //parmi liste/editeur/lecteur,
    chargement: function() {
        Carnet.store.getAll('').then(reponse => { 
            Carnet.notes = Object.values(reponse).filter(it => it.id); 
            Carnet.encharge=false;
            //console.log("chargé!!");
            m.redraw();
        }).catch(err=>{
            console.log("erreur de chargement");
            console.log(err);
        });
    },
    allerNote: function(id){
        m.route.set("/note/:id",{id:id});
    },
    ajout: function(){
        var nnote = Carnet.noteVide();
        //console.log("affichage store:")
        //console.log(Carnet.store)
        //console.log("fin store:")
        Carnet.store.add(nnote)
        Carnet.notes.push(nnote);
        Carnet.store.getListing('')
  .then(listing => console.log(listing));
        //console.log(Carnet.notes);
    },
    retirer: function(){
        //alert("effacement irréversible")
        Carnet.notes = Carnet.notes.filter(n=>n.id!=Carnet.note.id);
        Carnet.toggleModal();
        Carnet.store.remove(Carnet.note.id);
        //m(m.route.Link, {href: "#/liste"});
        m.route.set('#/liste');
    },
    toggleModal: function(){
        console.log("toggleModal")
        if(Carnet.effModal == ''){
            Carnet.effModal = '.is-active';
        }
        else {
            Carnet.effModal = '';
        }
        m.redraw();
    },
    edit: function(id,txt){
 
        Carnet.note[id] = txt;
        Carnet.note.lastEdited = new Date().toJSON();
        debounce(()=>Carnet.store.set(Carnet.note.id,Carnet.note),1000)();
        //Carnet.store.set(Carnet.note._id,Carnet.note);
        //var element = document.getElementById("contenu_editeur");
        //var c = parseInt(element.cols);
        //element.rows = (2+Carnet.note.content.split('\n').reduce((acc,str) => acc+Math.ceil((1+str.length)/c),0)).toString();
        //console.log(c,element.rows)
    },
    editSel: function(e,titre){
        Carnet.selectionne = titre.trim()
        if(e.keyCode == 13){
            console.log("keycode 13")
            e.target.value = ""
            Carnet.ajoutAvecTitre();
            Carnet.chargement();
        }
    },
    chargeNote: function(id){
        Carnet.store.get(id).then(reponse => { 
            Carnet.note = reponse; 
            Carnet.encharge=false;
            //console.log("chargé!!");
            m.redraw();
        }).catch(err=>{
            console.log("erreur de chargement");
            //console.log(err);
        });
        if(Carnet.notes.length==0) {
            Carnet.encharge=true;
        }
        else{
            //console.log("note charge:",id,Carnet.notes)
        Carnet.note = Carnet.notes.find(n => n.id == id);
        //console.log("chargé ?",Carnet.note)
        }
    },
    vue: function(){
        if(Carnet.viewNote == 'editeur'){
            Carnet.viewNote = 'lecteur';
        }
        else{
            Carnet.viewNote = 'editeur';
        }
        //m.redraw();
    },
    ajoutEtiq: function(e,tag){
        //console.log(e.keyCode)
        if(e.keyCode == 13){
            //console.log("keycode 13")
            var mesetiquettes = new Set(Carnet.note.etiquettes.concat(tag.split(',').map(mot=>mot.trim()).filter(mot=>mot.length>0)));
            Carnet.note.etiquettes = Array.from(mesetiquettes);
            e.target.value = ""
            Carnet.store.set(Carnet.note.id,Carnet.note);
            Carnet.toggleEtiq();
            Carnet.chargement();
        }
    },
    effaceEtiq: function(tag){
        Carnet.note.etiquettes = Carnet.note.etiquettes.filter(t=> t!==tag);
    },
    toggleEtiq: function(){
        Carnet.actMenu = !Carnet.actMenu;
    },
    selection: function(){
        var selection = window.getSelection();
        if(selection.toString().length>0){
            str = selection.toString().trim();
            Carnet.selectionne = str[0].toUpperCase()+str.slice(1);
        }
        //console.log(Carnet.selectionne)
        //console.log(Carnet.selectionne.length)
        //alert(Carnet.selectionne);
    },
    ajoutAvecTitre: function(){
        var nvlleNote = Carnet.noteVide();
        //console.log("affichage store:")
        //console.log(Carnet.store)
        //console.log("fin store:")
        //console.log(nvlleNote)
        //var selection = window.getSelection();
        if(Carnet.selectionne.length==0){

            //popup pour un titre
            //console.log(Document.activeElement)
           // Carnet.selectionne = HTMLInputElement.setSelectionRange().toString();
        }
        else {
            nvlleNote.title = Carnet.selectionne;
            Carnet.selectionne = ""
        }
        var doc = Carnet.store.add(nvlleNote)
        //console.log(Carnet.notes)
        //Carnet.chargeNote(doc.id)
        //Carnet.notes.push(nvlleNote);
        Carnet.chargement();
    },
    download: function(){
        console.log("fun download called on note",Carnet.note)
        var taglist = "["+Carnet.note.etiquettes.map(it=>'"'+it+'"').toString()+"]";
        var entete = "---\n"+"title: "+Carnet.note.title+"\ndate: "+Carnet.note.id+"\nmodified: "+Carnet.note.lastEdited+"\ntags:"+taglist+"\n---\n";
        var blob = new Blob([entete+Carnet.note.content], {type: "text/plain;charset=utf-8"});
        fs.saveAs(blob, Carnet.note.id+".md");
    }
}

widget.attach("widget");
//widget.close();
Carnet.store.on('change', function (evt) {
    Carnet.chargement();
    console.log('data was added, updated, or removed:', evt)
  });
Carnet.chargement();

/*// Applied globally on all textareas with the "autoExpand" class
$(document)
    .one('focus.autoExpand', 'textarea.autoExpand', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input.autoExpand', 'textarea.autoExpand', function(){
        var minRows = this.getAttribute('data-min-rows')|0, rows;
        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 16);
        this.rows = minRows + rows;
    });
*/

module.exports = Carnet