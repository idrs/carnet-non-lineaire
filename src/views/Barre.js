var m = require( "mithril");
var Carnet  = require("../models/carnet");
var Etiquette = require("./Etiquette")


function Barre(){
    return {
    view: function (vnode){
        //console.log('Barre')
        //console.log(vnode.attrs)
        var vueMenu;
        
        if(Carnet.viewNote=="lecteur"){
            vueMenu = "Editer";
        }
        else{
            vueMenu = "Voir";
        }
        var menuList = [{
            val: "Carnet",
            attr: {href:"#/liste",style:"font-variant: small-caps;"},
            id: "a.is-size-4.has-text-weight-bold.has-text-centered"
        },
        {
            id: ".level#nvellenote.is-mobile",
            attr:{},
            val: [m(".level-left",{style:"width:85%;"},m("input.input.is-size-6.is-primary",
                {
                    oninput: function (e) { Carnet.selectionne=e.target.value },
                    value: Carnet.selectionne,
                    placeholder: "Nouvelle note",
                })),
                m(".level.right",{style:"width:15%;"},m(".button", {onclick:Carnet.ajoutAvecTitre}, m("span.icon",
                m("ion-icon",{name:"add-circle"})
            )))
            ]   
        },
        { 
            id:"#etiquettes",
            attr:{},
            val:m(Etiquette,vnode.attrs)
        }
        /*,
        {
            val: vueMenu,
            id: "a",
            attr: {onclick:Carnet.vue},
            vue: "note"
        },
        {
            val: "Effacer",
            id: "a",
            attr: {onclick:()=>{Carnet.effmodal=".is-active";}},
            vue: "note"
        }*/
    ]
        if(!vnode.attrs.id){
            menuList = menuList.filter(m=>m.vue != "note" );
        }
        
        return m("aside.menu",{},m("ul.menu-list",
            menuList.map(item =>m("li", m(item.id,item.attr,item.val))),
            //m("li.has-text-right",m(Etiquette,vnode.attrs)),
            
            )

        )

    }
}
}

module.exports = Barre;
