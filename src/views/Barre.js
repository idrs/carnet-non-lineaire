var m = require( "mithril");
var Carnet  = require("../models/carnet");

function Barre(){
    return {
    view: function (vnode){
        console.log('Barre')
        console.log(vnode.attrs)
        var vueMenu;
        if(Carnet.viewNote=="lecteur"){
            vueMenu = "Editer";
        }
        else{
            vueMenu = "Voir";
        }
        var menuList = [{
            val: "Carnet",
            attr: {href:"#/liste"},
            id: "a.logo"
        },
        {
            val: vueMenu,
            id: "a.button",
            attr: {onclick:Carnet.vue},
            vue: "note"
        },
        {
            val: "Ajout note",
            id: "a.button",
            attr: {onclick:Carnet.ajoutAvecTitre}
        },
        {
            val: "Effacer",
            id: "a.button",
            attr: {onclick:Carnet.retirer,href:"#/liste"},
            vue: "note"
        }
    ]
        /*if(!vnode.attrs.id){
            menuList = menuList.filter(m=>m.vue != "note" );
        }*/
        
        return m("nav",{},
            menuList.map(item => m(item.id,item.attr,item.val))
        )

    }
}
}

module.exports = Barre;
