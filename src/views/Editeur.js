var m = require( "mithril");
//var throttle = require('lodash.throttle');

//import marked from "marked";
var Carnet = require("../models/carnet");
//var autosize = require("autosize");

function calculDuNombreDeLigne(){
    var elt = document.getElementById("editor")
    if(elt){var scrollWidth = elt.scrollWidth}
    else {var scrollWidth = Math.ceil(document.scrollWidth*10/12);}
    var lignes = Carnet.note.content.split(/\n/g);
    var nb = lignes.reduce((acc,s) => acc+Math.ceil(s.length/scrollWidth*9),0 )   //magic number
    return nb;
}

function Editeur(){

    return {
        view: function(){
            //console.log("editeur")
            //console.log((Carnet.note.content.match(/\n/g)||[]).length+1)
            return m('.box#editor.column',
                m("input.input.is-size-5", {
                    oninput: function (e) { Carnet.edit("title",e.target.value) },
                    value: Carnet.note.title,
                    placeholder: "Titre",
                }),
                m("textarea.textarea#contenu_editeur.has-fixed-size.autoExpand",{
                    oninput: function (e) { Carnet.edit("content",e.target.value)} ,
                    value: Carnet.note.content,
                    placeholder: "Texte",
                    //rows: calculDuNombreDeLigne(),
                    
                })
            )
        },
    }
}


module.exports = Editeur;
