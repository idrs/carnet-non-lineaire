var m = require( "mithril");
var throttle = require('lodash.throttle');

//import marked from "marked";
var Carnet = require("../models/carnet");

function Editeur(){

    return {
        view: function(){
            console.log("editeur")
            console.log((Carnet.note.texte.match(/\n/g)||[]).length+1)
            return m('.box#editor',
                m("input.input.is-size-5", {
                    oninput: function (e) { Carnet.edit("titre",e.target.value) },
                    value: Carnet.note.titre,
                    placeholder: "Titre",
                }),
                m("textarea.textarea",{
                    oninput: function (e) { Carnet.edit("texte",e.target.value)} ,
                    value: Carnet.note.texte,
                    placeholder: "Texte",
                    rows: (Carnet.note.texte.match(/\n/g)||[]).length+1,
                    style: "height: '100%'"
                })
            )
        },
    }
}

module.exports = Editeur;