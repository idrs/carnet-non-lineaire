//import marked from "marked";
var m = require("mithril");
var Carnet = require("../models/carnet")
var flatten = require('lodash.flatten')

function Etiquette(){
    return {
        view: function(vnode){
            //console.log(vnode);
            //console.log("note:",Carnet.note)
            var etiquettes;
            var boutonEfface = false;
            if(Carnet.note.id){
                etiquettes = Carnet.note.etiquettes;
                boutonEfface = true;
            }
            else {
                var etiquettesSet = new Set(flatten(Carnet.notes.map(n=> n.etiquettes)));
                etiquettes = Array.from(etiquettesSet);
            }
            
            return  m("#etiquettes.tags",etiquettes.map(tag=>
                        m("span.is-light",
                            m("a.is-size-7",{href:"#/tag/"+tag},tag),
                            m("button.delete.is-small"+(boutonEfface?"":".is-hidden"),{onclick:()=>Carnet.effaceEtiq(tag)})
                        ))
                    );
            
        }
    }
    
}

module.exports = Etiquette