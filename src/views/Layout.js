var m = require( "mithril");
var Barre = require("./Barre");


function Layout() {
    return {
    view: function(vnode) {
        return m(".container", [
            m(Barre,vnode.attrs),
            m(".columns", vnode.children)
        ])
    }
}
}

module.exports = Layout