var m = require( "mithril");
var Barre = require("./Barre");
var Carnet = require("../models/carnet")


function Layout() {
    return {
    view: function(vnode) {
        return m("#layout.columns.is-widescreen",{onmouseup:Carnet.selection},
            m(".column.is-2",m(Barre,vnode.attrs)),
            
            m("#note.column.is-10", vnode.children)
        )
    }
}
}

module.exports = Layout
