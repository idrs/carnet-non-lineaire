var m = require( "mithril");
var purify = require("dompurify")
//ar marked = require("marked");

// Create reference instance
const marked = require('marked');

// Get reference
const renderer = new marked.Renderer();

// Override function for Bulma headings
renderer.heading = function (text, level) {
    const escapedText = text.toLowerCase().replace(/[^\w]+/g, '-');
    var size = Math.min(parseInt(level)+2,6); 
    return `
          <h${level} id=${text} class="title is-${size.toString()}">
            ${text}
          </h${level}>`;
};


var Carnet  = require("../models/carnet");

function Lecteur(){
    console.log("lecteur")

    return {
        oninit: function(){

        },
        view: function(){
            var md_texte = purify.sanitize(marked("# "+Carnet.note.titre+"\n\n"+Carnet.note.texte, { renderer: renderer }));
            return m('div#viewer.box',{ondblclick:Carnet.vue},m(".content",m.trust(md_texte)));
        }
    }
}

module.exports = Lecteur