var m = require( "mithril");
var purify = require("dompurify")
//ar marked = require("marked");

// Create reference instance
const marked = require('marked');

// Get reference
const renderer = new marked.Renderer();

// Override function for Bulma headings
renderer.heading = function (text, level) {
    const escapedText = text.toLowerCase().replace(/[^\w]+/g, '-');
    var size = Math.min(parseInt(level)+2,6); 
    return `
          <h${level} id=${text} class="title is-${size.toString()}">
            ${text}
          </h${level}>`;
};


var Carnet  = require("../models/carnet");

function creationLienTitre(texte,titres){
    titres = titres.filter(t=>t[1].length>0).sort((a,b)=>b.length-a.length);
    var decoupe = texte.split(/(\[[^\]]*\]\([^\)]*\))/);
    for(titre of titres) {
        if(titre[1].toLowerCase() !== Carnet.note.title.toLowerCase()){
           
        var regx = new RegExp(titre[1],"giu");
        var nvdecoupe = [];
        for (morceau of decoupe) {
            if(morceau[0]!=="["){
                
                var tranches = 
                    morceau
                    .replace(regx,(cor,p)=>"["+cor+"](#/note/"+titre[0]+")")
                    .split(/(\[[^\]]*\]\([^\)]*\))/);
                
                nvdecoupe = nvdecoupe.concat(tranches);
            }
            else{
                nvdecoupe.push(morceau);
            }
            
        }
        decoupe = nvdecoupe;
        }
        
    }
    return decoupe.join("");
}

function Lecteur(){
    //console.log("lecteur")

    return {
        oninit: function(){

        },
        view: function(){
            var texte = Carnet.note.content;
            var texteLie = creationLienTitre(texte,Carnet.notes.map(n => [n.id,n.title]))

            //console.log(texteLie)
            var md_texte = purify.sanitize(marked("# "+Carnet.note.title+"\n\n"+texteLie, { renderer: renderer }));
            return m('div#viewer.box.column',{},m(".content",m.trust(md_texte)));
        }
    }
}

module.exports = Lecteur
