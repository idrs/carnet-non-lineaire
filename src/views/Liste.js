var m = require( "mithril");
//import marked from "marked";
var Carnet  = require("../models/carnet");


function Liste(){

    return {
        
        view: function(vnode){
            var notesFiltrees;
            console.log(vnode.attrs)
            if(vnode.attrs.etiq){
                notesFiltrees = Carnet.notes.filter(x=>(x.etiquettes.includes(vnode.attrs.etiq)));
            }
            else{
                notesFiltrees = Carnet.notes.slice();
            }
            //console.log(sort((a,b)=>Date.parse(b.modif)-Date.parse(a.modif),Carnet.notes ));
            return m(".column.is-three-quarters",
                notesFiltrees.sort((a,b)=>Date.parse(b.modif)-Date.parse(a.modif)).map(
                   
                    note => 
                   m(".box",
                        m("a",{href:"#/"+note._id},
                            m("p.is-size-5.has-text-weight-semibold",note.titre?note.titre:"Sans titre"),
                            m("p",note.texte.slice(0,80)+"...")))));
        },
    }
}

module.exports = Liste;