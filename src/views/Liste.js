var m = require( "mithril");
//import marked from "marked";
var Carnet  = require("../models/carnet");
var Etiquette = require("./Etiquette")

function titleOrContent(note){
    if(note.title){ 
        return note.title ;
    }
    else{
        var ind = note.content.indexOf('\n');
        if (ind == -1){
            ind = 60;
        }
        return note.content.substr(0,ind);
    }
}
function formatDate(jsondate){
    var date = new Date(jsondate)
    var dateTimeFormat = new Intl.DateTimeFormat('fr', { year: 'numeric', month: 'short', day: '2-digit' }) 
    var [{ value: jour },,{ value: mois },,{ value: annee }] = dateTimeFormat .formatToParts(date ) 

    return `${jour} ${mois} ${annee }`
}

function Liste(){

    return {
        oninit: function(){
            Carnet.chargement();
        },
        view: function(vnode){
            var notesFiltrees;
            Carnet.note = {};
            //console.log(vnode.attrs)
            if(vnode.attrs.etiq){
                notesFiltrees = Carnet.notes.filter(x=>(x.etiquettes.includes(vnode.attrs.etiq)));
            }
            else{
                notesFiltrees = Carnet.notes.slice();
            }
            //console.log(sort((a,b)=>Date.parse(b.modif)-Date.parse(a.modif),Carnet.notes ));
            return m("main.columns",
                m(".column.is-three-quarters",
                    notesFiltrees
                    .sort((a,b)=>Date.parse(b.lastEdited)-Date.parse(a.lastEdited))
                    .map(note => m(".box",
                        {onclick:()=>Carnet.allerNote(note.id)},                        
                        m("p.is-size-5.has-text-weight-semibold",titleOrContent(note)),
                        m("p",formatDate(note.id))
                        )
                    )
                )
                    //m(".column.is-one-quarter",m(Etiquette))
            );
        },
    }
}

module.exports = Liste;