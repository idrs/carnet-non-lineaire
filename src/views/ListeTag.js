var m = require( "mithril");
//import marked from "marked";
var Carnet  = require("../models/carnet");


function ListeTag(){

    return {
        view: function(vnode){
            var tag =vnode.attrs.tag;
            //console.log(sort((a,b)=>Date.parse(b.modif)-Date.parse(a.modif),Carnet.notes ));
            return m(".col-md-8.col-sm-12",m(".row",
                    Carnet.notes.filter(x=>(x.etiquettes.includes(tag))).sort((a,b)=>Date.parse(b.modif)-Date.parse(a.modif)).map(
                   
                    note => 
                        m(".card.note-item.small",m("a",{href:"#/"+note._id},
                            m("h4.section",note.titre?note.titre:"Sans titre"),
                            m("p.section",note.texte.slice(0,144)+"..."))))));
        },
    }
}

module.exports = ListeTag;