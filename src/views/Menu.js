//import marked from "marked";
var m = require("mithril");
var Carnet = require("../models/carnet")
function Menu(){
    return {
        view: function(vnode){
            console.log(vnode.attrs);
            if(vnode.attrs.id){
            
            return m(".column.is-one-quarter",
                
                m("input.input.nvlle-etiq",{
                    placeholder: "etiquette",
                    onkeypress:e => Carnet.ajoutEtiq(e,e.target.value)
                }),
                m("#etiquettes",Carnet.note.etiquettes.map(tag=>m("span.tag.is-light",m("a",{href:"#/tag/"+tag},tag))))
            )
            }
            else {
                return m(".column.is-one-quarter",
                
                m("#etiquettes",Carnet.notes.map(n=>n.etiquettes.map(tag=>m("span.tag.is-light",m("a",{href:"#/tag/"+tag},tag)))))
            )

            }
        }
    }
    
}

module.exports = Menu