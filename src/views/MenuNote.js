var m = require( "mithril");
//import marked from "marked";
var Carnet  = require("../models/carnet");

var Etiquette = require("./Etiquette")

function MenuNote(){

    return {
        oninit: function(vnode){
            
        },
        view: function(){
            var vueMenu;
            var actMenu=false;
            if(Carnet.viewNote=="lecteur"){
                vueMenu = "Editer";
                vueIcon = "create";
            }
            else{
                vueMenu = "Voir";
                vueIcon = "eye"
            }
            var drop = Carnet.actMenu?".dropdown.is-active":".dropdown";
                return m("#stickymenu.menu.column.is-narrow",
                        m("ul.menu-list" ,
                            m("li.is-inline-mobile",                       
                            m(".button.is-inline-block",{onclick:Carnet.vue},
                                m("span.icon",
                                    m("ion-icon",{name:vueIcon})
                                )
                            )/*,m("span",vueMenu)*/
                            ), // Voir/Editer
                            m("li.is-inline-mobile",
                            m(".button.is-inline-block",{onclick:Carnet.toggleModal},
                                m("span.icon",
                                    m("ion-icon",{name:"trash"})
                                )
                            )/*,m("span","Effacer")*/
                            ), // Trash
                            m("li.is-inline-mobile",                       
                            m(".button.is-inline-block",{onclick:Carnet.download},
                                m("span.icon",
                                    m("ion-icon",{name:"download"})
                                )
                            )/*,m("span",vueMenu)*/
                            ), // Telecharger
                            m("li.is-inline-mobile",
                            m(drop,
                                m(".dropdown-trigger",
                                    m("button.button",{['aria-haspopup']:"true",onclick:Carnet.toggleEtiq},
                                        m("span.icon",
                                            m("ion-icon",{name:"pricetag"})
                                        )
                                    )
                                ),
                                m(".dropdown-menu",
                                    m(".dropdown-content",
                                        m(".dropdown-item",
                                            m("input.input.nvlle-etiq.is-primary",{
                                                placeholder: "etiquette",
                                                onkeypress:e => Carnet.ajoutEtiq(e,e.target.value)
                                            })
                                        )
                                    )
                                )
                            )
                        ),
                        m("#effdialog.modal.is-clipped"+Carnet.effModal,
                            m(".modal-background",""),
                            m(".modal-content",
                                m(".box",
                                    m("p","Êtes-vous sûr de vouloir effacer cette note ? C'est irréversible."),
                                    m("button.button",{onclick: Carnet.retirer,href:"#/liste"},"Effacer"),
                                    m("button.button",{onclick:Carnet.toggleModal},"Annuler")))
                        
                        )
                    )
                    );
               
            
        },
    }
}

module.exports = MenuNote;
