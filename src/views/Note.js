var m = require( "mithril");
//import marked from "marked";
var Carnet  = require("../models/carnet");
var Editeur = require("./Editeur");
var Lecteur = require("./Lecteur");
var MenuNote = require("./MenuNote");
var Etiquette = require("./Etiquette");

function Note(){

    return {
        oninit: function(vnode){
            //console.log(vnode.attrs.id);
            Carnet.chargeNote(vnode.attrs.id);
            if(Carnet.note.title!="" && Carnet.note.content!=""){
                Carnet.viewNote = "lecteur";
            }
            else {
                Carnet.viewNote = "editeur";
            }
        },
        onupdate: function(vnode){
            //console.log(vnode.attrs.id);
            if(vnode.attrs.id != Carnet.note.id)
                {Carnet.chargeNote(vnode.attrs.id);}
            
        },
        view: function(){ 
            var note = "#main.columns";
            var conteneur = "#conteneur.column.is-10.columns";
            var menu = ".column.is-2";
            if(Carnet.encharge){
                return m(note,
                    m(conteneur,m("p.is-size-4","Chargement en cours")),
                    //m(menu,m(Etiquette))
                );
            }
            else{
            if(Carnet.viewNote == "editeur"){
                return m(note,
                    m(conteneur,m(MenuNote),m(Editeur)),
                    //m(menu,m(Etiquette))
                );
            }
            else {
                return m(note,
                    m(conteneur,m(MenuNote),m(Lecteur)),
                    //m(menu,m(Etiquette))
                );
            }}
        },
    }
}

module.exports = Note;
