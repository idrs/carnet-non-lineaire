var m = require( "mithril");
//import marked from "marked";
var Carnet  = require("../models/carnet");
var Editeur = require("./Editeur");
var Lecteur = require("./Lecteur");

function Note(){

    return {
        oninit: function(vnode){
            console.log(vnode.attrs.id);
            Carnet.chargeNote(vnode.attrs.id);
            if(Carnet.note.titre!="" && Carnet.note.texte!=""){
                Carnet.viewNote = "lecteur";
            }
            else {
                Carnet.viewNote = "editeur";
            }
        },
        view: function(){
            if(Carnet.viewNote == "editeur"){
                return m(".column.is-three-quarters",m(Editeur));
            }
            else {
                return m(".column.is-three-quarters",m(Lecteur));
            }
        },
    }
}

module.exports = Note;